package com.station29.mealtemple;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.tabs.TabLayout;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.OnBoardScreen;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class BoardingActivity extends BaseActivity {
    static final int NUMBER_OF_PAGES = 2;
    private ViewPager recyclerView;
    private BoardingViewAdapter boardingViewAdapter;
    private  MaterialButton btnContinue;
    private List<OnBoardScreen> list = new ArrayList<>();
    private TextView skipTv;
    String lang = " ";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_boarding_list);

        recyclerView = findViewById(R.id.viewpager);
        btnContinue = findViewById(R.id.btnContinue);
        if(getIntent().hasExtra("en")){
            lang = getIntent().getStringExtra("en");
        }
        if(getIntent().hasExtra("fr")){
            lang = getIntent().getStringExtra("fr");
        }
        Util.changeLanguage(BoardingActivity.this,lang);
        recyclerView = findViewById(R.id.viewpager);
        btnContinue = findViewById(R.id.btnContinue);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(recyclerView, true);
        skipTv = findViewById(R.id.skip_Tv);
        btnContinue.setText(R.string.continues);

        list = MockupData.getListBoardingScreen(BoardingActivity.this);
        boardingViewAdapter = new BoardingViewAdapter(getSupportFragmentManager(),MockupData.getListBoardingScreen(BoardingActivity.this),lang);
        recyclerView.setAdapter(boardingViewAdapter);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextCurrentItem(recyclerView.getCurrentItem(),true);
            }
        });

        skipTv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Util.changeLanguage(BoardingActivity.this,lang);
                Intent intent = new Intent(BoardingActivity.this,MainActivity.class);
                intent.putExtra("lang",lang);
                startActivity(intent);
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
               if(recyclerView.getCurrentItem()==3){
                   btnContinue.setText(R.string.I_got_it);
                   btnContinue.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           Intent intent = new Intent(BoardingActivity.this,MainActivity.class);
                           intent.putExtra("lang",lang);
                           startActivity(intent);
                           Constant.setStart("stop");
                           Util.start(BoardingActivity.this,"stop");
                           Util.changeLanguage(BoardingActivity.this,lang);
                       }
                   });
               }else {
                   btnContinue.setText(R.string.continues);
                   btnContinue.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           nextCurrentItem(recyclerView.getCurrentItem(),true);
                       }
                   });
               }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    public void nextCurrentItem (int item,boolean t) {
        recyclerView.setCurrentItem(item + 1,t);
            if(item + 1 == 3){
                btnContinue.setText(R.string.I_got_it);
                btnContinue.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View v) {
                       Intent intent = new Intent(BoardingActivity.this,MainActivity.class);
                       intent.putExtra("lang",lang);
                       startActivity(intent);
                       Constant.setStart("stop");
                       Util.start(BoardingActivity.this,"stop");
                       Util.changeLanguage(BoardingActivity.this,lang);
                   }
               });
            }

    }
    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}