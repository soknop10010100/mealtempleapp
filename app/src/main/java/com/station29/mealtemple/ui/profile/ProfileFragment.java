package com.station29.mealtemple.ui.profile;


import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;
import com.station29.mealtemple.R;
import com.station29.mealtemple.SplashScreenActivity;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.response.Config;
import com.station29.mealtemple.api.request.ConfigWs;
import com.station29.mealtemple.api.request.LoginWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.api.request.ServerUrlWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.BaseFragment;
import com.station29.mealtemple.util.Util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import id.zelory.compressor.Compressor;

import static android.app.Activity.RESULT_OK;
import static com.station29.mealtemple.api.MockupData.EMAIL;
import static com.station29.mealtemple.api.MockupData.LOGIN_PHONE;
import static com.station29.mealtemple.util.Util.convert;
import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class ProfileFragment extends BaseFragment {

    private final static int REQUEST_CODE_LOGIN = 987, REQUEST_CODE_EDIT_PROFILE = 687;
    private final static String LOGIN = "login", deviceType = "ANDROID";
    private TextView addressTv, favouriteTv, editTv, signoutTv, welcomeTv, phoneTv,payTv,myAccTv,emailTv,informationTv;
//    private static boolean booleanisProfile = false;
    private User  userProfile;
    private ImageView profileImage;
    private ImageView camera;
    //    private static final String IMAGE_DIRECTORY = "/YourDirectName";
    private int GALLERY = 1, CAMERA = 2;
    boolean imageClick = false;
    private FrameLayout edit_photo ;
    private String mCameraFileName;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_profile_account, container, false);
        // Hide some label
        addressTv = root.findViewById(R.id.user_address_btn);
        addressTv.setVisibility(View.GONE);
        payTv = root.findViewById(R.id.user_payment_method);
        payTv.setVisibility(View.GONE);
        favouriteTv = root.findViewById(R.id.user_favorite_btn);
        favouriteTv.setVisibility(View.GONE);
        editTv = root.findViewById(R.id.user_proedit_btn);
        editTv.setVisibility(View.GONE);
        signoutTv = root.findViewById(R.id.user_signout_btn);
        signoutTv.setVisibility(View.GONE);
        welcomeTv = root.findViewById(R.id.nameTv);
        phoneTv = root.findViewById(R.id.phoneTv);
        edit_photo = root.findViewById ( R.id.edit_photo );
        camera = root.findViewById(R.id.user_camera);
        camera.setVisibility(View.GONE);
        myAccTv = root.findViewById(R.id.txtMyAcc);
        profileImage = root .findViewById ( R.id.user_profile_image );
        emailTv = root.findViewById(R.id.user_email);
        informationTv = root.findViewById(R.id.account);

        if (!isConnectedToInternet ( mActivity )){//check internet connection
            popupMessage ( null, getResources ().getString ( R.string.failed_internet ),mActivity);
        }

        if (Constant.getBaseUrl ()==null&& BaseActivity.currentCountryCode!=null){
            getUrls();
        }else{
            String userToken = Util.getString("token", mActivity);
            if (LoginActivity.isDidLogin&&!userToken.isEmpty() || SignUpActivity.isDidLogin&&!userToken.isEmpty()){
                showInfos();
            } else if (!userToken.isEmpty()) {
                showInfos ();
            }
        }

        final String token = Util.getString("token",mActivity );
        if (token != null && !token.isEmpty()) {
            imageClick = true;
        } else {
            informationTv.setText(getString(R.string.you_are_not_signin));
            phoneTv.setText(getString(R.string.signin));
            phoneTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_signout, 0, 0, 0);
            profileImage.setImageResource(R.drawable.user_image);
            emailTv.setVisibility(View.GONE);
//            myAccTv.setVisibility(View.GONE);
            welcomeTv.setVisibility(View.GONE);
        }

        if(imageClick){
            edit_photo.setOnClickListener(onClickListener);
        }

        phoneTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                }, 150);
                Intent intent = new Intent(mActivity, LoginActivity.class);
                intent.putExtra("action", LOGIN);
                startActivityForResult(intent, REQUEST_CODE_LOGIN);
            }
        } );

        root.findViewById(R.id.user_address_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                },50);
                Intent intent = new Intent ( mActivity, DeliveryAddressActivity.class) ;
                intent.putExtra ( "user",userProfile );
                startActivityForResult(intent,REQUEST_CODE_LOGIN);

            }
        });

        root.findViewById(R.id.user_favorite_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,FavoriteActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });
        root.findViewById(R.id.user_payment_method).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,PaymentMethodActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        root.findViewById(R.id.user_signout_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constant.getBaseUrl() == null && BaseActivity.currentCountryCode != null) {
                    getUrls();
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.DialogTheme);
                    builder.setTitle(R.string.sign_out);
                    builder.setMessage(R.string.are_you_sure_you_want_to_sign_out);
                    builder.setPositiveButton(R.string.sign_out, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //clear token by SharedPreferences
                            new LoginWs().logoutPhoneEmail(mActivity, deviceType, new LoginWs.LoadLoginCallback() {
                                @Override
                                public void onLoadSuccess(String token, int userId, User user) {
                                    notShowInf();
                                }
                                @Override
                                public void onLoadFailed(String errorMessage, int code) {
                                    Util.clearString(mActivity);
                                }
                            });
                        }
                    });
                    builder.setNegativeButton(getString(R.string.cancel), null).show();
                }
            }
        });
        root.findViewById(R.id.user_language_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLangPopup();
            }
        });

        return root;

    }

    private  void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(mActivity);
        pictureDialog.setTitle(getString(R.string.select_action));
        String[] pictureDialogItems = {getString(R.string.choose_gallery), getString(R.string.take_photo)};
        pictureDialog.setItems(pictureDialogItems,
                 new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                                {
                                    requestPermissions( new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},GALLERY);
                                } else {
                                    choosePhotoFromGallery();
                                }
                                break;
                            case 1:
                                if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(mActivity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
                                {
                                    requestPermissions(new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE},CAMERA );

                                } else {
                                    takePhotoFromCamera();
                                }
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }
    private void takePhotoFromCamera() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);

        Date date = new Date();
        DateFormat df = new SimpleDateFormat("-mm-ss", Locale.US);

        String newPicFile = df.format(date) + ".jpg";
        String outPath = "/sdcard/" + newPicFile;
        File outFile = new File(outPath);

        mCameraFileName = outFile.toString();
        Uri outuri = Uri.fromFile(outFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outuri);
        startActivityForResult(intent, CAMERA);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == this.GALLERY && resultCode == RESULT_OK){
            if(data!= null ){
                Uri contentURI = data.getData();
                Picasso.get().load(contentURI).into(profileImage);
                String path = String.valueOf(getRealPathFromURI(contentURI,mActivity));
                savePictureProfile(Util.getBase64FromPath(path));
            }
        } else if (requestCode == CAMERA && resultCode == RESULT_OK ){
            if(mCameraFileName != null){
                File actualFile = new File(mCameraFileName);
                try {
                    actualFile = new Compressor(mActivity).compressToFile(actualFile);
                    Picasso.get().load(actualFile).into(profileImage);
                    String filePath = actualFile.getPath();
                    Bitmap bitmap = BitmapFactory.decodeFile(filePath);
                    savePictureProfile(convert(bitmap));
                } catch (IOException e) {
                    popupMessage(null,e.getMessage(),mActivity);
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_LOGIN) {
            if (data != null && data.hasExtra("action")) {
                String action = data.getStringExtra("action");
                if (data.getStringExtra("action") != null && "LOGIN".equalsIgnoreCase(action)) {
                    showInfos();
                    String token = Util.getString("token", mActivity);
                    if (token != null && !token.isEmpty()) {
                        imageClick = true;
                        edit_photo.setOnClickListener(onClickListener);
                    }
                }
            }
        }else if(resultCode == RESULT_OK && requestCode== REQUEST_CODE_EDIT_PROFILE){
            if (data != null && data.hasExtra ( "datauser" )){
                loadProfiles ();
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mActivity, R.string.camera_successful, Toast.LENGTH_LONG).show();
                takePhotoFromCamera();
            } else {
                Toast.makeText(mActivity, R.string.cannot_take_photo, Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == GALLERY) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(mActivity, R.string.have_gallery, Toast.LENGTH_LONG).show();
                choosePhotoFromGallery();
            } else {
                Toast.makeText(mActivity, R.string.cannot_pickgallery, Toast.LENGTH_LONG).show();
            }
        }
    }



    public  void savePictureProfile(String base64){
        HashMap<String,String> imageHashMap = new HashMap<>();
        imageHashMap.put("image", base64);
        new ProfileWs().changeProfile(mActivity,imageHashMap, profileViewAndUpdate);
    }
    private ProfileWs.ProfileViewAndUpdate profileViewAndUpdate = new ProfileWs.ProfileViewAndUpdate() {
        @Override
        public void onLoadProfileSuccess(User user) {

        }
        @Override
        public void onError(String errorMessage) {
            Toast.makeText(mActivity,errorMessage,Toast.LENGTH_LONG).show();
        }

        @Override
        public void onUpdateProfilesSuccess() {

        }
    };

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void showInfos(){
        informationTv.setVisibility(View.VISIBLE);
        informationTv.setText(R.string.information);
        addressTv.setVisibility(View.GONE);
        favouriteTv.setVisibility(View.GONE);
        payTv.setVisibility(View.GONE);
        myAccTv.setVisibility(View.VISIBLE);
        editTv.setVisibility(View.VISIBLE);
        camera.setVisibility(View.VISIBLE);
        emailTv.setVisibility(View.VISIBLE);
        phoneTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_phone_24,0,0,0);
        welcomeTv.setVisibility(View.VISIBLE);
        editTv.setOnClickListener ( new View.OnClickListener () {
                    @Override
                    public void onClick(final View v) {
                        if (Constant.getBaseUrl() == null && BaseActivity.currentCountryCode != null) {
                            getUrls();
                        } else {
                            v.setEnabled(false);
                            v.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    v.setEnabled(true);
                                }
                            }, 100);
                            Intent intent = new Intent(mActivity, EditProfileActivity.class);
                            intent.putExtra("user", userProfile);
                            startActivityForResult(intent, REQUEST_CODE_EDIT_PROFILE);
                        }
                    }
                } );
        signoutTv.setVisibility(View.VISIBLE);
        if(Constant.getBaseUrl()!=null)
            loadProfiles ();
        LoginActivity.isDidLogin= true;
        phoneTv.setEnabled(false);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    private void notShowInf(){
        profileImage.setImageResource(R.drawable.user_image);
        profileImage.setClickable(false);
        camera.setVisibility(View.GONE);
        addressTv.setVisibility(View.GONE);
        phoneTv.setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_signout,0,0,0);
        favouriteTv.setVisibility(View.GONE);
        editTv.setVisibility(View.GONE);
        payTv.setVisibility(View.GONE);
        signoutTv.setVisibility(View.GONE);
//        myAccTv.setVisibility(View.GONE);
//        welcomeTv.setText(getString(R.string.you_are_not_signin));
        informationTv.setText(getString(R.string.you_are_not_signin));
        welcomeTv.setVisibility(View.GONE);
        emailTv.setVisibility(View.GONE);
        phoneTv.setText(getString(R.string.signin));
        phoneTv.setEnabled(true);
        MockupData.setCurrentOrderDetail(null);
        MockupData.highLightItemIds = new HashMap<>();
        Util.clearString(mActivity);
        phoneTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (Constant.getBaseUrl() == null && BaseActivity.currentCountryCode != null) {
                    getUrls();
                }else {
                    v.setEnabled(false);
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            v.setEnabled(true);
                        }
                    }, 100);
                    Intent intent = new Intent(mActivity, LoginActivity.class);
                    intent.putExtra("action", LOGIN);
                    startActivityForResult(intent, REQUEST_CODE_LOGIN);
                    MockupData.USER_NAME = "";
                    LOGIN_PHONE = "";
                    EMAIL = "";
                }
            }
        });
        LoginActivity.isDidLogin = false;
//        ProfileFragment.isProfile = true;
        SignUpActivity.isDidLogin = false;
    }

    private void loadProfiles(){
        new ProfileWs ().viewProfiles ( mActivity , new ProfileWs.ProfileViewAndUpdate () {
            @Override
            public void onLoadProfileSuccess(User user) {
                if(Constant.getBaseUrl() != null) {
                    if(user.getEmail()!=null)
                        emailTv.setText(user.getEmail());
                    userProfile = user;
                    String star = "";
                    if (user.getPhoneNumber() != null) {
                        star = starDynamic(user.getPhoneNumber().length());
                    }
                    if (userProfile.getFirstName() == null && userProfile.getLastName() == null) {
                        welcomeTv.setText(String.format(Locale.US, "%s", getString(R.string.guest)));
                    } else if (userProfile.getLastName() == null) {
                        welcomeTv.setText(String.format(Locale.US, "%s", userProfile.getFirstName()));
                        MockupData.USER_NAME = userProfile.getFirstName();
                    } else if (userProfile.getFirstName() == null) {
                        welcomeTv.setText(String.format(Locale.US, "%s", userProfile.getLastName()));
                        MockupData.USER_NAME = userProfile.getLastName();
                    } else {
                        welcomeTv.setText(String.format(Locale.US, "%s  %s", userProfile.getFirstName(), userProfile.getLastName()));
                        MockupData.USER_NAME = userProfile.getFirstName() + " " + userProfile.getLastName();
                    }
                    if (userProfile.getPhoneNumber() != null) {
                        phoneTv.setText(String.format("%s%s%s", userProfile.getPhoneNumber().substring(0, 3), star.substring(3, userProfile.getPhoneNumber().length()), userProfile.getPhoneNumber().substring(userProfile.getPhoneNumber().length() - 3)));
                    } else {
                        phoneTv.setText("null");
                    }
//                    if(userProfile.getEmail()!=null){
//                        phoneTv.setText(userProfile.getEmail().replaceAll("(?<=.{1}).(?=[^@]*?.@)", "*"));
//                    } else {
//
//                    }
                    //user doesnt have profile image we gonna set it to R.drawable.user image
                    Glide.with(mActivity).load(userProfile.getUrl_Image()).apply(new RequestOptions().error(R.drawable.user_image).placeholder(R.drawable.user_image)).into(profileImage);
                }
            }

            @Override
            public void onError(String errorMessage) {
                if (isConnectedToInternet ( mActivity ))
                    Util.popupMessage ( null,errorMessage,mActivity );
            }
            @Override
            public void onUpdateProfilesSuccess() {

            }
        } );
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View view) {
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setEnabled(true);
                }
            },200);
            showPictureDialog();
        }
    };

    private String starDynamic (int phoneLeng){
        StringBuilder star = new StringBuilder();
        for (int i = 0; i <= phoneLeng; i++) {
            star.append("*");
        }
        return star.toString();
    }
    private void getUrls(){
        new ServerUrlWs().ReadServerUrl ( BaseActivity.countryCode, new ServerUrlWs.loadServerUrl () {
        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
        @Override
        public void onLoadSuccess(boolean success) {
            if (Constant.getBaseUrl()!=null) {
                config();
                String userToken = Util.getString("token", mActivity);
                if (LoginActivity.isDidLogin&&!userToken.isEmpty() || SignUpActivity.isDidLogin&&!userToken.isEmpty()){
                    showInfos();
                } else if (!userToken.isEmpty()) {
                   showInfos();
                }
            }
        }

        @Override
        public void onLoadFailed(String errorMessage){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                notShowInf();
            }
            popupMessage ( null,errorMessage, mActivity );
        }
    } );
    }
    private void config(){

        new ConfigWs().readConfig(BaseActivity.countryCode, new ConfigWs.ReadConfigCallback() {
            @Override
            public void onSuccess(Config config) {
                Constant.setConfig(config);
            }

            @Override
            public void onError(String message) {
                popupMessage(null, message, mActivity);
            }
        });
    }

    public static String getRealPathFromURI(Uri contentURI, Activity context) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = context.managedQuery(contentURI, projection, null,
                null, null);
        if (cursor == null)
            return null;
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        if (cursor.moveToFirst()) {
            // cursor.close();
            return cursor.getString(column_index);
        }
        // cursor.close();
        return null;
    }

    private void changeLangPopup() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.language));
        List<String> data = new ArrayList<>();
        data.add(getString(R.string.english));
        data.add(getString(R.string.france));

        int oldCheckPos;
        String lang = Util.getString("language", mActivity);
        if (lang.equals("") || lang.equals("en")) {
            oldCheckPos = 0;
        } else {
            oldCheckPos = 1;
        }
        ListAdapter listAdapter = new ListAdapter(mActivity, android.R.layout.simple_list_item_1, data, oldCheckPos);
        builder.setSingleChoiceItems(listAdapter, oldCheckPos, null);
        builder.setNegativeButton(getString(R.string.cancel), null);

        android.app.AlertDialog dialog = builder.create();
        final ListView listView = dialog.getListView();
        if (listView != null) {
            listView.setDivider(new ColorDrawable(Color.LTGRAY)); // set color
            listView.setDividerHeight(1);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    // To clear all focus
                    for (int k = 0 ; k< listView.getChildCount(); k++) {
                        View v = listView.getChildAt(k);
                        if (v instanceof TextView) {
                            ((TextView) v).setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,0,0);
                        }
                    }
                    // To select user choice
                    if (view instanceof TextView) {
                        TextView textView = (TextView) view;
                        textView.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_check_18, 0);
                    }

                    if (i == 0) {
                        Util.changeLanguage(mActivity, "en");
                    } else {
                        Util.changeLanguage(mActivity, "fr");
                    }

                    Intent intent = new Intent(mActivity, SplashScreenActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }
            });
        }
        dialog.show();
    }
}