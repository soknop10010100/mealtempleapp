package com.station29.mealtemple.ui.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.tabs.TabLayout;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.AddOn;
import com.station29.mealtemple.api.model.Menu;
import com.station29.mealtemple.api.model.OrderDetail;
import com.station29.mealtemple.api.model.OrderItem;
import com.station29.mealtemple.api.model.Product;
import com.station29.mealtemple.api.model.ProductDetail;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.request.ProductWs;
import com.station29.mealtemple.api.request.StoreWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.adpater.ProductItemSectionAdapter;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import io.github.luizgrp.sectionedrecyclerviewadapter.SectionedRecyclerViewAdapter;

import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class ShopDetailActivity extends BaseActivity implements View.OnClickListener {

    private BottomSheetBehavior<View> bottomSheetBehavior;
    private TextView itemCountTv, grandTotalTv, shopClose,shopTimeOpen;
    private Shop shop,saveShop;
    private List<OrderItem> mOrderItemList = new ArrayList<>();
    private final int REQUEST_CODE_CHECKOUT = 178;
    private final int ADD_CART = 353;
    private View bottomDesign;
    private HashMap<String,Object> requestParams = new HashMap<> (  );
    public static List<Integer> shopOpenHours = new ArrayList<>();
    private float valueTotalIntent,totalPrice;
    private int countItem;
    private String currencySign="";
    public static boolean checkShopCloseOpen = false;
    private int counter = 1, childCount;
    private  ImageView shopCover;
    private TabLayout tabLayout;
    private RecyclerView recyclerViewProduct;
    private Boolean isScrolling = true;
    private Toast toast;
    private ProgressBar progressBar;
    private ImageView upButton;
    private LinearLayoutManager linearLayoutManager;
    private AppBarLayout appbar;
    private List<Integer> countPositionList = new ArrayList<>();
    private List<String> countTabList = new ArrayList<>();
    private SectionedRecyclerViewAdapter sectionAdapter = new SectionedRecyclerViewAdapter();
    private ProductItemSectionAdapter itemSection;
    private List<Menu> menuList;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_detail);

        TextView sn = findViewById(R.id.shop_detail_name);
        TextView mo = findViewById(R.id.shop_detail_minorder);
        TextView dt = findViewById(R.id.shop_detail_delivery_time);
        TextView df = findViewById(R.id.shop_detail_delivery_fee);
        TextView sd = findViewById(R.id.shop_detail_distance);
        final ImageView checkedIsFavorite = findViewById(R.id.save_favorite_btn);
        RatingBar shopDetailRb = findViewById ( R.id.shop_detail_rated );
        tabLayout = findViewById(R.id.tablayout);
        itemCountTv = findViewById(R.id.item_count);
        grandTotalTv = findViewById(R.id.grand_total);
        bottomDesign = findViewById(R.id.bottom_design);
        shopClose = findViewById(R.id.shop_cover_close);
        shopTimeOpen = findViewById(R.id.shop_cover_closeText);
        progressBar = findViewById(R.id.progressBar);
        upButton = findViewById(R.id.upButton);
        appbar = findViewById(R.id.app_bar);

        recyclerViewProduct = findViewById(R.id.list_shop);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewProduct.setLayoutManager(linearLayoutManager);

        if (!isConnectedToInternet ( ShopDetailActivity.this )){
            popupMessage ( null, getResources ().getString ( R.string.failed_internet ),ShopDetailActivity.this); }

        checkShopCloseOpen = false;
        shopOpenHours = new ArrayList<>();

        bottomDesign.setOnClickListener(this);
        findViewById(R.id.backButton).setOnClickListener(this);

        upButton.setOnClickListener(this);

        if (getIntent() != null && getIntent().hasExtra("shop")) {
            shop = (Shop) getIntent().getSerializableExtra("shop");
            toast = Toast.makeText(getBaseContext(),getString(R.string.shop_is_close)+" "+shop.getName()+" "+getString(R.string.is_close),Toast.LENGTH_SHORT);
            if (shop == null)
                return;
            sn.setText(shop.getName());

            if (Constant.getConfig()!=null) currencySign = Constant.getConfig().getCurrencySign ();

               if(shop.getMin() != 0){
                   mo.setText(String.format(" %s %s", currencySign,Util.formatPrice ( shop.getMin() )));
               } else {
                   mo.setText(String.format(" %s %s", currencySign,Util.formatPrice ( 0 )));
               }
               if(shop.getDelivery_fee() != 0){
                   df.setText(String.format(" %s %s",currencySign,Util.formatPrice ( shop.getDelivery_fee() )));
               } else {
                   df.setText(String.format(" %s %s",currencySign,Util.formatPrice ( 0 )));
               }

          if (isMinDelivery(shop.getMinDelivery())>=60){
              int min ,hour;
              hour= isMinDelivery(shop.getMinDelivery())/60;
              min = isMinDelivery(shop.getMinDelivery())%60;

             if (min!=0) {
                 dt.setText(String.format("%s %s %s  "  ," hour(s) : %s mins",hour,min));
             }else {
                 dt.setText(String.format("%s %s  " ," hour(s)",hour));
             }
          }else {
              dt.setText(String.format("%s  " +" mins",isMinDelivery(shop.getMinDelivery())));
          }

          shopDetailRb.setRating ( shop.getShopRate () );
          sd.setText(String.format("%s km", "  "+Util.formatDistance(shop.getDistance())));

          if (MockupData.getCurrentOrderDetail() != null) requestParam(MockupData.getCurrentOrderDetail().getShop());
          else requestParam(shop);

        }
        //load shop menu detail
        if (isConnectedToInternet ( ShopDetailActivity.this )){
            if(Constant.getBaseUrl ()!=null)
                loadShopDetail ( BaseActivity.countryCode,shop.getShopId (),0,0 );
        }
        // Bottom sheet

        sn.setText(shop.getName());
        ImageView shopLogo = findViewById(R.id.shop_profile_image_detail);
        Glide.with(this)
                .load(shop.getShopProfileImage())
                .into(shopLogo);
        shopCover = findViewById(R.id.shop_cover);

        if (checkShopClose ( shop,false ) ){
            setCloseBanner ();
            if(Util.checkShopCloseServer())
                severClose();
        }

        Glide.with(shopCover.getContext())
                .load(shop.getImage())
                .into(shopCover);
        sd.setText(String.format("%s km", Util.formatDistance(shop.getDistance())));

        boolean isFavourite = readState();

        if (isFavourite) {
            counter = 2;
            checkedIsFavorite.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_black_24dp));
        } else {
            counter = 1;
            checkedIsFavorite.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black_24dp));
        }

        checkedIsFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                if (counter % 2 == 0) {
                    checkedIsFavorite.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_black_24dp));
                    saveState(true);
                    if (!isContain(shop)) {
                        MockupData.getFavouriteshops().add(shop);
                    }
                } else {
                    checkedIsFavorite.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black_24dp));
                    saveState(false);
                    if (isContain(shop)) {
                        removeItem(shop);
                    }
                }
            }
        });

        bottomSheetBehavior = BottomSheetBehavior.from(bottomDesign);
        // Show old cart when start shop screen
        if (MockupData.getCurrentOrderDetail() != null) {
            mOrderItemList.addAll(MockupData.getCurrentOrderDetail().getOrderItemList());
        }

        showCartBottomSheet ( null );

        recyclerViewProduct.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
                    isScrolling = true;
                }else{
                    isScrolling =false;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int firstPosition = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                childCount = linearLayoutManager.getItemCount();
                if(firstPosition > 5)
                    upButton.setVisibility(View.VISIBLE);
                if (!isScrolling) {
                    for (int i = 0; i < countPositionList.size(); i++) {
                        if (dy > 0 && firstPosition == countPositionList.get(i) ) {
                            tabLayout.getTabAt(i + 1).select();
                            break;
                        } else if (dy < 0 && firstPosition + 1 == countPositionList.get(i) ) {
                            tabLayout.getTabAt(i).select();
                            break;
                        }
                    }
                }
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(isScrolling) {
                    if(tab.getPosition()!=0)
                        appbar.setExpanded(false);
                    for(int i = 0; i< countTabList.size(); i++) {
                        if(tab.getText().equals(countTabList.get(i))) {
                            linearLayoutManager.scrollToPositionWithOffset(i == 0 ? 0 : countPositionList.get(i - 1), 0);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private boolean isContain(Shop shop) {
        List<Shop> shopList = MockupData.getFavouriteshops();
        for (Shop s : shopList) {
            if (s.getShopId() == shop.getShopId()) {
                return true;
            }
        }
        return false;
    }

    private void requestParam(Shop requestShop){
        requestParams.put ( "account_id",requestShop.getShopId () );
        requestParams.put ( "country_code" ,BaseActivity.countryCode);
        requestParams.put ( "latitute",BaseActivity.latitude  );
        requestParams.put ( "longitute",BaseActivity.longitude  );
        requestParams.put ( "deliveryFee",requestShop.getDelivery_fee () );
    }

    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.backButton) {
            onBackPressed();
        } else if (view.getId() == R.id.bottom_design) {
            Util.firebaseAnalytics(this,Constant.CLICK_ON,"go_to_cart");
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setEnabled(true);
                }
            },500);
            goCartScreen ();
        }else if(view.getId() == R.id.upButton){
            tabLayout.getTabAt(0).select();
            linearLayoutManager.scrollToPositionWithOffset(0,0);
            appbar.setExpanded(true);
        }
    }

    private void goCartScreen() {
        Intent intent = new Intent(ShopDetailActivity.this, CheckOutCartActivity.class);
        intent.putExtra("orderDetail", MockupData.getCurrentOrderDetail());
        intent.putExtra("shop",shop);
        if (MockupData.getCurrentOrderDetail ()!=null){
            //clear has map new value
            requestParams.put ( "account_id",MockupData.getCurrentOrderDetail ().getShop ().getShopId () );
            requestParams.put ( "country_Code" ,BaseActivity.countryCode);
            requestParams.put ( "latitute",BaseActivity.latitude );
            requestParams.put ( "longitute",BaseActivity.longitude );
        }
        requestParams.put ( "items",mOrderItemList );
        intent.putExtra ( "calculate",requestParams );
        //intent subtototal here
        startActivityForResult(intent, REQUEST_CODE_CHECKOUT);
    }

    //saveState and readState use for Favorite
    private void saveState(boolean isFavourite) {
        SharedPreferences aSharedPreferences = this.getSharedPreferences(
                "Favourite", Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences
                .edit();
        aSharedPreferencesEdit.putBoolean("shopId"+shop.getShopId(), isFavourite);
        aSharedPreferencesEdit.apply();
    }

    private boolean readState() {
        SharedPreferences aSharedPreferences = this.getSharedPreferences(
                "Favourite", Context.MODE_PRIVATE);
        return aSharedPreferences.getBoolean("shopId"+shop.getShopId(), false);
    }

    private void loadShopDetail(String countryCode, final int shopId, int cateId, int tagId){
        new StoreWs ().loadStoresDetail ( countryCode, shopId, cateId, tagId,BaseActivity.latitude , BaseActivity.longitude,new StoreWs.LoadStoreCallback () {
            @Override
            public void onLoadSuccess(List<Shop> shopList) {

            }

            @Override
            public void onLoadFailed(String errorMessage) {
                Util.popupMessage(null, errorMessage, ShopDetailActivity.this);
            }

            @Override
            public void onLoadStoreDetail(Shop shop) {
                 menuList = shop.getProductMenu () ;
                Collections.sort ( menuList, new Comparator<Menu> () {
                    @Override
                    public int compare(Menu men1, Menu men2) {
                        return  men1.getLabel ().compareTo ( men2.getLabel () );
                    }
                } );
                loadAllProducts(recyclerViewProduct);
            }
        } );
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean isFavourite = readState();
        final ImageView checkedIsFavorite = findViewById(R.id.save_favorite_btn);
        if (isFavourite) {
            checkedIsFavorite.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_black_24dp));
        } else {
            checkedIsFavorite.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_favorite_border_black_24dp));
        }
        if (!isConnectedToInternet ( ShopDetailActivity.this )){
            popupMessage ( null, getResources ().getString ( R.string.failed_internet ),ShopDetailActivity.this); }
    }

    private boolean isSameOption(List<AddOn> newItem, List<AddOn> oldItem) {
        // In case Options
        boolean isSameOption =false;
        List<Integer> newOptions = new ArrayList<>();
        List<Integer> oldOptions = new ArrayList<>();
        if (newItem.size()==0&&oldItem.size()==0){
            //no option/addon
            return true;
        }
        if (newItem.size() != oldItem.size()) {
            //compare size of add on
            return false;
        } else { // If same size, compare each option item
            int optionId;
            for (int i = 0; i <oldItem.size (); i++) {
                optionId = oldItem.get(i).getId();
                for (int k =0; k< newItem.size ();k++) {
                    //find the same optionid
                    int optId = newItem.get ( k ).getId ();
                    if (optionId == optId) newOptions.add ( optionId );//contain all same option for old compare to new
                }
                oldOptions.add(optionId);//add old item to compare the size of same option and new
            }
            if (newOptions.size()== oldOptions.size()) isSameOption= true;
        }
        return isSameOption;
    }

    private CartItemChangeListener onCartItemAddedListener = new CartItemChangeListener() {
        @Override
        public void onCartItemAdded(OrderItem orderItem) {
            if (orderItem == null) return;

            if( mOrderItemList.size() == 0) checkItemAddCart(orderItem,mOrderItemList);
            else checkItemAddCart(orderItem,MockupData.getCurrentOrderDetail().getOrderItemList());
        }
        @Override
        public void onExistingCartRemove() {
            mOrderItemList.clear();
        }
    };

    public interface CartItemChangeListener {
        void onCartItemAdded(OrderItem orderItem);
        void onExistingCartRemove();
    }

    private void checkItemAddCart(OrderItem orderItem,List<OrderItem> checkShopItem){
        boolean isSameOrderItem = false;
        int qty;
        for (int i = 0; i < checkShopItem.size(); i++) {
            OrderItem o = checkShopItem.get(i);//item in list , everything is true.
            if (o.getProductId() == orderItem.getProductId() && o.getComment().equals(orderItem.getComment()) && isSameOption(orderItem.getOption(), o.getOption()) && isSameOption(orderItem.getAddOns(), o.getAddOns())) {
                qty = o.getQty() + orderItem.getQty();
                isSameOrderItem = true;
                o.setQty(qty);
                break;
            }
        }
        if (!isSameOrderItem) checkShopItem.add(orderItem);
        showCartBottomSheet(checkShopItem);
        // Save Latest items in cart for use later
        saveLatestStateItems(checkShopItem);
    }

    private void showCartBottomSheet(List<OrderItem> orderItemList) {
        if (orderItemList == null)
            orderItemList = mOrderItemList;
        else if(MockupData.getCurrentOrderDetail()!=null){
            orderItemList = MockupData.getCurrentOrderDetail().getOrderItemList();
        }
        double grandTotal = 0;
        int count = 0;
        double subTotal = 0;
        for (int ik = 0; ik < orderItemList.size () ; ik++) {
                OrderItem i = orderItemList.get ( ik );

                subTotal = i.getPrice();
                if (i.getOption() != null) {
                    for (AddOn option : i.getOption()) {
                        subTotal += option.getPrice();
                    }
                }

                List<AddOn> listAddon = i.getAddOns();
                for (AddOn addOn : listAddon) { // some item has add on with price
                    subTotal += addOn.getPrice();
                }
                grandTotal += subTotal * i.getQty();
                count += i.getQty();
                if (count == 0) {
                    orderItemList.remove(i);
                }
            }


        if (MockupData.getCurrentOrderDetail() != null) {
            saveShop = MockupData.getCurrentOrderDetail().getShop();
            if (valueTotalIntent!=0){
                subTotal= valueTotalIntent;
                grandTotal=  subTotal-saveShop.getDelivery_fee ();
            }
        }

        if (count > 0) {
            bottomDesign.setVisibility(View.VISIBLE);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            bottomDesign.setVisibility(View.GONE);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        countItem = count;
        totalPrice= (float) grandTotal;

        itemCountTv.setText(String.format(Locale.US, "%d", count));
        grandTotalTv.setText(String.format("%s %s", currencySign,Util.formatPrice(grandTotal)));
    }

    private void saveLatestStateItems(List<OrderItem> mOrderItemList) {
        if (mOrderItemList.size() > 0) {
            OrderDetail myOrderDetail = new OrderDetail();
            myOrderDetail.setShop(shop);
            myOrderDetail.setCountry_code ( BaseActivity.countryCode );
            myOrderDetail.setLat ( BaseActivity.latitude );
            myOrderDetail.setLng ( BaseActivity.longitude );
            myOrderDetail.setStoreId(shop.getShopId());
            myOrderDetail.setDeliveryFee ( shop.getDelivery_fee () );
            myOrderDetail.setOrderItemList(mOrderItemList);
            MockupData.setCurrentOrderDetail(myOrderDetail);
            MockupData.getCurrentOrderDetail().setTotalPrice(totalPrice);
            MockupData.getCurrentOrderDetail().setItemCount(countItem);
            MockupData.getCurrentOrderDetail().setRequestParams(requestParams);
            // Save to static for use later when user try to add new item from different store
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CHECKOUT && resultCode == RESULT_OK) {
            // Edit Qty for Order Item
            OrderItem orderItem ;
            if (data != null ){
                if (data.hasExtra("orderItem")){    //from add to cart screen
                    orderItem = (OrderItem) data.getSerializableExtra("orderItem");
                    if (orderItem==null) return;
                    if(data.hasExtra("orderItemQty")) mOrderItemList = (List<OrderItem>) data.getSerializableExtra("orderItemQty");

                    // Edit Qty/
                    // Diff orderItem can have same product Id :( so we cannot compare by product id
                    for (int k = 0; k < mOrderItemList.size(); k++) {
                        OrderItem oItem = mOrderItemList.get(k);
                        assert orderItem != null;
                        if (oItem.getProductId() == orderItem.getProductId()) {
                            oItem.setQty(orderItem.getQty());
                            if (oItem.getQty() == 0){
                                mOrderItemList.remove(oItem);
                            }
                            break; // If found => stop finding
                        }
                    }
                }
                OrderDetail myCart = MockupData.getCurrentOrderDetail();
                HashMap<String, Integer> myCartMapItems = new HashMap<>();
                if (myCart != null) {
                    List<OrderItem> orderItems = myCart.getOrderItemList();
                    for (OrderItem oItem : orderItems) {
                        if (oItem.getQty() > 0) {
                            int qty = oItem.getQty();
                            String productId = String.valueOf(oItem.getProductId());
                            if (myCartMapItems.containsKey(productId)) {
                                qty += myCartMapItems.get(productId);
                            }
                            myCartMapItems.put(String.valueOf(oItem.getProductId()), qty);
                        }
                    }
                }
                // -----

                //get intent here0
                MockupData.setHighLightItemIds(myCartMapItems);
                sectionAdapter.notifyDataSetChanged();
                showCartBottomSheet(mOrderItemList);
            }
        }else{
            if (requestCode == ADD_CART && resultCode == Activity.RESULT_OK) { // Add new OrderItem
                OrderItem orderItem = (OrderItem) data.getSerializableExtra("orderItem");
                if (onCartItemAddedListener != null) {
                    if (data.hasExtra("clearCart"))
                        onCartItemAddedListener.onExistingCartRemove();
                    onCartItemAddedListener.onCartItemAdded(orderItem);
                    MockupData.setHighLightItemIds(getMapHighlight());// 'false' mean add new item to cart, true mean edit qty in cart
                }
                // ----
                OrderDetail myCart = MockupData.getCurrentOrderDetail();
                HashMap<String, Integer> myCartMapItems = new HashMap<>();
                if (myCart != null) {
                    List<OrderItem> orderItems = myCart.getOrderItemList();
                    for (OrderItem oItem : orderItems) {
                        if (oItem.getQty() > 0) {
                            int qty = oItem.getQty();
                            String productId = String.valueOf(oItem.getProductId());
                            if (myCartMapItems.containsKey(productId)) {
                                qty += myCartMapItems.get(productId);
                            }
                            myCartMapItems.put(String.valueOf(oItem.getProductId()), qty);
                        }
                    }

                }
                // -----
                MockupData.setHighLightItemIds(myCartMapItems);
                sectionAdapter.notifyDataSetChanged();
            }
        }
    }

    private void removeItem(Shop shop) {
        List<Shop> shopList = MockupData.getFavouriteshops();
        for (int i = 0 ; i< shopList.size() ;i++) {
            Shop s = shopList.get(i);
            if (s.getShopId() == shop.getShopId()) {
                MockupData.getFavouriteshops().remove(i);
            }
        }
    }

    private int isMinDelivery(Integer minShop){
        if (minShop != null){
            return minShop;
        }else {
            //if no minimum delivery we add 30 min
            return 30;
        }
    }

    public static Boolean checkShopClose(Shop shop,boolean isOrder) {
        boolean isShop_detail =((isOrder)?false:true);

        int open1 = 0, open2 = 0, close1 = 0, close2 = 0;
        if (shop.getOpenHours().get(0).getIsClose() != 1) {
            shopOpenHours.add(shop.getOpenHours().get(0).getOpenTime());
            shopOpenHours.add(shop.getOpenHours().get(0).getCloseTime());
        } else {
            shopOpenHours.add(open1);
            shopOpenHours.add(close1);
        }
        if (shop.getOpenHours().get(1).getIsClose() != 1) {
            shopOpenHours.add(shop.getOpenHours().get(1).getCloseTime());
            shopOpenHours.add(shop.getOpenHours().get(1).getOpenTime());
        } else {
            shopOpenHours.add(open2);
            shopOpenHours.add(close2);
        }
        Collections.sort(shopOpenHours);
        return isShop_detail;
    }

    private void setCloseBanner(){
        Calendar mCurrentTime = Calendar.getInstance();
        int tmrtime1=0,tmrTime2=0;
        int mH = (mCurrentTime.get(Calendar.HOUR_OF_DAY) * 60) + mCurrentTime.get(Calendar.MINUTE);
        tmrtime1 =shop.getOpenHours().get(2).getOpenTime();
        tmrTime2 = shop.getOpenHours().get(3).getOpenTime();
        if (shop.getVacation().size()!=0){
            shopClose.setVisibility(View.VISIBLE);
            shopTimeOpen.setVisibility(View.VISIBLE);
            checkShopCloseOpen = true;
            shopTimeOpen.setText ( String.format ( "%s",getString(R.string.shop_on_vacation) ));
        } else if(mH < shopOpenHours.get(0) || mH > shopOpenHours.get(1) && mH < shopOpenHours.get(2) || mH > shopOpenHours.get(3)){
            shopClose.setVisibility(View.VISIBLE);
            shopTimeOpen.setVisibility(View.VISIBLE);
            checkShopCloseOpen = true;
            if(mH <shopOpenHours.get(0))
                shopTimeOpen.setText ( String.format ( "%s %s %s%s", getString(R.string.open_at),shopOpenHours.get(0)/60,":",shopOpenHours.get(0)%60 < 10 ? "0"+shopOpenHours.get(0)%60 : shopOpenHours.get(0)%60 ) );
            else if (mH > shopOpenHours.get(1) && mH < shopOpenHours.get(2))
                shopTimeOpen.setText ( String.format ( "%s %s %s%s", getString(R.string.open_at),shopOpenHours.get(2)/60,":",shopOpenHours.get(2)%60 < 10 ? "0"+shopOpenHours.get(2)%60 : shopOpenHours.get(2)%60 ) );
            else {
                if(tmrtime1<tmrTime2 && shop.getOpenHours().get(2).getIsClose()!=1)
                    shopTimeOpen.setText(String.format("%s %s %s%s", getString(R.string.open_tmr_at), shop.getOpenHours().get(2).getOpenTime() / 60, ":", shop.getOpenHours().get(2).getOpenTime() % 60 < 10 ? "0" + shop.getOpenHours().get(2).getOpenTime() % 60 : shop.getOpenHours().get(2).getOpenTime() % 60));
                else if (shop.getOpenHours().get(3).getIsClose()!=1)
                    shopTimeOpen.setText(String.format("%s %s %s%s", getString(R.string.open_tmr_at), shop.getOpenHours().get(3).getOpenTime() / 60, ":", shop.getOpenHours().get(3).getOpenTime() % 60 < 10 ? "0" + shop.getOpenHours().get(3).getOpenTime() % 60 : shop.getOpenHours().get(3).getOpenTime() % 60));
                else
                    shopTimeOpen.setText (getString(R.string.today_and_tmr_is_close));
            }
        }
    }

    private void severClose(){
        shopClose.setVisibility(View.VISIBLE);
        shopTimeOpen.setVisibility(View.VISIBLE);
        checkShopCloseOpen = true;
        shopTimeOpen.setText ( String.format ( "%s",getString(R.string.server_clsoe)));
    }

    private ProductItemSectionAdapter.onProductItemClickListener onProductItemClickListener = new ProductItemSectionAdapter.onProductItemClickListener() {
        @Override
        public void onProductItemClick(final View itemView, Product item) {
            // start add to cart screen
            if (!ShopDetailActivity.checkShopCloseOpen) {
                itemView.setEnabled(false);
                itemView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        itemView.setEnabled(true);
                    }
                }, 300);
                Intent intent = new Intent(ShopDetailActivity.this, AddToCartActivity.class);
                intent.putExtra("item", item);
                intent.putExtra("shopId", shop.getShopId());
                startActivityForResult(intent, ADD_CART);
            }else{
                toast.show();
            }
        }
    };

    private HashMap<String, Integer> getMapHighlight() {
        OrderDetail myCart = MockupData.getCurrentOrderDetail();
        final HashMap<String, Integer> myCartMapItems = new HashMap<>();
        if (myCart != null) {
            List<OrderItem> orderItems = myCart.getOrderItemList();
            for (OrderItem oItem : orderItems) {
                if (oItem.getQty() > 0) {
                    int qty = oItem.getQty();
                    String productId = String.valueOf(oItem.getProductId());
                    if (myCartMapItems.containsKey(productId)) {
                        qty += myCartMapItems.get(productId);
                    }
                    myCartMapItems.put(String.valueOf(oItem.getProductId()), qty);
                }
            }
        }
        return myCartMapItems;
    }

    private void loadAllProducts(final RecyclerView recyclerView) {
        if (Constant.getBaseUrl() != null)
            new ProductWs().loadAllProducts(BaseActivity.countryCode, shop.getShopId(), 0, 0, 0, 0, new ProductWs.LoadProductCallback() {
                @Override
                public void onLoadSuccess(List<Product> productList) {

                }

                @Override
                public void onLoadFailed(String errorMessage) {

                }

                @Override
                public void onLoadProductDetail(ProductDetail productDetail) {

                }

                @Override
                public void onLoadAllProduct(HashMap<String, List<Product>> allProduct) {
                    progressBar.setVisibility(View.GONE);
                    int note = 0;
                    for (final Map.Entry<String, List<Product>> entry : allProduct.entrySet()) {
                        if (entry.getValue().size() > 0) {
                            tabLayout.addTab(tabLayout.newTab().setText(entry.getKey()));
                            countTabList.add(entry.getKey());
                            note = note+entry.getValue().size()+1;
                            countPositionList.add(note);
                            itemSection = new ProductItemSectionAdapter(entry.getKey(), entry.getValue(),onProductItemClickListener,getMapHighlight());
                            sectionAdapter.addSection(itemSection);
                        }
                    }
                    recyclerView.setAdapter(sectionAdapter);
                }
            });
    }

    @Override
    protected void onStop() {
        super.onStop();
        toast.cancel();
    }
}