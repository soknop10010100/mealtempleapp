package com.station29.mealtemple.ui.payment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import static com.station29.mealtemple.ui.BaseActivity.countryCode;

public class DefaultPaymentDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private OnDefaultPaymentSelect mListener;
    private List<Wallets> walletsList = new ArrayList<>();
    public static int notePosition = -1;
    private int adapterPosition;
    private User user;
    private boolean checkFirstOpen = true , checkDefaultPayment = false;
    private RecyclerView paymentRecyclerView;
    private ProgressBar progressBar;
    private CountryCodePicker countryCodePicker;
    private boolean checkStillUpdate = false;

    public static DefaultPaymentDialogFragment newInstance(List<Wallets> walletsList,User user) {
        Bundle args = new Bundle();
        DefaultPaymentDialogFragment fragment = new DefaultPaymentDialogFragment();
        args.putSerializable("walletList", (Serializable) walletsList);
        args.putSerializable("user",user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_credit_card_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getDialog().setCanceledOnTouchOutside(true);

        if (getArguments() != null) {
            walletsList = (List<Wallets>) getArguments().getSerializable("walletList");
            user = (User) getArguments().getSerializable("user");
        }
        countryCodePicker = new CountryCodePicker(getContext());

        paymentRecyclerView = view.findViewById(R.id.list);
        progressBar = view.findViewById(R.id.progress_loading);
        ImageView closeButton = view.findViewById(R.id.closeButton);

        paymentRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!checkStillUpdate)
                    dismiss();
            }
        });

        getWallet();

    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            mListener = (OnDefaultPaymentSelect) parent;
        } else {
            mListener = (OnDefaultPaymentSelect) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

    }

    public interface OnDefaultPaymentSelect {
        void onUpdateDefaultPayment(int walletsId , int defaultPayment);
    }

    private class ViewHolder extends RecyclerView.ViewHolder {

        final TextView text;
        final ImageView imgIcon,imgCheck;
        final RadioButton radioButton;

        ViewHolder(LayoutInflater inflater, final ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_item_list_dialog_item, parent, false));
            text = itemView.findViewById(R.id.text);
            imgIcon = itemView.findViewById(R.id.icon);
            imgCheck = itemView.findViewById(R.id.check);
            radioButton = itemView.findViewById(R.id.radioBtn);
        }

    }

    private class DefaultPaymentItemAdapter extends RecyclerView.Adapter<ViewHolder> {

        private List<Wallets> walletsList;
        private OnDefaultPaymentSelect onDefaultPaymentSelect;

        public DefaultPaymentItemAdapter(List<Wallets> walletsList,OnDefaultPaymentSelect onDefaultPaymentSelect) {
            this.walletsList = walletsList;
            this.onDefaultPaymentSelect = onDefaultPaymentSelect;
        }

        @NotNull
        @Override
        public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            final Wallets wallets = walletsList.get(position);
            adapterPosition = position;
            if(wallets != null) {
                if(position == 0 && !Util.checkIsHaveCash() ){
                    holder.text.setText(wallets.getBalance());
                    holder.imgIcon.setBackgroundResource(R.drawable.icons8_cash_50);
                    holder.imgIcon.setBackgroundTintList(ColorStateList.valueOf(R.color.colorPrimary));
                } else {
                    holder.text.setText(String.format("%s : %s %s",getString(R.string.wallet),wallets.getCurrency(),wallets.getBalance()));
                    holder.imgIcon.setBackgroundResource(R.drawable.ic_account_balance_wallet_black_24dp);
                    holder.imgIcon.setBackgroundTintList(ColorStateList.valueOf(R.color.colorPrimary));
                }

                if (checkFirstOpen && checkDefaultPayment ){
                    holder.radioButton.setChecked(wallets.getDefaultPayment() == 1);
                } else {
                    holder.radioButton.setChecked(notePosition == position);
                }

                if(!checkFirstOpen && notePosition == 0 && position != 0){
                    updateDefaultPayment(wallets.getWalletId(),0);
                }

                if(!checkDefaultPayment && position == 0 && checkFirstOpen){
                    holder.radioButton.setChecked(true);
                }

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkFirstOpen = false;
                        notePosition = position;
                        notifyDataSetChanged();
                        if(position != 0)
                            updateDefaultPayment(wallets.getWalletId(),1);
                    }
                });

                holder.radioButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkFirstOpen = false;
                        notePosition = position;
                        notifyDataSetChanged();
                        if(position != 0)
                            updateDefaultPayment(wallets.getWalletId(),1);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return walletsList.size();
        }

    }

    private String getPhoneNumber(){
        if(user.getPhoneNumber() != null && !user.getPhoneNumber().equals("")) {
            String phoneText = user.getPhoneNumber();
            String country;
            StringTokenizer defaultTokenizer = new StringTokenizer(phoneText);
            country = defaultTokenizer.nextToken();
            countryCodePicker.setCountryForPhoneCode(Integer.parseInt(country));
            phoneText = phoneText.replace(countryCode, "".trim());
            phoneText = phoneText.replaceAll("\\s+", "");
            return phoneText;
        }
        return null;
    }

    private void updateDefaultPayment(int walletsId , int defaultPayment){
        getDialog().setCanceledOnTouchOutside(false);
        checkStillUpdate = true;
        progressBar.setVisibility(View.VISIBLE);
        paymentRecyclerView.setVisibility(View.GONE);
        HashMap<String , Object> updateDefaultPayment = new HashMap<>();
        updateDefaultPayment.put("wallet_id",walletsId);
        updateDefaultPayment.put("phone_code", countryCodePicker.getFullNumberWithPlus());
        updateDefaultPayment.put("phone",getPhoneNumber());
        updateDefaultPayment.put("code", countryCode);
        if(user.getEmail() != null)
            updateDefaultPayment.put("email", user.getEmail());
        updateDefaultPayment.put("default",defaultPayment);
        new ProfileWs().updateProfiles(getActivity(), updateDefaultPayment, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user) {

            }

            @Override
            public void onError(String errorMessage) {
                getDialog().setCanceledOnTouchOutside(true);
                checkStillUpdate = false;
                progressBar.setVisibility(View.GONE);
                paymentRecyclerView.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(),errorMessage,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUpdateProfilesSuccess(boolean isSendSms,User user) {
                getDialog().setCanceledOnTouchOutside(true);
                checkStillUpdate = false;
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(),"Updated successfully.",Toast.LENGTH_SHORT).show();
                if(adapterPosition+1 == walletsList.size() && !checkFirstOpen)
                    dismiss();
            }
        });
    }

    private void getWallet(){
        new ProfileWs().viewProfiles(getActivity(), new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user) {
                progressBar.setVisibility(View.GONE);
                walletsList = user.getWalletsList();
                if(!Util.checkIsHaveCash())
                    walletsList.add(0,new Wallets(-1,getString(R.string.pay_by_cash),""));
                if( walletsList != null && !walletsList.isEmpty())
                    for (Wallets wallets : walletsList){
                        if(wallets.getDefaultPayment() == 1){
                            checkDefaultPayment = true;
                            break;
                        }
                    }
                DefaultPaymentItemAdapter defaultPaymentItemAdapter = new DefaultPaymentItemAdapter(walletsList, new OnDefaultPaymentSelect() {
                    @Override
                    public void onUpdateDefaultPayment(int walletsId, int defaultPayment) {

                    }
                });
                paymentRecyclerView.setAdapter(defaultPaymentItemAdapter);
                defaultPaymentItemAdapter.notifyDataSetChanged();
            }

            @Override
            public void onError(String errorMessage) {

            }

            @Override
            public void onUpdateProfilesSuccess(boolean isSendSms, User user) {

            }
        });
    }

}

