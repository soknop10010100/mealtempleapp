package com.station29.mealtemple.ui.home.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.Category;

import java.util.List;

public class PopCategoryAdapter extends RecyclerView.Adapter<PopCategoryAdapter.CatViewHolder> {

    private List<Category> categoryList;
    private CategoryClickListener mListener ;

    public PopCategoryAdapter(List<Category> categoryList, CategoryClickListener mListener) {
        this.categoryList = categoryList;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public CatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category_item, parent, false);
        return new CatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CatViewHolder holder, int position) {
        final Category category = categoryList.get(position);
        if (category != null) {
            Glide.with ( holder.itemView.getContext () ).load ( category.getLogo ()).into ( holder.imageView );
            holder.catNameTv.setText(category.getName());
            holder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onCategoryClick(category);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    class CatViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView catNameTv;
        View row;

        CatViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.icon);
            catNameTv = itemView.findViewById(R.id.name);
            row = itemView;
        }
    }

    public interface CategoryClickListener {
        void onCategoryClick(Category category);
    }

    public void clear() {
        int size = categoryList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                categoryList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
