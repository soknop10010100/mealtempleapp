package com.station29.mealtemple.ui.payment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.MyQRCode;
import com.station29.mealtemple.api.request.ScanMeWs;
import com.station29.mealtemple.util.Util;

public class ScanMeFragment extends DialogFragment {

    public static ScanMeFragment newInstance() {
        Bundle args = new Bundle();
        ScanMeFragment fragment = new ScanMeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_scan_me, container, false);

        ImageView myCodeImg = view.findViewById(R.id.my_code);
        ProgressBar progressBar = view.findViewById(R.id.progress);

        new ScanMeWs().getMyQRCode(view.getContext(), new ScanMeWs.ScanMeCallback() {
            @Override
            public void onSuccess(MyQRCode qrCode, JsonObject dataObj) {
                Bitmap qrCodeImageBitmap = Util.getQRCodeImage(dataObj.toString());
                myCodeImg.setImageBitmap(qrCodeImageBitmap);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(view.getContext(), error, Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
}
