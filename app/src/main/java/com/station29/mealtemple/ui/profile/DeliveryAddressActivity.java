package com.station29.mealtemple.ui.profile;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.MapsActivity;
import com.station29.mealtemple.util.Util;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class DeliveryAddressActivity extends BaseActivity implements View.OnClickListener {

    private final static int REQUEST_CODE_SET_LOCATION = 300;
    private TextView nameTv, phoneTv, houseTv;
    private Button addressButton;
    private CountryCodePicker ccp;
    public static String phoneNumber =null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_delivery_address );
        nameTv = findViewById(R.id.delivery_receiver_name);
        phoneTv = findViewById(R.id.delivery_receiver_phone);
        houseTv = findViewById(R.id.delivery_receiver_houseno);
        addressButton = findViewById(R.id.delivery_receiver_location);
        ccp = findViewById(R.id.ccp);
        ccp.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);
        addressButton.setOnClickListener(this);

        if(!MockupData.LOGIN_PHONE.equals("")) {
                String countryCode = "";
                String phoneText = MockupData.LOGIN_PHONE;
                StringTokenizer defaultTokenizer = new StringTokenizer(phoneText);
                countryCode = defaultTokenizer.nextToken();
                ccp.setCountryForPhoneCode(Integer.parseInt(countryCode));
                phoneText = phoneText.replace ( countryCode,"".trim () );
                phoneText = phoneText.replaceAll("\\s+","");
                phoneTv.setText(phoneText);
        }
        if (MockupData.USER_NAME != null) {
            nameTv.setText(MockupData.USER_NAME);
        }
        if(MockupData.getHouseNumber()!=null ){
            houseTv.setText(MockupData.getHouseNumber());
        }
        if (latitude != 0 && longitude != 0) {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (addresses.size() > 0) {
                    String fullAdd = addresses.get(0).getAddressLine(0);
                    addressButton.setText(fullAdd);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

       findViewById(R.id.save_delivery).setOnClickListener ( this );
        findViewById ( R.id.delivery_back_btn).setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                finish();
            }
        } );
    }

    @Override
    public void onBackPressed() {
            popupMessage ( getResources ().getString ( R.string.edit_delivery_address ) ,DeliveryAddressActivity.this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.delivery_receiver_location) {
            Intent intent = new Intent(DeliveryAddressActivity.this, MapsActivity.class);
            intent.putExtra ( "action", "EDITADDRESS" );
            startActivityForResult(intent, REQUEST_CODE_SET_LOCATION);
        } else if (view.getId() == R.id.save_delivery) {
            androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.DialogTheme);
         if (phoneTv.getText ().toString ().equals ( "" )) {
             builder.setMessage(getString(R.string.Invalid_Phone_Number));
             builder.setNegativeButton(getString(R.string.close), null);
             builder.show();
         }else {
             if (houseTv.getText ().toString ().equals ( "" )){
                 sendEditProfile ();
             }else {
                 sendEditProfile ();
             }
            }
        }
    }
    private  void sendEditProfile(){
        Intent intent = new Intent();
        if (!houseTv.getText ().toString ().equals ( "" ))
        {
            intent.putExtra("deliveryHouseNumber",houseTv.getText().toString());
        }
        intent.putExtra("deliveryName", getString(R.string.receiver_name) + " : "+ ((!nameTv.getText().toString().equals ( "" ))?nameTv.getText().toString(): getString(R.string.guest)));

        intent.putExtra("deliveryPhoneNumber",  "+"+ ccp.getFullNumber()+" "+  formatPhone(phoneTv.getText().toString().trim()));
        intent.putExtra("deliveryAddress",addressButton.getText().toString());
        MockupData.setHouseNumber(houseTv.getText().toString());
        MockupData.USER_NAME = ((!nameTv.getText().toString().equals ( "" ))?nameTv.getText().toString(): getString(R.string.guest));
        MockupData.DELIVERY_ADDRESS ="#"+houseTv.getText().toString()+" "+addressButton.getText().toString();
        MockupData.LOGIN_PHONE = "+"+ccp.getFullNumber()+" "+formatPhone(phoneTv.getText().toString());
        phoneNumber =  "+"+ccp.getFullNumber()+" "+formatPhone(phoneTv.getText().toString());
        MapsActivity.oldEditShippingDeliver = null;
        setResult(RESULT_OK, intent);
        finish();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SET_LOCATION && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("delivery_address")) {
                String deliveryAddress = data.getStringExtra("delivery_address");
                addressButton.setText(deliveryAddress);
            }
        }
    }

//    private String formatPhone(String fullPhoneNumber){
//        String code ;
//        if (fullPhoneNumber.length ()>=8){
//            if (phoneTv.getText ().toString ().equals ( " " )|| phoneTv.getText ().toString ().isEmpty ())return fullPhoneNumber;
//            code = fullPhoneNumber.substring ( 0,ccp.getFullNumber ().length ());
//            fullPhoneNumber = (code.contentEquals ( ccp.getFullNumber () ) ? fullPhoneNumber.substring ( ccp.getFullNumber ().length () ) : fullPhoneNumber );
//        }else {
//            //return old phone number
//            return fullPhoneNumber;
//        }
//        return fullPhoneNumber;
//    }
    private String formatPhone(String fullPhoneNumber) {
        String code;
        if (fullPhoneNumber.length() >= 8) {
            if (phoneTv.getText().toString().equals(" ") || phoneTv.getText().toString().isEmpty())
                return fullPhoneNumber;
            code = fullPhoneNumber.substring(0, ccp.getFullNumber().length());
            fullPhoneNumber = (code.contentEquals(ccp.getFullNumber()) ? fullPhoneNumber.substring(ccp.getFullNumber().length()) : fullPhoneNumber);
            if(fullPhoneNumber.length() < 8){
                return phoneTv.getText().toString();
            }
        } else {//855
            //return old phone number
            return fullPhoneNumber;
        }
        return fullPhoneNumber;
    }

    private void popupMessage( String message, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.DialogTheme);
        builder.setTitle(null);
        builder.setMessage(message);
        builder.setNegativeButton ( String.format ( Locale.US, "%s", activity.getString ( R.string.discard ) ), new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        } );
        builder.setPositiveButton ( String.format ( Locale.US, "%s", activity.getString ( R.string.update ) ), new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (phoneTv.getText ().toString ().equals ( "" )){
                    Util.popupMessage ( null,getResources ().getString(R.string.Invalid_Phone_Number),DeliveryAddressActivity.this );
                }else sendEditProfile ();

            }
        } );
        builder.show();
    }
}
