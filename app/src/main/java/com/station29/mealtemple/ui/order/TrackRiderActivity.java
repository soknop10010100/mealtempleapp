package com.station29.mealtemple.ui.order;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Waypoint;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.GPSTracker;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class TrackRiderActivity extends BaseActivity implements OnMapReadyCallback {

    private FirebaseAuth mAuth;
    private static final String TAG = TrackRiderActivity.class.getSimpleName();
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private Double latDriver;
    private Double lngDriver;
    private String driverId;
    private GoogleMap mMap;
    SupportMapFragment mapFragment;
    private final static int REQUEST_PERMISSION_LOCATION = 305;
    private  double  latClient,lngClient,userLat=0, userLng=0;
    private String typeOrder,status="";
    private Waypoint getWaypointCustomer,waypointDriver;
    private boolean checkMap = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_rider);

        //check permission location
        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

        if (getIntent() != null && getIntent().hasExtra("typeOrder") ) {
            typeOrder= (String) getIntent().getSerializableExtra("typeOrder");
            if (typeOrder.equalsIgnoreCase("DELIVERY")){// food
                driverId = (String) getIntent().getSerializableExtra("driverId");
                latClient = (Double) getIntent().getDoubleExtra("latClient",0);
                lngClient = (Double) getIntent().getDoubleExtra("lngClient",0);
                getWaypointCustomer = (Waypoint) getIntent().getSerializableExtra("waypointCustomer");
                waypointDriver = (Waypoint) getIntent().getSerializableExtra("waypointDriver");
                status = (String) getIntent().getSerializableExtra("status");
            }else if (typeOrder.equalsIgnoreCase("SERVICE")){//taxi
                latClient = (Double) getIntent().getDoubleExtra("latClient",0);
                lngClient = (Double) getIntent().getDoubleExtra("lngClient",0);
                driverId = (String) getIntent().getSerializableExtra("driverId");
                getWaypointCustomer = (Waypoint) getIntent().getSerializableExtra("waypointCustomer");
                waypointDriver = (Waypoint) getIntent().getSerializableExtra("waypointDriver");
                status = (String) getIntent().getSerializableExtra("status");
            }

        }
        checkPermission();
        callLocation();

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        String token = Util.getString("token", TrackRiderActivity.this);
        if (currentUser != null || currentUser == null && token != null) {
            loginFireStore();
        }

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

    }

    public void onClick(View v) {
        if (v == findViewById(R.id.location_meBtn)) {
            if (mMap!=null)getMyLocation(mMap);
        }
    }

    private void gettingData() {
        firebaseFirestore.collection("drivers").document("" + driverId).addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot != null && documentSnapshot.exists()) {
                        latDriver = documentSnapshot.getGeoPoint("geoPoint").getLatitude();
                        lngDriver = documentSnapshot.getGeoPoint("geoPoint").getLongitude();
                        mMap.clear();
                        if(MockupData.checkpickup && getWaypointCustomer!=null || status.equalsIgnoreCase("PICKED_UP") && getWaypointCustomer!=null || status.equalsIgnoreCase("PENDING ") ){
                            waypointDriver = null;
                            List<LatLng> arrayList = convertWaypoint(getWaypointCustomer);
                            drawMap(arrayList);
                        }else if(!status.equals("") && waypointDriver!=null){
                            List<LatLng> arrayList = convertWaypoint(waypointDriver);
                            drawMap(arrayList);
                        }else{
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latDriver,lngDriver), 15));
                        }
                        mMap.addMarker(new MarkerOptions()//driver
                                .position(new LatLng(latDriver, lngDriver))
                                .title(getString(R.string.rider)).icon(getMarkerIconFromDrawable(getResources().getDrawable(R.drawable.ic_baseline_motorcycle_24))));
                        if(waypointDriver !=null){//pick up point
                            if(!typeOrder.equals("DELIVERY")) {
                                mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(waypointDriver.getListLatLng().get(waypointDriver.getListLatLng().size() - 1).getLatitute(), waypointDriver.getListLatLng().get(waypointDriver.getListLatLng().size() - 1).getLongitute()))
                                        .title(getString(R.string.pick_up))
                                        .icon(getMarkerIconFromDrawable(getResources().getDrawable(R.drawable.ic_icon_pickup))));
                            }else{
                                mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(waypointDriver.getListLatLng().get(waypointDriver.getListLatLng().size() - 1).getLatitute(), waypointDriver.getListLatLng().get(waypointDriver.getListLatLng().size() - 1).getLongitute()))
                                        .title(getString(R.string.pick_up))
                                        .icon(getMarkerIconFromDrawable(getResources().getDrawable(R.drawable.ic_icon_pickup))));
                            }
                        }else{ //destinartion
                            mMap.addMarker(new MarkerOptions()
                                    .position(new LatLng(latClient, lngClient))
                                    .title(getString(R.string.destination))
                                    .icon(getMarkerIconFromDrawable(getResources().getDrawable(R.drawable.ic_icon_pickup))));
                        }
                }
            }
        });
    }

    private void loginFireStore() {
        mAuth.signInWithEmailAndPassword(BuildConfig.FIRESTORE_USERNAME, BuildConfig.FIRESTORE_PASSWORD).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    // Sign in success, update UI with the signed-in user's information
                    if (driverId != null) gettingData();
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w(TAG, "signInWithEmail:failure", task.getException());
                }
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if(!checkMap) {
            checkMap = true;
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(BaseActivity.latitude, BaseActivity.longitude), 15));
        }
        mMap.getUiSettings().setCompassEnabled(false);//hide compass
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);
    }

    private void checkPermission() {
        if (getApplicationContext() == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                checkGps();
            } else {
                if (getApplicationContext() == null) return;
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSION_LOCATION);
            }
        } else {
            checkGps();

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //no interent
        if (!isConnectedToInternet(TrackRiderActivity.this)) {
            popupMessage(null, getResources().getString(R.string.failed_internet), TrackRiderActivity.this);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSION_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                checkGps();
            } else {
                Toast.makeText(getApplicationContext(), getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void checkGps() {
        final LocationManager manager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(TrackRiderActivity.this,R.style.DialogTheme);
        builder.setMessage(R.string.your_gro)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_PERMISSION_LOCATION);
                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {//convert drawable to bitmap
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    private void callLocation() {
        GPSTracker gps = new GPSTracker(TrackRiderActivity.this);
        if (gps.canGetLocation()) {
            userLat = gps.getLatitude();
            userLng = gps.getLongitude();
        }
    }
    private void getMyLocation(GoogleMap mMap) {
        //current location zoom
        if (userLat>0&&userLng>0){
            LatLng latLng = new LatLng(userLat,userLng);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.animateCamera(cameraUpdate);
        }
    }

    private void drawMap(List<LatLng> latLng) {
        if (latLng.size() > 0) {
            PolylineOptions rectLine = new PolylineOptions().width(10).color(getResources().getColor(R.color.colorPrimary));//set line color to app color
            for (int i = 0; i < latLng.size(); i++) {
                rectLine.add(latLng.get(i));
            }
            mMap.addPolyline(rectLine);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(latLng.get(latLng.size()-1));
            builder.include(latLng.get(0));
            LatLngBounds bounds = builder.build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels / 2, (int) (getResources().getDisplayMetrics().widthPixels * 0.18));
            mMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    private List<LatLng> convertWaypoint(Waypoint waypoint){
        ArrayList<LatLng> routelist = new ArrayList<>();
        double lat, lng;
        if (waypoint.getListLatLng().size() > 0) {
            for (int i = 0; waypoint.getListLatLng().size() > i; i++) {
                lat = waypoint.getListLatLng().get(i).getLatitute();
                lng = waypoint.getListLatLng().get(i).getLongitute();
                routelist.add(new LatLng(lat, lng));
            }
        }
        return routelist;
    }
}