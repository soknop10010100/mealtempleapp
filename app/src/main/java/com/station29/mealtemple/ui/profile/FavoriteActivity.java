package com.station29.mealtemple.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.adpater.ShopListAdapter;
import com.station29.mealtemple.ui.home.ShopDetailActivity;
import java.util.List;

public class FavoriteActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_store);

        TextView titleTv = findViewById(R.id.title);
        titleTv.setText(R.string.favorite_store);

        if(MockupData.getFavouriteshops().isEmpty()) {
            TextView noFav = findViewById(R.id.fav_text);
            noFav.setText(R.string.no_data_avalible);
            noFav.setVisibility(View.VISIBLE);
        }

        final RecyclerView recyclerView = findViewById(R.id.list_cat);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        List<Shop> filelist = MockupData.getFavouriteshops();
        ShopListAdapter adapter = new ShopListAdapter(filelist, mListener);
        recyclerView.setAdapter(adapter);


        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }



    private ShopListAdapter.OnShopClickListener mListener = new ShopListAdapter.OnShopClickListener() {
        @Override
        public void onShopClick(View itemView, Shop shop) {
            Intent intent = new Intent(FavoriteActivity.this, ShopDetailActivity.class);
            intent.putExtra("shop", shop);
            startActivity(intent);
        }
    };

}

