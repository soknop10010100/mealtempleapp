package com.station29.mealtemple.ui.profile;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.button.MaterialButton;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.api.request.SignUpWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.delivery_package.DeliveryPackageActivity;
import com.station29.mealtemple.ui.home.OrderConfirmationActivity;
import com.station29.mealtemple.util.Util;

import java.util.HashMap;
import java.util.StringTokenizer;

public class SetChangePinActivity extends BaseActivity {

    private EditText pinEt1, pinEt2 , oldPinEt;
    private MaterialButton btnConfirm;
    private User user;
    private ImageView backButton,imgChangePin;
    private TextView title,btnForgotPin,txtForgotPin;
    private CountryCodePicker countryCodePicker;
    private boolean isForgotPin = false;
    private View progress_loading;
    private final int REQUEST_EDIT_PROFILE = 2341, REQUEST_VERIFY_SMS = 2321 , REQUEST_FORGOT_PIN = 324;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pin);

        initView();
        initData();
        initAction();
    }

    private void initView(){

        pinEt1 = findViewById(R.id.pinEt1);
        pinEt2 = findViewById(R.id.pinEt2);
        btnConfirm = findViewById(R.id.btnConfirm);
        oldPinEt = findViewById(R.id.oldPinEt);
        backButton = findViewById(R.id.backButton);
        title = findViewById(R.id.title);
        btnForgotPin = findViewById(R.id.btnForgotPin);
        progress_loading = findViewById(R.id.progress_loading);
        txtForgotPin = findViewById(R.id.txtForgotPin);
        imgChangePin = findViewById(R.id.imgChangePin);

    }

    private void initData(){

        if(getIntent() != null && getIntent().hasExtra("user")) {
            user = (User) getIntent().getSerializableExtra("user");
            if (user.isConfirmPin()){
                title.setText(R.string.change_pin);
            } else {
                oldPinEt.setVisibility(View.GONE);
                title.setText(R.string.set_pin);
                btnForgotPin.setVisibility(View.GONE);
            }
            isForgotPin = false;
        } else if (getIntent() != null && getIntent().hasExtra("forgotUser")) {
            user = (User) getIntent().getSerializableExtra("forgotUser");
            imgChangePin.setVisibility(View.VISIBLE);
            txtForgotPin.setText(R.string.please_input_uour_new_pin);
            oldPinEt.setVisibility(View.GONE);
            isForgotPin = true;
            title.setText(R.string.reset_pin);
            pinEt1.setHint(R.string.input_new_pin);
            pinEt2.setHint(R.string.input_confirm_new_pin);
            btnForgotPin.setVisibility(View.GONE);
        }
    }

    private void initAction(){

        countryCodePicker = new CountryCodePicker(this);

        btnConfirm.setOnClickListener(v -> {
            HashMap<String , Object> hashMap = new HashMap<>();
            if(!isForgotPin){
                progress_loading.setVisibility(View.VISIBLE);
                if(user != null && user.isConfirmPin() && !isForgotPin){
                    hashMap.put("old_pin_password",oldPinEt.getText().toString());
                }
                hashMap.put("pin_password",pinEt1.getText().toString());
                hashMap.put("pin_password_confirm",pinEt2.getText().toString());
                new PaymentWs().setUserPin(SetChangePinActivity.this,hashMap,onTopUpWalletsCallBack);
            } else {
                popMakeSureSendSMS();
            }
        });


        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("status",false);
                setResult(RESULT_OK);
                finish();
            }
        });


        btnForgotPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user.getPhoneNumber() != null && !user.getPhoneNumber().equals("")){
                    Intent intent = new Intent(SetChangePinActivity.this, SetChangePinActivity.class);
                    intent.putExtra("forgotUser",user);
                    startActivityForResult(intent,REQUEST_FORGOT_PIN);
                } else {
                    popupMessageUpdatePhone(getString(R.string.error),SetChangePinActivity.this);
                }
            }
        });

    }

    private void forgotPinParam(){
        HashMap<String,Object> userAccount = new HashMap<> (  ) ;

        String phone = user.getPhoneNumber() ;
        String code;
        StringTokenizer defaultTokenizer = new StringTokenizer(phone);
        code = defaultTokenizer.nextToken();
        countryCodePicker.setCountryForPhoneCode(Integer.parseInt(code));
        phone = phone.replace(code, "".trim());
        phone = phone.replaceAll("\\s+", "");

        userAccount.put("phone",phone);
        userAccount.put("phone_code",countryCodePicker.getFullNumberWithPlus());
        userAccount.put ( "code",countryCode );
        userAccount.put ( "user_type","USER_ANDROID");
        userAccount.put("function","Customer");

        new SignUpWs().forgetAccountPassword(userAccount ,requestServer);
    }

    private final SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer () {
        @Override
        public void onRequestSuccess() {
            progress_loading.setVisibility(View.GONE);
            HashMap<String , String> forgotPINMap = new HashMap<>();
            forgotPINMap.put("pin_password",pinEt1.getText().toString());
            forgotPINMap.put("pin_password_confirm",pinEt2.getText().toString());

            Intent intent = new Intent(SetChangePinActivity.this,ConfirmSmsActivity.class);
            intent.putExtra("forgotPINParam",forgotPINMap);
            startActivityForResult(intent,REQUEST_VERIFY_SMS);
        }

        @Override
        public void onRequestToken(String token, int id, User user) {

        }

        @Override
        public void onRequestFailed(String message) {
            progress_loading.setVisibility(View.GONE);
            Toast.makeText(SetChangePinActivity.this,message,Toast.LENGTH_SHORT).show();
        }
    };

    private final PaymentWs.onTopUpWalletsCallBack onTopUpWalletsCallBack = new PaymentWs.onTopUpWalletsCallBack() {
        @Override
        public void onSuccess(String message, int paymentId) {
            progress_loading.setVisibility(View.GONE);
            Toast.makeText(SetChangePinActivity.this,message,Toast.LENGTH_SHORT).show();
            Intent intent = new Intent();
            intent.putExtra("status",true);
            setResult(RESULT_OK,intent);
            finish();
        }

        @Override
        public void onError(String message) {
            progress_loading.setVisibility(View.GONE);
            Util.popupMessage(getString(R.string.error),message, SetChangePinActivity.this);
        }
    };

    private void popMakeSureSendSMS() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SetChangePinActivity.this,R.style.DialogTheme);
        builder.setMessage(String.format("%s : %s",getString(R.string.we_will_send_sms_to_verify_with_this_phone),user.getPhoneNumber()));
        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(pinEt1.getText().toString().equals(pinEt2.getText().toString()) && !pinEt1.getText().toString().equals("") && !pinEt2.getText().toString().equals("")){
                    progress_loading.setVisibility(View.VISIBLE);
                    forgotPinParam();
                } else {
                    progress_loading.setVisibility(View.GONE);
                    Util.popupMessage(getString(R.string.error),getString(R.string.pin_code_are_incorrect),SetChangePinActivity.this);
                }
            }
        });
        builder.show();
    }

    private void popupMessageUpdatePhone(String title, Activity activity) {
        Util.firebaseAnalytics(this, Constant.ERROR_MESSAGE,"user_information_missing");
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SetChangePinActivity.this,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(R.string.you_miss_data);
        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(SetChangePinActivity.this, EditProfileActivity.class);
                intent.putExtra("user",user);
                startActivityForResult(intent, REQUEST_EDIT_PROFILE);
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode== REQUEST_EDIT_PROFILE && resultCode == RESULT_OK){
            if(data != null){
                user = (User) data.getSerializableExtra("user");
            }
        } else if (requestCode == REQUEST_VERIFY_SMS && resultCode == RESULT_OK || requestCode == REQUEST_FORGOT_PIN && resultCode == RESULT_OK){
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra("status",false);
        setResult(RESULT_OK,intent);
        finish();
    }
}