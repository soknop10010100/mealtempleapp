package com.station29.mealtemple.ui.home.adpater;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.swipe.SwipeLayout;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.AddOn;

import com.station29.mealtemple.api.model.CouponDis;
import com.station29.mealtemple.api.model.DisItems;
import com.station29.mealtemple.api.model.OrderItem;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CartItemAdapter extends RecyclerView.Adapter<CartItemAdapter.ViewHolder> {


    private static List<OrderItem> orderItemList;
    private onItemEditedListener mOnItemEditedListener;
    private boolean isEditable = true;
    private int isView = 10;
    private CouponDis couponDis;
    private List<DisItems> disItemsList= new ArrayList<> (  );

    public CartItemAdapter(List<OrderItem> orderItemList, onItemEditedListener onItemEditedListener) {
        this.orderItemList = orderItemList;
        mOnItemEditedListener = onItemEditedListener;
    }

    public CartItemAdapter(List<OrderItem> orderItemList, onItemEditedListener onItemEditedListener, boolean isEditable, int isView) {
        this.orderItemList = orderItemList;
        this.mOnItemEditedListener = onItemEditedListener;
        this.isEditable = isEditable;
        this.isView =isView;
    }
    //for coupon
    public CartItemAdapter(CouponDis couponDis,List<DisItems> disItemsList, List<OrderItem> orderItemList, onItemEditedListener onItemEditedListener, boolean isEditable, int isView) {
        this.couponDis = couponDis;
        this.mOnItemEditedListener = onItemEditedListener;
        this.isEditable = isEditable;
        this.isView = isView;
        this.orderItemList = orderItemList;
        this.disItemsList = disItemsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order_item, parent, false);
        return new ViewHolder(row);
    }

    private  int count = 0;
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        // In case OrderConfirmation
//        int count = 0;
        String currencySign ;
        currencySign = ((Constant.getConfig ()!=null)? Constant.getConfig ().getCurrencySign (): "N/A");

        final OrderItem orderItem = orderItemList.get(position);
        if (orderItem != null) {
            count = orderItem.getQty();
            holder.numberTv.setText(String.format(Locale.US, "%d", count));
            if (!orderItem.getOption().isEmpty()) {
                holder.optionTv.setVisibility(View.VISIBLE);
                holder.optionTv.setText("");
                int noteIndex=0;
                for (AddOn option : orderItem.getOption()) {
                    noteIndex++;
                    if(orderItem.getOption().size()>1 && noteIndex!=orderItem.getOption().size()) holder.optionTv.append(option.getLabel() + "/ ");
                    else holder.optionTv.append(option.getLabel());
                }
            }
            holder.itemNameTv.setText(orderItem.getName());
            holder.specialInstructionTv.setText(orderItem.getComment());
            //set default image if no image
            Glide.with ( holder.itemView.getContext () ).load ( orderItem.getProductImage () ).apply ( new RequestOptions ().transforms ( new RoundedCorners ( 8 ) ) ).into ( holder.itemImage );

            List<AddOn> addOnList = orderItem.getAddOns();
            double orderItemPrice = orderItem.getPrice();
            if (!orderItem.getOption().isEmpty()) {
                for (AddOn option : orderItem.getOption()) {
                    orderItemPrice += option.getPrice();
                }
            }
            if (addOnList.size() > 0) {
                String optionText = "";
                int noteIndex=0;
                for (AddOn addOn : addOnList) {
                    noteIndex++;
                    if(addOnList.size()>1 && noteIndex!=addOnList.size()) optionText = optionText.concat(addOn.getLabel() + "/ ");
                    else optionText = optionText.concat(addOn.getLabel() + " ");
                    orderItemPrice += addOn.getPrice();
                }
                holder.addOnTv.setVisibility(View.VISIBLE);
                holder.addOnTv.setText(optionText);
            }
//              if afterprice have set line on old price for coupon
            if (disItemsList.size () != 0){
                DisItems disItems = new DisItems ();
                for (int i = 0; i < disItemsList.size (); i++) {
                    disItems= disItemsList.get ( i);
                    if (couponDis.getOption ().equalsIgnoreCase ( "product" )){
                        //product dis statment
                        if (disItems.getProductId ()== orderItem.getProductId () && disItems.getAfterDis ()!=0){
                            holder.priceTv.setText(String.format("%s %s", currencySign,   Util.formatPrice (orderItemPrice)));
                            holder.priceTv.setBackgroundResource ( R.drawable.cross_line );
                            holder.afterPriceTv.setText ( String.format("%s %s", currencySign,   Util.formatPrice (disItems.getAfterDis ())) );
                        }else {
                            holder.priceTv.setText(String.format("%s %s", currencySign,   Util.formatPrice (orderItemPrice)));
                        }
                    }else if (couponDis.getOption ().equalsIgnoreCase ( "option" )){
                        //option statement
                        if (disItems.getOptionId ()!=0){
                            int itemOptionId=0;
                            for (int h = 0; h <  orderItem.getOption ().size (); h++) {
                                if (disItems.getOptionId ()== orderItem.getOption ().get ( h ).getId ()){
                                    itemOptionId= orderItem.getOption ().get ( h ).getId ();
                                    break;
                                }
                            }
                            if (itemOptionId== disItems.getOptionId () && disItems.getProductId ()== orderItem.getProductId ()){
                                holder.priceTv.setText(String.format("%s %s", currencySign, Util.formatPrice ( orderItemPrice)));
                                holder.priceTv.setBackgroundResource ( R.drawable.cross_line );
                                holder.afterPriceTv.setText ( String.format("%s %s", currencySign,   Util.formatPrice (disItems.getAfterDis ())) );
                            }else {
                                holder.priceTv.setText(String.format("%s %s",currencySign,   Util.formatPrice (orderItemPrice)));
                            }
                        }
                    }else if (couponDis.getOption ().equalsIgnoreCase ( "product_group" )){
                        //product group statement
                        if (disItems.getProductId ()== orderItem.getProductId () && disItems.getAfterDis ()!=0){
                            holder.priceTv.setText(String.format("%s %s", currencySign,   Util.formatPrice (orderItemPrice)));
                            holder.priceTv.setBackgroundResource ( R.drawable.cross_line );
                            holder.afterPriceTv.setText ( String.format("%s %s", currencySign,   Util.formatPrice (disItems.getAfterDis ())) );
                        }else {
                            holder.priceTv.setText(String.format("%s %s", currencySign,   Util.formatPrice (orderItemPrice)));
                        }

                    }else if (couponDis.getOption ().equalsIgnoreCase ( "subtotal" )){
                        // subtotal statment

                        holder.priceTv.setText(String.format("%s %s", currencySign,   Util.formatPrice (orderItemPrice)));
                        holder.priceTv.setBackgroundResource ( 0 );
                        holder.afterPriceTv.setText(" ");
                    }
                }
            }else {
                if (orderItem.getAfterPrice ()!=0){
                    holder.priceTv.setText(String.format("%s %s", currencySign,   Util.formatPrice (orderItemPrice)));
                    holder.priceTv.setBackgroundResource ( R.drawable.cross_line );
                    holder.afterPriceTv.setText ( String.format("%s %s", currencySign,   Util.formatPrice (orderItem.getAfterPrice ())) );
                }else{
                    holder.priceTv.setText(String.format("%s %s", currencySign,   Util.formatPrice (orderItemPrice)));
                }
            }

            holder.subView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = orderItem.getQty();
                    final Activity m = (Activity) view.getContext();
                    //qty is the last one and user want to remove
                    if (count == 1 && holder.subView.isPressed()) {
                        //when qyt == 1 and user click -
                        AlertDialog.Builder builder = new AlertDialog.Builder(m, R.style.DialogTheme);
                        builder.setMessage(R.string.are_you_sure_to_remove);
                        builder.setNegativeButton(m.getString(R.string.cancel), null);//when user click cancel nothing remove
                        builder.setPositiveButton(m.getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //remove that items
                                count--;
                                //if the orderitem is the last i tems
                                //when user clcik ok mean qty =0
                                //resfresh the adapter

                                if (count == 0) {
                                    orderItemList.remove(position);
                                    if (mOnItemEditedListener != null) {
                                        mOnItemEditedListener.onOrderItemEditQty(orderItem, count,position,true);
                                    }
                                    notifyDataSetChanged();
                                }

                                if (orderItemList.size() == 0 || orderItem.getQty()==0 ) {
                                    //go to screen shop detail
                                    mOnItemEditedListener.onLastItemRemove(true);
                                    MockupData.setCurrentOrderDetail(null);
                                    notifyDataSetChanged();
                                    m.finish();
                                }
                            }
                        });
                        builder.setCancelable(false);
                        builder.show();
                    } else { // user have qty many and want to remove
                        count--;
                        orderItem.setQty(count);
                        holder.numberTv.setText(String.format(Locale.US, "%d", count));
                        if (mOnItemEditedListener != null)
                            mOnItemEditedListener.onOrderItemEditQty(orderItem, count,position,true);
                    }
                }
            });

            holder.addView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    count = orderItem.getQty();
                    count++;
                    holder.numberTv.setText(String.format(Locale.US, "%d", count));
                    //same option same add on

                    orderItem.setQty(count);
                    if (mOnItemEditedListener != null)
                        mOnItemEditedListener.onOrderItemEditQty(orderItem, count,position,true);
                        }
                    });
            //click hold item this for check out screen only
            holder.editItem_btn.setOnClickListener ( new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    mOnItemEditedListener.onOrderItemEditQty ( orderItem,orderItem.getQty (),position,false );
                    holder.swipe_layout.close ();
                }
            } );
            holder.buttonDeleteItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Activity m = (Activity) v.getContext();
                    AlertDialog.Builder builder = new AlertDialog.Builder(m, R.style.DialogTheme);
                    builder.setMessage(m.getString(R.string.do_you_want_to_remove));
                    builder.setNegativeButton(m.getString(R.string.cancel), null);//when user click cancel nothing remove
                    builder.setPositiveButton(m.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                                count=0;
                                orderItemList.remove(orderItem);
                                notifyDataSetChanged();
                            if (mOnItemEditedListener != null) {
                                    mOnItemEditedListener.onOrderItemEditQty(orderItem, count,position,true);
                                }
                                notifyDataSetChanged();
                            if (orderItemList.size() == 0 || orderItem.getQty()==0 ) {
                                //go to screen shop detail
                                mOnItemEditedListener.onLastItemRemove(true);
                                MockupData.setCurrentOrderDetail(null);
                                notifyDataSetChanged();
                                m.finish();
                            }
                        }
                    });
                    builder.setCancelable(false);
                    builder.show();
                }
            });

            holder.swipe_layout.addSwipeListener ( new SwipeLayout.SwipeListener () {
                @Override
                public void onStartOpen(SwipeLayout layout) {

                }

                @Override
                public void onOpen(SwipeLayout layout) {

                }

                @Override
                public void onStartClose(SwipeLayout layout) {

                }

                @Override
                public void onClose(SwipeLayout layout) {

                }

                @Override
                public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

                }

                @Override
                public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                    //set close to swipe for 2.5 second after user release click
                    new CountDownTimer(2500, 2500) {

                        public void onTick(long millisUntilFinished) {
                        }

                        public void onFinish() {
                            holder.swipe_layout.close ();
                        }

                    }.start();

                }
            } );

            if (!isEditable && isView == 1) {
                // this for view order
                //set default image if no image
//              Glide.with ( holder.itemView.getContext () ).load ( orderItem.getOrderImageUrl () ).apply ( new RequestOptions ().error (R.drawable.no_product_image ).transforms (  new RoundedCorners ( 8 ) ).placeholder ( R.drawable.no_product_image ) ).into ( holder.itemImage );
                Glide.with ( holder.itemView.getContext () ).load ( orderItem.getOrderImageUrl () ).apply ( new RequestOptions ().transforms (  new RoundedCorners ( 8 ) ) ).into ( holder.itemImage );
                holder.subView.setVisibility(View.GONE);
                holder.addView.setVisibility(View.GONE);
                holder.swipe_layout.setSwipeEnabled ( false );
                holder.linear_item.setBackgroundColor(Color.parseColor("#FFFFFF"));
            } else if (!isEditable && isView == 0) {
                // this for process the order
                holder.subView.setVisibility(View.GONE);
                holder.addView.setVisibility(View.GONE);
                holder.swipe_layout.setSwipeEnabled ( false );
                holder.linear_item.setBackgroundColor(Color.parseColor("#FFFFFF"));
            }

        }
    }

    @Override
    public int getItemCount() {
        return orderItemList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        View subView;
        View addView;
        TextView numberTv, priceTv, optionTv, addOnTv, itemNameTv,specialInstructionTv,afterPriceTv;
        ImageView itemImage;
        SwipeLayout swipe_layout;
        LinearLayout buttonDeleteItem ,editItem_btn,linear_item;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            subView = itemView.findViewById(R.id.sub);
            addView = itemView.findViewById(R.id.add);
            numberTv = itemView.findViewById(R.id.number);
            priceTv = itemView.findViewById(R.id.priceTv);
            optionTv = itemView.findViewById(R.id.options);
            addOnTv = itemView.findViewById(R.id.addons);
            itemNameTv = itemView.findViewById(R.id.name);
            buttonDeleteItem = itemView.findViewById(R.id.buttonDeleteItem);
            itemImage= itemView.findViewById ( R.id.product_image );
            specialInstructionTv =itemView.findViewById ( R.id.special_instruction );
            afterPriceTv = itemView.findViewById ( R.id.afterPriceTv );
            editItem_btn = itemView.findViewById ( R.id.editItem_btn );
            linear_item = itemView.findViewById(R.id.linear_item);
            swipe_layout = itemView.findViewById ( R.id.swipe_layout );

        }
    }

    public interface onItemEditedListener {
        void onOrderItemEditQty(OrderItem orderItem,int count ,int position,boolean isEditQty);
        void onLastItemRemove(boolean lastItemRemove);

    }
    public static List<OrderItem> getOrderItemList() {
        return orderItemList;
    }
}
