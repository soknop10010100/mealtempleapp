package com.station29.mealtemple.ui.payment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.PaymentHistory;
import com.station29.mealtemple.api.model.PaymentService;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Redirect;
import com.station29.mealtemple.util.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class OrangePayAlertDialog extends Dialog {

    private PaymentService paymentService;
    private EditText phoneNumber,amount;
    private MaterialButton submitBtn,notNowBtn;
    private Spinner spinner;
    private List<Wallets> walletsList;
    private Context mContext;
    private final Activity mActivity;
    private OnPaymentCallback onPaymentCallback;
    private CheckBox saveWallet;
    private RelativeLayout checkBox;

    public OrangePayAlertDialog(@NonNull Context context , Activity activity , PaymentService paymentService , List<Wallets> walletsList, OnPaymentCallback onPaymentCallback) {
        super(context);
        this.paymentService = paymentService;
        this.mContext = context;
        this.mActivity = activity;
        this.walletsList = walletsList;
        this.onPaymentCallback = onPaymentCallback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_dialog_topup_orange);

        initView();
        initAction();
    }

    private void initView(){
        spinner = findViewById(R.id.listWallet);
        phoneNumber = findViewById(R.id.phone_number);
        amount = findViewById(R.id.amount);
        submitBtn = findViewById(R.id.submitNowBtn);
        notNowBtn= findViewById(R.id.notNowBtn);
        saveWallet = findViewById(R.id.saveWallet);
        checkBox = findViewById(R.id.checkBox);
    }

    private void initAction(){

        if(paymentService.getPhone() != null && !paymentService.getPhone().equals("")){
            phoneNumber.setText(paymentService.getPhone());
            checkBox.setVisibility(View.GONE);
        }

        if(!walletsList.isEmpty() && walletsList.size() > 1)
            walletsList.remove(0);
        List<String> serverName = new ArrayList<>();
        serverName.add(mActivity.getString(R.string.please_select_wallets));
        for (Wallets wallets : walletsList) {
            serverName.add(wallets.getCurrency());
        }
        ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, serverName);
        serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(serviceAdapter);

        notNowBtn.setOnClickListener(v -> {
            onPaymentCallback.onPaymentFinish();
            dismiss();
        });

        submitBtn.setOnClickListener(v -> {
            if(amount.getText().toString().equals(""))
                Toast.makeText(mActivity, mActivity.getString(R.string.please_enter_amount),Toast.LENGTH_SHORT).show();
            else if (phoneNumber.getText().toString().equals(""))
                Toast.makeText(mActivity,mActivity.getString(R.string.please_input_your_phone_email),Toast.LENGTH_SHORT).show();
            else onPaymentCallback.onPaymentStart(getAllParam());
            dismiss();
        });
    }

    private HashMap<String ,Object> getAllParam(){
        HashMap<String, Object> topUpParam = new HashMap<>();
        topUpParam.put("psp_id", paymentService.getId());
        topUpParam.put("phone", phoneNumber.getText().toString());
        topUpParam.put("amount", amount.getText().toString());
        if(saveWallet.isChecked())
            topUpParam.put("is_save",1);
        else
            topUpParam.put("is_save",0);
        if(paymentService.getPhone() != null && !paymentService.getPhone().equals("")){
            topUpParam.put("is_save",0);
        }
        topUpParam.put("wallet_id",walletsList.get(0).getWalletId());
        if(BuildConfig.DEBUG)
            topUpParam.put("code","cm");
        else topUpParam.put("code", BaseActivity.countryCode);
        return topUpParam;
    }

    public interface OnPaymentCallback{
         void onPaymentStart(HashMap<String ,Object> param);
         void onPaymentFinish();
    }
}
