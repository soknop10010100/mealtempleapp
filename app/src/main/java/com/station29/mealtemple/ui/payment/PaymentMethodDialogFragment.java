package com.station29.mealtemple.ui.payment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.PaymentHistory;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.ui.payment.adapter.PaymentHistoryAdapter;
import com.station29.mealtemple.util.Util;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PaymentMethodDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private PaymentClickListener mPaymentClickListener;
    private List<Wallets> walletsList = new ArrayList<>();
    public static int notePosition  =-1;
    private PaymentItemAdapter paymentItemAdapter;
    private PaymentHistoryAdapter paymentHistoryAdapter;
    private List<PaymentHistory> paymentHistories;
    private boolean checkShipping = false;

    public static PaymentMethodDialogFragment newInstance(List<Wallets> walletsList,List<PaymentHistory> paymentHistories,boolean checkShipping) {
        final PaymentMethodDialogFragment fragment = new PaymentMethodDialogFragment();
        final Bundle args = new Bundle();
        args.putSerializable("walletList", (Serializable) walletsList);
        args.putSerializable("paymentHistory", (Serializable) paymentHistories);
        args.putBoolean("checkShipping",checkShipping);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_item_list_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NotNull View view, @Nullable Bundle savedInstanceState) {

        RecyclerView recyclerView = view.findViewById(R.id.list);
        RecyclerView paymentRecyclerView = view.findViewById(R.id.listPaymentMethod);
        LinearLayout paymentHistoryScreen = view.findViewById(R.id.payment_history);

        if (getArguments () != null) {
            walletsList = (List<Wallets>) getArguments().getSerializable("walletList");
            paymentHistories = (List<PaymentHistory>) getArguments().getSerializable("paymentHistory");
            checkShipping = getArguments().getBoolean("checkShipping");
            if(paymentHistories.isEmpty())
                paymentHistoryScreen.setVisibility(View.GONE);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        paymentRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        paymentItemAdapter = new PaymentItemAdapter(walletsList,checkShipping);
        recyclerView.setAdapter(paymentItemAdapter);
        paymentItemAdapter.notifyDataSetChanged();

        paymentHistoryAdapter = new PaymentHistoryAdapter(paymentHistories ,"order",paymentHistory);
        paymentRecyclerView.setAdapter(paymentHistoryAdapter);

        view.findViewById(R.id.closeButton).setOnClickListener(this);
        view.findViewById(R.id.btnTopUp).setOnClickListener(this);
        view.findViewById(R.id.btnAddPayment).setOnClickListener(this);
    }

    private final PaymentHistoryAdapter.OnClickOnPaymentHistory paymentHistory = new PaymentHistoryAdapter.OnClickOnPaymentHistory() {
        @Override
        public void onClickOnPayment(PaymentHistory paymentHistory) {
            notePosition = -2;
            mPaymentClickListener.onaPayByPaymentHistory(paymentHistory);
            paymentHistoryAdapter.notifyDataSetChanged();
            dismiss();
        }

        @Override
        public void onClickOnRemove(int paymentId, int position) {

        }
    };

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            mPaymentClickListener = (PaymentClickListener) parent;
        } else {
            mPaymentClickListener = (PaymentClickListener) context;
        }
    }

    @Override
    public void onDetach() {
        mPaymentClickListener = null;
        super.onDetach();
    }

    public interface PaymentClickListener {
        void onPayByWallet( String typePay, int walletId);
        void onaPayByPaymentHistory(PaymentHistory paymentHistory);
        void onOpenPaymentMethod();
    }

    private static class ViewHolder extends RecyclerView.ViewHolder {

        final TextView text;
        final ImageView imgIcon,imgCheck;
        final RadioButton radioButton;

        ViewHolder(LayoutInflater inflater, final ViewGroup parent) {
            // TODO: Customize the item layout
            super(inflater.inflate(R.layout.fragment_item_list_dialog_item, parent, false));
            text = itemView.findViewById(R.id.text);
            imgIcon = itemView.findViewById(R.id.icon);
            imgCheck = itemView.findViewById(R.id.check);
            radioButton = itemView.findViewById(R.id.radioBtn);
        }
    }

    private class PaymentItemAdapter extends RecyclerView.Adapter<ViewHolder> {

        private final List<Wallets> walletsList;
        private boolean checkShipping = false;

        PaymentItemAdapter(List<Wallets> walletsList,boolean checkShipping) {
            this.walletsList = walletsList;
            this.checkShipping = checkShipping;
        }

        @NotNull
        @Override
        public ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()), parent);
        }

        @SuppressLint("ResourceAsColor")
        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final Wallets wallets = walletsList.get(position);
            if(wallets != null) {

                if(position == 0 && !Util.checkIsHaveCash() && !checkShipping ){
                    holder.text.setText(wallets.getBalance());
                    holder.imgIcon.setBackgroundResource(R.drawable.icons8_cash_50);
                    holder.imgIcon.setBackgroundTintList(ColorStateList.valueOf(R.color.colorPrimary));
                } else {
                    holder.text.setText(String.format("%s :%s %s",getString(R.string.wallet),wallets.getCurrency(),wallets.getBalance()));
                    holder.imgIcon.setBackgroundResource(R.drawable.ic_account_balance_wallet_black_24dp);
                    holder.imgIcon.setBackgroundTintList(ColorStateList.valueOf(R.color.colorPrimary));
                }
                if (notePosition == position) holder.radioButton.setChecked(true);
                else holder.radioButton.setChecked(false);
                holder.itemView.setOnClickListener(v -> {
                    if (mPaymentClickListener != null) {
                        PaymentHistoryAdapter.noteHistoryPostion = -1;
                        notePosition = position;
                        mPaymentClickListener.onPayByWallet(holder.text.getText().toString(),wallets.getWalletId());
                        paymentItemAdapter.notifyDataSetChanged();
                        dismiss();
                    }
                });
                holder.radioButton.setOnClickListener(v -> {
                    if (mPaymentClickListener != null) {
                        PaymentHistoryAdapter.noteHistoryPostion = -1;
                        notePosition = position;
                        mPaymentClickListener.onPayByWallet(holder.text.getText().toString(),wallets.getWalletId());
                        paymentItemAdapter.notifyDataSetChanged();
                        dismiss();
                    }
                });
            }
        }
        @Override
        public int getItemCount() {
            return walletsList.size();
        }

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.closeButton) {
            paymentItemAdapter.notifyDataSetChanged();
            dismiss();
        } else if (view.getId() == R.id.btnAddPayment){
            paymentHistoryAdapter.notifyDataSetChanged();
            mPaymentClickListener.onOpenPaymentMethod();
            dismiss();
        }

    }

}
