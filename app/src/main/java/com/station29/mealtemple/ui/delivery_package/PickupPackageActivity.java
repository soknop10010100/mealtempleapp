package com.station29.mealtemple.ui.delivery_package;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.MapsActivity;

import java.util.StringTokenizer;

public class PickupPackageActivity extends BaseActivity {

    private TextInputEditText phoneReceiver,userName;
    private EditText addressDetail;
    private  TextView pickUpTv ;
    private final int REQUEST_ADDRESS_PICK = 5679;
    private String deliveryAddress,code;
    double late,lng;
    private CountryCodePicker ccp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup_package);
        TextView titleTv = findViewById(R.id.title);
        ImageView backBtn = findViewById(R.id.backButton);
        titleTv.setText(R.string.pick_up_package);
        Button btnAdd = findViewById(R.id.btnAdd);
        phoneReceiver = findViewById(R.id.txtReceiverPhones);
        userName = findViewById(R.id.user_name);
        addressDetail = findViewById(R.id.add_detail_pick);
        pickUpTv = findViewById(R.id.deliveryPickUp);
        ccp = findViewById(R.id.ccp);
        ccp.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);

        pickUpTv.setText(MockupData.DELIVERY_ADDRESS);

        if (getIntent() != null && getIntent().hasExtra("name_sender")){
            userName.setText(getIntent().getStringExtra("name_sender"));
            String phoneText = getIntent().getStringExtra("name_phone");
            userName.setText(MockupData.USER_NAME);
//            StringTokenizer defaultTokenizer = new StringTokenizer(phoneText);
//            countryCode = defaultTokenizer.nextToken();
//            ccp.setCountryForPhoneCode(Integer.parseInt(countryCode));
            phoneText = phoneText.replace("+855", "".trim());
            phoneReceiver.setText(phoneText);
        } else {
            String phoneText = MockupData.LOGIN_PHONE;
            userName.setText(MockupData.USER_NAME);
            StringTokenizer defaultTokenizer = new StringTokenizer(phoneText);
            countryCode = defaultTokenizer.nextToken();
            ccp.setCountryForPhoneCode(Integer.parseInt(countryCode));
            phoneText = phoneText.replace(countryCode, "".trim());
            phoneText = phoneText.replaceAll("\\s+", "");
            phoneReceiver.setText(phoneText);
        }
        if(getIntent().hasExtra("detail_pick")){
            String detail = getIntent().getStringExtra("detail_pick") ;
            addressDetail.setText(detail);
        }
        if(getIntent().hasExtra("late")){
            late = getIntent().getDoubleExtra("late",-1);
        }
        if(getIntent().hasExtra("lng")){
            lng = getIntent().getDoubleExtra("lng",-1);
        }

        pickUpTv.setOnClickListener(v -> {
            Intent intent = new Intent(PickupPackageActivity.this, MapsActivity.class);
            startActivityForResult(intent,REQUEST_ADDRESS_PICK);
        });

        backBtn.setOnClickListener(v -> onBackPressed());

        btnAdd.setOnClickListener(v -> {
            if(userName.getText().toString().isEmpty()){
                Toast.makeText(PickupPackageActivity.this,getString( R.string.please_input_your_name),Toast.LENGTH_LONG).show();
            } else if(phoneReceiver.getText().toString().isEmpty()){
                Toast.makeText(PickupPackageActivity.this, getString(R.string.please_input_sender_phone),Toast.LENGTH_LONG).show();
            } else {
                Intent intent = new Intent();
                intent.putExtra("addressPickUp", pickUpTv.getText().toString());
                intent.putExtra("senderName",userName.getText().toString());
                intent.putExtra("senderPhone",ccp.getDefaultCountryCodeWithPlus()+formatPhone(phoneReceiver.getText().toString()));
                intent.putExtra("detail_pic",addressDetail.getText().toString());
                intent.putExtra("lat_pick",late);
                intent.putExtra("lng_pick",lng);
                setResult ( RESULT_OK, intent );
                finish();
            }

        });

    }
    private String formatPhone(String fullPhoneNumber) {
        String code;
        if (fullPhoneNumber.length() >= 8) {
            if (phoneReceiver.getText().toString().equals("") || phoneReceiver.getText().toString().isEmpty())
                return fullPhoneNumber;
            code = fullPhoneNumber.substring(0, ccp.getFullNumber().length());
            fullPhoneNumber = (code.contentEquals(ccp.getFullNumber()) ? fullPhoneNumber.substring(ccp.getFullNumber().length()) : fullPhoneNumber);
            if(fullPhoneNumber.length() < 8){
                return phoneReceiver.getText().toString();
            }
        } else {//855
            //return old phone number
            return fullPhoneNumber;
        }
        return fullPhoneNumber;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null && resultCode == RESULT_OK){
            if (requestCode == REQUEST_ADDRESS_PICK ){
                if(data.hasExtra("delivery_address")){
                   deliveryAddress = data.getStringExtra("delivery_address");
                    pickUpTv.setText(deliveryAddress);
                }
                if(data.hasExtra("late")){
                    late = data.getDoubleExtra("late",-1);
                }
                if(data.hasExtra("lng")){
                    lng = data.getDoubleExtra("lng",-1);
                }
            }

        }
    }
}