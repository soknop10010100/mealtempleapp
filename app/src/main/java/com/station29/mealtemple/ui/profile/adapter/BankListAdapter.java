package com.station29.mealtemple.ui.profile.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.Bank;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class BankListAdapter extends RecyclerView.Adapter<BankListAdapter.MyViewHolder> {

    private List<Bank> bankArrayList;
    private BankListItemClicklistener bankListItemClicklistener;

    public BankListAdapter(List<Bank> bankArrayList, BankListItemClicklistener bankListItemClicklistener) {
        this.bankArrayList = bankArrayList;
        this.bankListItemClicklistener = bankListItemClicklistener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_bank_list_item, parent, false);
        return new MyViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        final Bank bank = bankArrayList.get(position);
        holder.button.setText(bank.getName());
        Glide.with(holder.button.getContext())
                .load(Constant.kessStoreUrl +bank.getLogo())
                .into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                holder.button.setCompoundDrawablesWithIntrinsicBounds(resource,null,null,null);
            }
        });
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bankListItemClicklistener.onBanklistItemClick(holder.button,bank);
            }
        });
    }

    @Override
    public int getItemCount() {
        return bankArrayList.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {

        Button button;
        View row;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            button = itemView.findViewById(R.id.bank_item);
            row = itemView;
        }
    }
    public interface BankListItemClicklistener{
        void onBanklistItemClick(View itemView,Bank bank);
    }
}
