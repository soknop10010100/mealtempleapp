package com.station29.mealtemple.ui.taxi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.Order;
import com.station29.mealtemple.api.model.SendPackageDetail;
import com.station29.mealtemple.api.model.Services;
import com.station29.mealtemple.api.model.Waypoint;
import com.station29.mealtemple.api.model.response.OrderResponseDetail;
import com.station29.mealtemple.api.model.response.ResponseOrderDetailTaxi;
import com.station29.mealtemple.api.request.OrderWs;
import com.station29.mealtemple.api.request.SendPackageWs;
import com.station29.mealtemple.api.request.TaxiWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.delivery_package.DeliveryPackageActivity;
import com.station29.mealtemple.ui.order.OrderDetailActivity;
import com.station29.mealtemple.util.Util;

import java.util.HashMap;
import java.util.List;

public class FindDriverActivity extends BaseActivity {

    private int orderId;
    private TextView findingDriver;
    private HashMap<String,Object> orderTaxi;
    private CountDownTimer countDownTimer;
    String pick="",drop="";
    String send = "",phoneCode,typeOrder="",description = "";
    double latPick,lngPick,latDrop,lngDrop;
    HashMap<String,Object> resendPackage = new HashMap<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_driver);

        findingDriver = findViewById(R.id.txtfindDriver);


        if(getIntent()!=null && getIntent().hasExtra("orderId")){
            String stringOrderId =  getIntent().getStringExtra("orderId");
            assert stringOrderId != null;
            orderId = Integer.parseInt(stringOrderId);
            if(getIntent().hasExtra("orderTaxi")){
                orderTaxi = (HashMap<String, Object>) getIntent().getSerializableExtra("orderTaxi");
            }
            if(getIntent().hasExtra("type_order")){
                typeOrder = getIntent().getStringExtra("type_order");
            }

        }else if(getIntent()!=null&&getIntent().hasExtra("order_Id")){
           int id = getIntent().getIntExtra("order_Id",-1);
           orderId = id;
            if(getIntent().hasExtra("send_package")){
                 send = getIntent().getStringExtra("send_package");
            }
            if(getIntent().hasExtra("add_pick")){
                pick = getIntent().getStringExtra("add_pick");
            }
            if(getIntent().hasExtra("add_drop")){
                drop = getIntent().getStringExtra("add_drop");
            }
            if(getIntent().hasExtra("latePick")){
                latPick = getIntent().getDoubleExtra("latePick",-1);
            }
            if(getIntent().hasExtra("lngPick")){
                lngPick = getIntent().getDoubleExtra("lngPick",-1);
            }
            if(getIntent().hasExtra("latDrop")){
                latDrop = getIntent().getDoubleExtra("latDrop",-1);
            }
            if(getIntent().hasExtra("lngDrop")){
                lngDrop = getIntent().getDoubleExtra("lngDrop",-1);
            }
            if(getIntent().hasExtra("phone_code")){
                phoneCode = getIntent().getStringExtra("phone_code");
            }
            if(getIntent().hasExtra("type_order")){
                typeOrder = getIntent().getStringExtra("type_order");
            }
            if(getIntent().hasExtra("description")){
                description = getIntent().getStringExtra("description");
            }

            if(getIntent().hasExtra("param"))
                resendPackage = (HashMap<String, Object>) getIntent().getSerializableExtra("param");
        }
        countTime();

        findViewById(R.id.btnCancelTaxi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                },100);
                popupMessageCancle(getString(R.string.order),FindDriverActivity.this);
            }
        });

    }
    private  void popupMessageCancle(String title, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(R.string.are_you_sure_to_cancel);
        builder.setNegativeButton ( getResources ().getString ( R.string.cancel ) ,null);
        builder.setPositiveButton(getResources ().getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(Constant.getBaseUrl()!=null)
                    cancelDriver();
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        popupMessageCancle(getString(R.string.order),FindDriverActivity.this);
    }

    private void countTime(){
            countDownTimer = new CountDownTimer(120000, 1000) {
            @SuppressLint("SetTextI18n")
            public void onTick(long millisUntilFinished) {
                findingDriver.setText(getString(R.string.find_driver_in) +" "+ millisUntilFinished / 1000 + " s");
            }
            public void onFinish() {
                checkOrderStatus(orderId);
            }

        }.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(countDownTimer !=null){
            countDownTimer.cancel();
        }
    }

    private  void popupMessageFindDriver(ResponseOrderDetailTaxi detailTaxi, Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.DialogTheme);
      //  if (title != null) builder.setTitle(title);
        builder.setMessage(R.string.do_you_want_to_book_again);
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelDriver();
            }
        });
        builder.setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(detailTaxi.getTypeOrder().equalsIgnoreCase("service")){
                    orderTaxi.put("order_id",String.valueOf(orderId));
                    orderTaxi.put("country_code",BaseActivity.countryCode);
                    new TaxiWs().orderDriverService(FindDriverActivity.this,orderTaxi, new TaxiWs.getDriverCallback() {
                        @Override
                        public void onLoadSuccess(List<Services> servicesList, Waypoint waypoint) {
                        }

                        @Override
                        public void onLoadSuccessWithoutDrop(List<Services> servicesList) {

                        }

                        @Override
                        public void onLoadFailed(String errorMessage,int code) {
                            Util.popupMessage(getString(R.string.error),errorMessage,FindDriverActivity.this);
                        }

                        @Override
                        public void onOrder(String id) {
                            countTime();
                        }
                    });
                } else {
                    resendPackage.put("order_id",orderId);
                    new SendPackageWs().resendPackage(FindDriverActivity.this,resendPackage, new SendPackageWs.loadSendPackage() {
                        @Override
                        public void onLoadSuccessfully(int orderId) {
                            countTime();
                        }

                        @Override
                        public void onLoadGetDetailSuccess(SendPackageDetail detail) {

                        }

                        @Override
                        public void onLoadingFail(String message,int code) {
                            Util.popupMessage(getString(R.string.error),message,FindDriverActivity.this);
                        }
                    });
                }


            }
        });
        builder.show();
    }

    private void cancelDriver(){
        new TaxiWs().cancelTaxi(FindDriverActivity.this, orderId , new TaxiWs.getDriverCallback() {
            @Override
            public void onLoadSuccess(List<Services> servicesList, Waypoint waypoint) {

            }

            @Override
            public void onLoadSuccessWithoutDrop(List<Services> servicesList) {

            }

            @Override
            public void onLoadFailed(String errorMessage,int code) {
                Util.popupMessage(null,errorMessage,FindDriverActivity.this);
            }

            @Override
            public void onOrder(String orderId) {
                if(send.equals("send")){
                    Intent intent = new Intent(FindDriverActivity.this,DeliveryPackageActivity.class);
                    intent.putExtra("orderId",orderId);
                    intent.putExtra("pick",pick);
                    intent.putExtra("drop",drop);
                    intent.putExtra("lat_p",latPick);
                    intent.putExtra("lng_p",lngPick);
                    intent.putExtra("lat_d",latDrop);
                    intent.putExtra("lng_d",lngDrop);
                    intent.putExtra("phoneCode",phoneCode);
                    intent.putExtra("description",description);
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(FindDriverActivity.this, TaxiMapActivity.class);
                    intent.putExtra("dropLatLng",orderTaxi);
                   // Toast.makeText(getBaseContext(),orderId,Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }

            }
        });
    }

    private void checkOrderStatus(int orderId){
        new OrderWs().readOrderDetail(orderId, FindDriverActivity.this,"service", new OrderWs.GetOrderListener() {
            @Override
            public void onLoadOrderSuccess(List<Order> orderList) {

            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(FindDriverActivity.this,errorMessage,Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onLoadDetailSuccess(OrderResponseDetail detail) {
            }

            @Override
            public void onLoadDetailTaxiSuccess(final ResponseOrderDetailTaxi detailTaxi) {
                if (detailTaxi.getDeliveryStatus().equals("ACCEPTED") || detailTaxi.getDeliveryStatus().equals("send")) {
                    Intent intent1 = new Intent(getBaseContext(), OrderDetailActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent1.putExtra("status", detailTaxi.getDeliveryStatus());
                    intent1.putExtra("typeOrder", detailTaxi.getTypeOrder());
                    intent1.putExtra("orderId", String.valueOf(detailTaxi.getOrderId()));
                    startActivity(intent1);
                    finish();
                } else {
                    popupMessageFindDriver(detailTaxi , FindDriverActivity.this);
                }
            }
        });
    }
}