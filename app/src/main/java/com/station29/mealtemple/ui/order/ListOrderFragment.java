package com.station29.mealtemple.ui.order;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.Order;
import com.station29.mealtemple.api.model.response.OrderResponseDetail;
import com.station29.mealtemple.api.model.response.ResponseOrderDetailTaxi;
import com.station29.mealtemple.api.request.OrderWs;
import com.station29.mealtemple.ui.BaseFragment;
import com.station29.mealtemple.ui.order.adpater.ListOrderAdapter;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.station29.mealtemple.util.Util.isConnectedToInternet;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListOrderFragment extends BaseFragment {

    private String status ;
    private ListOrderAdapter adapter;
    private List<Order> mOrderList = new ArrayList<>();
    private TextView emptyText;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private static final int REQUEST_REFRESH_ORDER= 976;
    private int statusPosition;
    private String listStatuses;
    private Boolean isScrolling = true;
    private int currentItem, total , scrollDown, currentPage = 1 ,size = 10 ,sizePage =10;

    static ListOrderFragment newInstance(String status,int statusPosition) {
        Bundle args = new Bundle();
        args.putString("status", status);
        args.putInt("statusPosition",statusPosition);
        ListOrderFragment fragment = new ListOrderFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View contentView = inflater.inflate(R.layout.fragment_list_order, container, false);
        final RecyclerView recyclerView = contentView.findViewById(R.id.list_order);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager ( mActivity );
        recyclerView.setLayoutManager(linearLayoutManager);
        emptyText = contentView.findViewById(R.id.empty_text);
        progressBar = contentView.findViewById(R.id.progressBar);
        swipeRefreshLayout = contentView.findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                OrderFragment.storesPager.getAdapter().notifyDataSetChanged();
            }
        });

        if (getArguments()!= null && getArguments().containsKey("status")) {
            status = (String) getArguments().getSerializable("status");
            statusPosition = getArguments().getInt("statusPosition");
            if(statusPosition == 0){
                listStatuses = "NEW,PENDING,PREPARING,DELIVERING";
            }else if(statusPosition == 1){
                listStatuses = "COMPLETE";
            }else if(statusPosition == 2){
                listStatuses = "REJECT,CANCELLED,FAILED";
            }
            if(mOrderList.size()!=0) {
                adapter.clear();
                currentPage = 1;
                size =10;
            }
        }
        final String userToken = Util.getString("token", mActivity);

        if (!userToken.isEmpty()&&status!=null) {
            loadOrderTask(recyclerView);

        }else {
            emptyText.setVisibility(View.VISIBLE);
        }

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(mOrderList.size() - 1);
                            if(!userToken.isEmpty())
                            loadOrderTask(recyclerView);
                            size+=10;
                        }
                    }
                }

            }
        });


        return contentView;
    }

    private ListOrderAdapter.OrderItemClickListener itemClickListener = new ListOrderAdapter.OrderItemClickListener() {
        @Override
        public void onOrderItemClick(Order order) {
            Intent intent = new Intent(mActivity, OrderDetailActivity.class);
            intent.putExtra("order", order);
            intent.putExtra("status", order.getStatus());
            startActivityForResult(intent,REQUEST_REFRESH_ORDER);
        }

    };

    private void loadOrderTask(RecyclerView recyclerView) {
        new OrderWs().listOrder(statusPosition,mActivity,listStatuses,currentPage,sizePage,getOrderListener);
        adapter = new ListOrderAdapter(mOrderList, itemClickListener);
        recyclerView.setAdapter(adapter);
    }

    private OrderWs.GetOrderListener getOrderListener = new OrderWs.GetOrderListener () {
        @Override
        public void onLoadOrderSuccess(List<Order> orderList) {
            if(orderList.size()!=0){
                emptyText.setVisibility(View.GONE);
            }
            mOrderList.addAll(orderList);
            if (mOrderList.isEmpty()) {
                emptyText.setVisibility(View.VISIBLE);
            }
            progressBar.setVisibility ( View.GONE );
            swipeRefreshLayout.setRefreshing(false);
            adapter.notifyDataSetChanged();
        }

        @Override
        public void onError(String errorMessage) {
            if (isConnectedToInternet ( mActivity ))   {
//                popupMessage(null, errorMessage, mActivity);
                Toast.makeText(mActivity,errorMessage,Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onLoadDetailSuccess(OrderResponseDetail detail) {

        }

        @Override
        public void onLoadDetailTaxiSuccess(ResponseOrderDetailTaxi detailTaxi) {

        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_REFRESH_ORDER && resultCode == RESULT_OK) {
            OrderFragment.storesPager.getAdapter().notifyDataSetChanged();
        }
    }
}
