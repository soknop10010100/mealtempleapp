package com.station29.mealtemple.ui.order;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.response.Config;
import com.station29.mealtemple.api.request.ConfigWs;
import com.station29.mealtemple.api.request.ServerUrlWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.BaseFragment;
import com.station29.mealtemple.ui.home.adpater.MenuStoreFragmentsPagerAdapter;
import com.station29.mealtemple.util.Util;

import java.util.List;

import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class OrderFragment extends BaseFragment {

    public static ViewPager storesPager;
    MenuStoreFragmentsPagerAdapter storesPagerAdapter;
    ProgressBar progressBar;
    TextView txtNolocation;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_order, container, false);

        // Build body
        TabLayout tabLayout = root.findViewById(R.id.tablayout);
        storesPager = root.findViewById(R.id.view_pager);
        progressBar =root.findViewById(R.id.progressBar);
        txtNolocation = root.findViewById(R.id.txtnoLocation);
        storesPagerAdapter  = new MenuStoreFragmentsPagerAdapter(getChildFragmentManager());
        if (!isConnectedToInternet ( mActivity )){//check internet connection
            popupMessage ( null, getResources ().getString ( R.string.failed_internet ),mActivity);
        }
        if (Constant.getBaseUrl () ==null){
            getUrls();
        }else {
            if (Constant.getConfig()!=null){
                progressBar.setVisibility(View.GONE);
                txtNolocation.setVisibility(View.GONE);
                storesPager.setVisibility(View.VISIBLE);
                List<String> tapItems = MockupData.getOrderTab(mActivity);
                for(int i = 0 ; i < tapItems.size() ; i++){
                    storesPagerAdapter.addFragment(ListOrderFragment.newInstance(tapItems.get(i),i), tapItems.get(i));
                }
                storesPager.setAdapter(storesPagerAdapter);
            }else {
               config();
            }
        }
        tabLayout.setupWithViewPager(storesPager);

        return root;

    }
    private void getUrls(){ new ServerUrlWs().readServerUrl( BaseActivity.countryCode, new ServerUrlWs.loadServerUrl () {
        @Override
        public void onLoadSuccess(boolean success) {
            if (Constant.getBaseUrl()!=null)config();

        }

        @Override
        public void onLoadFailed(String errorMessage) {
            txtNolocation.setVisibility(View.VISIBLE);
            Util.popupMessage ( null,errorMessage, mActivity );
        }
    } );
    }
    private void config(){
        new ConfigWs().readConfig(BaseActivity.countryCode, new ConfigWs.ReadConfigCallback() {
            @Override
            public void onSuccess(Config config) {
                Constant.setConfig(config);
                if (Constant.getConfig()!=null){
                    progressBar.setVisibility(View.GONE);
                    storesPager.setVisibility(View.VISIBLE);
                    List<String> tapItems = MockupData.getOrderTab(mActivity);
                    for(int i = 0 ; i < tapItems.size() ; i++){
                        storesPagerAdapter.addFragment(ListOrderFragment.newInstance(tapItems.get(i),i), tapItems.get(i));
                    }
                    storesPager.setAdapter(storesPagerAdapter);
                }
            }

            @Override
            public void onError(String message) {
                popupMessage(null, message, mActivity);
            }
        });
    }

}