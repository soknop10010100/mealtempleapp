package com.station29.mealtemple.ui.payment.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.PaymentHistory;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {

    private final List<PaymentHistory> paymentHistories;
    private final OnClickOnPaymentHistory onClickOnPaymentHistory;
    public static int noteHistoryPostion = -1;
    private final String paymentScreen;

    public PaymentHistoryAdapter(List<PaymentHistory> paymentHistories , String paymentScreen, OnClickOnPaymentHistory onClickOnPaymentHistory) {
        this.paymentHistories = paymentHistories;
        this.onClickOnPaymentHistory = onClickOnPaymentHistory;
        this.paymentScreen = paymentScreen;
    }

    @NotNull
    @Override
    public PaymentHistoryAdapter.ViewHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate( R.layout.fragment_list_payment_history, parent, false);
        return new PaymentHistoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentHistory paymentHistory = paymentHistories.get(position);
        if(paymentHistory != null){
            Glide.with(holder.pspLogo.getContext())
                    .load(paymentHistory.getLogo())
                    .into( holder.pspLogo );
            holder.paymentToken.setText(paymentHistory.getPspName() +" : "+paymentHistory.getPaymentToken());

            holder.radioButton.setChecked(position == noteHistoryPostion);

            if ("payment".equals(paymentScreen)) {
                holder.radioButton.setVisibility(View.GONE);
                holder.btnDeletePayment.setVisibility(View.VISIBLE);
            } else if ("deposit".equals(paymentScreen)){
                holder.radioButton.setVisibility(View.GONE);
                holder.btnDeletePayment.setVisibility(View.GONE);
            } else holder.btnDeletePayment.setVisibility(View.GONE);

            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noteHistoryPostion = position;
                    onClickOnPaymentHistory.onClickOnPayment(paymentHistory);
                }
            });
            holder.radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noteHistoryPostion = position;
                    onClickOnPaymentHistory.onClickOnPayment(paymentHistory);
                }
            });
            holder.btnDeletePayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickOnPaymentHistory.onClickOnRemove(paymentHistory.getId(),position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return paymentHistories.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView paymentToken;
        private final RadioButton radioButton;
        private final ImageView pspLogo;
        private final View view;
        private final ImageButton btnDeletePayment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            view = itemView;
            pspLogo = itemView.findViewById(R.id.pspLogo);
            paymentToken = itemView.findViewById(R.id.text);
            radioButton = itemView.findViewById(R.id.radioBtn);
            btnDeletePayment = itemView.findViewById(R.id.deletePaymentBtn);
        }
    }

    public interface OnClickOnPaymentHistory{
        void onClickOnPayment(PaymentHistory paymentHistory);
        void onClickOnRemove(int paymentId, int position);
    }
}
