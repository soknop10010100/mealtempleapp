package com.station29.mealtemple.ui;


import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.Fragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {

    public Activity mActivity;

    public BaseFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (mActivity == null && context instanceof BaseActivity){
            mActivity = (BaseActivity)context;
        }
    }
}
