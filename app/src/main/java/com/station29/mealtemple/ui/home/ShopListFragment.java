package com.station29.mealtemple.ui.home;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.request.StoreWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.BaseFragment;
import com.station29.mealtemple.ui.home.adpater.ShopListAdapter;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShopListFragment extends BaseFragment {

    private int mCategoryId , mTagId;
    private TextView emptyTv;
    private ProgressBar progressBar;
    private Boolean isScrolling = true;
    private int currentItem, total , scrollDown, currentPage = 1 ,size = 10 ,sizePage =10;
    private List<Shop> shopList = new ArrayList<>();
    private ShopListAdapter adapter;
    public static boolean isPullAble =true;
    private ImageView imgNoSever;
    public static ShopListFragment newInstance() {
        return new ShopListFragment();
    }

    static ShopListFragment newInstance(int categoryId) {
        Bundle args = new Bundle();
        args.putInt("categoryId", categoryId);
        ShopListFragment fragment = new ShopListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    static ShopListFragment newInstance(int categoryId, int tagId) {
        Bundle args = new Bundle ();
        args.putInt("categoryId", categoryId);
        args.putInt("tagId", tagId);
        ShopListFragment fragment = new ShopListFragment();
        fragment.setArguments ( args );
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_shop, container, false);

        emptyTv = root.findViewById(R.id.empty_text);
        progressBar = root.findViewById(R.id.progressBar);
        imgNoSever = root.findViewById(R.id.iamgeNoSever);

        final RecyclerView recyclerView = root.findViewById(R.id.list_shop);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager ( mActivity );
        recyclerView.setLayoutManager(linearLayoutManager);

        if (getArguments() != null && getArguments().containsKey("categoryId")) {
            mCategoryId = getArguments().getInt("categoryId");
            Intent data = new Intent();
            data.putExtra("categoryId",mCategoryId);
        }
        if (getArguments() != null && getArguments().containsKey("tagId")) {
            mTagId = getArguments().getInt("tagId");
            if(shopList.size()!=0) {
                adapter.clear();
                currentPage = 1;
                size =10;
            }
        }

        loadAllShop (recyclerView);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(shopList.size() - 1);
                            loadAllShop(recyclerView);
                            size+=10;
                        }
                    }
                }

            }
        });
        return root;
    }

    private ShopListAdapter.OnShopClickListener mListener = new ShopListAdapter.OnShopClickListener() {
        @Override
        public void onShopClick(final View itemView, Shop shop) {
            Util.firebaseAnalytics(mActivity,Constant.SHOP_ID,shop.getShopId()+"");
            itemView.setEnabled(false);
            itemView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    itemView.setEnabled(true);
                }
            },300);
            Intent intent = new Intent(mActivity, ShopDetailActivity.class);
            intent.putExtra ( "shop",shop );
            startActivity(intent);
        }
    };

    private void loadAllShop(final RecyclerView recyclerView){
        adapter = new ShopListAdapter(shopList, mListener);
        if(MockupData.FINDLOCATION){
            adapter.clear();
            currentPage = 1;
            size = 10;
        }
        recyclerView.setAdapter ( adapter );
        if(Constant.getBaseUrl() != null) {
            new StoreWs().loadStores(BaseActivity.countryCode, mCategoryId, mTagId, currentPage, sizePage, BaseActivity.latitude, BaseActivity.longitude, new StoreWs.LoadStoreCallback() {
                @Override
                public void onLoadSuccess(List<Shop> list) {
                    shopList.addAll(list);
                    adapter.notifyDataSetChanged ();
                    progressBar.setVisibility(View.GONE);
                    MockupData.FINDLOCATION = false;
                    isPullAble =false;
                    if (shopList.isEmpty()) {
                        emptyTv.setVisibility(View.VISIBLE);
                        imgNoSever.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onLoadFailed(String errorMessage) {
                  if (Util.isConnectedToInternet ( mActivity )) Util.popupMessage(null, errorMessage, mActivity);
                }

                @Override
                public void onLoadStoreDetail(Shop shop) {
                }
            });
        }
    }

}

