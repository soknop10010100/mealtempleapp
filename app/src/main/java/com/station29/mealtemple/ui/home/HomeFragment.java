package com.station29.mealtemple.ui.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Category;
import com.station29.mealtemple.api.model.PaymentService;
import com.station29.mealtemple.api.model.StoreGroup;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.response.Config;
import com.station29.mealtemple.api.request.CategoryWs;
import com.station29.mealtemple.api.request.ConfigWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.api.request.ServerUrlWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.BaseFragment;
import com.station29.mealtemple.ui.delivery_package.DeliveryPackageActivity;
import com.station29.mealtemple.ui.home.adpater.MenuStoreFragmentsPagerAdapter;
import com.station29.mealtemple.ui.home.adpater.PopCategoryAdapter;
import com.station29.mealtemple.ui.payment.ScanMeFragment;
import com.station29.mealtemple.ui.payment.TransferActivity;
import com.station29.mealtemple.ui.profile.EditProfileActivity;
import com.station29.mealtemple.ui.profile.LoginActivity;
import com.station29.mealtemple.ui.profile.SetChangePinActivity;
import com.station29.mealtemple.ui.taxi.TaxiMapActivity;
import com.station29.mealtemple.util.AbsolutefitLayourManager;
import com.station29.mealtemple.util.DotsIndicatorDecoration;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class HomeFragment extends BaseFragment implements View.OnClickListener {

    private final static int REQUEST_CODE_EDIT_ADDRESS = 234;
    private final int REQUEST_CODE_EDIT_PROFILE = 790;
    private final int REQUEST_CODE_LOGIN = 199, REQUEST_PIN_CODE =2342, REQUEST_TRANSFER = 3521,REQUEST_SCAN_CODE =1341;
    private TextView deliveryLocationTv, txtNoSever, txtWalletMoney;
    private LinearLayout search, layoutCategorise,totalBalance;
    public static Config splashConfig;
    private MenuStoreFragmentsPagerAdapter storesPagerAdapter;
    private PopCategoryAdapter popCategoryAdapter;
    private TabLayout tabLayout;
    private ViewPager storesPager;
    private RecyclerView recyclerViewCategories;
    private final List<Category> categories = new ArrayList<>();
    private SwipeRefreshLayout refreshLayout;
    private AppBarLayout appbar;
    private int radius, dotsHeight, color1, color;
    private ImageView imgNoSever;
    private NestedScrollView layoutTab;
    private boolean checkViewprofile = false;
    private String lang;
    private User profileUser;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View root = inflater.inflate(R.layout.fragment_home, container, false);
        ShopListFragment.isPullAble = true;

        // Build header
        recyclerViewCategories = root.findViewById(R.id.list_cat);
        layoutCategorise = root.findViewById(R.id.layoutCategorise);
        layoutTab = root.findViewById(R.id.layoutTab);
        txtNoSever = root.findViewById(R.id.txtNoSever);
        imgNoSever = root.findViewById(R.id.iamgeNoSever);
        txtWalletMoney = root.findViewById(R.id.walletMoney);
        totalBalance = root.findViewById(R.id.total_balance);
        txtNoSever.setVisibility(View.GONE);
        imgNoSever.setVisibility(View.GONE);
        Util.start(mActivity,"stop");
        if(mActivity.getIntent().hasExtra("lang")){
            lang = mActivity.getIntent().getStringExtra("lang");
        } else {
            lang ="en";
        }
        Util.changeLanguage(mActivity,lang);
        radius = getResources().getDimensionPixelSize(R.dimen.radius);
        dotsHeight = getResources().getDimensionPixelSize(R.dimen.dots_height);
        color1 = ContextCompat.getColor(root.getContext(), R.color.colorPrimary);
        color = ContextCompat.getColor(root.getContext(), R.color.colorPrimary);

        final AbsolutefitLayourManager absolutefitLayourManager = new AbsolutefitLayourManager(root.getContext(), 2, RecyclerView.HORIZONTAL, false, 4);
        recyclerViewCategories.setLayoutManager(absolutefitLayourManager);

        // if phone has sim => code
        tabLayout = root.findViewById(R.id.tablayout);
        tabLayout.setVisibility(View.GONE);
        storesPager = root.findViewById(R.id.view_pager);
        appbar = root.findViewById(R.id.app_bar);
        storesPagerAdapter = new MenuStoreFragmentsPagerAdapter(getChildFragmentManager());

        List<StoreGroup> tapItems = MockupData.getShopGroupArrayList();
        for (StoreGroup s : tapItems) {
            MockupData.FINDLOCATION = true;//clear adapter every time on create view
            storesPagerAdapter.addFragment(ShopListFragment.newInstance(), s.getLabel());
        }
        //pull refresh
        refreshLayout = root.findViewById(R.id.pull_fresh);
        refreshLayout.setColorSchemeResources(R.color.colorPrimary);
        // Build body
        deliveryLocationTv = root.findViewById(R.id.home_delivery_location);
        search = root.findViewById(R.id.map_delivery_location1);
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                }, 100);
                Intent intent = new Intent(mActivity, MapsActivity.class);
                startActivityForResult(intent, REQUEST_CODE_EDIT_ADDRESS);
            }
        });


        //set swipe below the app bar
        int start = getResources().getDimensionPixelSize(R.dimen.indicator_start);
        int end = getResources().getDimensionPixelSize(R.dimen.indicator_end);
        refreshLayout.setProgressViewOffset(true, start, end);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!ShopListFragment.isPullAble) {
                    reLoadFragment(HomeFragment.this);
                } else {
                    refreshLayout.setRefreshing(false);
                }
            }

        });
        //set swipe for only expanded of app bar
        appbar.addOnOffsetChangedListener(new AppBarLayout.BaseOnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    refreshLayout.setEnabled(false);
                } else if (verticalOffset == 0) {
                    // Expanded
                    refreshLayout.setEnabled(true);
                }
            }
        });

        ImageView searchStore = root.findViewById(R.id.btnSearchAllStore);
        searchStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, ShopSearchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        if (MockupData.DELIVERY_ADDRESS != null && !MockupData.DELIVERY_ADDRESS.equals("")) {
            deliveryLocationTv.setText(MockupData.DELIVERY_ADDRESS);
        }
        getAllWebService();

        root.findViewById(R.id.scan).setOnClickListener(this);
        root.findViewById(R.id.my_code).setOnClickListener(this);
        root.findViewById(R.id.transfer).setOnClickListener(this);
        root.findViewById(R.id.reward).setOnClickListener(this);

        return root;
    }

    private void initPopularCategories(final RecyclerView recyclerViewCategories) {
        if (categories.size() != 0) {
            popCategoryAdapter.clear();
        }
        String lang = Util.getString("language", mActivity);
        popCategoryAdapter = new PopCategoryAdapter(categories, categoryClickListener);
        recyclerViewCategories.setAdapter(popCategoryAdapter);
        if (Constant.getBaseUrl() != null) {
            new CategoryWs().listMainCategories(BaseActivity.countryCode, lang, new CategoryWs.LoadCategoryCallback() {
                @Override
                public void onLoadSuccess(List<Category> categoryList) {
                    Log.d("mainCategory", categoryList.size() + "");
                    if (categories.size() != 0) categories.clear();
                    categories.addAll(categoryList);
                    if (categories.size() > 7)
                        recyclerViewCategories.addItemDecoration(new DotsIndicatorDecoration(radius, radius * 4, dotsHeight, color1, color));
                    popCategoryAdapter.notifyDataSetChanged();
                }

                @Override
                public void onLoadFailed(String errorMessage) {
                    if (isConnectedToInternet(mActivity))
                        Util.popupMessage(null, errorMessage, mActivity);
                }
            });
        } else {
            Log.d("mainCategory", "null base url");
        }
    }

    private final PopCategoryAdapter.CategoryClickListener categoryClickListener = new PopCategoryAdapter.CategoryClickListener() {
        @Override
        public void onCategoryClick(Category category) {
            String token = Util.getString("token", mActivity);
            if (category.getType().equalsIgnoreCase("SHOP")) {
                Intent intent = new Intent(mActivity, ShopListActivity.class);
                intent.putExtra("category", category);
                startActivity(intent);

            } else if (category.getType().equalsIgnoreCase("TAXI")) {

                if(token.isEmpty()) loginAlert();
                else if (MockupData.LOGIN_PHONE != null && MockupData.LOGIN_PHONE.equals("") || MockupData.LOGIN_PHONE == null && MockupData.USER_NAME == null || MockupData.USER_NAME.equals("") || MockupData.USER_NAME.equals("Guest"))
                    popupMessageUpdatePhone(null);
                else if (profileUser != null && !profileUser.isConfirmPin()) {
                    Intent intent = new Intent(mActivity, SetChangePinActivity.class);
                    intent.putExtra("user",profileUser);
                    startActivityForResult(intent,REQUEST_PIN_CODE);
                } else {
                    Intent intent = new Intent(mActivity, TaxiMapActivity.class);
                    intent.putExtra("category", category);
                    startActivity(intent);
                }

            } else if (category.getType().equalsIgnoreCase("SEND")) {
                if (token.isEmpty()) {
                    loginAlert();
                } else if (MockupData.LOGIN_PHONE != null && MockupData.LOGIN_PHONE.equals("") || MockupData.LOGIN_PHONE == null && MockupData.USER_NAME == null || MockupData.USER_NAME.equals("") || MockupData.USER_NAME.equals("Guest")) {
                    popupMessageUpdatePhone(null);
                } else {
                    Intent intent = new Intent(mActivity, DeliveryPackageActivity.class);
                    startActivity(intent);
                }
            }
        }
    };

    private void popupMessageUpdatePhone(String title) {
        Util.firebaseAnalytics(mActivity, Constant.ERROR_MESSAGE, "user_information_missing");
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity, R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(R.string.you_miss_data);
        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                checkViewprofile = true;
                viewProfile();
            }
        });
        builder.show();
    }

    private void loginAlert() {
        Util.firebaseAnalytics(mActivity, Constant.ERROR_MESSAGE, "not_login");
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity, R.style.DialogTheme);
        builder.setMessage(R.string.you_havent_acc);
        builder.setNegativeButton(mActivity.getString(R.string.cancel), null);
        builder.setPositiveButton(mActivity.getString(R.string.checkLogin), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(mActivity, LoginActivity.class);
                intent.putExtra("action", "checkout");
                startActivityForResult(intent, REQUEST_CODE_LOGIN);
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_EDIT_ADDRESS && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("delivery_address")) {
                String deliveryAddress = data.getStringExtra("delivery_address");
                MockupData.FINDLOCATION = true;
//              MockupData.setCurrentOrderDetail(null);
                MockupData.highLightItemIds = new HashMap<>();
                deliveryLocationTv.setText(deliveryAddress);
                getUrls();
            }
        } else if (requestCode == REQUEST_CODE_LOGIN && resultCode == RESULT_OK) {
            viewProfile();
            if (data != null && data.hasExtra("userPhone")) {
                String phone = data.getStringExtra("userPhone");
                if (phone == null || phone.equals("")) {
                    popupMessageUpdatePhone(null);
                }
            }
        } else if (requestCode == REQUEST_CODE_EDIT_PROFILE && resultCode == RESULT_OK) {
            Intent intent = new Intent(mActivity, DeliveryPackageActivity.class);
            intent.putExtra("user",profileUser);
            startActivity(intent);
        } else if (resultCode == RESULT_OK){
            if (requestCode == REQUEST_PIN_CODE || requestCode == REQUEST_TRANSFER || requestCode == REQUEST_SCAN_CODE )
                viewProfile();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //no interent
        if (!isConnectedToInternet(mActivity)) {
            popupMessage(null, getResources().getString(R.string.failed_internet), mActivity);
            ShopListFragment.isPullAble = false;
        }
        if (getView() == null) {
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    // handle back button's click listener

                    mActivity.finishAffinity();
                    return true;
                }
                return false;
            }
        });
    }

    private void cofig() {
        if(Constant.getBaseUrl()!=null){
            new ConfigWs().readConfig(BaseActivity.countryCode, new ConfigWs.ReadConfigCallback() {
                @Override
                public void onSuccess(Config config) {
                    splashConfig = config;
                    // move cash to the last index of list
                    for (PaymentService paymentService : Constant.getConfig().getPaymentServiceList()) {
                        if (paymentService.getIsCash() == 1) {
                            Constant.getConfig().getPaymentServiceList().remove(paymentService);
                            Constant.getConfig().getPaymentServiceList().add(Constant.getConfig().getPaymentServiceList().size(), paymentService);
                            break;
                        }
                    }
                }

                @Override
                public void onError(String message) {
                    popupMessage(null, message, mActivity);
                }
            });
        }

    }

    private void getAllWebService() {
        initPopularCategories(recyclerViewCategories);
        storesPager.setAdapter(storesPagerAdapter);
        tabLayout.setupWithViewPager(storesPager);
        viewProfile();
        cofig();
        layoutCategorise.setVisibility(View.VISIBLE);
        layoutTab.setVisibility(View.VISIBLE);
        txtNoSever.setVisibility(View.GONE);
        imgNoSever.setVisibility(View.GONE);
        if (!NotificationManagerCompat.from(mActivity).areNotificationsEnabled())
            Util.buildAlertMessageNoNotification(mActivity);
    }

    private void getUrls() {
        new ServerUrlWs().readServerUrl(BaseActivity.countryCode, new ServerUrlWs.loadServerUrl() {
            @Override
            public void onLoadSuccess(boolean success) {
                if (success && isAdded()) {
                    getAllWebService();
                }
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                layoutCategorise.setVisibility(View.GONE);
                layoutTab.setVisibility(View.GONE);
                txtNoSever.setVisibility(View.VISIBLE);
                imgNoSever.setVisibility(View.VISIBLE);
            }
        });
    }

    private void viewProfile() {
        String token = Util.getString("token", mActivity);
        if (token != null && !token.isEmpty() && Constant.getBaseUrl() != null) {
            new ProfileWs().viewProfiles(mActivity, new ProfileWs.ProfileViewAndUpdate() {
                @Override
                public void onLoadProfileSuccess(User user) {
                    profileUser = user;
                    totalBalance.setVisibility(View.VISIBLE);
                    txtWalletMoney.setText(user.getWalletsList().get(0).getBalance() + " " + user.getWalletsList().get(0).getCurrency());
                    if (user.getLastName() != null && user.getFirstName() != null) {
                        MockupData.USER_NAME = user.getFirstName() + " " + user.getLastName();
                    } else if (user.getFirstName() != null) {
                        MockupData.USER_NAME = user.getFirstName();
                    } else if (user.getLastName() != null) {
                        MockupData.USER_NAME = user.getLastName();
                    } else
                        MockupData.USER_NAME = "Guest";

                    if (user.getPhoneNumber() != null) {
                        MockupData.LOGIN_PHONE = user.getPhoneNumber();
                    } else MockupData.LOGIN_PHONE = "";

                    if (checkViewprofile) {
                        checkViewprofile = false;
                        Intent intent = new Intent(mActivity, EditProfileActivity.class);
                        intent.putExtra("user", user);
                        startActivityForResult(intent, REQUEST_CODE_EDIT_PROFILE);
                    }
                }

                @Override
                public void onError(String errorMessage) {
                    Util.clearString(mActivity);
                }

                @Override
                public void onUpdateProfilesSuccess(boolean isSendSms, User user) {

                }
            });
        }
    }

    private void reLoadFragment(Fragment fragment) {
        Fragment currentFragment = fragment;
        MockupData.FINDLOCATION = true;
        ShopListFragment.isPullAble = true;
        if (currentFragment instanceof HomeFragment) {
            FragmentTransaction fragTransaction = getParentFragmentManager().beginTransaction();
            fragTransaction.detach(currentFragment);
            fragTransaction.attach(currentFragment);
            fragTransaction.commit();
            Log.i("INFO_TAG", "reloading fragment finish");
            refreshLayout.setRefreshing(false);

        } else Log.i("INFO_TAG", "fragment reloading failed");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.scan) {
            if (profileUser != null && !profileUser.isConfirmPin()) {
                Intent intent = new Intent(mActivity, SetChangePinActivity.class);
                intent.putExtra("user",profileUser);
                startActivityForResult(intent,REQUEST_PIN_CODE);
            } else if(isLoggedIn()) {
                Intent intent = new Intent(mActivity,ScannerActivity.class);
                intent.putExtra("user",profileUser);
                startActivityForResult(intent,REQUEST_SCAN_CODE);
            } else {
                startActivity(new Intent(mActivity, LoginActivity.class));
            }
        } else if (v.getId() == R.id.transfer){
            if (profileUser != null && !profileUser.isConfirmPin()) {
                Intent intent = new Intent(mActivity, SetChangePinActivity.class);
                intent.putExtra("user",profileUser);
                startActivityForResult(intent,REQUEST_PIN_CODE);
            } else if(isLoggedIn()) {
                Intent intent = new Intent(mActivity,TransferActivity.class);
                intent.putExtra("user",profileUser);
                intent.putExtra("transfer",true);
                startActivityForResult(intent, REQUEST_TRANSFER);
            } else {
                startActivity(new Intent(mActivity, LoginActivity.class));
            }
        } else if ( v.getId() == R.id.reward) {
            Toast.makeText(mActivity, getString(R.string.coming_soon), Toast.LENGTH_SHORT).show();
        } else if (v.getId() == R.id.my_code) {
            if (isLoggedIn()) {
                ScanMeFragment scanMeFragment = ScanMeFragment.newInstance();
                scanMeFragment.show(getChildFragmentManager(), ScanMeFragment.class.getName());
            } else {
                startActivity(new Intent(mActivity, LoginActivity.class));
            }
        }
    }

    private boolean isLoggedIn() {
        String accessToken = Util.getString("token", mActivity);
        return !accessToken.isEmpty();
    }
}
