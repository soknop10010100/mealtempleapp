package com.station29.mealtemple.ui.profile;

import android.os.Bundle;
import android.view.View;

import com.station29.mealtemple.R;
import com.station29.mealtemple.ui.BaseActivity;

public class CreditDebitCardActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_debit_card);

        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
