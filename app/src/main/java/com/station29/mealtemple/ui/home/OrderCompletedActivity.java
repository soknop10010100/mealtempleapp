package com.station29.mealtemple.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.ui.BaseActivity;

public class  OrderCompletedActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_completed);


        findViewById(R.id.checkOrderHistory).setOnClickListener(this);
        findViewById(R.id.closeButton).setOnClickListener(this);
        MockupData.setCurrentOrderDetail(null);// Clear current order so user can add new item to cart
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.checkOrderHistory) {
            Intent intent = new Intent(OrderCompletedActivity.this, MainActivity.class);
            intent.putExtra("tabId", R.id.navigation_dashboard);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (view.getId() == R.id.closeButton) {
            Intent intent = new Intent(OrderCompletedActivity.this, MainActivity.class);
            intent.putExtra("tabId", R.id.navigation_dashboard);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
        startActivity(new Intent(OrderCompletedActivity.this, MainActivity.class));
    }
}
