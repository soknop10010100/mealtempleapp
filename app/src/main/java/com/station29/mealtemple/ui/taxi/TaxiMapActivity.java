package com.station29.mealtemple.ui.taxi;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Services;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.WayPointLatLng;
import com.station29.mealtemple.api.model.Waypoint;
import com.station29.mealtemple.api.model.response.Config;
import com.station29.mealtemple.api.request.ConfigWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.api.request.ServerUrlWs;
import com.station29.mealtemple.api.request.TaxiWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.MapsActivity;
import com.station29.mealtemple.ui.home.ShopListFragment;
import com.station29.mealtemple.ui.taxi.adapter.HistoryPlaceAdapter;
import com.station29.mealtemple.util.GPSTracker;
import com.station29.mealtemple.util.Util;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class TaxiMapActivity extends BaseActivity implements OnMapReadyCallback {

    private final static int REQUEST_CODE_EDIT_ADDRESS = 235;
    private TextView txtFrom, txtTo;
    private GoogleMap mMap;
    private ImageView centerImage, pinOnMap , clearText;
    private Geocoder geocoder;
    private boolean checkDrawMap = false, callLocation = false;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private final static int REQUEST_LOCATION_PERMISSION = 172;
    private final static int REQUEST_CODE_ENABLE_LOCATION = 127;
    private int AUTOCOMPLETE = 2;
    private String swipeSearch;
    private double pickLat, pickLng, dropLat, dropLng, userLat = 0, userLng = 0;
    private int width = 10;
    private List<Services> listServices;
    private RecyclerView recyclerView;
    private Button btnBook;
    private List<WayPointLatLng> wayPointLatLngList = new ArrayList<>();
    private Waypoint TaxiWaypoint;
    private HashMap<String, String> pickDropPlace = new HashMap<>();
    private HistoryPlaceAdapter historyPlaceAdapter;
    private ImageButton myLocationBtn;
    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    String message = "" ;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taxi_map);

        txtFrom = findViewById(R.id.txtFrom);
        txtTo = findViewById(R.id.txtTo);
        centerImage = findViewById(R.id.center_image);
        pinOnMap = findViewById(R.id.pinOnMap);
        btnBook = findViewById(R.id.btnBookTaxi);
        recyclerView = findViewById(R.id.listPlaceHistory);
        myLocationBtn = findViewById(R.id.location_meBtn);
        clearText = findViewById(R.id.clear);
        progressBar = findViewById(R.id.progress_loading);
        //check location permission
        checkPermission();
        initLocationCallBack();
        getUrls();
        callLocation();
        btnBook.setEnabled(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 1, RecyclerView.HORIZONTAL, false));

        if (getArrayList("placeHistory") != null) {
            wayPointLatLngList = getArrayList("placeHistory");
            historyPlaceAdapter = new HistoryPlaceAdapter(wayPointLatLngList, onPlaceClickListener);
            recyclerView.setAdapter(historyPlaceAdapter);
        }

        if (getIntent() != null && getIntent().hasExtra("dropLatLng")) {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            pickDropPlace = (HashMap<String, String>) getIntent().getSerializableExtra("dropLatLng");
            if(pickDropPlace != null && pickDropPlace.containsKey("dropoff_latitute") && pickDropPlace.containsKey("dropoff_longitute")) {
                dropLat = Double.parseDouble(pickDropPlace.get("dropoff_latitute"));
                dropLng = Double.parseDouble(pickDropPlace.get("dropoff_longitute"));
                try {
                    txtTo.setText(geocoder.getFromLocation(dropLat, dropLng, 1).get(0).getAddressLine(0));
                    clearText.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else{
                dropLat = 0.0;
                dropLat = 0.0;
            }
            pickLat = Double.parseDouble(pickDropPlace.get("pickup_latitute"));
            pickLng = Double.parseDouble(pickDropPlace.get("pickup_longitute"));
            try {
                txtFrom.setText(geocoder.getFromLocation(pickLat, pickLng, 1).get(0).getAddressLine(0));
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(dropLat != 0.0 && dropLng != 0.0)
                getDriverServer();

        }

        pinOnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Util.firebaseAnalytics(TaxiMapActivity.this,Constant.CLICK_ON,"pin_on_map");
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                }, 100);
                Intent intent = new Intent(TaxiMapActivity.this, MapsActivity.class);
                intent.putExtra("gotoMap", "gotoMap");
                intent.putExtra("userLat", userLat);
                intent.putExtra("userLng", userLng);
                startActivityForResult(intent, REQUEST_CODE_EDIT_ADDRESS);
            }
        });

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress();
            }
        });

        clearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPress();
            }
        });

        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                }, 100);
                pickDropPlace.put("pickup_latitute", String.valueOf(pickLat));
                pickDropPlace.put("pickup_longitute", String.valueOf(pickLng));
                pickDropPlace.put("dropoff_latitute", String.valueOf(dropLat));
                pickDropPlace.put("dropoff_longitute", String.valueOf(dropLng));
                if(String.valueOf(dropLat).equals("0.0") &&  String.valueOf(dropLng).equals("0.0")){
                    getDriverServer();
                } else if  (message.equalsIgnoreCase("Sorry, Your booking is out zone.")){
                    Util.popupMessage(getString(R.string.error), "Sorry, Your booking is out zone.", TaxiMapActivity.this);
                }else{
                    Intent intent = new Intent(TaxiMapActivity.this, ChooseTransportActivity.class);
                    intent.putExtra("servicesList", (Serializable) listServices);
                    intent.putExtra("taxiWaypoint", TaxiWaypoint);
                    intent.putExtra("pickDropPlace", (Serializable) pickDropPlace);
                    intent.putExtra("pickup_lat", pickLat);
                    intent.putExtra("pickup_long",pickLng);
                    intent.putExtra("drop_lat", dropLat);
                    intent.putExtra("drop_long", dropLng);
                    startActivity(intent);
                }
            }
        });

        txtTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                }, 100);
                swipeSearch = "goto";
                searchMap();
            }
        });

        txtFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                }, 100);
                swipeSearch = "from";
                searchMap();
            }
        });

        geocoder = new Geocoder(this, Locale.getDefault());
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
    }

    public void onClick(View v) {
        if (v == myLocationBtn) {
            if (mMap != null) getMyLocation(mMap);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setCompassEnabled(false);//hide compass
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(userLat, userLng), 15));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
//        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @SuppressLint("LogConditional")
            @Override
            public void onCameraIdle() {
                if (!checkDrawMap)
                    displayAddressByImageCenter();

            }
        });
        mMap.getUiSettings().setZoomControlsEnabled(false);
        btnBook.setEnabled(true);
    }

    private String displayAddressByImageCenter() {
        try {
            int h = centerImage.getHeight();
            int w = centerImage.getWidth() / 2;
            int X = (int) centerImage.getX() + w;
            int Y = (int) centerImage.getY() + h / 2;
            LatLng ll = mMap.getProjection().fromScreenLocation(new Point(X, Y + 40));
            List<Address> addresses = geocoder.getFromLocation(ll.latitude, ll.longitude, 1);
            String fullAdd = getFullAddress(addresses);
            Log.d("addressTest", fullAdd + ":" + ll.latitude +","+ll.longitude);
            txtFrom.setText(String.format("%s", fullAdd));
            pickLat = ll.latitude;
            pickLng = ll.longitude;
            return fullAdd;
        } catch (IOException e) {
            Log.d(e.getClass().getName(), "onResult: " + e.getMessage());
        }
        return "";
    }

    private String getFullAddress(List<Address> addresses) {
        if (addresses.size() > 0) {
            return addresses.get(0).getAddressLine(0);
        }
        return "";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                if (swipeSearch.equals("goto")) {
                    dropLat = place.getLatLng().latitude;
                    dropLng = place.getLatLng().longitude;
                    WayPointLatLng wayPointLatLng = new WayPointLatLng(dropLat, dropLng);
                    wayPointLatLngList.add(0, wayPointLatLng);
                    if (wayPointLatLngList.size() == 6)
                        wayPointLatLngList.remove(5);
                    saveArrayList(wayPointLatLngList, "placeHistory");
                    getDriverServer();
                    txtTo.setText(place.getAddress());
                    clearText.setVisibility(View.VISIBLE);
                } else {
                    txtFrom.setText(place.getAddress());
                    pickLat = place.getLatLng().latitude;
                    pickLng = place.getLatLng().longitude;
                    if(!String.valueOf(dropLat).equals("0.0") && !String.valueOf(dropLng).equals("0.0")){
                        getDriverServer();
                    }
                    if(txtTo.getText().toString().isEmpty())
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                Util.popupMessage(getString(R.string.error), status.getStatusMessage(), this);
            }
        }
        if (requestCode == REQUEST_CODE_EDIT_ADDRESS && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("delivery_address")) {

                String deliveryAddress = data.getStringExtra("delivery_address");
                dropLat = data.getDoubleExtra("lat", 0);
                dropLng = data.getDoubleExtra("lng", 0);
                WayPointLatLng wayPointLatLng = new WayPointLatLng(dropLat, dropLng);
                wayPointLatLngList.add(0, wayPointLatLng);
                if (wayPointLatLngList.size() == 6)
                    wayPointLatLngList.remove(5);
                saveArrayList(wayPointLatLngList, "placeHistory");
                getDriverServer();
                btnBook.setEnabled(true);

                txtTo.setText(deliveryAddress);
                clearText.setVisibility(View.VISIBLE);
            }
        }
    }

    private void searchMap() {
        Util.firebaseAnalytics(TaxiMapActivity.this,Constant.CLICK_ON,"search_map");
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS, Place.Field.LAT_LNG);

        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
                .build(TaxiMapActivity.this);
        startActivityForResult(intent, AUTOCOMPLETE);
    }

    private void getDriverServer() {
        progressBar.setVisibility(View.VISIBLE);
        if (Constant.getBaseUrl() != null)
            new TaxiWs().getDriverServices(pickLat, pickLng, dropLat, dropLng, new TaxiWs.getDriverCallback() {
                @Override
                public void onLoadSuccess(List<Services> servicesList, Waypoint waypoint) {
                    if(waypoint!=null) {
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.GONE);
                        btnBook.setVisibility(View.VISIBLE);
                        listServices = servicesList;
                        TaxiWaypoint = waypoint;
                        mMap.clear();
                        btnBook.setEnabled(true);
                        checkDrawMap = true;
                        centerImage.setVisibility(View.GONE);
                        drawMap(waypoint);
                    }
                }

                @Override
                public void onLoadSuccessWithoutDrop(List<Services> servicesList) {
                    progressBar.setVisibility(View.GONE);
                    Intent intent = new Intent(TaxiMapActivity.this, ChooseTransportActivity.class);
                    intent.putExtra("servicesList", (Serializable) servicesList);
                    intent.putExtra("pickDropPlace", (Serializable) pickDropPlace);
                    intent.putExtra("pickup_lat", pickLat);
                    intent.putExtra("pickup_long",pickLng);
                    intent.putExtra("drop_lat", dropLat);
                    intent.putExtra("drop_long", dropLng);
                    startActivity(intent);
                }

                @Override
                public void onLoadFailed(String errorMessage,int code) {
                    progressBar.setVisibility(View.GONE);
                    message = errorMessage;
                    if(message.equalsIgnoreCase("Sorry, Your booking is out zone.")){
                        Util.popupMessage(getString(R.string.error), "Sorry, Your booking is out zone.", TaxiMapActivity.this);
                    }else {
                        Util.popupMessage(getString(R.string.error), errorMessage, TaxiMapActivity.this);
                    }

                }

                @Override
                public void onOrder(String OrderId) {

                }
            });
    }

    private void drawMap(Waypoint waypoint) {
        ArrayList<LatLng> routelist = new ArrayList<>();
        double lat, lng;
        if (waypoint.getListLatLng().size() > 0) {
            for (int i = 0; waypoint.getListLatLng().size() > i; i++) {
                lat = waypoint.getListLatLng().get(i).getLatitute();
                lng = waypoint.getListLatLng().get(i).getLongitute();
                routelist.add(new LatLng(lat, lng));
            }
        }
        if (routelist.size() > 0) {
            PolylineOptions rectLine = new PolylineOptions().width(this.width).color(getResources().getColor(R.color.colorPrimary));//set line color to app color
            for (int i = 0; i < routelist.size(); i++) {
                rectLine.add(routelist.get(i));
            }
            mMap.addMarker(new MarkerOptions().position(routelist.get(routelist.size() - 1)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_off)));
            mMap.addMarker(new MarkerOptions().position(routelist.get(0)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_pickup)));
            // Adding route on the map
            mMap.addPolyline(rectLine);
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(routelist.get(routelist.size() - 1));
            builder.include(routelist.get(0));
            LatLngBounds bounds = builder.build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, getResources().getDisplayMetrics().widthPixels, getResources().getDisplayMetrics().heightPixels / 2, (int) (getResources().getDisplayMetrics().widthPixels * 0.18));
            mMap.animateCamera(cameraUpdate);
        }
    }

    private HistoryPlaceAdapter.OnPlaceClickListener onPlaceClickListener = new HistoryPlaceAdapter.OnPlaceClickListener() {
        @Override
        public void onPlaceClick(View itemView, WayPointLatLng wayPointLatLng) throws IOException {
            Util.firebaseAnalytics(TaxiMapActivity.this,Constant.CLICK_ON,"map_history");
            recyclerView.setVisibility(View.GONE);
            btnBook.setVisibility(View.VISIBLE);
            dropLat = wayPointLatLng.getLatitute();
            dropLng = wayPointLatLng.getLongitute();
            Geocoder geocoder = new Geocoder(TaxiMapActivity.this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(wayPointLatLng.getLatitute(), wayPointLatLng.getLongitute(), 1);
            String fullAdd = addresses.get(0).getAddressLine(0);
            txtTo.setText(fullAdd);
            getDriverServer();
        }
    };

    public void saveArrayList(List<WayPointLatLng> list, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        try {
            Gson gson = new Gson();
            String json = gson.toJson(list);
            editor.putString(key, json);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<WayPointLatLng> getArrayList(String key) {
        List<WayPointLatLng> getArrayLists = new ArrayList<>();
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<List<WayPointLatLng>>() {}.getType();
            getArrayLists = gson.fromJson(json, type);
            return getArrayLists;
        } catch (Exception e) {
            e.getMessage();
        }
        return getArrayLists;
    }

    @Override
    public void onBackPressed() {
        backPress();
    }

    private void backPress() {
        btnBook.setEnabled(true);
        if (txtTo.getText().toString().equals("")) {
            finishAffinity();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            if (wayPointLatLngList.size() != 0) {
                historyPlaceAdapter = new HistoryPlaceAdapter(wayPointLatLngList, onPlaceClickListener);
                recyclerView.setAdapter(historyPlaceAdapter);
            }
            dropLng = 0;dropLat = 0;
            txtTo.setText("");
            recyclerView.setVisibility(View.GONE);
            btnBook.setVisibility(View.VISIBLE);
            mMap.clear();
            checkDrawMap = false;
            centerImage.setVisibility(View.VISIBLE);
            getMyLocation(mMap);
            clearText.setVisibility(View.GONE);
        }
    }

    private void getMyLocation(GoogleMap mMap) {
        //current location zoom
        GPSTracker tracker = new GPSTracker(this);
        if (!tracker.canGetLocation()) {
            tracker.showSettingsAlert();
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(tracker.getLatitude(),tracker.getLongitude()), 15));

        }
    }

    private void checkPermission() {
        if (getApplicationContext() == null) return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int permissionCheck = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {

                checkGps();
            } else {
                if (getApplicationContext() == null) return;
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION_PERMISSION);
            }
        }else{
            checkGps();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                checkGps();
            } else {
                Toast.makeText(getApplicationContext(), R.string.permission_denied, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        startLocationUpdates();
        if (!Util.isConnectedToInternet (TaxiMapActivity.this )){
            Util.popupMessage ( null, getResources ().getString ( R.string.failed_internet ),TaxiMapActivity.this);
            ShopListFragment.isPullAble =false;
        }
    }

    private void checkGps() {
        final LocationManager manager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            callLocation = true;
            buildAlertMessageNoGps();
        } else {
            callLocation();
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(TaxiMapActivity.this,R.style.DialogTheme);
        builder.setMessage(R.string.your_gps)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_CODE_ENABLE_LOCATION);
                    }
                })
                .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void stopLocationUpdates() {
        FusedLocationProviderClient fusedLocationProviderClient = new FusedLocationProviderClient(TaxiMapActivity.this);
        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    private void startLocationUpdates() {
        checkPermission();
        fusedLocationProviderClient = new FusedLocationProviderClient(TaxiMapActivity.this);
        fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                locationCallback,
                Looper.myLooper());
    }

    private void initLocationCallBack(){
        if(callLocation) {
            locationRequest = new LocationRequest();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(50);
            locationRequest.setFastestInterval(50);
        }
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if(!MockupData.DELIVERY_ADDRESS.equals(""))
                    fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                if (locationResult == null) {
                    return;
                }
                if (callLocation) {
                    for (Location location : locationResult.getLocations()) {
                        userLat = location.getLatitude();
                        userLng = location.getLongitude();
                        callLocation = false;
                        fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                    }
                }
            }

        };
    }

    private void callLocation() {
        GPSTracker gps = new GPSTracker(TaxiMapActivity.this);
        if (gps.canGetLocation()) {
           userLat = gps.getLatitude();
           userLng = gps.getLongitude();
        }
    }

    private void getUrls() {
        new ServerUrlWs().readServerUrl(BaseActivity.countryCode, new ServerUrlWs.loadServerUrl() {
            @Override
            public void onLoadSuccess(boolean success) {
                if (Constant.getBaseUrl() != null) config();
                viewProfile();
            }

            @Override
            public void onLoadFailed(String errorMessage) {

                Util.popupMessage(null, errorMessage, TaxiMapActivity.this);
            }
        });
    }
    private void config () {
        new ConfigWs().readConfig(BaseActivity.countryCode, new ConfigWs.ReadConfigCallback() {
            @Override
            public void onSuccess(Config config) {
                Constant.setConfig(config);

            }

            @Override
            public void onError(String message) {
                popupMessage(null, message, TaxiMapActivity.this);
            }
        });
    }
    private void viewProfile () {
        String token = Util.getString("token", TaxiMapActivity.this);
        if (token != null && !token.isEmpty()) {
            new ProfileWs().viewProfiles(TaxiMapActivity.this, new ProfileWs.ProfileViewAndUpdate() {
                @Override
                public void onLoadProfileSuccess(User user) {

                    if (user.getLastName() != null && user.getFirstName() != null) {
                        MockupData.USER_NAME = user.getFirstName() + " " + user.getLastName();
                    } else if (user.getFirstName() != null) {
                        MockupData.USER_NAME = user.getFirstName();
                    } else if (user.getLastName() != null) {
                        MockupData.USER_NAME = user.getLastName();
                    } else MockupData.USER_NAME = "Guest";

                    if (user.getPhoneNumber() != null) {
                        MockupData.LOGIN_PHONE = user.getPhoneNumber();
                    } else MockupData.LOGIN_PHONE = "";
                }

                @Override
                public void onError(String errorMessage) {
                    Util.clearString(TaxiMapActivity.this);
                    if (isConnectedToInternet(TaxiMapActivity.this))
                        popupMessage(null, errorMessage, TaxiMapActivity.this);
                }

                @Override
                public void onUpdateProfilesSuccess(boolean isSendSms , User user) {

                }
            });
        }
    }
}
