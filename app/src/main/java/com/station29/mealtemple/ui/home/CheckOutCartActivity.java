package com.station29.mealtemple.ui.home;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.AddOn;
import com.station29.mealtemple.api.model.ItemRequestParam;
import com.station29.mealtemple.api.model.OrderDetail;
import com.station29.mealtemple.api.model.OrderItem;
import com.station29.mealtemple.api.model.PreView;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.OrderWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.adpater.CartItemAdapter;
import com.station29.mealtemple.ui.profile.EditProfileActivity;
import com.station29.mealtemple.ui.profile.LoginActivity;
import com.station29.mealtemple.util.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.station29.mealtemple.util.Util.isConnectedToInternet;

public class CheckOutCartActivity extends BaseActivity implements View.OnClickListener {

    private List<OrderItem> orderItemArrayList;
    private Shop shop,shopRefresh;
    private final int REQUEST_CODE_CONFIRM = 918;
    private final int REQUEST_CODE_EDIT_ITEMS = 908;
    private TextView subTotalTv,totalQtyTv,shopNameTv;
    private final int REQUEST_CODE_LOGIN = 198;
    private float valuesDataIntent;
    private Integer positions;
    private PreView preViewed;
    private HashMap<String,Object> requestParams = new HashMap<> (  );
    private RecyclerView recyclerView;
    private final int REQUEST_CODE_EDIT_PROFILE = 5678;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_check_out_cart );

        totalQtyTv = findViewById ( R.id.totalQty );
        subTotalTv =findViewById ( R.id.subTotal );
        shopNameTv =findViewById ( R.id.shopName );
        progressBar = findViewById(R.id.progressBar);

        findViewById ( R.id.cart_checkout ).setOnClickListener ( this );
        findViewById ( R.id.backButton ).setOnClickListener ( this );

        TextView titleTv = findViewById(R.id.title);
        titleTv.setText ( R.string.cart );

        Intent intent = getIntent();
        orderItemArrayList = new ArrayList<>();

        if (intent.hasExtra("orderDetail")) {
            OrderDetail orderDetail = (OrderDetail) intent.getSerializableExtra("orderDetail");
            if (orderDetail == null) return;
            orderItemArrayList = orderDetail.getOrderItemList();
            shop = orderDetail.getShop();
        }
        if (intent.hasExtra("shop")){
            shopRefresh = (Shop) intent.getSerializableExtra("shop");
            if (shopRefresh==null)return;
            if (shopRefresh.getShopId ()== shop.getShopId ()) shop = shopRefresh;
        }

        if (intent.hasExtra ( "calculate" )){
            HashMap<String, Object> hashMap = (HashMap<String, Object>)intent.getSerializableExtra("calculate");
            if (hashMap==null)return;
            requestParams.put("account_id",hashMap.get("account_id") );
            requestParams.put ( "country_code",hashMap.get ( "country_code" ) );
            requestParams.put ("latitute" ,hashMap.get ( "latitute" ) );
            requestParams.put ("longitute" ,hashMap.get ( "longitute" )  );
            List<Integer> itemId = new ArrayList<> (  );
            for (int i = 0; i <orderItemArrayList.size (); i++) {
                itemId.add ( orderItemArrayList.get ( i ).getProductId () );
            }
            requestParams.put ( "item_ids",itemId);

            List<ItemRequestParam> itemParamsRequest = new ArrayList<>();
            for (OrderItem orderItem : orderItemArrayList) {
                itemParamsRequest.add ( getItemRequestParam(orderItem) );
            }

            requestParams.put ("items",itemParamsRequest );
        }
        shopNameTv.setText ( String.format ( Locale.US,"%s" , shop.getName ()) );

        initPriceDetail();

        recyclerView = findViewById(R.id.cart_items);
        recyclerView.setLayoutManager(new LinearLayoutManager (this));
        recyclerView.setAdapter(new CartItemAdapter(orderItemArrayList, onOrderItemEditQtyListener));

    }
    @Override
    public void onClick(final View view) {
        if (view.getId () == R.id.cart_checkout ){
            progressBar.setVisibility(View.VISIBLE);
            Util.firebaseAnalytics(this,Constant.CLICK_ON,"cart_checkout");
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setEnabled(true);
                }
            },1000);
            String token = Util.getString("token", CheckOutCartActivity.this);
            if (token.isEmpty()) {
                progressBar.setVisibility(View.GONE);
                loginAlert();
            }else if ( MockupData.LOGIN_PHONE != null && MockupData.LOGIN_PHONE.equals("")||MockupData.LOGIN_PHONE == null && MockupData.USER_NAME == null || MockupData.USER_NAME.equals("") || MockupData.USER_NAME.equals("Guest")) {
                progressBar.setVisibility(View.GONE);
                popupMessageUpdatePhone(getString(R.string.profile), CheckOutCartActivity.this);
            }else {
                viewOrder(requestParams);
            }
            //request server
        }else if (view.getId ()==R.id.backButton){
            finish ();
        }
    }

    private void loginAlert(){
        Util.firebaseAnalytics(this,Constant.ERROR_MESSAGE,"not_login");
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(CheckOutCartActivity.this,R.style.DialogTheme);
        builder.setMessage(R.string.you_havent_acc);
        builder.setNegativeButton(CheckOutCartActivity.this.getString(R.string.cancel),null);
        builder.setPositiveButton(CheckOutCartActivity.this.getString(R.string.checkLogin ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(CheckOutCartActivity.this, LoginActivity.class);
                intent.putExtra("action", "checkout");
                startActivityForResult(intent, REQUEST_CODE_LOGIN);
            }
        });
        builder.show();
    }

    private CartItemAdapter.onItemEditedListener onOrderItemEditQtyListener = new CartItemAdapter.onItemEditedListener() {
        @Override
        public void onOrderItemEditQty(OrderItem orderItem,int count,int position,boolean isEditQty) { // Here just change Qty of Item
            boolean isSameOrderItem = false;
            //start go to add to cart for edit
            if (isEditQty){
                //this case only edit qty
                editOrderItems ( orderItem,count,isSameOrderItem ,false);
            }else {
                //user click on edit all by orderItems
                positions = position;
                Intent intents = new Intent(CheckOutCartActivity.this,AddToCartActivity.class);
                intents.putExtra ( "action","EDIT" );
                intents.putExtra ( "orderitems",orderItem );
                intents.putExtra ( "count" ,count);
                intents.putExtra ( "shopedit",shop );
                //remove that item from list
                orderItemArrayList.remove ( orderItem );
                startActivityForResult ( intents,REQUEST_CODE_EDIT_ITEMS );
            }


        }

        @Override
        public void onLastItemRemove(boolean lastItemRemove) {
            if (lastItemRemove) findViewById ( R.id.cart_checkout ).setEnabled(false);
        }
    };
    private ItemRequestParam getItemRequestParam( OrderItem orderItem){
        List<AddOn> addons = orderItem.getAddOns ();
        List<Integer> addonIds = new ArrayList<> ();

        // Coz Ws require only ids
        for (AddOn addOn : addons) {
            addonIds.add ( addOn.getId () );
        }

        // Coz Ws require only ids
        List<Integer> optionsIds = new ArrayList<> ();
        List<AddOn> options = orderItem.getOption ();
        for (AddOn option : options) {
            optionsIds.add ( option.getId () );
        }
        ItemRequestParam itemParams = new ItemRequestParam ();
        itemParams.setProductId ( orderItem.getProductId () );
        itemParams.setQty ( orderItem.getQty () );
        itemParams.setComment ( orderItem.getComment () );
        itemParams.setAddOnIds ( addonIds );
        itemParams.setOptionIds ( optionsIds );
        return  itemParams;
    }

    private void initPriceDetail(){
        double subTotal = calculateTotalPrice(orderItemArrayList);
        Intent intent = new Intent ( this, OrderConfirmationActivity.class );
        intent.putExtra ( "subtotalcart", subTotal);
        intent.putExtra ( "preView",preViewed );
        setResult(RESULT_OK,intent);
        int totalQty = 0;
        for (OrderItem orderItem : orderItemArrayList) {
            totalQty += orderItem.getQty();
        }
        subTotalTv.setText(String.format("%s : %s %s",getString(R.string.sub_total),Constant.getConfig ()!=null?Constant.getConfig ().getCurrencySign ():"" ,Util.formatPrice(subTotal)));
        totalQtyTv.setText(String.format(Locale.US,"%s : %d",getString(R.string.qty) ,totalQty));
    }

    private void viewOrder(HashMap<String,Object> params){
        if (Constant.getBaseUrl()!=null){
            new OrderWs ().preViewOrder ( params, this, new OrderWs.CreateOrderListener () {
                @Override
                public void onCreateSuccess(String message) {

                }

                @Override
                public void onCreateError(String error,int code) {
                    progressBar.setVisibility(View.GONE);
                    if (error.equalsIgnoreCase("Unauthenticated.")) {
                        loginAlert();
                    } else {
                        Util.popupMessage ( null,error,CheckOutCartActivity.this );
                    }
                }

                @Override
                public void onViewOrder(PreView preView) {
                    progressBar.setVisibility(View.GONE);
                    //do something  here whit has map with inten
                    preViewed = preView;
                    Intent intent = new Intent(CheckOutCartActivity.this, OrderConfirmationActivity.class);
                    intent.putExtra("orderDetail", MockupData.getCurrentOrderDetail () );
                    intent.putExtra("shop",shopRefresh);
                    intent.putExtra ( "preView" ,preViewed);
                    startActivityForResult(intent, REQUEST_CODE_CONFIRM);
                }
            } );
        }
     }

    private double calculateTotalPrice(List<OrderItem> orderItemArrays) {
        double subTotal = 0;
        for (OrderItem orderItem : orderItemArrays) {

            double pricePerUnit = orderItem.getPrice(); // init price
            if (orderItem.getOption() != null) { // if has option, price ~ option price
                for (AddOn option : orderItem.getOption()) {
                    pricePerUnit += option.getPrice();
                }
            }
            if (orderItem.getAddOns().size() > 0) { // sum price of add on
                List<AddOn> addOnList = orderItem.getAddOns();
                for (AddOn addOn : addOnList) {
                    pricePerUnit += addOn.getPrice();
                }
            }
            subTotal += pricePerUnit * orderItem.getQty(); // sum price of all items in cart
        }

        return subTotal;
    }

    private void saveLatestStateItems(List<OrderItem> mOrderItemList) {
        if (mOrderItemList.size() > 0) {
            OrderDetail myOrderDetail = new OrderDetail();
            myOrderDetail.setShop(shop);
            myOrderDetail.setStoreId(shop.getShopId());
            myOrderDetail.setOrderItemList(mOrderItemList);
            MockupData.setCurrentOrderDetail(myOrderDetail);
            MockupData.getCurrentOrderDetail().getRequestParams().put("account_id",shop.getShopId());
            MockupData.getCurrentOrderDetail().getRequestParams().put("country_code" ,BaseActivity.countryCode);
            MockupData.getCurrentOrderDetail().setRequestParams(requestParams);
            MockupData.setHighLightItemIds(getMapHighlight());
            // Save to static for use later when user try to add new item from different store
        }
    }

    private HashMap<String, Integer> getMapHighlight() {
        OrderDetail myCart = MockupData.getCurrentOrderDetail();
        final HashMap<String, Integer> myCartMapItems = new HashMap<>();
        if (myCart != null) {
            List<OrderItem> orderItems = myCart.getOrderItemList();
            for (OrderItem oItem : orderItems) {
                if (oItem.getQty() > 0) {
                    int qty = oItem.getQty();
                    String productId = String.valueOf(oItem.getProductId());
                    if (myCartMapItems.containsKey(productId)) {
                        qty += myCartMapItems.get(productId);
                    }
                    myCartMapItems.put(String.valueOf(oItem.getProductId()), qty);
                }
            }
        }
        return myCartMapItems;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode== REQUEST_CODE_CONFIRM && resultCode == RESULT_OK){
            if (data != null ){
                if (data.hasExtra("tipCount")){
                    int tip = data.getIntExtra("tipCount", 0);
                    data.putExtra ( "subtotal", tip);
                }
                setResult ( RESULT_OK,data );
            }
        } else if (requestCode == REQUEST_CODE_LOGIN && resultCode == RESULT_OK) {
            if(data !=null && data.hasExtra("userPhone")){
                String phone = data.getStringExtra("userPhone");
                if(phone == null || phone.equals("")) {
                    popupMessageUpdatePhone("Profile",CheckOutCartActivity.this);
                }else{
                    viewOrder(requestParams);
                }
            }
        } else if (requestCode == REQUEST_CODE_EDIT_PROFILE && resultCode == RESULT_OK){
            viewOrder(requestParams);
        }
        else if (requestCode == REQUEST_CODE_EDIT_ITEMS &&resultCode ==RESULT_OK){
            if (data!=null&& data.hasExtra ( "orderItem" )){
                OrderItem orderItem = (OrderItem) data.getSerializableExtra ( "orderItem" ) ;
                if (orderItem==null) return;
                //is  same order or not
                boolean isSameOrderItem = false;
                int qty;
                for (int i = 0; i < orderItemArrayList.size(); i++) {
                    OrderItem o = orderItemArrayList.get(i);//item in list , everything is true.
                    if (o.getProductId() == orderItem.getProductId() && o.getComment().equals(orderItem.getComment()) && isSameOption(orderItem.getOption(), o.getOption()) && isSameOption(orderItem.getAddOns(), o.getAddOns())) {
                        qty = o.getQty() + orderItem.getQty();
                        isSameOrderItem = true;
                        o.setQty(qty);
                        break;
                    }
                }
                if (!isSameOrderItem) orderItemArrayList.add(orderItem);
                initPriceDetail ();
                MockupData.getCurrentOrderDetail ().setOrderItemList (orderItemArrayList );
                MockupData.getCurrentOrderDetail().getRequestParams().put("account_id",shop.getShopId());
                MockupData.getCurrentOrderDetail().getRequestParams().put("country_code" ,BaseActivity.countryCode);
                MockupData.getCurrentOrderDetail().setRequestParams(requestParams);
                requestParams.remove ( "items" );//remove items from has map and add new items
                List<ItemRequestParam> itemParamsRequest = new ArrayList<>();
                for (OrderItem orderItems : MockupData.getCurrentOrderDetail ().getOrderItemList ()) {
                    itemParamsRequest.add ( getItemRequestParam(orderItems) );
                }
                requestParams.put ("items",itemParamsRequest );// add new items
                recyclerView.setAdapter ( new CartItemAdapter ( orderItemArrayList,onOrderItemEditQtyListener ) );
//              editOrderItems ( orderItem,orderItem.getQty (),false,true );
                //iseditoption is true because we know it had been edit after send orderitem from add to cart screen
            }
        }
    }

    private boolean isSameOption(List<AddOn> newItem, List<AddOn> oldItem) {
        // In case Options
        boolean isSameOption =false;
        List<Integer> newOptions = new ArrayList<>();
        List<Integer> oldOptions = new ArrayList<>();
        if (newItem.size()==0&&oldItem.size()==0){
            //no option/addon
            return true;
        }
        if (newItem.size() != oldItem.size()) {
            //compare size of add on
            return false;
        } else { // If same size, compare each option item
            int optionId;
            for (int i = 0; i <oldItem.size (); i++) {
                optionId = oldItem.get(i).getId();
                for (int k =0; k< newItem.size ();k++) {
                    //find the same optionid
                    int optId = newItem.get ( k ).getId ();
                    if (optionId == optId) newOptions.add ( optionId );//contain all same option for old compare to new
                }
                oldOptions.add(optionId);//add old item to compare the size of same option and new
            }
            if (newOptions.size()== oldOptions.size()) isSameOption= true;
        }
        return isSameOption;
    }

    private void editOrderItems(OrderItem orderItem,int count,boolean isSameOrderItem,boolean isEditOption ) {
        for (int z = 0; z<orderItemArrayList.size() ; z++) {
            OrderItem o = orderItemArrayList.get(z);
            if (count == 0 && orderItem.getProductId() == o.getProductId() && isSameOption(orderItem.getOption(), o.getOption()) && isSameOption(orderItem.getAddOns(), o.getAddOns()) && o.getComment().equals(orderItem.getComment())) {
                //orderitem remove items
                orderItemArrayList.remove(o);
                initPriceDetail();
                break;
            } else if (count != 0 && orderItem.getProductId() == o.getProductId() && isSameOption(orderItem.getOption(), o.getOption()) && isSameOption(orderItem.getAddOns(), o.getAddOns()) && o.getComment().equals(orderItem.getComment())) {
                    o.setQty(orderItem.getQty()); // Override Qty
                    initPriceDetail();
                    isSameOrderItem = true;
                    break;
            }else if (positions!=null && isEditOption){
                orderItemArrayList.set ( positions ,orderItem);
                initPriceDetail ();
                isSameOrderItem =true;
                break;
            }
        }
        //refresh adapter
        recyclerView.setAdapter(new CartItemAdapter(orderItemArrayList, onOrderItemEditQtyListener));
        initPriceDetail();

        MockupData.getCurrentOrderDetail().getRequestParams().put("account_id",shop.getShopId());
        MockupData.getCurrentOrderDetail().getRequestParams().put("country_code" ,BaseActivity.countryCode);
        MockupData.getCurrentOrderDetail().setRequestParams(requestParams);


        if (!isSameOrderItem && orderItem.getQty() == 0)
            orderItemArrayList.add(orderItem) ;
        //put params to calcute to server
        List<ItemRequestParam> itemParamsRequest = new ArrayList<>();
        for (OrderItem orderItems : orderItemArrayList) {
            itemParamsRequest.add ( getItemRequestParam(orderItems) );
        }
        requestParams.put ("items",itemParamsRequest );
        saveLatestStateItems ( orderItemArrayList );
        Intent data = new Intent();
        data.putExtra("orderItem", orderItem);
        data.putExtra("orderItemQty", (Serializable) orderItemArrayList);
        MockupData.getCurrentOrderDetail().setOrderItemList(orderItemArrayList);
        data.putExtra ( "subtotal",valuesDataIntent );
        setResult(RESULT_OK, data);
    }

    private  void popupMessageUpdatePhone(String title, Activity activity) {
        Util.firebaseAnalytics(this,Constant.ERROR_MESSAGE,"user_information_missing");
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(CheckOutCartActivity.this,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(R.string.you_miss_data);
        builder.setNegativeButton(getResources().getString(R.string.cancel), null);
        builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                viewProfile();
            }
        });
        builder.show();
    }

    private void viewProfile(){
        if(Constant.getBaseUrl()!=null){
            String token = Util.getString("token", CheckOutCartActivity.this);
            if (token != null && !token.isEmpty()) {
                new ProfileWs().viewProfiles(CheckOutCartActivity.this, new ProfileWs.ProfileViewAndUpdate() {
                    @Override
                    public void onLoadProfileSuccess(User user) {
                        Intent intent = new Intent(CheckOutCartActivity.this, EditProfileActivity.class);
                        intent.putExtra("user",user);
                        startActivityForResult(intent, REQUEST_CODE_EDIT_PROFILE);
                    }

                    @Override
                    public void onError(String errorMessage) {
                        if (errorMessage.equalsIgnoreCase("Unauthenticated.")) {
                            Util.clearString(CheckOutCartActivity.this);
                        }else if (isConnectedToInternet ( CheckOutCartActivity.this ))
                            Util.popupMessage(getString(R.string.error), errorMessage,CheckOutCartActivity.this);
                    }

                    @Override
                    public void onUpdateProfilesSuccess(boolean isSendSms, User user) {

                    }



                });
            }
        }

    }
}
