package com.station29.mealtemple.ui.delivery_package;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputEditText;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.delivery_package.adapter.SelectItemsCategoryAdapter;
import com.station29.mealtemple.ui.home.MapsActivity;
import com.station29.mealtemple.util.AbsolutefitLayourManager;

import java.util.ArrayList;

public class DropPackageActivity extends BaseActivity {

    private EditText addressTxt,descriptionTxt;
    private TextView dropTv;
    private TextInputEditText  txtUserName,txtPhone;
    private Spinner chooseSpinner;
    private CountryCodePicker ccp;
    private final int REQUEST_ADDRESS_DROP = 3324;
    private String cateGory;
    private int mSelectedPos = -1 ,spinnerPosition;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_package);
        TextView titleTv = findViewById(R.id.title);
        ImageView backBtn = findViewById(R.id.backButton);
        Button btnConfirm = findViewById(R.id.btnConfirm);
        dropTv = findViewById(R.id.txtDrop);
        addressTxt = findViewById(R.id.add_detail);
        txtPhone = findViewById(R.id.txRePhone);
        txtUserName = findViewById(R.id.user_names);
        descriptionTxt = findViewById(R.id.descriptionTxt);
        chooseSpinner = findViewById(R.id.chooseW);
        ccp = findViewById(R.id.ccp);

        ccp.setDefaultCountryUsingNameCodeAndApply(countryCode);

        titleTv.setText(R.string.drop_off_package);
        dropTv.setText(MockupData.DELIVERY_ADDRESS);
        if(MockupData.getState()!=null){
            mSelectedPos = MockupData.getState();
        }
        RecyclerView recyclerView = findViewById(R.id.listca);
        final AbsolutefitLayourManager absolutefitLayourManager = new AbsolutefitLayourManager(DropPackageActivity.this,2, RecyclerView.HORIZONTAL, false,4);
        recyclerView.setLayoutManager(absolutefitLayourManager);
        recyclerView.setAdapter(new SelectItemsCategoryAdapter(MockupData.getCategory(), itemClick,mSelectedPos));


        backBtn.setOnClickListener(v -> onBackPressed());

        dropTv.setOnClickListener(v -> {
            Intent intent = new Intent(DropPackageActivity.this, MapsActivity.class);
            startActivityForResult(intent,REQUEST_ADDRESS_DROP);
        });

        ArrayList<String> listWeight = new ArrayList<>();
        listWeight.add(getString(R.string.choose));
        listWeight.add("0.25 kg");
        listWeight.add("0.5 kg");
        for(int i = 1 ; i <= 15 ; i++){
            listWeight.add(i+" kg");
        }
        ArrayAdapter<String> serviceAdapter = new ArrayAdapter<String>(DropPackageActivity.this, android.R.layout.simple_spinner_item, listWeight);
        chooseSpinner.setAdapter(serviceAdapter);

        chooseSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnConfirm.setOnClickListener(v -> {
            if(txtUserName.getText().toString().isEmpty()){
                Toast.makeText(DropPackageActivity.this, getString(R.string.please_input_your_name),Toast.LENGTH_LONG).show();
            } else if(txtPhone.getText().toString().isEmpty()){
                Toast.makeText(DropPackageActivity.this,getString(R.string.please_input_receiver_phone) ,Toast.LENGTH_LONG).show();
            } else if(spinnerPosition == 0){
                Toast.makeText(DropPackageActivity.this,getString( R.string.please_input_items_weight),Toast.LENGTH_LONG).show();
            }
            else {
                if(cateGory.isEmpty()){
                    cateGory = getString(R.string.document);
                }
                Intent intent = new Intent();
                intent.putExtra("address_drop",dropTv.getText().toString());
                intent.putExtra("add_detail",addressTxt.getText().toString());
                intent.putExtra("name_drop",txtUserName.getText().toString());
                intent.putExtra("phone_drop",ccp.getDefaultCountryCodeWithPlus()+" "+ formatPhone(txtPhone.getText().toString()));
                intent.putExtra("category_drop",cateGory);
                intent.putExtra("sate",mSelectedPos);
                intent.putExtra("weight",chooseSpinner.getSelectedItem().toString());
                intent.putExtra("description_drop",descriptionTxt.getText().toString());
                intent.putExtra("lat_drop",latitude);
                intent.putExtra("lng_drop",longitude);
                intent.putExtra("country_code",ccp.getFullNumber());
                intent.putExtra("user",user);

                MockupData.setReceiverAddressDrop(dropTv.getText().toString());
                MockupData.setReceiverName(txtUserName.getText().toString());
                MockupData.setReceiverPhone(ccp.getDefaultCountryCodeWithPlus()+" "+ formatPhone(txtPhone.getText().toString()));
                MockupData.setReceiverAddress(addressTxt.getText().toString());
                MockupData.setReceiverCategory(cateGory);
                MockupData.setItemWeight(chooseSpinner.getSelectedItem().toString());
                MockupData.setReceiverDescription(descriptionTxt.getText().toString());
                setResult(RESULT_OK,intent);
                finish();
            }

        });
        if(getIntent().hasExtra("user")){
            user = (User) getIntent().getSerializableExtra("user");
        }
        if(getIntent().hasExtra("receiver_name")){
            String name = getIntent().getStringExtra("receiver_name");
            txtUserName.setText(name);
        }
        if(getIntent().hasExtra("receiver_phone")){
            String phone = getIntent().getStringExtra("receiver_phone");
            String receiverPhone = phone.replace("+855","");
            txtPhone.setText(receiverPhone.trim());
        }
        if(getIntent().hasExtra("add_detail")){
            String addDetail = getIntent().getStringExtra("add_detail");
            addressTxt.setText(addDetail);
        }
       if(getIntent().hasExtra("description")){
            String description = getIntent().getStringExtra("description");
            descriptionTxt.setText(description);
        }
        if(getIntent().hasExtra("cate_gory")){
            String cate = getIntent().getStringExtra("cate_gory");
           cateGory = cate;
            MockupData.setState(0);
        }
        if(getIntent().hasExtra("weight_tx")){
            String itemWeight = getIntent().getStringExtra("weight_tx");
            for(int i=0;i<listWeight.size();i++){
                if(listWeight.get(i).equals(itemWeight)){
                    chooseSpinner.setSelection(i);
                    break;
                }

            }
        }

    }

    private String formatPhone(String fullPhoneNumber) {
        String code;
        if (fullPhoneNumber.length() >= 8) {
            if (txtPhone.getText().toString().equals(" ") || txtPhone.getText().toString().isEmpty())
                return fullPhoneNumber;
            code = fullPhoneNumber.substring(0, ccp.getFullNumber().length());
            fullPhoneNumber = (code.contentEquals(ccp.getFullNumber()) ? fullPhoneNumber.substring(ccp.getFullNumber().length()) : fullPhoneNumber);
            if(fullPhoneNumber.length() < 8){
                return txtPhone.getText().toString();
            }
        } else {//855
            //return old phone numberx
            return fullPhoneNumber;
        }
        return fullPhoneNumber;
    }

    private SelectItemsCategoryAdapter.CategoryItemClick itemClick = new SelectItemsCategoryAdapter.CategoryItemClick() {
        @Override
        public void onCategoryClick(String category,int position,int stat) {
            mSelectedPos = position;
            cateGory =  category;
            MockupData.setState(mSelectedPos);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null){
            if (requestCode == REQUEST_ADDRESS_DROP && resultCode == RESULT_OK){
                if(data.hasExtra("delivery_address")){
                    String deliveryAddress = data.getStringExtra("delivery_address");
                    dropTv.setText(deliveryAddress);
                }
                if(data.hasExtra("lat")){
                    latitude = data.getDoubleExtra("lat",-1);
                }
                if(data.hasExtra("lng")){
                    longitude = data.getDoubleExtra("lng",-1);
                }
            }
        }
    }


}