package com.station29.mealtemple.ui.payment.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.PaymentTransaction;
import com.station29.mealtemple.util.DateUtil;

import java.util.ArrayList;
import java.util.List;

public class PaymentTransactionAdapter extends RecyclerView.Adapter<PaymentTransactionAdapter.ViewHolder>  {

    private List<PaymentTransaction> paymentTransactions = new ArrayList<>();
    private OnClickPaymentTransaction onClickPaymentTransaction;

    public PaymentTransactionAdapter(List<PaymentTransaction> paymentTransactions, OnClickPaymentTransaction onClickPaymentTransaction) {
        this.paymentTransactions = paymentTransactions;
        this.onClickPaymentTransaction = onClickPaymentTransaction;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order, parent, false);
        return new PaymentTransactionAdapter.ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentTransaction paymentTransaction = paymentTransactions.get(position);
        if(paymentTransaction != null){

            if(paymentTransaction.getStatus().equals("SUCCESSFULL"))
                holder.status.setTextColor(Color.parseColor("#3d752d"));
            else if(paymentTransaction.getStatus().equals("PENDING"))
                holder.status.setTextColor(Color.parseColor("#6d6d6d"));
            else holder.status.setTextColor(Color.parseColor("#af1022"));

            String type = "";
            if(paymentTransaction.getType() != null)
                if(paymentTransaction.getType().equals("TOP_UP") || paymentTransaction.getType().equals("IN")) {
                    type = "+";
                    holder.amountTv.setTextColor(Color.parseColor("#3d752d"));
                } else {
                    type = "-";
                    holder.amountTv.setTextColor(Color.parseColor("#af1022"));
                }
            else holder.amountTv.setTextColor(Color.parseColor("#9e9e9e"));

            holder.descriptionTv.setTextSize(17);
            holder.order_number.setVisibility(View.GONE);
            holder.row.setPadding(20,20,20,25);
            holder.descriptionTv.setText(paymentTransaction.getDescription());
            holder.dateTv.setText(DateUtil.formatDateUptoCurrentRegion(paymentTransaction.getCreatedAt()));
            if(paymentTransaction.getAmount() != null)
                holder.amountTv.setText(String.format("%s %s%s",paymentTransaction.getCurrencyCode(),type,paymentTransaction.getAmount()));
            else holder.amountTv.setText("");
            holder.status.setText(paymentTransaction.getStatus());

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickPaymentTransaction.onClickTransaction(paymentTransaction);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return paymentTransactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView amountTv, dateTv, descriptionTv , status,order_number;
        View row;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            order_number = itemView.findViewById(R.id.order_number);
            amountTv = itemView.findViewById(R.id.total_price);
            dateTv = itemView.findViewById(R.id.date);
            descriptionTv = itemView.findViewById(R.id.shop_name);
            row = itemView;
            status = itemView.findViewById(R.id.status);
        }
    }

    public interface OnClickPaymentTransaction{
        void onClickTransaction(PaymentTransaction paymentTransaction);
    }

    public void clear() {
        int size = paymentTransactions.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                paymentTransactions.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }

}
