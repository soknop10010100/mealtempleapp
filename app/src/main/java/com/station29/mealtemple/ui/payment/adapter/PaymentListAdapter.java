package com.station29.mealtemple.ui.payment.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.PaymentService;

import java.util.List;

public class PaymentListAdapter extends RecyclerView.Adapter<PaymentListAdapter.PaymentViewHolder> {

    private final SetPaymentClick setPaymentClick;
    private final String clickType;
    private final int size;
    private final List<PaymentService> paymentServices;

    public PaymentListAdapter(SetPaymentClick setPaymentClick , String clickType, int size, List<PaymentService> paymentServices) {
        this.setPaymentClick = setPaymentClick;
        this.clickType = clickType;
        this.size = size;
        this.paymentServices = paymentServices;
    }

    @NonNull
    @Override
    public PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate( (!clickType.equals("order")) ? R.layout.layout_payment_list : R.layout.layout_payment_list_dialog, parent, false);
        return new PaymentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentViewHolder holder, int position) {
        PaymentService paymentService = paymentServices.get(position);
        if(paymentService != null && paymentService.getIsCash() == 0 && paymentService.getStatus() == 1){
            holder.namePayment.setText(paymentService.getPspName());
            Glide.with(holder.imgPayment.getContext())
                    .load(paymentService.getLogo())
                    .into( holder.imgPayment );
            holder.view.setOnClickListener(v -> setPaymentClick.onClickPaymentClick(paymentService));
        }
    }

    @Override
    public int getItemCount() {
        return size;
    }

    static class PaymentViewHolder extends RecyclerView.ViewHolder {

        private final ImageView imgPayment;
        private final TextView namePayment;
        private final View view;

        public PaymentViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;

            imgPayment = itemView.findViewById(R.id.payment_image);
            namePayment = itemView.findViewById(R.id.payment_name);
        }
    }

    public interface SetPaymentClick{
        void onClickPaymentClick(PaymentService paymentService);
    }
}


