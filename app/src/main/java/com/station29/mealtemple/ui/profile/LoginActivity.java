package com.station29.mealtemple.ui.profile;

import android.app.Person;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.LoginWs;
import com.station29.mealtemple.api.request.SignUpWs;
import com.station29.mealtemple.fcm.MyFirebaseMessagingService;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import static com.station29.mealtemple.util.Util.popupMessage;

public class LoginActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener{

    public static boolean isDidLogin = false;
    private final static int REQUEST_CODE_SIGNUP = 564, RC_SIGN_IN = 1;
    private String action = null;
    private EditText editTextPhone,editTextEmail;
    private CountryCodePicker ccp;
    private String EMAIL = "Use your Email Address", PHONE = "Use your Phone Number";
    private String inputType = EMAIL,userType = "USER_ANDROID";
    private CallbackManager callbackManager;
    private LoginButton loginButton;
    private GoogleSignInClient googleSignInClient;
    private GoogleApiClient googleApiClient;
    private SignInButton googleSignInButton;
    private HashMap<String,String>account = new HashMap<> (  );


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getExistingFirebaseToken();
//        EMAIL = getString(R.string.use_your_email_address);
//        PHONE = getString(R.string.use_your_phone);

        loginButton = findViewById(R.id.login_button);
        googleSignInButton = findViewById(R.id.sign_in_button);
        ccp = findViewById(R.id.ccp);
        ccp.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);

        findViewById ( R.id.view_register ).setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class );
                if (getIntent().hasExtra("action") && getIntent().getStringExtra("action").equals("checkout")) {
                    intent.putExtra("action", "checkout");
                }
                startActivityForResult (intent, REQUEST_CODE_SIGNUP);
                finish ();
            }
        } );

        findViewById(R.id.btnForgetPass).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this , ForgotPasswordActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        if (getIntent() != null && getIntent().getStringExtra("action") != null) {
            action = getIntent().getStringExtra("action");
        }

        findViewById(R.id.btnBackLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView appVersionTv = findViewById(R.id.version_app);
        appVersionTv.setText ( String.format ( Locale.US , "%s %s (%s)",getResources ().getString ( R.string.version ),BuildConfig.VERSION_NAME,BuildConfig.VERSION_CODE ) );

        final EditText passwordEt = findViewById(R.id.txtpassword);
        editTextEmail = findViewById(R.id.txtEmail);
        findViewById(R.id.email_border).setVisibility(View.GONE);
        editTextPhone = findViewById(R.id.txtPhoneNumber);

        final TextView start = findViewById(R.id.text1);

        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                view.setEnabled(false);
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view.setEnabled(true);
                    }
                },500);
                if (Constant.getBaseUrl ()!=null) loginPhoneEmail();
            }
        });

        final TextView switchInputType = findViewById(R.id.btnChange);
        switchInputType.setTag(inputType);
        switchInputType.setText(getString(R.string.use_your_email));
        switchInputType.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                MockupData.EMAIL = "";
                MockupData.LOGIN_PHONE = "";
                if (switchInputType.getTag().equals(PHONE)) {
                    inputType = EMAIL;
                    start.setText ( getString(R.string.start_with_your_phone) );
                    findViewById(R.id.email_border).setVisibility(View.GONE);
                    findViewById(R.id.phone_border).setVisibility(View.VISIBLE);
                    ccp.setVisibility(View.VISIBLE);
                    passwordEt.clearFocus ();
                    editTextEmail.getText().clear();
                    switchInputType.setText(getString(R.string.use_your_email));
                } else {
                    inputType = PHONE;
                    start.setText ( getString(R.string.start_with_your_email) );
                    findViewById(R.id.phone_border).setVisibility(View.GONE);
                    ccp.setVisibility(View.GONE);
                    findViewById(R.id.email_border).setVisibility(View.VISIBLE);
                    passwordEt.clearFocus ();
                    editTextPhone.getText().clear();
                    switchInputType.setText(getString(R.string.use_your_phone));
                }
                v.setTag(inputType);
            }
        });

        //login with facebook
        loginButton.setPermissions(Arrays.asList("public_profile","email"));
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            Profile profile=Profile.getCurrentProfile();
                            HashMap<String,String> accountFacebook = new HashMap<>();
                            accountFacebook.put("provider_id",object.getString("id"));
                            if(object.has("email"))
                                accountFacebook.put("email",object.getString("email"));
                            accountFacebook.put("firstname",object.getString("first_name"));
                            accountFacebook.put("lastname",object.getString("last_name"));
                            int dimensionPixelSize = getResources().getDimensionPixelSize(com.facebook.R.dimen.com_facebook_profilepictureview_preset_size_large);
                            accountFacebook.put("image",String.valueOf(profile.getProfilePictureUri(dimensionPixelSize,dimensionPixelSize)));
                            new SignUpWs().SignUpFbGoogle("facebook",accountFacebook,requestServer);
                        } catch (JSONException e) {
                            popupMessage("Error",getString(R.string.something_went),LoginActivity.this);
                        }
                    }
                });
                Bundle parameter = new Bundle();
                parameter.putString("fields","email,id,first_name,last_name,picture.type(large)");
                graphRequest.setParameters(parameter);
                graphRequest.executeAsync();

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                popupMessage(getString(R.string.error),getString(R.string.something_went_wrong),LoginActivity.this);
            }
        });

        // login with google
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestProfile()
                .build();
        googleApiClient=new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    public void onClick(View v) {
//        ImageButton fb = findViewById(R.id.btnFacebook);
//        ImageButton google = findViewById(R.id.btnGoogle);
        View fb = findViewById(R.id.view_facebook);
        View google = findViewById(R.id.view_google);
        if (v == fb) {
            loginButton.performClick();
        }else if(v == google ){
           signIn();
        }
    }

    private View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (!hasFocus){
                Util.hideSoftKeyboard(view);
            }
        }
    };
        //here
    private void handleSignInResult(@NotNull GoogleSignInResult result){
            if(result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                HashMap<String,String> accountGoogle = new HashMap<> (  ) ;
                accountGoogle.put("provider_id",account.getId());
                accountGoogle.put("email",account.getEmail());
                accountGoogle.put("firstname",account.getGivenName());
                Log.d("firstname",account.getFamilyName());
                String name = account.getFamilyName();
                accountGoogle.put("lastname",account.getFamilyName());
                Log.d("lastname",account.getGivenName());
                if(account.getPhotoUrl()!=null)
                    accountGoogle.put("image",String.valueOf(account.getPhotoUrl()));
                googleSignInClient.signOut();
                new SignUpWs().SignUpFbGoogle("google", accountGoogle, requestServer);

            } else {
                popupMessage(getString(R.string.error),getString(R.string.something_went_wrong),LoginActivity.this);
            }
        }

    private SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer() {
        @Override
        public void onRequestSuccess() {

        }

        @Override
        public void onRequestToken(String token, int userId , User user) {
            googleSignInClient.signOut();
            if(user.getPhoneNumber()!=null)
                MockupData.LOGIN_PHONE = user.getPhoneNumber();
            if(user.getFirstName()!=null && user.getLastName()!=null)
                MockupData.USER_NAME = user.getFirstName()+" "+user.getLastName();
            Util.saveString("token", token, LoginActivity.this);
            Util.saveString("user_id", String.valueOf(userId),LoginActivity.this);
            MyFirebaseMessagingService.sendRegistrationToServer(Util.getString(Constant.FIREBASE_TOKEN, LoginActivity.this),LoginActivity.this);
            if (getIntent() != null && getIntent().hasExtra("action") && getIntent().getStringExtra("action").equalsIgnoreCase("checkout")) {
                Intent intent = new Intent();
                intent.putExtra("userPhone",user.getPhoneNumber());
                setResult(RESULT_OK);
                finish();
            } else {
                gotoProfile();
            }
        }

        @Override
        public void onRequestFailed(String message) {

        }
    };

    private void signIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(intent, RC_SIGN_IN);
    }

    public void gotoProfile(){
        Intent intent = new Intent(this,ProfileFragment.class);
        intent.putExtra("action", action);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);

        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onStop() {
        super.onStop();
        LoginManager.getInstance().logOut();
    }

    public void loginPhoneEmail(){
        MockupData.setAccountSignUp(null);
        final EditText passwordEt = findViewById(R.id.txtpassword);

        if (!editTextPhone.getText().toString().isEmpty()) {
            account.put("phone", editTextPhone.getText().toString().trim());
            MockupData.LOGIN_PHONE =  "+"+ccp.getFullNumber()+editTextPhone.getText().toString();
        } if (!editTextEmail.getText().toString().isEmpty()){
            account.put("email",editTextEmail.getText().toString().trim());
        }
        account.put("password",passwordEt.getText().toString().trim());
        account.put("phone_code",ccp.getDefaultCountryCodeWithPlus ());
        account.put("function","Customer");
        MockupData.EMAIL = editTextEmail.getText().toString();
        new LoginWs().loginPhoneEmail(account, LoginActivity.this , loadLoginCallback);
    }

    private LoginWs.LoadLoginCallback loadLoginCallback = new LoginWs.LoadLoginCallback() {
        @Override
        public void onLoadSuccess(String token, int userId,User user) {
            if(user.getPhoneNumber()!=null)
                MockupData.LOGIN_PHONE = user.getPhoneNumber();
            if(user.getFirstName()!=null && user.getLastName()!=null)
                MockupData.USER_NAME = user.getFirstName()+" "+user.getLastName();
            Util.saveString("token", token, LoginActivity.this);
            Util.saveString("user_id", String.valueOf(userId),LoginActivity.this);
            String fcmToken = Util.getString(Constant.FIREBASE_TOKEN, LoginActivity.this);
            MyFirebaseMessagingService.sendRegistrationToServer(fcmToken,LoginActivity.this);
            if (getIntent() != null && getIntent().hasExtra("action") && getIntent().getStringExtra("action").equalsIgnoreCase("checkout")) {
                Intent intent = new Intent();
                intent.putExtra("userPhone",user.getPhoneNumber());
                setResult(RESULT_OK);
                finish();
            } else {
                gotoProfile();
            }
        }

        @Override
        public void onLoadFailed(String errorMessage, int code) {
            if(code == 403){
                Toast toast = Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG);
                toast.show();
                Intent intent = new Intent(LoginActivity.this,ConfirmSmsActivity.class);
                account.put("user_type",userType);
                intent.putExtra("phoneEmail",account);
                startActivity(intent);
            }else popupMessage(null, errorMessage, LoginActivity.this);
        }
    };

    private void getExistingFirebaseToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        if (task.getResult()!= null) {
                            String token = task.getResult().getToken();
                            Util.saveString(Constant.FIREBASE_TOKEN, token, LoginActivity.this);
                            Log.d(Constant.FIREBASE_TOKEN, token);
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
