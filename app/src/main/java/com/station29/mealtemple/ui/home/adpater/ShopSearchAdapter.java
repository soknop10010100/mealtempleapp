package com.station29.mealtemple.ui.home.adpater;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.Shop;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ShopSearchAdapter extends RecyclerView.Adapter<ShopSearchAdapter.SearchShopViewHolder> {

    private List<Shop> shopList ;
    private OnShopClickListener mListener ;

    public ShopSearchAdapter(List<Shop> shopList,OnShopClickListener listener) {
        this.shopList = shopList;
        this.mListener = listener;
    }
    @NonNull
    @Override
    public SearchShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_search, parent, false);
        return new SearchShopViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchShopViewHolder holder, int position) {
        final Shop shop = shopList.get(position);
        if (shop != null) {
            holder.shopNameTv.setText(shop.getName());
            Glide.with(holder.circleImageView.getContext())
                    .load(shop.getShopProfileImage())
                    .into(holder.circleImageView);
            holder.shopNameTv.setOnClickListener ( new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    mListener.onShopClick ( holder.shopNameTv,shop );
                }
            } );
        }
    }

    @Override
    public int getItemCount() {
        return shopList.size();
    }

    class SearchShopViewHolder extends RecyclerView.ViewHolder {

        TextView shopNameTv;
        View view;
        CircleImageView circleImageView;
        SearchShopViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            shopNameTv = itemView.findViewById(R.id.shop_name );
            circleImageView = itemView.findViewById(R.id.shop_profileImage_homescreen);
        }
    }

    public interface OnShopClickListener {
        void onShopClick(View itemView, Shop shop);
    }

}
