package com.station29.mealtemple.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.station29.mealtemple.R;

import org.jetbrains.annotations.NotNull;

public class ShippingMethodDialogFragment extends BottomSheetDialogFragment {

    private onClickOnShipping onClickOnShipping;
    private LinearLayout btnByMyOwn , btnKiwiDelivery;
    private RadioButton radioMyOwn , radioKiwiDelivery;
    private TextView txtMyOwn , txtKiwiDelivery;
    private int position;
    private ImageView closeButton;

    public static ShippingMethodDialogFragment newInstance(int position) {
        Bundle args = new Bundle();
        ShippingMethodDialogFragment fragment = new ShippingMethodDialogFragment();
        args.putInt("position",position);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
          return inflater.inflate(R.layout.fragment_shipping_method_dialog, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        initData();
        initAction();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        final Fragment parent = getParentFragment();
        if (parent != null) {
            onClickOnShipping = (ShippingMethodDialogFragment.onClickOnShipping) parent;
        } else {
            onClickOnShipping = (ShippingMethodDialogFragment.onClickOnShipping) context;
        }
    }

    private void initData(){
        if(getArguments() != null){
            position = getArguments().getInt("position");
        }
    }

    private void initView(View view){
        btnByMyOwn = view.findViewById(R.id.btnByMyOwn);
        btnKiwiDelivery = view.findViewById(R.id.btnKiwiDelivery);
        radioKiwiDelivery = view.findViewById(R.id.kiwiRadioBtn);
        radioMyOwn = view.findViewById(R.id.OwnRadioBtn);
        txtKiwiDelivery = view.findViewById(R.id.textKiwiDelivery);
        txtMyOwn = view.findViewById(R.id.textMyOwn);
        closeButton = view.findViewById(R.id.closeButton);
    }

    private void initAction(){

        if(position == 1){
            radioMyOwn.setChecked(true);
            radioKiwiDelivery.setChecked(false);
        } else if (position == 2) {
            radioMyOwn.setChecked(false);
            radioKiwiDelivery.setChecked(true);
        }

        radioMyOwn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickOnShipping.onClickShippingType(txtMyOwn.getText().toString(),1);
                dismiss();
            }
        });
        radioKiwiDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickOnShipping.onClickShippingType(txtKiwiDelivery.getText().toString(),2);
                dismiss();
            }
        });
        btnKiwiDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickOnShipping.onClickShippingType(txtKiwiDelivery.getText().toString(),2);
                dismiss();
            }
        });
        btnByMyOwn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickOnShipping.onClickShippingType(txtMyOwn.getText().toString(),1);
                dismiss();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    public interface onClickOnShipping{
        void onClickShippingType(String txtChoose, int position);
    }
}
