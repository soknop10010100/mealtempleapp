package com.station29.mealtemple.ui.delivery_package;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.PaymentHistory;
import com.station29.mealtemple.api.model.SendPackageDetail;
import com.station29.mealtemple.api.model.SendPackageRequestParam;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.api.request.SendPackageWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.OrderConfirmationActivity;
import com.station29.mealtemple.ui.payment.PasswordConfirmActivity;
import com.station29.mealtemple.ui.payment.PaymentDepositActivity;
import com.station29.mealtemple.ui.payment.PaymentMethodDialogFragment;
import com.station29.mealtemple.ui.payment.adapter.PaymentHistoryAdapter;
import com.station29.mealtemple.ui.payment.PaymentMethodActivity;
import com.station29.mealtemple.ui.profile.SetChangePinActivity;
import com.station29.mealtemple.ui.taxi.FindDriverActivity;
import com.station29.mealtemple.util.TopUpPayment;
import com.station29.mealtemple.util.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.station29.mealtemple.util.Util.formatDuration;

public class DeliveryPackageActivity extends BaseActivity implements PaymentMethodDialogFragment.PaymentClickListener {

    private LinearLayout linearPick;
    private double latitude_drop ,longitude_drop, latitude_pick,longitude_pick;
    private String deliveryAddress ="",name_sender="",phone_receiver="",name_receiver="",
            cate="",description="",address_pick="",phone_sender="",detailPickup="",weight="",detailDrop="",phoneCode,pinCode;
    private TextView dropOff,pickOff,senderName,senderPhone,receiverName,
            receiverPhone,cateTv,weightTv,priceTv,pricePerDisTv,disTv,titleProgress;
    private final int REQUEST_ADDRESS_PICK_Up = 3325,REQUEST_ADDRESS_DROP_OFF = 3326,REQUEST_PAYMENT_METHOD= 2623,REQUEST_DEPOSIT_METHOD=123,REQUEST_PIN_CODE = 5234,REQUEST_PIN_CODE_PIN = 5235;
    private TextView setPaymentMethod;
    private View progressLoading;
    private final String PAYMENT_CASH = "Pay by cash", PAY_WALLET = "Pay by wallet";
    private final int REQUEST_SEND_PACKAGE= 1245;
    private List<Wallets> walletsList = new ArrayList<>();
    private List<PaymentHistory> paymentHistories = new ArrayList<>();
    private int walletId,orderPaymentId;
    private final SendPackageRequestParam requestParam = new SendPackageRequestParam();
    private boolean checkPayOnline = false,checkShipping = false;
    private PaymentHistory paymentHistory;
    private User myUser;
    private  MaterialButton btnNext;
    boolean isPin,isOnResume;
    private SendPackageDetail sendPackageDetail;
    private LinearLayout linearLayoutDescription;
    private TextView descriptionTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_package);

        PaymentHistoryAdapter.noteHistoryPostion = -1;
        PaymentMethodDialogFragment.notePosition = -1;

        TextView titleTv = findViewById(R.id.title);
        ImageView backBtn = findViewById(R.id.backButton);
        btnNext = findViewById(R.id.btnNextPackage);
        RelativeLayout btnPayment = findViewById(R.id.payment);
        dropOff = findViewById(R.id.deliveryDrop);
        pickOff = findViewById(R.id.deliveryPickUp);
        senderName = findViewById(R.id.senderName);
        senderPhone = findViewById(R.id.senderPhone);
        receiverName = findViewById(R.id.receiverName);
        receiverPhone = findViewById(R.id.receiverPhone);
        cateTv = findViewById(R.id.cateItem);
        weightTv = findViewById(R.id.itemWi);
        priceTv = findViewById(R.id.totalP);
        linearPick = findViewById(R.id.sender);
        setPaymentMethod  = findViewById(R.id.setPaymentMethod);
        pricePerDisTv = findViewById(R.id.price_per);
        disTv = findViewById(R.id.dist);
        progressLoading = findViewById(R.id.progressBar);
        titleProgress = findViewById(R.id.title_view);
        linearLayoutDescription = findViewById(R.id.lin_description);
        descriptionTv = findViewById(R.id.descriptiontext);
        progressLoading.setVisibility(View.GONE);
        if(Constant.getBaseUrl()!=null) getWallet();
        if(getIntent().hasExtra("user")){
            myUser = (User) getIntent().getSerializableExtra("user");
            if(myUser != null)
                isPin = myUser.isConfirmPin();
        }

        if(getIntent().hasExtra("pick")){
            String pick = getIntent().getStringExtra("pick");
            pickOff.setText(pick);
        }else {
            pickOff.setText(MockupData.DELIVERY_ADDRESS);
            latitude_pick = BaseActivity.latitude;
            longitude_pick = BaseActivity.longitude;
        }
        if(getIntent().hasExtra("drop")){
            String drop = getIntent().getStringExtra("drop");
            dropOff.setText(drop);
        }
        if(getIntent()!=null){
            phone_receiver = MockupData.getReceiverPhone();
            name_receiver = MockupData.getReceiverName();
            deliveryAddress = MockupData.getReceiverAddressDrop();
        }
        if( getIntent() != null && getIntent().hasExtra("orderId")){
            deliveryAddress = MockupData.getReceiverAddressDrop();
            address_pick = MockupData.getSenderAddress();
            phone_receiver = MockupData.getReceiverPhone();
            name_receiver = MockupData.getReceiverName();
            cate = MockupData.getReceiverCategory();
            weight = MockupData.getItemWeight();
            detailDrop = MockupData.getDetailDrop();
            detailPickup = MockupData.getDetailPick();

            if(MockupData.getSenderPhone().isEmpty()){
                name_sender = MockupData.USER_NAME;
            } else {
                name_sender = MockupData.getSenderName();
            }
            if(MockupData.getSenderPhone().isEmpty()){
                phone_sender = MockupData.LOGIN_PHONE;
            }else {
                phone_sender = MockupData.getSenderPhone();
            }
            senderName.setText(name_sender);
            senderPhone.setText(phone_sender);
            linearPick.setVisibility(View.VISIBLE);
            receiverName.setText(MockupData.getReceiverName());
            receiverPhone.setText(MockupData.getReceiverPhone().trim());
            cateTv.setText(MockupData.getReceiverCategory());
            weightTv.setText(MockupData.getItemWeight());
            pricePerDisTv.setText(MockupData.getPricePerUit());
            priceTv.setText(MockupData.getTotalPrice());
            disTv.setText(MockupData.getDISTANCE());

            if(getIntent().hasExtra("lat_p")){
                latitude_pick = getIntent().getDoubleExtra("lat_p",-1);
            }
            if(getIntent().hasExtra("lng_p")){
                longitude_pick = getIntent().getDoubleExtra("lng_p",-1);
            }
            if(getIntent().hasExtra("lat_d")){
                latitude_drop = getIntent().getDoubleExtra("lat_d",-1);
            }
            if(getIntent().hasExtra("lng_d")){
                longitude_drop = getIntent().getDoubleExtra("lng_d",-1);
            }
            if(getIntent().hasExtra("pick")){
                String pick = getIntent().getStringExtra("pick");
                pickOff.setText(pick);
            }
            if(getIntent().hasExtra("drop")){
                String drop = getIntent().getStringExtra("drop");
                dropOff.setText(drop);
            }
            if(getIntent().hasExtra("phoneCode")){
                phoneCode = getIntent().getStringExtra("phoneCode");
            }
            if(getIntent().hasExtra("description")){
                description = getIntent().getStringExtra("description");
            }
            getData();
            if(!description.isEmpty()){
                linearLayoutDescription.setVisibility(View.VISIBLE);
                descriptionTv.setText(description);
            }
            btnNext.setEnabled(true);
            progressLoading.setVisibility(View.GONE);
        } else {
            receiverName.setText("");
            receiverPhone.setText("");
            cateTv.setText(getString(R.string.document));
            weightTv.setText("");
            detailDrop = "";
            description = "";
            linearLayoutDescription.setVisibility(View.GONE);
            detailPickup = "";
        }

        titleTv.setText(R.string.delivery_package);
      //  pickOff.setText(MockupData.DELIVERY_ADDRESS);

        name_sender = MockupData.USER_NAME;
        phone_sender = MockupData.LOGIN_PHONE;
        senderName.setText(name_sender);
        senderPhone.setText(phone_sender);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
                Intent intent = new Intent(DeliveryPackageActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!dropOff.getText().toString().equals("")){
                    if(!checkPayOnline)
                        if (walletId != -1 && pinCode == null || walletId != -1 && pinCode.equals("")){
                            if(!isPin){
                                Intent intent = new Intent(DeliveryPackageActivity.this, SetChangePinActivity.class);
                                intent.putExtra("user",myUser);
                                startActivityForResult(intent,REQUEST_PIN_CODE_PIN);
                            }else {
                                Intent intent = new Intent(DeliveryPackageActivity.this , PasswordConfirmActivity.class);
                                startActivityForResult(intent,REQUEST_PIN_CODE);
                            }
                        } else sendPackage();
                    else {
                        if (pinCode == null || pinCode.equals("")) {
                            if (!myUser.isConfirmPin()) {
                                Intent intent = new Intent(DeliveryPackageActivity.this, SetChangePinActivity.class);
                                intent.putExtra("user", myUser);
                                startActivityForResult(intent, REQUEST_PIN_CODE_PIN);
                            } else {
                                Intent intent = new Intent(DeliveryPackageActivity.this, PasswordConfirmActivity.class);
                                startActivityForResult(intent, REQUEST_PIN_CODE);
                            }
                        }
                    }
                } else {
                    Util.popupMessage(null,getString(R.string.please_input_drop_off),DeliveryPackageActivity.this);
                }
            }
        });

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPaymentDialog(walletsList,paymentHistories);
            }
        });
        pickOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeliveryPackageActivity.this,PickupPackageActivity.class);
                intent.putExtra("name_sender",senderName.getText().toString());
                intent.putExtra("name_phone",senderPhone.getText().toString());
                intent.putExtra("detail_pick",detailPickup);
                intent.putExtra("late",latitude_pick );
                intent.putExtra("lng",longitude_pick);
                intent.putExtra("user",myUser);
                startActivityForResult(intent,REQUEST_ADDRESS_PICK_Up);
            }
        });
        dropOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DeliveryPackageActivity.this,DropPackageActivity.class);
                intent.putExtra("receiver_name",receiverName.getText().toString());
                intent.putExtra("receiver_phone",receiverPhone.getText().toString());
                intent.putExtra("weight_tx",weightTv.getText().toString());
                intent.putExtra("cate_gory",cateTv.getText().toString());
                intent.putExtra("add_detail",detailDrop);
                intent.putExtra("description",description);
                intent.putExtra("user",myUser);
                startActivityForResult(intent,REQUEST_ADDRESS_DROP_OFF);
            }
        });

    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null && resultCode == RESULT_OK){
            if(requestCode == REQUEST_ADDRESS_PICK_Up){

                if(data.hasExtra("detail_pic")){
                    detailPickup = data.getStringExtra("detail_pic");
                    MockupData.setDetailPick(detailPickup);
                }
                if(data.hasExtra("addressPickUp")){
                    address_pick = data.getStringExtra("addressPickUp");
                    if(!detailPickup.isEmpty()) pickOff.setText( detailPickup +", "+ address_pick);
                    else pickOff.setText(address_pick);
                    MockupData.setSenderAddress(address_pick);
                }
                if(data.hasExtra("senderName")){
                    name_sender = data.getStringExtra("senderName");
                    senderName.setText(name_sender);
                    MockupData.setSenderName(name_sender);
                }
                if(data.hasExtra("senderPhone")){
                    phone_sender = data.getStringExtra("senderPhone");
                    senderPhone.setText(phone_sender);
                    MockupData.setSenderPhone(phone_sender);
                }
                if(data.hasExtra("lat_pick")){
                    latitude_pick = data.getDoubleExtra("lat_pick",-1);
                } else {
                    latitude_pick = BaseActivity.latitude;
                }
                if(data.hasExtra("lng_pick")){
                    longitude_pick = data.getDoubleExtra("lng_pick",-1);
                } else {
                    longitude_pick = BaseActivity.longitude;
                }

                if(longitude_drop!=0&&longitude_drop!=0)
                getPackage(String.valueOf(latitude_pick),String.valueOf(longitude_pick),String.valueOf(latitude_drop),String.valueOf(longitude_drop),BaseActivity.countryCode);

            }if (requestCode == REQUEST_ADDRESS_DROP_OFF ){
                if(data.hasExtra("add_detail")){
                    detailDrop = data.getStringExtra("add_detail");
                    MockupData.setDetailDrop(detailDrop);
                }
                if(data.hasExtra("address_drop")){
                    deliveryAddress = data.getStringExtra("address_drop");
                    if(!detailDrop.isEmpty()) dropOff.setText(detailDrop +", "+deliveryAddress);
                    else dropOff.setText(deliveryAddress);
                }

                if(data.hasExtra("weight")){
                    weight = data.getStringExtra("weight");
                    weightTv.setText(weight);
                }
                if(data.hasExtra("name_drop")){
                   name_receiver = data.getStringExtra("name_drop");
                    receiverName.setText(name_receiver);
                }
                if(data.hasExtra("phone_drop")){
                    phone_receiver = data.getStringExtra("phone_drop");
                    receiverPhone.setText(phone_receiver.trim());
                }
                if(data.hasExtra("category_drop")){
                   cate = data.getStringExtra("category_drop");
                    cateTv.setText(cate);
                }
                if(data.hasExtra("description_drop")){
                    description = data.getStringExtra("description_drop");
                    if(!description.isEmpty()) {
                        linearLayoutDescription.setVisibility(View.VISIBLE);
                        descriptionTv.setText(description);
                    }else {
                        linearLayoutDescription.setVisibility(View.GONE);
                    }

                }

                if(data.hasExtra("lat_drop")){
                    latitude_drop = data.getDoubleExtra("lat_drop",-1);
                    MockupData.setLateDrop(latitude_drop);
                } else {
                    latitude_drop = BaseActivity.latitude;
                }
                if(data.hasExtra("lng_drop")){
                    longitude_drop = data.getDoubleExtra("lng_drop",-1);
                    MockupData.setLngDrop(longitude_drop);
                }else {
                    longitude_drop = BaseActivity.longitude;
                }
                if(data.hasExtra("country_code")){
                    phoneCode = data.getStringExtra("country_code");
                }
                if(latitude_pick!=0&&longitude_pick!=0)
                getPackage(String.valueOf(latitude_pick),String.valueOf(longitude_pick),String.valueOf(latitude_drop),String.valueOf(longitude_drop),BaseActivity.countryCode);
                linearPick.setVisibility(View.VISIBLE);
                btnNext.setEnabled(true);
            } else if (requestCode == REQUEST_PIN_CODE){
                progressLoading.setVisibility(View.GONE);
                isOnResume = true;
                if(data.hasExtra("pin")){
                    pinCode = data.getStringExtra("pin");
                    if(checkPayOnline){
                        topUpOrangePay(paymentHistory,sendPackageDetail.getTotalPrice());
                    } else {
                        sendPackage();
                    }
                } else {
                    progressLoading.setVisibility(View.GONE);
                }
            }else if(requestCode == REQUEST_PIN_CODE_PIN ){
                if( data.hasExtra("status")){
                    boolean status = data.getBooleanExtra("status",false);
                    if(status)
                        isPin = true;
                }
                progressLoading.setVisibility(View.GONE);
            } else if(requestCode == REQUEST_SEND_PACKAGE){
                if(data.hasExtra("pick")){
                    String pick = data.getStringExtra("pick");
                    pickOff.setText(pick);
                }
                if(data.hasExtra("drop")){
                    String drop = data.getStringExtra("drop");
                    pickOff.setText(drop);
                }
            }
        }else if (resultCode == RESULT_OK && requestCode == REQUEST_PAYMENT_METHOD){
            progressLoading.setVisibility(View.GONE);
           if(Constant.getBaseUrl()!=null) getWallet();
        }
    }

    private HashMap<String,Object> getData(){
        List<SendPackageRequestParam> params = new ArrayList<>();
        HashMap<String,Object> hashMap = new HashMap<>();
        if(latitude_pick!=0)
            requestParam.setPickupLatitute(latitude_pick);
        if(longitude_pick!=0)
            requestParam.setPickupLongitute(longitude_pick);
        if(latitude_drop!=0)
            requestParam.setDropoffLatitute(latitude_drop);
        if(longitude_drop!=0)
            requestParam.setDropoffLongitute(longitude_drop);
        if(!name_receiver.isEmpty())
            requestParam.setNameReceiver(name_receiver);
        if(!name_sender.isEmpty())
            requestParam.setNameSender(name_sender);
            requestParam.setPaymentType(PAYMENT_CASH);
            if(latitude_pick!=0)
        requestParam.setPickupLatitute(latitude_pick);
        if(longitude_pick!=0)
        requestParam.setPickupLongitute(longitude_pick);
        requestParam.setDropoffLatitute(latitude_drop);
        requestParam.setDropoffLongitute(longitude_drop);
        requestParam.setNameReceiver(name_receiver);
        requestParam.setNameSender(name_sender);
        if(walletId == -1 ){
            Util.firebaseAnalytics(DeliveryPackageActivity.this,Constant.PAYMENT_TYPE, PAYMENT_CASH);
            requestParam.setPaymentType(PAYMENT_CASH);
        } else {
            Util.firebaseAnalytics(DeliveryPackageActivity.this,Constant.PAYMENT_TYPE, PAY_WALLET);
            requestParam.setPaymentType(PAY_WALLET);
            hashMap.put("wallet_id",walletId);
            hashMap.put("pin_code",pinCode);
        }
        if(!priceTv.getText().toString().isEmpty())
            requestParam.setDeliveryFee(priceTv.getText().toString());
        if(!phone_receiver.isEmpty())
            requestParam.setPhoneReceiver(phone_receiver);
        if(!phone_sender.isEmpty())
            requestParam.setPhoneSender(phone_sender);
        if(!weight.isEmpty())
            requestParam.setWeight(weight.replace("kg",""));
        requestParam.setRiderTip("");
        requestParam.setPackageDetail(description);
        if (!cate.isEmpty())
            requestParam.setTypePackage(cate);
        requestParam.setAddressDetailReceiver(detailDrop);
        requestParam.setDropoffAddress(deliveryAddress);
        requestParam.setPickupAddress(address_pick);
        requestParam.setAddressDetailSender(detailPickup);
        requestParam.setPickupAddress(pickOff.getText().toString());
        requestParam.setCountryCode(BaseActivity.countryCode);
        requestParam.setPhoneCode(phoneCode);
        params.add(requestParam);
        hashMap.put("send",params);
        MockupData.setAccountSignUp(hashMap);
        return hashMap;
    }

    private void sendPackage(){
        btnNext.setEnabled(false);
        new SendPackageWs().sendPackage(DeliveryPackageActivity.this, getData(), new SendPackageWs.loadSendPackage() {
            @Override
            public void onLoadSuccessfully(int orderId) {
                btnNext.setEnabled(false);
                Intent intent = new Intent(DeliveryPackageActivity.this,FindDriverActivity.class);
                intent.putExtra("order_Id",orderId);
                intent.putExtra("send_package","send");
                intent.putExtra("add_pick",pickOff.getText().toString());
                intent.putExtra("add_drop",dropOff.getText().toString());
                intent.putExtra("latePick",latitude_pick);
                intent.putExtra("lngPick",longitude_pick);
                intent.putExtra("latDrop",latitude_drop);
                intent.putExtra("lngDrop",longitude_drop);
                intent.putExtra("description",description);
                intent.putExtra("type_order","send");
                intent.putExtra("phone_code",BaseActivity.countryCode);
                intent.putExtra("param",getData());
                intent.putExtra("country_code",BaseActivity.countryCode);
                startActivityForResult(intent,REQUEST_SEND_PACKAGE);
            }

            @Override
            public void onLoadGetDetailSuccess(SendPackageDetail detail) {

            }

            @Override
            public void onLoadingFail(String message,int code) {
                Log.d("sendPackage",message+"");
                pinCode = "";
                progressLoading.setVisibility(View.GONE);
                btnNext.setEnabled(true);
                if(code ==405){
                    popupPayment(getString(R.string.error),message,DeliveryPackageActivity.this,code);
                }else {
                    Util.popupMessage(null, message, DeliveryPackageActivity.this);
                }
            }
        });
    }

    @Override
    public void onPayByWallet( String typePay, int walletId1) {
        checkPayOnline = false;
        setPaymentMethod.setText(typePay);
        walletId = walletId1;
    }

    @Override
    public void onaPayByPaymentHistory(PaymentHistory onPaymentHistory) {
        paymentHistory = onPaymentHistory;
        checkPayOnline = true;
        setPaymentMethod.setText(String.format("%s : %s",onPaymentHistory.getPspName(),onPaymentHistory.getPaymentToken()));
    }

    @Override
    public void onOpenPaymentMethod() {
        PaymentHistoryAdapter.noteHistoryPostion = -1;
        Intent intent = new Intent(DeliveryPackageActivity.this, PaymentMethodActivity.class);
        intent.putExtra("user",myUser);
        startActivityForResult(intent,REQUEST_PAYMENT_METHOD);
    }

    public void loadPaymentDialog(List<Wallets> wallets, List<PaymentHistory> paymentHistories) {
        PaymentMethodDialogFragment dialogFragment = PaymentMethodDialogFragment.newInstance(wallets, paymentHistories,checkShipping);
        dialogFragment.show(getSupportFragmentManager(), "PaymentMethodDialogFragment");
    }

    private void getPackage(String pickLat, String pickLng, String dropLat, String dropLng,String countryCod) {
        new SendPackageWs().getSendPackage(DeliveryPackageActivity.this, pickLat, pickLng, dropLat, dropLng,countryCod, new SendPackageWs.loadSendPackage() {
            @Override
            public void onLoadSuccessfully(int OrderId) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onLoadGetDetailSuccess(SendPackageDetail detail) {
                sendPackageDetail = detail;
                pricePerDisTv.setText(Constant.getConfig().getCurrencySign() +" "+ detail.getCostPerKm());
                String duration;
                if(detail.getTotalMin()>60){
                    double durat = detail.getTotalMin();
                   duration = formatDuration((int) (durat/60))+Util.formatTime(((int) Math.ceil(durat%60)));
                }else {
                    duration = Util.formatTime(((int) Math.ceil(detail.getTotalMin())));
                }
                disTv.setText(Util.formatPrice(detail.getTotalDistance())+" km "+"/ "+ duration);
                priceTv.setText(Constant.getConfig().getCurrencySign() +" "+ Util.formatPrice(detail.getTotalPrice()));
                MockupData.setTotalPrice(Constant.getConfig().getCurrencySign() +" "+ Util.formatPrice(detail.getTotalPrice()));
                MockupData.setDISTANCE(Util.formatPrice(detail.getTotalDistance())+" km "+"/ "+duration);
                MockupData.setPricePerUit(Constant.getConfig().getCurrencySign() +" "+ detail.getCostPerKm());
            }

            @Override
            public void onLoadingFail(String message,int code) {
                progressLoading.setVisibility(View.GONE);
                Util.popupMessage(null,message,DeliveryPackageActivity.this);
            }
        });
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void getWallet(){
        new ProfileWs().viewProfiles(DeliveryPackageActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user) {
                progressLoading.setVisibility(View.GONE);
                walletsList = user.getWalletsList();
                paymentHistories = user.getPaymentHistories();
                myUser = user;
                isPin = myUser.isConfirmPin();
                if(!Util.checkIsHaveCash())
                    walletsList.add(0,new Wallets(-1,PAYMENT_CASH,""));
                for(int i =0 ; walletsList.size() > i ; i++){
                    if (walletsList.get(i).getDefaultPayment() == 1){
                        setPaymentMethod.setText(String.format("%s : %s %s",getString(R.string.wallet),walletsList.get(i).getCurrency(),walletsList.get(i).getBalance()));
                        walletId = walletsList.get(i).getWalletId();
                        PaymentMethodDialogFragment.notePosition = i;
                        break;
                    } else if (!Util.checkIsHaveCash()) {
                        PaymentMethodDialogFragment.notePosition =0;
                        walletId = -1;
                        setPaymentMethod.setText(PAYMENT_CASH);
                    }
                }
            }

            @Override
            public void onError(String errorMessage) {
                progressLoading.setVisibility(View.GONE);
                Toast.makeText(DeliveryPackageActivity.this,errorMessage,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUpdateProfilesSuccess(boolean isSendSms,User user) {

            }
        });
    }

    private void topUpOrangePay(PaymentHistory paymentHistory,Double amount){
        progressLoading.setVisibility(View.VISIBLE);
        HashMap<String , Object > hashMap = new HashMap<>();
        hashMap.put("psp_id",paymentHistory.getPivot().getPspId());
        hashMap.put("phone",paymentHistory.getPaymentToken());
        int changeAmount = (int) Math.round(amount);
        hashMap.put("amount",changeAmount);
        if(!Util.checkIsHaveCash()) walletId = walletsList.get(1).getWalletId();
        else walletId = walletsList.get(0).getWalletId();
        hashMap.put("wallet_id",walletId);
        if(BuildConfig.DEBUG)
            hashMap.put("code","cm");
        else hashMap.put("code",countryCode);
        hashMap.put("pin_code",pinCode);
        TopUpPayment.onTopUpOrange(DeliveryPackageActivity.this, hashMap, new TopUpPayment.OnResponse() {
            @Override
            public void onSuccess(String message, int paymentId) {
                Log.d("checkPaymentId", "onSuccess: "+paymentId);
                orderPaymentId = paymentId;
                countTime();
                titleProgress.setText(R.string.place_verify_code);
                count = 0;
            }

            @Override
            public void onResponseMessage(String message) {
                pinCode = "";
                progressLoading.setVisibility(View.GONE);
                Util.popupMessage(getString(R.string.error),message,DeliveryPackageActivity.this);
            }
        });
    }

    private int count = 0;

    private void countTime(){
        new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("werwrewerwer"," second :"+  millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.d("werwrewerwer"," count :"+ count);
                if(count != 5)
                    getStatusTopUp();
                else {
                    count = 0;
                    titleProgress.setText(getString(R.string.loading));
                    progressLoading.setVisibility(View.GONE);
                    if(!isFinishing())
                        Toast.makeText(DeliveryPackageActivity.this,"Time out",Toast.LENGTH_LONG).show();
                }
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(checkPayOnline && !isOnResume)
            countTime();
        else isOnResume = false;
    }

    private void getStatusTopUp(){
            new PaymentWs().getPaymentStatus(DeliveryPackageActivity.this, orderPaymentId, new PaymentWs.onTopUpWalletsCallBack() {
                @Override
                public void onSuccess(String status, int paymentId) {
                    count++;
                    if(status.equalsIgnoreCase("SUCCESSFULL")){
                        sendPackage();
                    } else {
                        countTime();
                    }
                }

                @Override
                public void onError(String message) {
                    count = 5;
                    Util.popupMessage(null,message,DeliveryPackageActivity.this);
                }
            });
    }

    private void popupPayment(String title, String message, Activity activity, int code) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(code == 405 && myUser != null ){
                    Intent intent = new Intent(DeliveryPackageActivity.this, PaymentDepositActivity.class);
                    intent.putExtra("wallets", (Serializable) walletsList);
                    intent.putExtra ( "user",myUser );
                    intent.putExtra("size",Util.checkVisiblePaymentService());
                    startActivityForResult(intent,REQUEST_DEPOSIT_METHOD);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
}