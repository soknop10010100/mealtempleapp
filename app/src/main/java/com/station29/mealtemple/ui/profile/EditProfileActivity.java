package com.station29.mealtemple.ui.profile;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;

import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

public class EditProfileActivity extends BaseActivity implements View.OnClickListener {

    private User user;
    EditText firstNameEt, lastNameEt, phoneNumberEt, emailEt;
    HashMap<String, Object> userData = new HashMap<>();
    private String phoneText, genderSp;
    Spinner genderSpinner;
    private CountryCodePicker countryCodePicker;
    private final int REQUEST_CODE_EDIT_PROFILE = 5678;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        findViewById(R.id.back_btn).setOnClickListener(this);
        findViewById(R.id.save_profile).setOnClickListener(this);
        countryCodePicker = findViewById(R.id.ccp);
        countryCodePicker.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);

        firstNameEt = findViewById(R.id.first_name);
        lastNameEt = findViewById(R.id.last_name);
        genderSpinner = findViewById(R.id.gender);
        phoneNumberEt = findViewById(R.id.phone_number);
        emailEt = findViewById(R.id.email_address);
        progressBar = findViewById(R.id.progressBar);

        final List<String> genderList = new ArrayList<>();
        genderList.add(getString(R.string.select_gender));
        genderList.add(getString(R.string.female));
        genderList.add(getString(R.string.male));

        ArrayAdapter<String> spAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, genderList) {
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setTypeface(ResourcesCompat.getFont(EditProfileActivity.this, R.font.montserrat_medium_body));
                return view;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);

                TextView text = (TextView) v.findViewById(android.R.id.text1);
                text.setTypeface(ResourcesCompat.getFont(EditProfileActivity.this, R.font.montserrat_medium_body));
                return v;
            }
        };

        spAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        genderSpinner.setAdapter(spAdapter);

        genderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                genderSp = ((position == 0) ? "" : genderList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if (getIntent() != null && getIntent().hasExtra("user")) {
            user = (User) getIntent().getSerializableExtra("user");
            if (user == null)
                return;
            //set data to textview or edittext
            firstNameEt.setText(((user.getFirstName() == null && user.getLastName() == null) ? getString(R.string.guest) : user.getFirstName()));
//            firstNameEt.setText ( user.getFirstName () );
            lastNameEt.setText(user.getLastName());

            if (user.getGender() != null) {
                int position = 0;
                if (user.getGender().equalsIgnoreCase("FEMALE")) {
                    position = 1;
                } else position = 2;
                genderSpinner.setSelection(position);
                genderSp = (String) genderSpinner.getItemAtPosition(position);
            }
            if (user.getEmail() != null) {
                emailEt.setEnabled(false);
            }

            if (user.getPhoneNumber() != null && !user.getPhoneNumber().isEmpty()) {
                String countryCode = "";
                phoneText = user.getPhoneNumber();
                StringTokenizer defaultTokenizer = new StringTokenizer(phoneText);
                countryCode = defaultTokenizer.nextToken();
                countryCodePicker.setCountryForPhoneCode(Integer.parseInt(countryCode));
                phoneText = phoneText.replace(countryCode, "".trim());
                phoneText = phoneText.replaceAll("\\s+", "");
                phoneNumberEt.setText(phoneText);
                if (user.getPhoneNumber() != null && user.getEmail() != null) {
                    emailEt.setEnabled(false);
                }
            } else {
                countryCodePicker.setVisibility(View.VISIBLE);
            }
            emailEt.setText(user.getEmail());
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.back_btn) {
            Util.hideSoftKeyboard(v);
            popupMessage(getResources().getString(R.string.edit_profile_address), EditProfileActivity.this);
        } else if (v.getId() == R.id.save_profile) {
            progressBar.setVisibility(View.VISIBLE);
            if (countryCodePicker.getSelectedCountryNameCode().equalsIgnoreCase(BaseActivity.countryCode)) {
                Util.hideSoftKeyboard(v);
                saveProfile();
            } else {
                Util.popupMessage(getString(R.string.error), getString(R.string.you_can_not_edit_another_country_phone), this);
            }
        }
    }

    private void saveProfile() {
        if (user != null) {
            if (phoneNumberEt.getText().toString().trim().isEmpty()) {
                Util.popupMessage(null, "Please input your phone number.", EditProfileActivity.this);
            } else {
                userData.put("phone", formatPhone(phoneNumberEt.getText().toString().trim()));
                userData.put("phone_code", countryCodePicker.getFullNumber());
                userData.put("code", BaseActivity.countryCode);
                userData.put("email", emailEt.getText().toString());
                userData.put("firstname", ((firstNameEt.getText().toString().equals("") && lastNameEt.getText().toString().equals("")) ? "Guest" : firstNameEt.getText().toString().trim()));
                userData.put("lastname", lastNameEt.getText().toString().trim());
                userData.put("gender", "MALE");
                userData.put("shipping_address", ((user.getShipping_address() != null) ? user.getShipping_address() : ""));
                userData.put("image", user.getUrl_Image());
                userData.put("user_type", "USER_ANDROID");
                MockupData.setAccountSignUp(userData);
                new ProfileWs().updateProfiles(this, userData, profileViewAndUpdate);
            }
        } else {
            Util.popupMessage(null, getString(R.string.invalid), EditProfileActivity.this);
        }
    }

    private ProfileWs.ProfileViewAndUpdate profileViewAndUpdate = new ProfileWs.ProfileViewAndUpdate() {
        @Override
        public void onLoadProfileSuccess(User user) {

        }

        @Override
        public void onError(String errorMessage) {
            progressBar.setVisibility(View.GONE);
            Util.popupMessage(null, errorMessage, EditProfileActivity.this);
        }

        @Override
        public void onUpdateProfilesSuccess(boolean isSendSms,User user) {
            progressBar.setVisibility(View.GONE);
            MockupData.LOGIN_PHONE = phoneText;
            if (isSendSms) {
                Intent intent = new Intent(EditProfileActivity.this, ConfirmSmsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivityForResult(intent, REQUEST_CODE_EDIT_PROFILE);
            } else {
                androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this, R.style.DialogTheme);
                builder.setMessage(getString(R.string.updated));
                builder.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!phoneNumberEt.getText().toString().isEmpty())
                            MockupData.LOGIN_PHONE = countryCodePicker.getDefaultCountryCodeWithPlus() + " " + phoneNumberEt.getText().toString();
                        MockupData.USER_NAME = firstNameEt.getText().toString() + lastNameEt.getText().toString();
                        Intent intent = new Intent();
                        intent.putExtra("datauser", userData);
                        intent.putExtra("user",user);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.setCanceledOnTouchOutside(false);
                if(!isFinishing())
                dialog.show();
            }
//
        }
    };

    private String formatPhone(String fullPhoneNumber) {
        String code;
        if (fullPhoneNumber.length() >= 8) {
            if (phoneNumberEt.getText().toString().equals(" ") || phoneNumberEt.getText().toString().isEmpty())
                return fullPhoneNumber;
            code = fullPhoneNumber.substring(0, countryCodePicker.getFullNumber().length());
            fullPhoneNumber = (code.contentEquals(countryCodePicker.getFullNumber()) ? fullPhoneNumber.substring(countryCodePicker.getFullNumber().length()) : fullPhoneNumber);
            if(fullPhoneNumber.length() < 8){
                return phoneNumberEt.getText().toString();
            }
        } else {//855
            //return old phone number
            return fullPhoneNumber;
        }
        return fullPhoneNumber;
    }

    private void popupMessage(String message, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity, R.style.DialogTheme);
        builder.setTitle(null);
        builder.setMessage(message);
        builder.setNegativeButton(String.format(Locale.US, "%s", activity.getString(R.string.discard)), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.update)), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressBar.setVisibility(View.VISIBLE);
                saveProfile();
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        popupMessage(getResources().getString(R.string.edit_profile_address), EditProfileActivity.this);
    }

//    Set result back to screen that call it
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_EDIT_PROFILE && resultCode == RESULT_OK) {
            setResult(RESULT_OK);
            finish();
        }
    }
}
