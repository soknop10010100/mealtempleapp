package com.station29.mealtemple.ui.payment;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.PaymentTransaction;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.payment.adapter.PaymentTransactionAdapter;
import com.station29.mealtemple.util.DateUtil;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

public class PaymentTransactionActivity extends BaseActivity {

    private RecyclerView recyclerView;
    private ImageView backBtn;
    private PaymentTransactionAdapter adapter;
    private final List<PaymentTransaction> paymentTransactions = new ArrayList<>();
    private boolean isScrolling;
    private int currentItem, total , scrollDown, currentPage = 1 ,size = 10;
    private ProgressBar progressBar;
    private LinearLayoutManager linearLayoutManager;
    private View progressLoading;
    private SwipeRefreshLayout refreshLayout;
    private LinearLayout txtToAccount;
    private TextView emptyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_transaction);

        initView();
        initAction();
    }

    private void initView(){
        recyclerView = findViewById(R.id.list_payment_transaction);
        backBtn = findViewById(R.id.backButton);
        progressBar = findViewById(R.id.loadMoreProgressBar);
        progressLoading = findViewById(R.id.progressBar);
        refreshLayout = findViewById(R.id.swipe_layout);
        emptyText = findViewById(R.id.emtyText);
    }

    private void initAction(){

        progressLoading.setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.title)).setText(R.string.payment_transaction);

        linearLayoutManager = new LinearLayoutManager(PaymentTransactionActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        loadListPaymentTransaction(recyclerView);

        backBtn.setOnClickListener(v -> finish());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItem = linearLayoutManager.getChildCount();
                total = linearLayoutManager.getItemCount();
                scrollDown = linearLayoutManager.findFirstVisibleItemPosition();
                if(dy > 0) {
                    if (isScrolling && (currentItem + scrollDown == total)) {
                        if(total == size) {
                            isScrolling = false;
                            currentPage++;
                            progressBar.setVisibility(View.VISIBLE);
                            recyclerView.scrollToPosition(paymentTransactions.size() - 1);
                            loadListPaymentTransaction(recyclerView);
                            size+=10;
                        }
                    }
                }

            }
        });

        refreshLayout.setColorSchemeResources(R.color.colorPrimary);

        refreshLayout.setOnRefreshListener(() -> {
            currentPage = 1;
            size = 10;
            adapter.clear();
            loadListPaymentTransaction(recyclerView);
        });
    }

    private void loadListPaymentTransaction(RecyclerView recyclerView) {
        new PaymentWs().getUserPaymentTransaction(PaymentTransactionActivity.this,currentPage,paymentTransaction);
        adapter = new PaymentTransactionAdapter(paymentTransactions,onClickPaymentTransaction);
        recyclerView.setAdapter(adapter);
    }

    private final PaymentWs.onGetPaymentTransaction paymentTransaction = new PaymentWs.onGetPaymentTransaction() {
        @Override
        public void onSuccess(List<PaymentTransaction> paymentTransaction) {
            progressLoading.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            paymentTransactions.addAll(paymentTransaction);
            refreshLayout.setRefreshing(false);
            adapter.notifyDataSetChanged();
            if(paymentTransactions.isEmpty())
                emptyText.setVisibility(View.VISIBLE);
            else emptyText.setVisibility(View.GONE);
        }

        @Override
        public void onError(String message) {
            progressLoading.setVisibility(View.GONE);
            Util.popupMessage(null,message,PaymentTransactionActivity.this);
        }
    };

    private final PaymentTransactionAdapter.OnClickPaymentTransaction onClickPaymentTransaction = new PaymentTransactionAdapter.OnClickPaymentTransaction() {
        @Override
        public void onClickTransaction(PaymentTransaction paymentTransaction) {
            popUpDetailWallet(paymentTransaction);
        }
    };

    private void popUpDetailWallet(PaymentTransaction paymentTransaction){
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(PaymentTransactionActivity.this,R.style.DialogTheme);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_detail_wallets, null);
        builder.setView(view);
        TextView tnxIdTv = view.findViewById(R.id.id_tnx);
        TextView dateTv = view.findViewById(R.id.date_tan);
        TextView amountDetailTv = view.findViewById(R.id.amount_o);
        TextView toAccountTv = view.findViewById(R.id.to_acc);
        TextView fromAccountTv = view.findViewById(R.id.from_acc);
        TextView typeTv = view.findViewById(R.id.type);
        LinearLayout shareTo = view.findViewById(R.id.share);
        txtToAccount = view.findViewById(R.id.txtToAccount);
        final Dialog dialog = builder.create();
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        String type = "";
        if(paymentTransaction.getType() != null && paymentTransaction.getType().equals("TOP_UP") ||paymentTransaction.getType() != null && paymentTransaction.getType().equals("IN")) {
            type = "+";
            amountDetailTv.setTextColor(Color.parseColor("#00c853"));
        }else if (paymentTransaction.getType() != null && paymentTransaction.getType().equals("OUT")) {
            type = "-";
            amountDetailTv.setTextColor(Color.parseColor("#af1022"));
        }

        if(paymentTransaction.getAmount() != null)
            amountDetailTv.setText(String.format("%s %s%s",paymentTransaction.getCurrencyCode(),type,paymentTransaction.getAmount()));
        else amountDetailTv.setText("");

        tnxIdTv.setText(paymentTransaction.getTxnId());
        dateTv.setText(DateUtil.formatDateFromServer(paymentTransaction.getCreatedAt()));

        if("IN".equals(paymentTransaction.getType())){
            toAccountTv.setText(paymentTransaction.getUserName());
            fromAccountTv.setText(paymentTransaction.getRefUserName());
        } else if ("TOP_UP".equals(paymentTransaction.getType())){
            txtToAccount.setVisibility(View.GONE);
            fromAccountTv.setText(paymentTransaction.getUserName());
        } else {
            toAccountTv.setText(paymentTransaction.getRefUserName());
            fromAccountTv.setText(paymentTransaction.getUserName());
        }
        typeTv.setText(paymentTransaction.getType());

        shareTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "This is the text that will be shared.");
                startActivity(Intent.createChooser(sharingIntent,"Share using"));
            }
        });

        dialog.show();
    }

}
