package com.station29.mealtemple.ui.taxi.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.Services;

import java.util.List;
import java.util.Locale;

public class ChooseTransportAdapter extends RecyclerView.Adapter<ChooseTransportAdapter.ChooseTransportViewHolder> {

    private final OnTransportClickListener onTransportClickListener;
    private final List<Services> servicesList;
    private final boolean checkWithoutDropLatLng;
    private final int i;

    public ChooseTransportAdapter(OnTransportClickListener onTransportClickListener, List<Services> servicesList ,boolean checkWithoutDropLatLng,int i) {
        this.onTransportClickListener = onTransportClickListener;
        this.servicesList = servicesList;
        this.checkWithoutDropLatLng = checkWithoutDropLatLng;
        this.i = i;
    }

    @NonNull
    @Override
    public ChooseTransportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_choose_transport, parent, false);
        return new ChooseTransportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ChooseTransportViewHolder holder, int position) {
        final Services services = servicesList.get(position);

        if(services!=null){
            holder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    v.setEnabled(false);
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            v.setEnabled(true);
                        }
                    },100);
                    onTransportClickListener.onTransportClick(holder.view, services);
                }
            });
            Glide.with(holder.imageView.getContext()).load(services.getServiceIcon()).into(holder.imageView);
            holder.nameTransport.setText(services.getServiceType());
            if (!checkWithoutDropLatLng) {
                if(i == 0){
                    holder.priceTransport.setText(String.format(Locale.US,"%s %s", services.getCurrencySign(), services.getTotalPrice()));
                }else {
                    holder.priceTransport.setText(String.format(Locale.US,"%s %s", services.getCurrencySign(), services.getTotalPrice()));
                    holder.priceAfterDiscountTv.setText(String.format(Locale.US,"%s %s", services.getCurrencySign(), services.getPriceAfterDiscount()));
                    if(services.getTotalPrice().equalsIgnoreCase(String.valueOf(services.getPriceAfterDiscount()))){
                        holder.line.setVisibility(View.GONE);
                        holder.priceAfterDiscountTv.setVisibility(View.GONE);
                    }else {
                        holder.priceTransport.setTextSize(12);
                        holder.line.setVisibility(View.VISIBLE);
                    }
                }

            } else {
                holder.priceTransport.setText(String.format(Locale.US,"%s %s ","From "+ services.getCurrencySign(),services.getFlagDownCost()));
            }
        }

    }

    @Override
    public int getItemCount() {
        return servicesList.size();
    }

    public interface OnTransportClickListener {
        void onTransportClick(View itemView, Services services);
    }

    static class ChooseTransportViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView priceTransport,nameTransport,priceAfterDiscountTv;
        View view,line;

        ChooseTransportViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            imageView = itemView.findViewById(R.id.transportImg);
            priceTransport = itemView.findViewById(R.id.txtPriceTransport);
            nameTransport = itemView.findViewById(R.id.txtNameTransport);
            priceAfterDiscountTv = itemView.findViewById(R.id.txtPriceAfter);
            line = itemView.findViewById(R.id.line_i);
        }
    }

    public void clear() {
        int size = servicesList.size ();
        if (size > 0) {
            servicesList.subList(0, size).clear();
            notifyItemRangeRemoved ( 0, size );
        }
    }

}
