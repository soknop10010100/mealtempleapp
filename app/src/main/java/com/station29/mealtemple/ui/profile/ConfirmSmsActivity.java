package com.station29.mealtemple.ui.profile;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.poovam.pinedittextfield.SquarePinField;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.api.request.SignUpWs;
import com.station29.mealtemple.fcm.MyFirebaseMessagingService;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import java.util.HashMap;
import java.util.Locale;

public class ConfirmSmsActivity extends BaseActivity {

    private String action = null;
    private HashMap<String,Object> postVerifyCode = new HashMap<> (  );
    private SquarePinField pin;
    private View progressBar;
    private boolean isChangePIN = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_sms);

        pin = findViewById ( R.id.squareField );
        progressBar = findViewById(R.id.progressBar);

        if (getIntent() != null && getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
        }
        if(getIntent() != null && getIntent().hasExtra("phoneEmail")){
            HashMap<String,Object>account = (HashMap<String, Object>) getIntent().getSerializableExtra("phoneEmail");
            MockupData.setAccountSignUp(account);
        }

        if(getIntent() != null && getIntent().hasExtra("forgotPINParam")){
            isChangePIN = true;
            findViewById(R.id.countTime).setVisibility(View.GONE);
            postVerifyCode = (HashMap<String, Object>) getIntent().getSerializableExtra("forgotPINParam");
        }

        pin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()==6){
                    Util.hideSoftKeyboard(findViewById(R.id.squareField));
                    if(!isChangePIN)
                        verifyCode();
                    else{
                        postVerifyCode.put("reset_pin_code",s.toString());
                        changePIN();
                    }

                }
            }
        });

        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.GONE);
                if (pin.getText() != null && !pin.getText().toString().isEmpty()  && pin.length ()== 6){
                    verifyCode();
                }else {
                    Toast.makeText ( getApplicationContext (), getString(R.string.invalid_code), Toast.LENGTH_SHORT ).show ();
                }
        }
        });

        findViewById(R.id.btnBackConfirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        countTime();

        findViewById(R.id.countTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                },100);
                countTime();
                MockupData.getAccountSignUp().put("function","Customer");
                new SignUpWs().forgetAccountPassword(MockupData.getAccountSignUp () ,requestServer);
            }
        });

    }

    private void changePIN(){
        new PaymentWs().setUserPin(ConfirmSmsActivity.this, postVerifyCode, new PaymentWs.onTopUpWalletsCallBack() {
            @Override
            public void onSuccess(String message , int paymentId) {
                progressBar.setVisibility(View.GONE);
                setResult(RESULT_OK);
                finish();
                Toast.makeText(ConfirmSmsActivity.this,"success",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String message) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ConfirmSmsActivity.this,message,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void countTime(){
        final TextView countTime=findViewById(R.id.countTime);
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                countTime.setText(String.format(Locale.US,"%s: %d", getString(R.string.resend_in), millisUntilFinished / 1000));
                countTime.setEnabled(false);
            }

            public void onFinish() {
                countTime.setText(R.string.resend);
                countTime.setEnabled(true);
            }
        }.start();
    }

    private void verifyCode(){
        HashMap<String, Object> getAccountVerify= MockupData.getAccountSignUp ();
        if (getAccountVerify.containsKey ( "email" )){
            postVerifyCode.put ( "email",getAccountVerify.get ( "email" ) );
        } if (getAccountVerify.containsKey ( "phone" )){
            postVerifyCode.put ( "phone",getAccountVerify.get ( "phone" ) );
        }
        postVerifyCode.put ( "pin_code",pin.getText ().toString ());
        postVerifyCode.put ( "user_type",getAccountVerify.get ( "user_type" ) );
        postVerifyCode.put ( "phone_code",getAccountVerify.get("phone_code"));
        postVerifyCode.put("code",BaseActivity.countryCode);
        new SignUpWs ().verifyAccountCode ( postVerifyCode,requestServer );
    }

    private SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer () {
        @Override
        public void onRequestSuccess() {

        }

        @Override
        public void onRequestToken(String token, int id, User user) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText ( getApplicationContext (), getString(R.string.success), Toast.LENGTH_SHORT ).show ();
            if(user.getPhoneNumber()!=null)
                MockupData.LOGIN_PHONE = user.getPhoneNumber();
            if(user.getFirstName()!=null && user.getLastName()!=null)
                MockupData.USER_NAME = user.getFirstName()+" "+user.getLastName();
            Util.saveString("token", token, ConfirmSmsActivity.this);
            Util.saveString("user_id", String.valueOf(id), ConfirmSmsActivity.this);
            String fcmToken = Util.getString(Constant.FIREBASE_TOKEN, ConfirmSmsActivity.this);
            MyFirebaseMessagingService.sendRegistrationToServer(fcmToken,ConfirmSmsActivity.this);
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onRequestFailed(String message) {
            progressBar.setVisibility(View.GONE);
            Util.popupMessage(getString(R.string.error),message,ConfirmSmsActivity.this);
        }
    };

}
