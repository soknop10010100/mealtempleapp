package com.station29.mealtemple.ui.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.request.StoreWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.adpater.ShopSearchAdapter;
import com.station29.mealtemple.util.Util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class ShopSearchActivity extends BaseActivity {

    private ShopSearchAdapter adapter;
    private RecyclerView recyclerView,recyclerViewHistory;
    private int size =30,cateId =0 , currentPage =1;
    private EditText editTxtStoreName;
    private TextView historyTxt ,noResultTxt;
    private  List<Shop> shopsList = new ArrayList<>(),allShopList = new ArrayList<>();
    private boolean isNewShop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.layout_search);

        recyclerView = findViewById ( R.id.search_shop_nameRe );
        recyclerView.setLayoutManager ( new LinearLayoutManager ( this ) );

        recyclerViewHistory = findViewById(R.id.search_shop_history);
        recyclerViewHistory.setLayoutManager(new LinearLayoutManager(this));

        historyTxt = findViewById(R.id.historyTxt);
        noResultTxt = findViewById(R.id.no_result_text);

        if (!Util.isConnectedToInternet ( ShopSearchActivity.this )) popupMessage ( null, getResources ().getString ( R.string.failed_internet ),ShopSearchActivity.this);

        allShopList = bindAllShop();

        findViewById ( R.id.backButton ).setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                onBackPressed ();
            }
        } );

        editTxtStoreName = findViewById ( R.id.search_store_name );

        editTxtStoreName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Constant.getBaseUrl ()!=null)
                    new StoreWs().searchStore(BaseActivity.countryCode, editTxtStoreName.getText().toString(), latitude, longitude, size, cateId, loadStoreCallback);
            }
        });

        editTxtStoreName.addTextChangedListener ( new TextWatcher () {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                if (isConnectedToInternet ( ShopSearchActivity.this )){
                    noResultTxt.setVisibility(View.GONE);
                    if(!editTxtStoreName.getText().toString().isEmpty()) {
                        recyclerView.setVisibility(View.VISIBLE);
                        if(getArrayList("shopHistory")!=null) {
                            recyclerViewHistory.setVisibility(View.GONE);
                        }
                        historyTxt.setVisibility(View.GONE);
                        if (Constant.getBaseUrl ()!=null) new StoreWs().searchStore(BaseActivity.countryCode, s.toString(), latitude, longitude, size, cateId, loadStoreCallback);
                    }else{
                        recyclerView.setVisibility(View.GONE);
                        historyTxt.setVisibility(View.VISIBLE);
                        if(getArrayList("shopHistory")!=null && getArrayList("shopHistory").containsKey(BaseActivity.countryCode)) {
                            if(getArrayList("shopHistory")!=null) {
                                recyclerViewHistory.setVisibility(View.VISIBLE);
                                adapter = new ShopSearchAdapter((getArrayList("shopHistory").get(BaseActivity.countryCode)), listener);
                                recyclerViewHistory.setAdapter(adapter);
                            }
                        }

                    }
                }
            }
        } );
    }

    private StoreWs.LoadStoreCallback loadStoreCallback = new StoreWs.LoadStoreCallback() {
        @Override
        public void onLoadSuccess(List<Shop> shopList) {
            noResultTxt.setVisibility(View.GONE);
            if(shopList.size()==0 && !editTxtStoreName.getText().toString().equals("")) {
                noResultTxt.setVisibility(View.VISIBLE);
                noResultTxt.setText(String.format("%s %s", getString(R.string.no_result_for), editTxtStoreName.getText().toString()));
            }
            adapter = new ShopSearchAdapter(shopList, listener);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

        }

        @Override
        public void onLoadFailed(String errorMessage) {
          if (Util.isConnectedToInternet ( ShopSearchActivity.this )) Util.popupMessage(getString(R.string.error),errorMessage,ShopSearchActivity.this);
        }

        @Override
        public void onLoadStoreDetail(Shop shop) {

        }
    };

    private void saveArrayList(HashMap<String,List<Shop>> list, String key){
        SharedPreferences  prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        try {
            Gson gson = new Gson();
            String json = gson.toJson(list);
            editor.putString(key, json);
            editor.apply();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private HashMap<String,List<Shop>> getArrayList(String key){
        HashMap<String,List<Shop>> getArrayLists = new HashMap<>();
        try {
            SharedPreferences  prefs = PreferenceManager.getDefaultSharedPreferences(this);
            Gson gson = new Gson();
            String json = prefs.getString(key, null);
            Type type = new TypeToken<HashMap<String,List<Shop>>>() {}.getType();
            getArrayLists =gson.fromJson(json, type);
            return getArrayLists;
        }catch (Exception e){

        }
        return getArrayLists;


    }

    private boolean isContain(Shop shop) {
        if(getArrayList("shopHistory")!=null) {
            HashMap<String,List<Shop>> shoplist = getArrayList("shopHistory");
                if(shoplist.containsKey(BaseActivity.countryCode)){
                    for(Shop shop1 : shoplist.get(BaseActivity.countryCode)){
                        if (shop1.getShopId() == shop.getShopId()) {
                            return true;
                        }
                    }
                }
            }
        return false;
    }

    private ShopSearchAdapter.OnShopClickListener listener = new ShopSearchAdapter.OnShopClickListener () {
        @Override
        public void onShopClick(final View itemView, Shop shop) {
            itemView.setEnabled(false);
            itemView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    itemView.setEnabled(true);
                }
            },200);

            if ( !isContain(shop)) {
                shopsList.add(0,shop);
                if(getArrayList("shopHistory")!=null) {
                    if (shopsList.size() == 6)
                       shopsList.remove(5);
                }
                MockupData.getShopHistory().put(BaseActivity.countryCode, shopsList);
                saveArrayList(MockupData.getShopHistory(),"shopHistory");
            }
            Intent intent = new Intent(ShopSearchActivity.this, ShopDetailActivity.class);
            intent.putExtra("shop", shop);
            startActivity(intent);
        }
    };
    //bind all show to over the old search shop detail
    private List<Shop> bindAllShop(){
        final List<Shop> shopAllList = new ArrayList<>();
        if (Constant.getBaseUrl()!=null){
            new StoreWs().loadStores(BaseActivity.countryCode, 0, 0, currentPage, size, BaseActivity.latitude, BaseActivity.longitude, new StoreWs.LoadStoreCallback() {
                @Override
                public void onLoadSuccess(List<Shop> list) {

                    shopAllList.addAll(list);
                    isNewShop = true;
                        if( isNewShop && getArrayList("shopHistory")!=null && getArrayList("shopHistory").containsKey(BaseActivity.countryCode)) {
                            if(getArrayList("shopHistory").get(BaseActivity.countryCode)!=null) {
                                if (allShopList.size()!=0 && getArrayList("shopHistory").get(BaseActivity.countryCode).size()!=0) {
                                    List<Shop>  shopList = overRideShop(allShopList,getArrayList("shopHistory").get(BaseActivity.countryCode));
                                    MockupData.getShopHistory().put(BaseActivity.countryCode,shopList);
                                    saveArrayList(MockupData.getShopHistory(),"shopHistory");
                                    adapter = new ShopSearchAdapter(shopList, listener);
                                    recyclerViewHistory.setAdapter(adapter);
                                    shopsList.addAll(shopList);
                                }else {
                                    adapter = new ShopSearchAdapter(getArrayList("shopHistory").get(BaseActivity.countryCode), listener);
                                    recyclerViewHistory.setAdapter(adapter);
                                }
                            }
                        }
                }

                @Override
                public void onLoadFailed(String errorMessage) {
                }

                @Override
                public void onLoadStoreDetail(Shop shop) {
                }
            });
        }
        return shopAllList;
    }

    private List<Shop> overRideShop(List<Shop> newShopLists, List<Shop> oldShopLists){
        for (int i = 0; i < newShopLists.size(); i++) {
            int newShopId= newShopLists.get(i).getShopId();
            for (int j = 0; j < oldShopLists.size(); j++) {
                int oldShopId = oldShopLists.get(j).getShopId();
                if (newShopId== oldShopId){
                    oldShopLists.add(newShopLists.get(i));
                    oldShopLists.remove(j);
                }
            }
        }
        return oldShopLists;
    }

}
