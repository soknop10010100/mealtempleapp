package com.station29.mealtemple.ui.home;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.AddOn;
import com.station29.mealtemple.api.model.CouponDis;
import com.station29.mealtemple.api.model.ItemRequestParam;
import com.station29.mealtemple.api.model.OrderDetail;
import com.station29.mealtemple.api.model.OrderItem;
import com.station29.mealtemple.api.model.OrderRequestParams;
import com.station29.mealtemple.api.model.PaymentHistory;
import com.station29.mealtemple.api.model.PreView;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.model.SurCharges;
import com.station29.mealtemple.api.model.Tax;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.api.model.response.OpenHour;
import com.station29.mealtemple.api.request.CouponWs;
import com.station29.mealtemple.api.request.OrderWs;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.adpater.CartItemAdapter;
import com.station29.mealtemple.ui.payment.PasswordConfirmActivity;
import com.station29.mealtemple.ui.payment.PaymentDepositActivity;
import com.station29.mealtemple.ui.payment.adapter.PaymentHistoryAdapter;
import com.station29.mealtemple.ui.payment.PaymentMethodDialogFragment;
import com.station29.mealtemple.ui.profile.DeliveryAddressActivity;
import com.station29.mealtemple.ui.payment.PaymentMethodActivity;
import com.station29.mealtemple.ui.profile.SetChangePinActivity;
import com.station29.mealtemple.util.DateUtil;
import com.station29.mealtemple.util.TopUpPayment;
import com.station29.mealtemple.util.Util;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import static com.station29.mealtemple.ui.home.ShopDetailActivity.shopOpenHours;
import static com.station29.mealtemple.util.DateUtil.getDeliveryToday;
import static com.station29.mealtemple.util.DateUtil.getDeliveryTomorrow;
import static com.station29.mealtemple.util.DateUtil.isMinDelivery;
import static com.station29.mealtemple.util.Util.popupMessage;

public class  OrderConfirmationActivity extends BaseActivity implements View.OnClickListener, PaymentMethodDialogFragment.PaymentClickListener , ShippingMethodDialogFragment.onClickOnShipping {

    private List<OrderItem> orderItemArrayList = new ArrayList<>();
    private Shop shop, shopRefresh;
    private EditText couponCode;
    private TextView txtName, txtPhoneNumber, txtAddressTv, deliveryFeeTv, surChargesTv, deliveryTimeTv, discountTv, riderTip,addTip , totalQtyTv, vatTv, totalPriceTv, subTotalTv, tipAmountTv, setPaymentMethod,titleProgress,setShippingMethod;
    private final static int REQUEST_CODE_EDIT_ADDRESS = 345,REQUEST_DEPOSIT_METHOD = 957 ,REQUEST_PIN_CODE = 9022,REQUEST_PIN_CODE_PIN = 9023;
    private TextView subtotal1Tv, totalQty1Tv, discountValuesTv, tax1Tv, surcharges1Tv, deliveryFeeValuesTv, ridertip1Tv;
    private String deliveryTime = "", timeSelected, currentCouponCode,currencySign="",mCouponToken, password;
    private int tipCount ,walletId,clickShippingPosition , orderPaymentId;
    private double tipTotal, subTotal, discountTotal = 0, tipUnit = 0, grandTotalCoupon = 0;
    private OrderDetail orderDetail;
    private OpenHour openHourMorning, openHourAfternoon;
    private ArrayAdapter<String> laterTodayDel = null, tomorrowDel = null;
    private List<Integer> todayTime = new ArrayList<>(), tomorrowTime = new ArrayList<>();
    private PreView preView;
    private RecyclerView recyclerView;
    private OrderRequestParams couponParam;
    private View progressLoading;
    private final String PAYMENT_CASH = "Pay by cash", PAYMENT_WALLETS = "Pay by wallet", BY_MY_OWN="by_my_own" , KIWIGO_DELIVERY ="kiwigo_delivery" ;
    private List<Wallets> walletsList = new ArrayList<>();
    private boolean checkPayOnline = false,checkShipping = false;
    private PaymentHistory paymentHistory;
    private List<PaymentHistory> paymentHistories;
    private User myUser;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isPin , isOnResume ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        PaymentMethodDialogFragment.notePosition = -1;
        PaymentHistoryAdapter.noteHistoryPostion = -1;

        TextView titleTv = findViewById(R.id.title);
        titleTv.setText(getString(R.string.cart_detail));
        discountTv = findViewById(R.id.discount_coupon);
        totalQtyTv = findViewById(R.id.totalQty);
        surChargesTv = findViewById(R.id.surcharge);
        deliveryFeeTv = findViewById(R.id.delivery_fee);
        subTotalTv = findViewById(R.id.subTotal);
        vatTv = findViewById(R.id.taxAfter);
        totalPriceTv = findViewById(R.id.totalTv);
        couponCode = findViewById(R.id.coupon_code);
        subtotal1Tv = findViewById(R.id.subTotal1);
        totalQty1Tv = findViewById(R.id.totalQty1);
        discountValuesTv = findViewById(R.id.discount_coupon1);
        tax1Tv = findViewById(R.id.taxAfter1);
        surcharges1Tv = findViewById(R.id.surcharge1);
        deliveryFeeValuesTv = findViewById(R.id.delivery_fee1);
        ridertip1Tv = findViewById(R.id.tip_rider1);
        progressLoading = findViewById(R.id.progress_loading);
        titleProgress = findViewById(R.id.title_view);
        txtName = findViewById(R.id.text_delivery_name);
        txtPhoneNumber = findViewById(R.id.text_delivery_phone);
        txtAddressTv = findViewById(R.id.text_delivery_address);
        setPaymentMethod = findViewById(R.id.setPaymentMethod);
        deliveryTimeTv = findViewById(R.id.delivery_time);
        recyclerView = findViewById(R.id.cart_items);
        tipAmountTv = findViewById(R.id.amount_tip);
        riderTip = findViewById(R.id.tip_rider);
        setShippingMethod = findViewById(R.id.setShippingMethod);
        addTip = findViewById(R.id.add_tip);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);

        progressLoading.setVisibility(View.VISIBLE);
        couponCode.setOnFocusChangeListener(onFocusChangeListener);
        getWallet();
        //get currency sign
        currencySign = ((Constant.getConfig() != null) ? Constant.getConfig().getCurrencySign() : "N/A");

        Intent intent = getIntent();
        orderItemArrayList = new ArrayList<>();
        if (intent.hasExtra("orderDetail")) {
            orderDetail = (OrderDetail) intent.getSerializableExtra("orderDetail");
            if (orderDetail == null) return;
            orderItemArrayList = orderDetail.getOrderItemList();
            shop = orderDetail.getShop();
            //subTotal = calculateTotalPrice(orderItemArrayList);
        }if (intent.hasExtra("preView")) {
            preView = (PreView) intent.getSerializableExtra("preView");
            if (preView == null) return;
            subTotal = preView.getSubtotal();
            totalPriceTv.setText(String.format(Locale.US, "%s %s", currencySign, Util.formatPrice(preView.getGrandTotal())));
            //if tax and surcharge have
            viewTaxAndSurcharge();
            shopOpenHours = new ArrayList<>();
            ShopDetailActivity.checkShopClose(MockupData.getCurrentOrderDetail().getShop(), true);
        }
        if (intent.hasExtra("shop")) {
            // in case shop owner change time while user was look by
            shopRefresh =(Shop) intent.getSerializableExtra("shop");
            if (shopRefresh==null)return;
            if (!shopRefresh.getMinDelivery().equals(shop.getMinDelivery()) && shopRefresh.getShopId()==shop.getShopId()){
                //if vendor change mindelivery immediately > set new shop as mindelivery to current order
                MockupData.getCurrentOrderDetail().setShop(shopRefresh);
                shop = MockupData.getCurrentOrderDetail().getShop();
            }
        }
        String fullInfo = "";
        if (MockupData.USER_NAME != null && !MockupData.USER_NAME.equals("")){
            txtName.setVisibility(View.GONE);
            txtName.setText(String.format(" %s", !MockupData.USER_NAME.equals("") ? MockupData.USER_NAME : getString(R.string.quest)));
        } if (MockupData.LOGIN_PHONE !=null && !MockupData.LOGIN_PHONE.equals("")){
            txtPhoneNumber.setVisibility(View.VISIBLE);
            txtPhoneNumber.setText(String.format(" %s", MockupData.LOGIN_PHONE));
        } if (latitude != 0 && longitude != 0) {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (addresses.size() > 0) {
                    String addressLine = addresses.get(0).getAddressLine(0);
                    fullInfo = fullInfo.concat("#"+MockupData.HOUSE_NUMBER +" ").concat( ((MapsActivity.oldEditShippingDeliver!=null)?MapsActivity.oldEditShippingDeliver:addressLine)).concat("\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (DeliveryAddressActivity.phoneNumber!=null){
            MockupData.LOGIN_PHONE =DeliveryAddressActivity.phoneNumber;
            txtPhoneNumber.setVisibility(View.VISIBLE);
            txtPhoneNumber.setText(String.format(" %s", MockupData.LOGIN_PHONE));
        }
        deliveryFeeTv.setText(String.format("%s : ", getString(R.string.deliveryFee)));
        deliveryFeeValuesTv.setText(String.format("%s %s", currencySign,Util.formatPrice(preView.getDelivery_fee())));
        //when come to this screen if delivery time is in vacation it disable
//        if (isInVocation ()){
//            setDeliveryClose ();
//        }else {
//            List<OpenHour> openHourToday = new ArrayList<> (  ),openHourTmr = new ArrayList<> (  );
//            for (OpenHour openHour :shop.getOpenHours ()){
//                if (openHour.getDay ()==getToday ()){
//                    openHourToday.add ( openHour );
//                }else if (openHour.getDay ()==getTomorrow ()){
//                    openHourTmr.add(openHour);
//                }
//            }
//
//            if (openHourToday.size ()==2){
//                if (openHourToday.get ( 0 ).getCloseTime ()>openHourToday.get ( 1 ).getCloseTime ()){
//                    openHourToday.add(openHourToday.get ( 0 ));
//                    openHourToday.remove ( 0 );
//                }
//            }
//
//            if (openHourTmr.size ()==2){
//                if (openHourTmr.get ( 0 ).getCloseTime ()>openHourTmr.get ( 1 ).getCloseTime ()){
//                    openHourTmr.add(openHourTmr.get ( 0 ));
//                    openHourTmr.remove ( 0 );
//                }
//            }
//
//            if (openHourToday.size ()==0 || isDayVacation ( getToday () )){
//                deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'>Shop is Closed, Today.</font>".toUpperCase(Locale.US)) );
//            }else  if (openHourTmr.size ()==0 || isDayVacation ( getTomorrow () )) {
//                deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'>Shop is Closed, Tomorrow.</font>".toUpperCase(Locale.US)) );
//            }else {
//                if (getWorkingHour( openHourToday ).get ( "hour" ).size ()==0 && getWorkingHour( openHourTmr ).get ( "hour" ).size ()==0){
//                    setDeliveryClose ();
//                }else if  (getWorkingHour( openHourToday ).get ( "hour" ).size ()==0 || isDayVacation ( getToday () )){
//
//                    deliveryTimeTv.setText( Html.fromHtml( "<font color='red'>Shop is Closed , Today.</font>".toUpperCase(Locale.US)));
//                    if (MockupData.DELIVERY_TIME!=null && MockupData.DELIVERY_TIME_SHOPID==shop.getShopId ()){
//                        deliveryTimeTv.setText(String.format(Locale.US,("%s"),MockupData.DELIVERY_TIME));
//                    }
//                }else if (getWorkingHour( openHourTmr ).get ( "hour" ).size ()==0|| isDayVacation ( getTomorrow () )){
//
//                    deliveryTimeTv.setText( Html.fromHtml( "<font color='red'>Shop is Closed , Tomorrow.</font>".toUpperCase(Locale.US)));
//                    if (MockupData.DELIVERY_TIME!=null&& MockupData.DELIVERY_TIME_SHOPID==shop.getShopId ()){
//                        deliveryTimeTv.setText(String.format(Locale.US,("%s"),MockupData.DELIVERY_TIME));
//                    }
//                }else {
//                    deliveryTimeTv.setText ( Html.fromHtml( "<span color='black' style= 'font-wight:bold'>Please Select Delivery time</span>") );
//                    if (MockupData.DELIVERY_TIME!=null&& MockupData.DELIVERY_TIME_SHOPID==shop.getShopId ()){
//                        deliveryTimeTv.setText(String.format(Locale.US,("%s"),MockupData.DELIVERY_TIME));
//                    }else {
//                        deliveryTimeTv.setText ( Html.fromHtml( "<span color='black' style= 'font-wight:bold'>Please Select Delivery time</span>") );
//                    }
//                }
//            }
//        }
        if (!isDayVacation(getToday())) {// today is not in vacation
            deliveryTimeASAP();//contain Asap time
        } else {
            //set shop close cant order
            setDeliveryClose();
        }
        findViewById(R.id.edit_time).setOnClickListener(this);
        if (shop.isOpen()) { // Not allow client to edit delivery time if store is working.
            deliveryTimeTv.setVisibility(View.GONE);
            findViewById(R.id.edit_time).setVisibility(View.GONE);
        }
        txtAddressTv.setText(fullInfo);
        if (REQUEST_CODE_EDIT_ADDRESS != 345) {
            startActivityForResult(new Intent(OrderConfirmationActivity.this, DeliveryAddressActivity.class), REQUEST_CODE_EDIT_ADDRESS);
        }
        findViewById(R.id.backButton).setOnClickListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CartItemAdapter(orderItemArrayList, onOrderItemEditQtyListener, false, 0));


        findViewById(R.id.edit_delivery_address).setOnClickListener(this);
        findViewById(R.id.sub_tip).setOnClickListener(this);
        findViewById(R.id.add_tip).setOnClickListener(this);
        findViewById(R.id.place_order).setOnClickListener(this);
        findViewById(R.id.apply_coupon).setOnClickListener(this);
        findViewById(R.id.payment).setOnClickListener(this);
        findViewById(R.id.shipping).setOnClickListener(this);

        tipAmountTv.setText(Util.formatPrice(tipTotal));
        riderTip.setText(String.format("%s : ", getString(R.string.rider_tip)));
        ridertip1Tv.setText(String.format("%s %s",currencySign,Util.formatPrice(tipTotal)));

        TextView shopNameTv = findViewById(R.id.shopName);
        if (shop != null) shopNameTv.setText(shop.getName());
        initPriceDetail();

        if (tipCount > 0) {
            (findViewById(R.id.sub_tip)).setEnabled(true);
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                addTip.setEnabled(true);
                (findViewById(R.id.sub_tip)).setEnabled(true);
                tipAmountTv.setText(Util.formatPrice(tipTotal));
                getWallet();
            }
        });

    }

    private CartItemAdapter.onItemEditedListener onOrderItemEditQtyListener = new CartItemAdapter.onItemEditedListener() {
        @Override
        public void onOrderItemEditQty(OrderItem orderItem, int count, int position, boolean isEditQty) { // Here just change Qty of Item
            for (OrderItem o : orderItemArrayList) {
                if (o.getProductId() == orderItem.getProductId() && o.getComment().equals(orderItem.getComment())) {
                    o.setQty(orderItem.getQty()); // Override Qty
                    break;
                }
            }
            initPriceDetail();
            Intent data = new Intent();
            data.putExtra("orderItem", orderItem);
            setResult(RESULT_OK, data);
        }

        @Override
        public void onLastItemRemove(boolean lastItemRemove) {

        }
    };

    private void initPriceDetail(){
        //doesnt need anymore , this is use only change qty of items but now it doesnt need to do that here
        totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(getTotalPrice(false))));
        int totalQty = 0;
        for (OrderItem orderItem : orderItemArrayList) {
            totalQty += orderItem.getQty();
        }

        Intent data = new Intent();
        data.putExtra ( "total", getTotalPrice (false) );
        setResult(RESULT_OK,data);
        subTotalTv.setText(String.format(Locale.US,"%s :",getString(R.string.sub_total)));
        subtotal1Tv.setText(String.format(Locale.US,"%s %s",currencySign ,Util.formatPrice(subTotal)));
        discountTv.setText(String.format(Locale.US,"%s :",getString(R.string.discount)));
        discountValuesTv.setText(String.format(Locale.US,"%s %s", currencySign, Util.formatPrice ( discountTotal )));
        totalQtyTv.setText(String.format(Locale.US,"%s :",getString(R.string.total_qty)));
        totalQty1Tv.setText(String.format(Locale.US,"%s",totalQty));
    }

    private double getTotalPrice(boolean ownDelivery) {
        //if total from coupon doesnt have
        if(ownDelivery)
            return preView.getGrandTotal() - preView.getDelivery_fee();
        if (grandTotalCoupon!=0){
            return grandTotalCoupon+tipTotal+preView.getDelivery_fee();
        }
        return preView.getGrandTotal()+tipTotal;
        //if have reutrn the subtotal from coupon
    }

    private void viewTaxAndSurcharge(){
        if (preView.getTaxs ().size ()==0){
            vatTv.setVisibility ( View.GONE );
            tax1Tv.setVisibility(View.GONE);
        }else {
            String tax="";
            String taxTotal="";
            Tax taxs =null;
            for (int i = 0; i < preView.getTaxs ().size (); i++) {
                taxs = preView.getTaxs ().get ( i );
                tax= tax+ String.format(getString(R.string.tax) +"( %s %s%s ) :%s",taxs.getNameTax(),taxs.getAmount(),"%","\n") ;
                taxTotal = taxTotal + String.format("%s %s%s",currencySign,Util.formatPrice ( taxs.getTaxAmount () ),"\n");
            }
            vatTv.setText (String.format ( Locale.US,"%s",tax.trim () )  );
            tax1Tv.setText(String.format(Locale.US,"%s",taxTotal.trim () ));
        }

        if (preView.getSurCharges ().size ()==0){
            surChargesTv.setVisibility ( View.GONE );
            surcharges1Tv.setVisibility(View.GONE);
        }else {
            String surCh ="";
            String surChTotal="";
            for (int i = 0; i < preView.getSurCharges ().size (); i++) {
                SurCharges surCharges = preView.getSurCharges ().get ( i );
                surCh= surCh + String.format(getString(R.string.surcharges) +"( %s %s%s ) :%s",surCharges.getNameSurCharges(),surCharges.getAmount(),"%","\n") ;
                surChTotal = surChTotal + String.format("%s %s%s",currencySign,Util.formatPrice ( surCharges.getSurChargeAmount()),"\n");
            }
            surChargesTv.setText (String.format ( Locale.US,"%s",surCh.trim () )  );
            surcharges1Tv.setText(String.format(Locale.US,"%s",surChTotal.trim()));
        }
    }

    private OrderRequestParams orderRequestParams(){
        List<Integer> items = new ArrayList<>();
        List<ItemRequestParam> itemRequestParams = new ArrayList<>();
        for (OrderItem orderItem : orderItemArrayList) {
            itemRequestParams.add(getItemRequestParam(orderItem));
            items.add(orderItem.getProductId());
        }
        OrderRequestParams params = new OrderRequestParams();
        params.setStoreId(shop.getShopId());
        params.setCouponCode(couponCode.getText().toString().trim());
        params.setCouponToken(mCouponToken); // Add later
        params.setRiderTip(String.valueOf(tipTotal));
        if(MockupData.getHouseNumber().isEmpty()){
            params.setShippingAddress(txtAddressTv.getText ().toString ());
        }else{
            params.setShippingAddress(MockupData.getHouseNumber()+" "+txtAddressTv.getText ().toString ());
        }
        params.setReceiverPhone(MockupData.LOGIN_PHONE); // Add Later
        if (!MockupData.DELIVERY_DATE.equals("")) {
//                    params.setDeliveryTime(DateUtil.formatTimeZoneServer ( MockupData.DELIVERY_DATE ));
//                    MockupData.DELIVERY_DATE = "";
            params.setDeliveryTime(DateUtil.formatTimeZoneServer(MockupData.DELIVERY_DATE));
            Log.d("DATED", DateUtil.formatTimeZoneServer(MockupData.DELIVERY_DATE));
        }
//                else {
////                  params.setDeliveryTime(DateUtil.formatTimeZoneServer ( deliveryTime ));
//                    params.setDeliveryTime(DateUtil.formatTimeZoneServer ( MockupData.DELIVERY_DATE ));
//                    MockupData.DELIVERY_DATE = "";
//                }

        params.setItemIds(items);
        params.setItems(itemRequestParams);
        params.setFirst_name(MockupData.USER_NAME);
        params.setCountry_code(BaseActivity.countryCode);
        params.setLat(BaseActivity.latitude);
        params.setLng(BaseActivity.longitude);

        if(clickShippingPosition==1){
            params.setShippingMethod(BY_MY_OWN);
            params.setDelivery_fee(0);
        } else {
            params.setDelivery_fee(preView.getDelivery_fee());
            params.setShippingMethod(KIWIGO_DELIVERY);
        }

        if(walletId == -1 ){
            Util.firebaseAnalytics(OrderConfirmationActivity.this,Constant.PAYMENT_TYPE, PAYMENT_CASH);
            params.setPaymentType(PAYMENT_CASH);
        } else {
            Util.firebaseAnalytics(OrderConfirmationActivity.this,Constant.PAYMENT_TYPE, PAYMENT_WALLETS);
            params.setPaymentType(PAYMENT_WALLETS);
            params.setWalletId(walletId);
            params.setPinCode(password);
        }

        return params;
    }

    private void popupInform() {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderConfirmationActivity.this, R.style.DialogTheme);
        builder.setTitle(R.string.check);
        builder.setMessage(getString(R.string.your_items_will) + " " + txtAddressTv.getText().toString());
        progressLoading.setVisibility(View.GONE);
        builder.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressLoading.setVisibility(View.VISIBLE);
                if (!checkPayOnline) {
                    if (walletId != -1 && password == null || walletId != -1 && password.equals("") && myUser.isConfirmPin() ){
                        if(!isPin){
                            Intent intent = new Intent(OrderConfirmationActivity.this, SetChangePinActivity.class);
                            intent.putExtra("user",myUser);
                            startActivityForResult(intent,REQUEST_PIN_CODE_PIN);
                        } else {
                            Intent intent = new Intent(OrderConfirmationActivity.this , PasswordConfirmActivity.class);
                            startActivityForResult(intent,REQUEST_PIN_CODE);
                        }
                    }
                    else postOrder(orderRequestParams());
                }else
                    if( password == null || password.equals("")){
                        if(!myUser.isConfirmPin()){
                            Intent intent = new Intent(OrderConfirmationActivity.this, SetChangePinActivity.class);
                            intent.putExtra("user",myUser);
                            startActivityForResult(intent,REQUEST_PIN_CODE_PIN);
                        } else {
                            Intent intent = new Intent(OrderConfirmationActivity.this , PasswordConfirmActivity.class);
                            startActivityForResult(intent,REQUEST_PIN_CODE);
                        }
                    }

            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.setCancelable(false);
        builder.show();
    }

    @Override
    public void onClick(final View view) {
        String tip = Constant.getConfig().getRiderTip();
        if (tip != null && !tip.isEmpty()) {
            tipUnit = Float.parseFloat(tip);
        }
        if (view.getId() == R.id.backButton) {
            if (!couponCode.getText().toString().isEmpty()) {
                popupMessageDiscart(null, this);
            } else finish();
        } else if (view.getId() == R.id.edit_delivery_address) {
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setEnabled(true);
                }
            }, 300);
            startActivityForResult(new Intent(OrderConfirmationActivity.this, DeliveryAddressActivity.class), REQUEST_CODE_EDIT_ADDRESS);
        } else if (view.getId() == R.id.place_order) {
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setEnabled(true);
                }
            }, 300);

            Util.firebaseAnalytics(this,Constant.CLICK_ON,"place_order");
            Calendar mCurrentTime = Calendar.getInstance();
            int mH = (mCurrentTime.get(Calendar.HOUR_OF_DAY) * 60) + mCurrentTime.get(Calendar.MINUTE);
            CountryCodePicker cp = new CountryCodePicker(this);
            cp.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);
            String cp1 = cp.getFullNumberWithPlus();
            String ph = new StringTokenizer(MockupData.LOGIN_PHONE).nextToken();

            if (!ph.equals(cp1)) {
                popupDeliveryAddress(getString(R.string.error), getString(R.string.you_need_add_phone), this);
            } else if (PaymentMethodDialogFragment.notePosition == -1 && !checkPayOnline) {
                popupPayment(getString(R.string.error), getString(R.string.you_need_to_select_payment), this,0);
            } else if (subTotal < shop.getMin()) {
                popupMessage(getString(R.string.can_not_place_order), getString(R.string.store_minimum_order) + Constant.getConfig().getCurrencySign() + " " + Util.formatPrice(shop.getMin()), this);
            } else if (deliveryTime.equals("")) {
                popupMessageDeliveryTime(null, getString(R.string.please_select_devlivery_time), this);
            } else if (mH < shopOpenHours.get(0) || mH > shopOpenHours.get(1) && mH < shopOpenHours.get(2) || mH > shopOpenHours.get(3)) {
                Util.popupMessage("Error", getString(R.string.shop_is_close)+" "+shop.getName()+" "+getString(R.string.close), OrderConfirmationActivity.this);
            } else if(Util.checkShopCloseServer()){
                Util.popupMessage(getString(R.string.error), getString(R.string.shop_is_close)+" "+ getString(R.string.server_clsoe), OrderConfirmationActivity.this);
            } else {
                progressLoading.setVisibility(View.VISIBLE);
                popupInform();
            }
        } else if (view.getId() == R.id.sub_tip) {
            if (tipCount > 0) {
                tipCount--;
            }
            if (tipCount == 0) {
                (findViewById(R.id.sub_tip)).setEnabled(false);
            }
            tipTotal = tipCount * tipUnit;
            tipAmountTv.setText(Util.formatPrice(tipTotal));
            totalPriceTv.setText(String.format("%s %s", currencySign , Util.formatPrice(getTotalPrice(false))));
            riderTip.setText(getString(R.string.rider_tip));
            ridertip1Tv.setText(String.format("%s %s",currencySign,Util.formatPrice(tipTotal)));
            MockupData.getCurrentOrderDetail ().setTipCount ( tipCount );
        } else if (view.getId() == R.id.add_tip) {
            (findViewById(R.id.sub_tip)).setEnabled(true);
            tipCount++;
            tipTotal = tipCount * tipUnit;
            tipAmountTv.setText(Util.formatPrice(tipTotal));
            totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(getTotalPrice(false))));
            riderTip.setText(getString(R.string.rider_tip));
            ridertip1Tv.setText(String.format("%s %s",currencySign,Util.formatPrice(tipTotal)));
            MockupData.getCurrentOrderDetail ().setTipCount ( tipCount );
        } else if (view.getId() == R.id.apply_coupon) {
            Util.firebaseAnalytics(this,Constant.CLICK_ON,"apply_coupon");
            if (!couponCode.getText().toString().isEmpty()) {
                if (couponCode.getText().toString().trim().equals(currentCouponCode)) {
                    popupMessage(getString(R.string.error), getString(R.string.the_copon_code_are_already_applied), OrderConfirmationActivity.this);
                    InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                } else {
                    setCoupons();
                    verifyCoupon(couponParam);
                    InputMethodManager imm = (InputMethodManager) getSystemService(getApplicationContext().INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
                }
            } else {
                popupMessage(getString(R.string.error), getString(R.string.coupon_code_can_not_empty), OrderConfirmationActivity.this);
            }
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setEnabled(true);
                }
            }, 500);
        } else if (view.getId() == R.id.payment) {
            loadPaymentDialog(walletsList,paymentHistories);
        } else if (view.getId() == R.id.edit_time) {
            //if shop on vacation close the all selected time!
//            if (!isInVocation ()){
//                timeSelected();
//            }else {
//                setDeliveryClose ();
//            }
            if (!isDayVacation(getToday())) {// today is not in vacation
                deliveryTimeASAP();//contain Asap time
            } else {
                setDeliveryClose();
            }
        } else if (view.getId() == R.id.shipping){
            loadShippingDialog();
        }
    }

    public void loadShippingDialog( ) {
        ShippingMethodDialogFragment dialogFragment = ShippingMethodDialogFragment.newInstance(clickShippingPosition);
        dialogFragment.show(getSupportFragmentManager(), "PaymentMethodDialogFragment");
    }

    private void setCoupons() {
        List<Integer> items = new ArrayList<>();
        List<ItemRequestParam> itemRequestParams = new ArrayList<>();
        couponParam = new OrderRequestParams();
        couponParam.setStoreId(shop.getShopId());
        couponParam.setCouponCode(couponCode.getText().toString().trim());
        for (OrderItem orderItem : orderItemArrayList) {
            itemRequestParams.add(getItemRequestParam(orderItem));
            items.add(orderItem.getProductId());

            couponParam.setItemIds(items);
            couponParam.setItems(itemRequestParams);
        }
    }

    private ItemRequestParam getItemRequestParam(OrderItem orderItem) {
        List<AddOn> addons = orderItem.getAddOns();
        List<Integer> addonIds = new ArrayList<>();

        // Coz Ws require only ids
        for (AddOn addOn : addons) {
            addonIds.add(addOn.getId());
        }
        // Coz Ws require only ids
        List<Integer> optionsIds = new ArrayList<>();
        List<AddOn> options = orderItem.getOption();
        for (AddOn option : options) {
            optionsIds.add(option.getId());
        }
        ItemRequestParam itemParams = new ItemRequestParam();
        itemParams.setProductId(orderItem.getProductId());
        itemParams.setQty(orderItem.getQty());
        itemParams.setComment(orderItem.getComment());
        itemParams.setAddOnIds(addonIds);
        itemParams.setOptionIds(optionsIds);
        return itemParams;
    }

    private boolean isInVocation() {
        if (shop != null && shop.getVacation().size() == 2) {
            if (shop.getVacation().contains(getToday()) && shop.getVacation().contains(getTomorrow())) {
                return true;
            }
        }
        return false;
    }

    private boolean isDayVacation(int day) {
        if (shop.getVacation().contains(day)) {
            return true;
        }
        return false;
    }

    private void verifyCoupon(OrderRequestParams params) {
        new CouponWs().couponVerify(OrderConfirmationActivity.this, params, new CouponWs.VerifyCouponListener() {

            @Override
            public void onVerifyCouponSuccess(CouponDis disOrder, String couponToken, float values, String message) {
                mCouponToken = couponToken;
                // set adapter to do the highlight...
                // specific error for coupon is a must in case user is wait , and nothing happen
                int size = disOrder.getDisItemsList().size();
                if (disOrder.getOption() == null) {
                    Util.popupMessage(getString(R.string.coupon_code), message, OrderConfirmationActivity.this);
                } else if (disOrder.getOption().equals("option") && size != 0 || disOrder.getOption().equals("product") && size != 0 || disOrder.getOption().equals("product_group") && size != 0) {
                    recyclerView.setAdapter(new CartItemAdapter(disOrder, disOrder.getDisItemsList(), orderItemArrayList, onOrderItemEditQtyListener, false, 0));
                    setPriceAfterCoupon(disOrder, message);
                } else if (disOrder.getOption().equals("subtotal")) {
                    recyclerView.setAdapter(new CartItemAdapter(disOrder, disOrder.getDisItemsList(), orderItemArrayList, onOrderItemEditQtyListener, false, 0));
                    setPriceAfterCoupon(disOrder, message);
                } else if (disOrder.getOption().equals("Option") && size == 0) {
                    // this case that coupon is option but items doesnt have option
                    popupMessage(getString(R.string.coupon_code), message, OrderConfirmationActivity.this);
                } else if (disOrder.getOption().equals("product") && size == 0) {
                    // this case that coupon is product but order doesnt have that product
                    popupMessage(getString(R.string.coupon_code), message, OrderConfirmationActivity.this);
                } else if (disOrder.getOption().equals("product_group") && size == 0) {
                    // this case that coupon is product group but selected product doesnt in that types of product group.
                    popupMessage(getString(R.string.coupon_code), message, OrderConfirmationActivity.this);
                }
            }

            @Override
            public void onVerifyError(String message) {
                popupMessage(null, message, OrderConfirmationActivity.this);
            }
        });
    }

    private void setPriceAfterCoupon(CouponDis disOrder,String message ){
        Toast.makeText ( getApplicationContext (), message, Toast.LENGTH_LONG ).show ();
        grandTotalCoupon = disOrder.getAfterDis () ;
        subTotalTv.setText ( String.format ( Locale.US, "%s :", getString(R.string.sub_total) ) );
        subtotal1Tv.setText ( String.format ( "%s %s", currencySign, Util.formatPrice ( disOrder.getBeforeDis () ) ) );
        totalPriceTv.setText ( String.format ( Locale.US, "%s %s",currencySign, Util.formatPrice ( getTotalPrice (false)) ) );
        discountTv.setText ( String.format ( Locale.US, "%s : ", getString(R.string.discount) ) );
        discountValuesTv.setText ( String.format ( "%s %s", currencySign, Util.formatPrice ( disOrder.getTotalDiscount () ) ) );
        if(disOrder.getTaxList().size()!=0){
            String tax="";
            String taxTotal="";
            Tax taxs =null;
            for (int i = 0; i < disOrder.getTaxList ().size (); i++) {
                taxs = disOrder.getTaxList ().get ( i );
                tax= tax+ String.format("Tax( %s %s%s ) :%s",taxs.getNameTax(),taxs.getAmount(),"%","\n") ;
                taxTotal = taxTotal + String.format("%s %s%s",currencySign,Util.formatPrice ( taxs.getTaxAmount () ),"\n");
            }
            vatTv.setText (String.format ( Locale.US,"%s",tax.trim () )  );
            tax1Tv.setText(String.format(Locale.US,"%s",taxTotal.trim () ));
        }
    }

    public void loadPaymentDialog( List<Wallets> list,List<PaymentHistory> paymentHistories) {
        PaymentMethodDialogFragment dialogFragment = PaymentMethodDialogFragment.newInstance(list, paymentHistories,checkShipping);
        dialogFragment.show(getSupportFragmentManager(), "PaymentMethodDialogFragment");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_EDIT_ADDRESS && resultCode == RESULT_OK && data != null) {
            if(data.hasExtra("deliveryName")) {
                txtName.setVisibility(View.GONE); txtPhoneNumber.setVisibility(View.VISIBLE);
                txtName.setText(String.format("%s", data.getStringExtra("deliveryName")));
                if(data.hasExtra("deliveryHouseNumber")) {
                    txtAddressTv.setText ( String.format ( "#"+" %s  %s" , data.getStringExtra("deliveryHouseNumber"),data.getStringExtra ( "deliveryAddress" ) ) );
                }else{
                    txtAddressTv.setText ( String.format ( " %s" ,data.getStringExtra ( "deliveryAddress" ) ) );
                }
                txtPhoneNumber.setText(String.format("%s",data.getStringExtra("deliveryPhoneNumber")));
            }
            if(longitude!=0 && latitude!=0){
              viewOrder();
            }
        }else if ((data != null && data.hasExtra("MyTipTotal"))){

            tipTotal = data.getFloatExtra("MyTipTotal", 0);

        } else if (requestCode == REQUEST_DEPOSIT_METHOD && resultCode == RESULT_OK ){

            progressLoading.setVisibility(View.VISIBLE);
            getWallet();

        } else if (requestCode == REQUEST_PIN_CODE && resultCode == RESULT_OK){

            isOnResume = true;
            progressLoading.setVisibility(View.GONE);
            if(data != null){
                progressLoading.setVisibility(View.VISIBLE);
                password = data.getStringExtra("pin");
                if(checkPayOnline){
                    if(clickShippingPosition == 1)
                        topUpByHistory(paymentHistory,getTotalPrice(true));
                    else
                        topUpByHistory(paymentHistory,getTotalPrice(false));
                } else {
                    postOrder(orderRequestParams());
                }
            } else {
                progressLoading.setVisibility(View.GONE);
            }

        } else if(requestCode == REQUEST_PIN_CODE_PIN && resultCode ==RESULT_OK && data != null){
            if(data.hasExtra("status")){
                boolean status = data.getBooleanExtra("status",false);
                if(status)
                    isPin = true;
            }
            progressLoading.setVisibility(View.GONE);
        }
    }

    private HashMap<String , Object> preViewAgain(){
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("account_id", shop.getShopId());
        hashMap.put ( "country_code",BaseActivity.countryCode);
        hashMap.put ("latitute" ,latitude);
        hashMap.put ("longitute" ,longitude);
        List<Integer> itemId = new ArrayList<> (  );
        for (int i = 0; i <orderItemArrayList.size (); i++) {
            itemId.add ( orderItemArrayList.get ( i ).getProductId () );
        }
        hashMap.put ( "item_ids",itemId);

        List<ItemRequestParam> itemParamsRequest = new ArrayList<>();
        for (OrderItem orderItem : orderItemArrayList) {
            itemParamsRequest.add ( getItemRequestParam(orderItem) );
        }

        hashMap.put ("items",itemParamsRequest );
        return hashMap;
    }

    private void viewOrder(){
        if (Constant.getBaseUrl()!=null){
            new OrderWs ().preViewOrder ( preViewAgain(), this, new OrderWs.CreateOrderListener () {
                @Override
                public void onCreateSuccess(String message) {

                }

                @Override
                public void onCreateError(String error,int code) {
                    if (error.equalsIgnoreCase("Unauthenticated.")) {

                    } else {
                        Util.popupMessage ( null,error,OrderConfirmationActivity.this );
                    }
                }

                @Override
                public void onViewOrder(PreView preViews) {
                    //do something  here whit has map with inten
                  preView = preViews;
                  deliveryFeeValuesTv.setText(String.format(Locale.US,"%s %s",currencySign,Util.formatPrice(preView.getDelivery_fee())));
                  totalPriceTv.setText(String.format(Locale.US, "%s %s", currencySign, Util.formatPrice(preView.getGrandTotal())));
                }
            } );
        }
    }

    @Override
    public void onPayByWallet(String typePay, int walletId1) {
        checkPayOnline = false;
        setPaymentMethod.setText(typePay);
        walletId = walletId1;
    }

    @Override
    public void onaPayByPaymentHistory(PaymentHistory onPaymentHistory) {
        paymentHistory = onPaymentHistory;
        checkPayOnline = true;
        setPaymentMethod.setText(String.format("%s : %s",onPaymentHistory.getPspName(),onPaymentHistory.getPaymentToken()));
    }

    @Override
    public void onOpenPaymentMethod() {
        if(!Util.checkIsHaveCash() && myUser.getWalletsList().get(0).getWalletId() == -1)
            myUser.getWalletsList().remove(0);
        Intent intent = new Intent(OrderConfirmationActivity.this,PaymentMethodActivity.class);
        intent.putExtra("user",myUser);
        startActivityForResult(intent,REQUEST_DEPOSIT_METHOD);
    }

    private int getToday(){
        Calendar mCurrentTime = Calendar.getInstance();
        int mDay = mCurrentTime.get ( Calendar.DAY_OF_WEEK );
        return mDay;
    }

    private int getTomorrow(){
        if (getToday ()==7) return 1;
        else  return getToday ()+1;
    }

    private void  deliveryTimeASAP(){
            // we use current time + min delivery time
          Calendar mCurrentTime = Calendar.getInstance();
          int mHour = mCurrentTime.get ( Calendar.HOUR_OF_DAY );
          int mMinute = mCurrentTime.get(Calendar.MINUTE);
          mMinute =mMinute+isMinDelivery ( shop.getMinDelivery () );
          if (mMinute>=60){
              mHour+= mMinute/60;
              mMinute = mMinute %60;
          }
          MockupData.DELIVERY_DATE = DateUtil.getCurrentDateNewFormat ()+" "+mHour+":" +mMinute +": 00";
          deliveryTime = getString(R.string.delivery_asap);
          deliveryTimeTv.setText ( String.format ( Locale.US,"%s",deliveryTime ) );

    }

    private void timeSelected(){

        final List<String> deliveryDay = new ArrayList<> (  );
        final Spinner spinnerTime = new Spinner ( this ),spinnerDay = new Spinner ( this );;
        List<OpenHour> openHourToday = new ArrayList<> (  ),openHourTmr = new ArrayList<> (  );
        final List<Integer> minutesListTomorrow = new ArrayList<> (  ),minutesListToday = new ArrayList<> (  ), vocationList = shop.getVacation ();

        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.HORIZONTAL);

        Calendar mCurrentTime = Calendar.getInstance();
        int mDay = mCurrentTime.get ( Calendar.DAY_OF_WEEK );
        AlertDialog.Builder builder = new AlertDialog.Builder ( this,R.style.DialogTheme );

        for (OpenHour openHour :shop.getOpenHours ()){
            if (openHour.getDay ()==getToday ()){
                openHourToday.add ( openHour );
            }else if (openHour.getDay ()==getTomorrow ()){
                openHourTmr.add(openHour);
            }
        }
        //incase of time is wrong place
        //example for ws respone shift2 before shift 1
        if (openHourToday.size ()==2){
            if (openHourToday.get ( 0 ).getCloseTime ()>openHourToday.get ( 1 ).getCloseTime ()){
                openHourToday.add(openHourToday.get ( 0 ));
                openHourToday.remove ( 0 );
            }
        }

        if (openHourTmr.size ()==2){
            if (openHourTmr.get ( 0 ).getCloseTime ()>openHourTmr.get ( 1 ).getCloseTime ()){
                openHourTmr.add(openHourTmr.get ( 0 ));
                openHourTmr.remove ( 0 );
            }
        }

        if (vocationList.isEmpty ()){
            //check if the store is close on today 0r tmr.
            if (openHourToday.size ()!=0){
                if(getWorkingHour( openHourToday ).get ( "hour" ).size ()==0){
                    deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'>"+ getString(R.string.shop_is_closed_today) +"</font>".toUpperCase(Locale.US)) );
                }else {
                    deliveryDay.add ( "Later Today" );
                    todayTime = getWorkingHour( openHourToday ).get ( "hour" );
                    minutesListToday.addAll ( getWorkingHour( openHourToday ).get ( "min" ) );
                }
            }else {
                deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'>"+ getString(R.string.shop_is_closed_today) +"</font>".toUpperCase(Locale.US)) );
            }

            if (openHourTmr.size ()!=0){
                if (getWorkingHour( openHourTmr ).get ( "hour" ).size ()==0){
                    deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'> "+getString(R.string.shop_is_closed_tmr)+"</font>".toUpperCase(Locale.US)) );
                }else {
                    deliveryDay.add ( "Tomorrow" );
                    tomorrowTime = getWorkingHour( openHourTmr ).get ( "hour" );
                    minutesListTomorrow.addAll ( getWorkingHour( openHourTmr ).get ( "min" ) );
                }
                if (todayTime.size ()==1){
                    todayTime.remove ( 0 );
                    deliveryDay.remove ( 0 );
                }
            }else {
                deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'>"+getString(R.string.shop_is_closed_tmr)+"</font>".toUpperCase(Locale.US)) );
            }


        } else if (vocationList.contains ( mDay )) {
            if (openHourTmr.size ()!=0){
                deliveryDay.add ( "Tomorrow" );
                tomorrowTime = getWorkingHour( openHourTmr ).get ( "hour" );
                minutesListTomorrow.addAll ( getWorkingHour( openHourTmr ).get ( "min" ) );
            }else {
                deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'>"+ getString(R.string.shop_is_closed_tmr) +"</font>".toUpperCase(Locale.US)) );
            }
        }else if (vocationList.contains ( getTomorrow() )){
            if (openHourToday.size ()!=0){
                deliveryDay.add ( "Later Today" );
                todayTime = getWorkingHour( openHourToday ).get ( "hour" );
                minutesListToday.addAll ( getWorkingHour( openHourToday ).get ( "min" ) );
            }else {
                deliveryTimeTv.setText ( Html.fromHtml( "<font color='red'>"+ getString(R.string.shop_is_closed_today) +"</font>".toUpperCase(Locale.US)) );
            }
        }

        final ArrayAdapter<String> dayAD = new ArrayAdapter<> ( this, android.R.layout.simple_list_item_1,deliveryDay);
        if(todayTime.size () !=0   ){
            //check if delivery time is out <so it only show   we have to remove it>
            if (getDeliveryToday ( todayTime ,minutesListToday,shop.getMinDelivery()).size ()!=0){
                laterTodayDel = new ArrayAdapter<> ( this, android.R.layout.simple_list_item_1, getDeliveryToday ( todayTime ,minutesListToday,shop.getMinDelivery()));
            }else if (getDeliveryToday ( todayTime ,minutesListToday,shop.getMinDelivery()).size ()==0){
                if (deliveryDay.contains ( "Later Today" ) && getDeliveryToday ( todayTime ,minutesListToday,shop.getMinDelivery()).size ()<=1){
                    deliveryDay.remove ( 0 );
                    deliveryTimeTv.setText( Html.fromHtml( "<font color='red'> " + getString(R.string.shop_is_closed_today) +"</font>".toUpperCase(Locale.US)));
                }
            }
        }else if (deliveryDay.contains ( "Later Today" ) && openHourToday.size ()<=1){
            deliveryDay.remove ( 0 );
        }

        if (tomorrowTime.size ()!=0){
            tomorrowDel  = new ArrayAdapter<> ( this, android.R.layout.simple_list_item_1, getDeliveryTomorrow ( tomorrowTime,minutesListTomorrow,shop.getMinDelivery() ));

        }
        //---------------
        if (deliveryDay.size ()==0){
            builder.setTitle ( Html.fromHtml( "<font color='red'>" + getString(R.string.shop_is_close) +"</font>".toUpperCase(Locale.US)) );
            setDeliveryClose ();
            builder.setPositiveButton ( getString(R.string.ok), new DialogInterface.OnClickListener () {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            } );
        }else if (getWorkingHour( openHourToday ).get ( "hour" ).size ()==0 && getWorkingHour( openHourTmr ).get ( "hour" ).size ()==0){
            builder.setTitle ( Html.fromHtml( "<font color='red'>" + getString(R.string.shop_is_close) +"</font>".toUpperCase(Locale.US)) );
            setDeliveryClose ();
            builder.setPositiveButton ( getString(R.string.ok), new DialogInterface.OnClickListener () {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            } );
        }else {
            spinnerTime.setLayoutParams ( new LinearLayout.LayoutParams ( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT ,Gravity.END ) );
            spinnerDay.setLayoutParams ( new LinearLayout.LayoutParams ( ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT ) );
            spinnerDay.setAdapter ( dayAD );

            spinnerDay.setOnItemSelectedListener ( new AdapterView.OnItemSelectedListener () {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (deliveryDay.size ()==2){
                        spinnerTime.setAdapter((position==0)? laterTodayDel: tomorrowDel);
                    }else if (deliveryDay.contains ( "Tomorrow" )){
                        spinnerTime.setAdapter ( tomorrowDel );
                    }else if (deliveryDay.contains ( "Later Today" )){
                        spinnerTime.setAdapter ( laterTodayDel );
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            } );

            spinnerTime.setOnItemSelectedListener ( new AdapterView.OnItemSelectedListener () {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (spinnerDay.getSelectedItem ().toString ().equals ( "Tomorrow" )){
                        timeSelected = getDeliveryTomorrow ( tomorrowTime,minutesListTomorrow ,shop.getMinDelivery() ).get ( position );
                        deliveryTime = DateUtil.getTomorrowDate ()+" "+timeSelected.trim ()+":00";
                        deliveryTimeTv.setText ( String.format ( Locale.US,("%s , %s"), getString(R.string.tomorrow),timeSelected));
                        MockupData.DELIVERY_DATE =deliveryTime;
                    }else {
                        timeSelected = getDeliveryToday ( todayTime ,minutesListToday , shop.getMinDelivery()).get ( position );
                        deliveryTime = DateUtil.getCurrentDateNewFormat ()+" "+timeSelected.trim ()+":00";
                        deliveryTimeTv.setText ( String.format ( Locale.US,("%s , %s"),getString(R.string.later_today),timeSelected));
                        MockupData.DELIVERY_DATE =deliveryTime;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            } );

            layout.addView ( spinnerDay );
            layout.addView ( spinnerTime );

            builder.setTitle ( R.string.select_delivery_time );
            builder.setView (layout);
            builder.setPositiveButton ( getString(R.string.confirm), new DialogInterface.OnClickListener () {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String delTimes = String.format(Locale.US,"%s , %s", spinnerDay.getSelectedItem (),spinnerTime.getSelectedItem ()) ;
                    deliveryTimeTv.setText (delTimes);
                    MockupData.DELIVERY_TIME_SHOPID = shop.getShopId ();
                    MockupData.DELIVERY_TIME= delTimes;
                }
            } );
        }
        builder.show ();
    }

    private HashMap<String,List<Integer>> getWorkingHour(List<OpenHour> openHours){

        HashMap <String, List<Integer>> map = new HashMap<> (  );
        final List<Integer> calHours = new ArrayList<> (  );
        final List<Integer> calMiniutes = new ArrayList<> (  );
        openHourMorning= openHours.get ( 0 );
        openHourAfternoon = openHours.get ( 1 );
        //calculate to find min of real time
        if (openHourMorning.isOpen () && openHourAfternoon.isOpen ()){
            for (OpenHour openHour : openHours){
                //shop open 2 shift
                calHours.add ( openHour.getOpenTime ()/60 );
                calHours.add ( openHour.getCloseTime ()/60 );
                calMiniutes.add ( openHour.getOpenTime ()%60 );
                calMiniutes.add ( openHour.getCloseTime ()%60 );
            }
        }else if (openHourMorning.isOpen ()&& !openHourAfternoon.isOpen ()){
            // if shop open morning
            calHours.add ( openHourMorning.getOpenTime ()/60 );
            calHours.add ( openHourMorning.getCloseTime ()/60 );
            calMiniutes.add ( openHourMorning.getOpenTime ()%60 );
            calMiniutes.add ( openHourMorning.getCloseTime ()%60 );
        }else if (openHourAfternoon.isOpen ()&& !openHourMorning.isOpen ()) {
            //if shop open afternoon
            calHours.add ( openHourAfternoon.getOpenTime ()/60 );
            calHours.add ( openHourAfternoon.getCloseTime ()/60 );
            calMiniutes.add ( openHourAfternoon.getOpenTime ()%60 );
            calMiniutes.add ( openHourAfternoon.getCloseTime ()%60 );
        }
        map.put ( "hour",calHours );
        map.put ( "min" ,calMiniutes);
        return map;
    }
    // hide the keyboard when do something
    private View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (!hasFocus){
                Util.hideSoftKeyboard(view);
            }
        }
    };

    private void afterPostOrder(){
        progressLoading.setVisibility(View.GONE);
        MockupData.setCurrentOrderDetail(null);
        MockupData.DELIVERY_TIME=null;
        MockupData.DELIVERY_DATE = null;
        MockupData.highLightItemIds = new HashMap<>();
        MapsActivity.oldEditShippingDeliver=null;
        DeliveryAddressActivity.phoneNumber=null;
        MockupData.setHouseNumber ( "" );
        startActivity(new Intent(OrderConfirmationActivity.this, OrderCompletedActivity.class));
        finish();
    }

    // Build OrderDetail Params
    private void postOrder(OrderRequestParams orderDetail){
        new OrderWs().createOrder(orderDetail, this, new OrderWs.CreateOrderListener() {
            @Override
            public void onCreateSuccess(String success) {
                afterPostOrder();
            }
            @Override
            public void onCreateError(String error,int code) {
                password = "";
                progressLoading.setVisibility(View.GONE);
                findViewById(R.id.place_order).setEnabled(true);
                if(code == 405)
                    popupPayment("",error,OrderConfirmationActivity.this,code);
                else popupOrderMessage(null, error, OrderConfirmationActivity.this);
            }

            @Override
            public void onViewOrder(PreView preView) {

            }

        });
    }

    @Override
    public void onBackPressed() {
        // this case when user click default android btn
        if(progressLoading.getVisibility() != View.VISIBLE) {
            if (!couponCode.getText().toString().isEmpty()) {
                popupMessageDiscart(null, this);
            } else {
                super.onBackPressed();
                //this case for add back
            }
        }
    }

    private  void popupMessageDiscart(String title, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(getResources ().getString ( R.string.suspend_order ));
        builder.setNegativeButton ( getResources ().getString ( R.string.cancel ) ,null);
        builder.setPositiveButton(getResources ().getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish ();
            }
        });
        builder.show();
    }

    private void popupPayment(String title, String message, Activity activity, final int code) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(code == 405){
                    Intent intent = new Intent(OrderConfirmationActivity.this,PaymentDepositActivity.class);
                    intent.putExtra("wallets", (Serializable) walletsList);
                    intent.putExtra ( "user",myUser );
                    intent.putExtra("size",Util.checkVisiblePaymentService());
                    startActivityForResult(intent,REQUEST_DEPOSIT_METHOD);
                } else {
                    loadPaymentDialog(walletsList,paymentHistories);
                }
            }
        });
        builder.show();
    }

    private void popupOrderMessage(String title, final String message, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(message != null && message.equals("The phone is Incorrect.") || message.equals(getString(R.string.sorry_your_delivery_is_out_zone))){
                    startActivityForResult(new Intent(OrderConfirmationActivity.this, DeliveryAddressActivity.class), REQUEST_CODE_EDIT_ADDRESS);
                }
            }
        });
        builder.show();
    }

    private  void popupDeliveryAddress(String title, String message, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivityForResult(new Intent(OrderConfirmationActivity.this, DeliveryAddressActivity.class), REQUEST_CODE_EDIT_ADDRESS);
            }
        });
        builder.show();
    }

    private  void popupMessageDeliveryTime(final String title, String message, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isInVocation ()){
                    timeSelected();
                }else {
                    setDeliveryClose ();
                }
            }
        });
        builder.show();
    }

    private void setDeliveryClose(){
        findViewById ( R.id.place_order ).setEnabled ( false );
        findViewById ( R.id.edit_time ).setVisibility ( View.GONE );
        deliveryTimeTv.setText( Html.fromHtml( "<font color='red'>" + getString(R.string.shop_is_close) +"</font>".toUpperCase(Locale.US)));
    }

    private void getWallet(){
        new ProfileWs().viewProfiles(OrderConfirmationActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user) {
                progressLoading.setVisibility(View.GONE);
                walletsList = user.getWalletsList();
                paymentHistories = user.getPaymentHistories();
                swipeRefreshLayout.setRefreshing(false);
                myUser = user;
                isPin = user.isConfirmPin();

                checkShipping =false;
                checkPayOnline = false;
                clickShippingPosition = 2; // auto select kiwiGo delivery
                setShippingMethod.setText(R.string.kiwi_go_delivery);
                PaymentHistoryAdapter.noteHistoryPostion =-1;

                if(!Util.checkIsHaveCash())
                    walletsList.add(0,new Wallets(-1,PAYMENT_CASH,""));
                for(int i =0 ; walletsList.size() > i ; i++){
                    if (walletsList.get(i).getDefaultPayment() == 1){
                        setPaymentMethod.setText(String.format("%s : %s %s",getString(R.string.wallet),walletsList.get(i).getCurrency(),walletsList.get(i).getBalance()));
                        walletId = walletsList.get(i).getWalletId();
                        PaymentMethodDialogFragment.notePosition = i;
                        break;
                    } else if (!Util.checkIsHaveCash()) {
                        PaymentMethodDialogFragment.notePosition =0;
                        walletId = -1 ;
                        setPaymentMethod.setText(PAYMENT_CASH);
                    }
                }
            }

            @Override
            public void onError(String errorMessage) {
                progressLoading.setVisibility(View.GONE);
                Toast.makeText(OrderConfirmationActivity.this,errorMessage,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUpdateProfilesSuccess(boolean isSendSms , User user) {

            }
        });
    }

    private void topUpByHistory(PaymentHistory paymentHistory, Double amount){
        Log.d("OnTopUp", "  ontopup");
        HashMap<String , Object > hashMap = new HashMap<>();
        hashMap.put("psp_id",paymentHistory.getPivot().getPspId());
        hashMap.put("phone",paymentHistory.getPaymentToken());
        int changeAmount = (int) Math.round(amount);
        hashMap.put("amount",changeAmount);
        for(Wallets wallets : walletsList){
            if(wallets.getWalletId() != -1)
                walletId = wallets.getWalletId();
        }
        hashMap.put("wallet_id",walletId);
        if(BuildConfig.DEBUG)
            hashMap.put("code","cm");
        else hashMap.put("code",countryCode);
        hashMap.put("pin_code",password);
        TopUpPayment.onTopUpOrange(OrderConfirmationActivity.this, hashMap, new TopUpPayment.OnResponse() {
            @Override
            public void onSuccess(String message, int paymentId) {
                Log.d("checkPaymentId", "onSuccess: "+paymentId);
                orderPaymentId = paymentId;
                countTime();
                titleProgress.setText(R.string.place_verify_code);
                count = 0;
            }

            @Override
            public void onResponseMessage(String message) {
                password = "";
                progressLoading.setVisibility(View.GONE);
                Util.popupMessage(getString(R.string.error),message,OrderConfirmationActivity.this);
            }
        });
    }

    private int count = 0;

    private void countTime(){
        new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("werwrewerwer"," second :"+  millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.d("werwrewerwer"," count :"+ count);
                if(count != 5)
                    getStatusTopUp();
                else {
                    count = 0;
                    titleProgress.setText(getString(R.string.loading));
                    progressLoading.setVisibility(View.GONE);
                    if(!isFinishing())
                        Toast.makeText(OrderConfirmationActivity.this,"Time out",Toast.LENGTH_LONG).show();
                }
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(checkPayOnline && !isOnResume)
            countTime();
        else isOnResume = false;
        Log.d("checkOnResume","   Resume");
    }

    private void getStatusTopUp(){
        new PaymentWs().getPaymentStatus(OrderConfirmationActivity.this, orderPaymentId, new PaymentWs.onTopUpWalletsCallBack() {
            @Override
            public void onSuccess(String status, int paymentId) {
                count++;
                if(status.equalsIgnoreCase("SUCCESSFULL")){
                    postOrder(orderRequestParams());
                } else {
                    countTime();
                }
            }

            @Override
            public void onError(String message) {
                count = 5;
                Util.popupMessage(null,message,OrderConfirmationActivity.this);
            }
        });
    }

    @Override
    public void onClickShippingType(String txtChoose,int position) {
        clickShippingPosition = position;
        if(position == 1){
            if(!Util.checkIsHaveCash() && walletsList.get(0).getWalletId() == -1)
                walletsList.remove(0);

            setPaymentMethod.setText(String.format("%s : %s %s",getString(R.string.wallet),walletsList.get(0).getCurrency(),walletsList.get(0).getBalance()));
            walletId = walletsList.get(0).getWalletId();
            PaymentMethodDialogFragment.notePosition = 0;
            PaymentHistoryAdapter.noteHistoryPostion = -1;
            checkPayOnline = false;
            checkShipping = true;

            addTip.setEnabled(false);
            (findViewById(R.id.sub_tip)).setEnabled(false);

            tipAmountTv.setText(Util.formatPrice(0));
            ridertip1Tv.setText(String.format("%s %s",currencySign,Util.formatPrice(0)));
            deliveryFeeValuesTv.setText(String.format("%s %s",currencySign,Util.formatPrice(0)));
            totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(getTotalPrice(true))));
        } else {
            if(!Util.checkIsHaveCash() && walletsList.get(0).getWalletId() != -1) {
                walletsList.add(0, new Wallets(-1, PAYMENT_CASH, ""));
                PaymentMethodDialogFragment.notePosition = 1;
            }
            checkPayOnline = false;
            checkShipping = false;
            if(PaymentHistoryAdapter.noteHistoryPostion != -1){
                checkPayOnline = true;
                PaymentMethodDialogFragment.notePosition = -1;
            }

            addTip.setEnabled(true);
            (findViewById(R.id.sub_tip)).setEnabled(true);
            tipAmountTv.setText(Util.formatPrice(tipTotal));
            ridertip1Tv.setText(String.format("%s %s",currencySign,Util.formatPrice(tipTotal)));
            totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(getTotalPrice(false))));
            deliveryFeeValuesTv.setText(String.format("%s %s",currencySign,Util.formatPrice(preView.getDelivery_fee())));
        }
        setShippingMethod.setText(txtChoose);
    }

}