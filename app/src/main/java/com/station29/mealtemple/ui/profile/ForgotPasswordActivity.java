package com.station29.mealtemple.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.SignUpWs;
import com.station29.mealtemple.ui.BaseActivity;

import java.util.HashMap;

public class ForgotPasswordActivity extends BaseActivity {


    private String EMAIL = "Use your Email Address" ;
    private String PHONE = "Use your Phone Number" ;
    private String inputType = EMAIL, action = null, userType = "USER_ANDROID", countryCode = BaseActivity.countryCode;
    private CountryCodePicker ccp;
    private EditText editTextPhone, editTextEmail;
    private Button btnRest ;
    private ImageButton btnBack;
    public static boolean isDidLogin = false;
    private final  int REQUEST_CODE_CONFIRM_SMS = 456;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        ccp = findViewById(R.id.ccp);
        editTextEmail = findViewById(R.id.txtEmail);
        findViewById(R.id.email_border).setVisibility(View.GONE);
        editTextPhone = findViewById(R.id.txtPhoneNumber);
        btnRest = findViewById(R.id.btnRest);
        btnBack = findViewById(R.id.btnBackLogin);
        progressBar = findViewById(R.id.progressBar);

        ccp.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);

        final TextView switchInputType = findViewById(R.id.btnChange);
        switchInputType.setTag(inputType);
        switchInputType.setText(getString(R.string.use_your_email));
        switchInputType.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                MockupData.EMAIL = "";
                MockupData.LOGIN_PHONE = "";
                if (switchInputType.getTag().equals(PHONE)) {
                    inputType = EMAIL;
                    findViewById(R.id.email_border).setVisibility(View.GONE);
                    findViewById(R.id.phone_border).setVisibility(View.VISIBLE);
                    ccp.setVisibility(View.VISIBLE);
                    editTextEmail.getText().clear();
                    switchInputType.setText(getString(R.string.use_your_email));
                } else {
                    inputType = PHONE;
                    findViewById(R.id.phone_border).setVisibility(View.GONE);
                    ccp.setVisibility(View.GONE);
                    findViewById(R.id.email_border).setVisibility(View.VISIBLE);
                    editTextPhone.getText().clear();
                    switchInputType.setText(getString(R.string.use_your_phone));
                }
                v.setTag(inputType);

            }
        });

        btnRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                progressBar.setVisibility(View.VISIBLE);
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                },500);
                if(editTextPhone.getText().toString().isEmpty()){
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ForgotPasswordActivity.this, R.string.please_input_your_phone_email,Toast.LENGTH_LONG).show();
                } else {
                    forgetPass();
                }

            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
    public void forgetPass(){
        HashMap<String,Object> userAccount = new HashMap<> (  ) ;
        if (!editTextPhone.getText().toString().isEmpty()) {
            userAccount.put("phone",editTextPhone.getText().toString().trim());
            userAccount.put("phone_code", ccp.getFullNumber());
        }else if (!editTextEmail.getText().toString().isEmpty()){
            userAccount.put("email",editTextEmail.getText().toString().trim());
        }
        userAccount.put ( "code",countryCode );
        userAccount.put ( "user_type",userType );
        userAccount.put("function","Customer");
        MockupData.setAccountSignUp ( userAccount );

        new SignUpWs().forgetAccountPassword(userAccount ,requestServer);
    }
    private SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer () {
        @Override
        public void onRequestSuccess() {
            progressBar.setVisibility(View.GONE);
            isDidLogin = true;
            Intent intent = new Intent(ForgotPasswordActivity.this, ConfirmSmsActivity.class);
            startActivityForResult(intent, REQUEST_CODE_CONFIRM_SMS);
        }

        @Override
        public void onRequestToken(String token, int id, User user) {

        }

        @Override
        public void onRequestFailed(String message) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText ( getApplicationContext (), message, Toast.LENGTH_SHORT ).show ();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CONFIRM_SMS && resultCode == RESULT_OK){
            // Go to New Password Screen
            Intent intent = new Intent(ForgotPasswordActivity.this, NewPasswordActivity.class);
            startActivity(intent);
        }
    }
}
