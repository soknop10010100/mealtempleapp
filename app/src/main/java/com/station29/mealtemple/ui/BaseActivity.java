package com.station29.mealtemple.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ActivityInfo;

import android.os.Bundle;

public class BaseActivity extends AppCompatActivity {

    public static double latitude;
    public static double longitude;
    public static String countryCode ;
    public static String currentCountryCode;
    public static boolean isFirst = false ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set the whole app to PORTRAIT
        setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        if(countryCode!=null)
//        if(countryCode.equalsIgnoreCase("kh")){
//            getTheme().applyStyle(R.style.AppTheme,true);
//        }else if(countryCode.equalsIgnoreCase("mm")){
//            getTheme().applyStyle(R.style.AppThemeMM, true);
//        }
    }
}
