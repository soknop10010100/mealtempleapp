package com.station29.mealtemple.ui.payment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.PaymentHistory;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.payment.adapter.PaymentHistoryAdapter;
import com.station29.mealtemple.util.Util;

import java.io.Serializable;

public class PaymentMethodActivity extends BaseActivity {

    private RecyclerView recyclerViewListPaymentHistory;
    private TextView balanceTv , currencyTv ,noHistoryTv;
    private User myUser;
    private PaymentHistoryAdapter paymentHistoryAdapter;
    private int removePosition;
    private ImageView backButton;
    private LinearLayout btnDeposit;
    private final int REQUEST_PAYMENT_DEPOSIT = 542;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        initView();
        initData();
        initAction();
    }

    private void initView(){
        recyclerViewListPaymentHistory = findViewById(R.id.list_payment_history);
        balanceTv = findViewById(R.id.balanceTv);
        currencyTv = findViewById(R.id.currencyTv);
        noHistoryTv = findViewById(R.id.txtNoHistory);
        backButton = findViewById(R.id.backButton);
        btnDeposit = findViewById(R.id.btnDeposit);
        progressBar = findViewById(R.id.progressBar);
    }

    private void initData(){
        if(getIntent().hasExtra("user") && getIntent().getSerializableExtra("user") != null){
            myUser = (User) getIntent().getSerializableExtra("user");
        }
    }

    private void initAction(){
        ((TextView) findViewById(R.id.title)).setText(getString(R.string.payment_method));

        currencyTv.setText(String.format("%s : %s",getString(R.string.currency), myUser.getWalletsList().get(0).getCurrency()));
        balanceTv.setText(String.format("%s : %s",getString(R.string.balance), myUser.getWalletsList().get(0).getBalance()));

        recyclerViewListPaymentHistory.setLayoutManager(new LinearLayoutManager(this));
        paymentHistoryAdapter = new PaymentHistoryAdapter(myUser.getPaymentHistories(),"payment",paymentHistory);
        recyclerViewListPaymentHistory.setAdapter(paymentHistoryAdapter);

        if(myUser.getPaymentHistories().isEmpty()){
            noHistoryTv.setVisibility(View.VISIBLE);
        }

        backButton.setOnClickListener(v -> {
            setResult(RESULT_OK);
            finish();
        });

        btnDeposit.setOnClickListener(v -> {
            Intent intent = new Intent(PaymentMethodActivity.this, PaymentDepositActivity.class);
            intent.putExtra("wallets", (Serializable) myUser.getWalletsList());
            intent.putExtra ( "user",myUser );
            intent.putExtra("size", Util.checkVisiblePaymentService());
            startActivityForResult(intent,REQUEST_PAYMENT_DEPOSIT);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_PAYMENT_DEPOSIT && resultCode == RESULT_OK){
            progressBar.setVisibility(View.VISIBLE);
            getWallet();
        }
    }

    private final PaymentHistoryAdapter.OnClickOnPaymentHistory paymentHistory = new PaymentHistoryAdapter.OnClickOnPaymentHistory() {
        @Override
        public void onClickOnPayment(PaymentHistory paymentHistory) {

        }

        @Override
        public void onClickOnRemove(int paymentId , int position) {
            removePosition = position;
            popupDeleteMessage(getString(R.string.are_you_sure_to_delete_your_paymnet_history),paymentId,PaymentMethodActivity.this);
        }
    };

    private void popupDeleteMessage(String message, int paymentId, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressBar.setVisibility(View.VISIBLE);
                new PaymentWs().deletePaymentHistory(PaymentMethodActivity.this, paymentId, new PaymentWs.onTopUpWalletsCallBack() {
                    @Override
                    public void onSuccess(String message, int paymentId) {
                        progressBar.setVisibility(View.GONE);
                        myUser.getPaymentHistories().remove(removePosition);
                        paymentHistoryAdapter.notifyDataSetChanged();
                        if(myUser.getPaymentHistories().isEmpty())
                            noHistoryTv.setVisibility(View.VISIBLE);
                        Toast.makeText(PaymentMethodActivity.this,message,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(String message) {
                        progressBar.setVisibility(View.GONE);
                        Toast.makeText(PaymentMethodActivity.this,message,Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.show();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

    private void getWallet(){
        new ProfileWs().viewProfiles(PaymentMethodActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user) {
                progressBar.setVisibility(View.GONE);
                myUser = user;

                if(myUser.getPaymentHistories().isEmpty()) {
                    noHistoryTv.setVisibility(View.VISIBLE);
                } else {
                    noHistoryTv.setVisibility(View.GONE);
                }

                currencyTv.setText(String.format("%s : %s",getString(R.string.currency),user.getWalletsList().get(0).getCurrency()));
                balanceTv.setText(String.format("%s : %s",getString(R.string.balance),user.getWalletsList().get(0).getBalance()));

                paymentHistoryAdapter = new PaymentHistoryAdapter(user.getPaymentHistories(),"payment",paymentHistory);
                recyclerViewListPaymentHistory.setAdapter(paymentHistoryAdapter);
            }

            @Override
            public void onError(String errorMessage) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(PaymentMethodActivity.this,errorMessage,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUpdateProfilesSuccess(boolean isSendSms, User user) {

            }
        });
    }
}

