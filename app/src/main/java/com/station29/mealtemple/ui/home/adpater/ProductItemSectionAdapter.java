package com.station29.mealtemple.ui.home.adpater;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Product;
import com.station29.mealtemple.util.Util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.github.luizgrp.sectionedrecyclerviewadapter.Section;
import io.github.luizgrp.sectionedrecyclerviewadapter.SectionParameters;

public class ProductItemSectionAdapter extends Section{
    /**
     * Create a Section object based on {@link SectionParameters}.
     *
     * @param sectionParameters section parameters
     */

    private String title;
    private List<Product> list;
    private onProductItemClickListener onProductItemClickListener;
    private HashMap<String, Integer> highLightItemIds;

    public ProductItemSectionAdapter(String title, List<Product> list, onProductItemClickListener onProductItemClickListener, HashMap<String, Integer> highLightItemIds) {

        super(SectionParameters.builder()
                .itemResourceId(R.layout.layout_product_item)
                .headerResourceId(R.layout.layout_product_header)
                .build());
        this.title = title;
        this.list = list;
        this.onProductItemClickListener = onProductItemClickListener;
        this.highLightItemIds = highLightItemIds;
    }

    @Override
    public int getContentItemsTotal() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ItemTabHolder(view);
    }

    @Override
    public void onBindItemViewHolder(RecyclerView.ViewHolder ItemTabHolder, int i) {
        final ItemTabHolder itemHolder = (ItemTabHolder) ItemTabHolder;

        final Product item = list.get(i);
        if(item !=null) {
            String currencySign = "";
            if (Constant.getConfig() != null)
                currencySign = Constant.getConfig().getCurrencySign();
            // when product doesnt have image we set default image.

            Glide.with(itemHolder.itemView.getContext()).load(item.getProductImg()).into(itemHolder.img);
            itemHolder.nameTv.setText(String.format("%s ", item.getName()));
            itemHolder.priceTv.setText(String.format("%s %s", currencySign, Util.formatPrice(item.getPrice())));
            itemHolder.detailTv.setText(String.format("%s", item.getDescription() == null ? "" : item.getDescription()));
            itemHolder.row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onProductItemClickListener.onProductItemClick(itemHolder.row, item);
                }
            });
            if (MockupData.getHighLightItemIds().containsKey(String.valueOf(item.getId()))) {
                itemHolder.verticalLine.setVisibility(View.VISIBLE);
                itemHolder.cartQtyTv.setVisibility(View.VISIBLE);
                itemHolder.cartQtyTv.setText(String.format(Locale.US, "X %d", MockupData.getHighLightItemIds().get(String.valueOf(item.getId()))));
            } else {
                itemHolder.verticalLine.setVisibility(View.GONE);
                itemHolder.cartQtyTv.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder getHeaderViewHolder(final View view) {
        return new ItemHeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(final RecyclerView.ViewHolder holder) {
        final ItemHeaderHolder headerHolder = (ItemHeaderHolder) holder;
        headerHolder.tvTitle.setText(title);
    }

    public interface onProductItemClickListener {
        void onProductItemClick(View itemView,Product item);
    }



    class ItemTabHolder extends RecyclerView.ViewHolder {

        ImageView img;
        TextView nameTv, priceTv, cartQtyTv, detailTv;
        View row, verticalLine;
        ImageView isPopular;

        ItemTabHolder(@NonNull View itemView) {
            super ( itemView );

            row = itemView;
            img = itemView.findViewById ( R.id.item_img );
            nameTv = itemView.findViewById ( R.id.name );
            priceTv = itemView.findViewById ( R.id.price );
            verticalLine = itemView.findViewById ( R.id.vertical_line );
            cartQtyTv = itemView.findViewById ( R.id.cart_qty );
            //isPopular = itemView.findViewById ( R.id.isPopular_item );
            detailTv = itemView.findViewById ( R.id.detail );
        }
    }

    class ItemHeaderHolder extends RecyclerView.ViewHolder{

         TextView tvTitle;

        ItemHeaderHolder(@NonNull View view) {
            super(view);

            tvTitle = view.findViewById(R.id.tvTitle);
        }
    }
}