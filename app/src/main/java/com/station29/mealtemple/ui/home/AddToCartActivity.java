package com.station29.mealtemple.ui.home;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.CompoundButtonCompat;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.AddOn;
import com.station29.mealtemple.api.model.Option;
import com.station29.mealtemple.api.model.OrderItem;
import com.station29.mealtemple.api.model.Product;
import com.station29.mealtemple.api.model.ProductDetail;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.request.ProductWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.station29.mealtemple.util.Util.isConnectedToInternet;
import static com.station29.mealtemple.util.Util.popupMessage;

public class AddToCartActivity extends BaseActivity implements View.OnClickListener {

    private Product mProduct;
    private ProductDetail pDetail;
    private TextView qtyTv,totalPriceTv,itemPriceTv, productName,itemDetailTv,defaultPrice,txtDecropstion;
    private int shopId,count = 1;
    private boolean emptyCart;
    private Shop shop;
    private EditText specialInstructionTv;
    private LinearLayout listOption;
    private double addOnPrice ,subTotal;
    private List<AddOn> mAddonList = new ArrayList<>();
     private List<AddOn> mOptions = new ArrayList<>();
    private String currencySign,action=null;
    private Bitmap bitmapUrl;
    private ImageView buttonBack,imageView;
    private OrderItem orderItem;
    private Context context;
    private LinearLayout layout;
    private LinearLayout detail_frame;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_cart);

        productName = findViewById(R.id.txtproductName);
        defaultPrice = findViewById(R.id.default_price);
        buttonBack= findViewById(R.id.backButton);
        imageView= findViewById(R.id.image);
        itemDetailTv = findViewById ( R.id.detail );
        detail_frame = findViewById(R.id.detail_frame);
        layout = findViewById(R.id.linnea);
        txtDecropstion = findViewById(R.id.txtDescription);
        progressBar = findViewById(R.id.progressBar);

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        specialInstructionTv =findViewById ( R.id.special_instruction );
        itemPriceTv = findViewById(R.id.price);
        MaterialButton addToCartBtn = findViewById(R.id.addToCartButton);

        //get currencysign
        currencySign = ((Constant.getConfig ()!=null)?Constant.getConfig ().getCurrencySign ():"");
        totalPriceTv = findViewById(R.id.totalPrice);
        if (!isConnectedToInternet ( AddToCartActivity.this )){
            popupMessage ( null, getResources ().getString ( R.string.failed_internet ),AddToCartActivity.this);
        }

        if (getIntent() != null && getIntent().hasExtra("item")) {
            mProduct = (Product) getIntent().getSerializableExtra("item");
            shopId = (int) getIntent().getSerializableExtra("shopId");
            if (mProduct == null) return;
            productName.setText(mProduct.getName());
            defaultPrice.setText(String.format("%s %s", Constant.getConfig().getCurrencySign(), Util.formatPrice(mProduct.getPrice())));
            Glide.with (imageView.getContext () )
                    .load ( mProduct.getProductImg() )
                    .into ( imageView );
            if (isConnectedToInternet ( AddToCartActivity.this )){
                if (Constant.getBaseUrl ()!=null) {
                    progressBar.setVisibility(View.VISIBLE);
                    loadItemDetailTask(mProduct.getAccountId(), mProduct.getId());
                }
            }
            //get detail for item
            if(mProduct.getDescription()==null){
                txtDecropstion.setVisibility(View.GONE);
                itemDetailTv.setVisibility(View.GONE);
            }
            itemDetailTv.setText(mProduct.getDescription() != null ? mProduct.getDescription() : "");

        }else if (getIntent ()!=null && getIntent ().hasExtra ( "action" )){
            orderItem = (OrderItem) getIntent ().getSerializableExtra ( "orderitems" );
            shop =(Shop) getIntent ().getSerializableExtra ( "shopedit" );
            count = (int) getIntent ().getSerializableExtra ( "count" );
            shopId = shop.getShopId ();
            action = (String) getIntent ().getSerializableExtra ( "action" );
            if (orderItem==null) return;
            productName.setText(orderItem.getName ());
            defaultPrice.setText(String.format("%s %s", Constant.getConfig().getCurrencySign(), Util.formatPrice(orderItem.getPrice())));
            Glide.with (imageView.getContext () )
                    .load ( orderItem.getProductImage () )
                    .into ( imageView );
            if (isConnectedToInternet ( AddToCartActivity.this )){
                if (Constant.getBaseUrl ()!=null) {
                    progressBar.setVisibility(View.VISIBLE);
                    loadItemDetailTask(shopId, orderItem.getProductId());
                }
            }
            isValid();
            //get detail for item
            txtDecropstion.setVisibility(View.GONE);
            itemDetailTv.setVisibility(View.GONE);
            specialInstructionTv.setText ( orderItem.getComment () );

            addToCartBtn.setText ( getResources ().getString ( R.string.update ) );//change string of add to cart to update
        }

        // Load Product Detail to set up price, add on & option
        qtyTv = findViewById(R.id.number);
        View subView = findViewById(R.id.sub);
        View addView = findViewById(R.id.add);

        qtyTv.setText(String.format(Locale.US, "%d", count));

        subView.setOnClickListener(this);
        addView.setOnClickListener(this);
        addToCartBtn.setOnClickListener(this);

        specialInstructionTv.setOnFocusChangeListener(onFocusChangeListener);
        // List Options
        listOption = findViewById(R.id.list_option);

    }


    private boolean isValid () {
        return pDetail != null && pDetail.getOption().size() == mOptions.size();
    }

    private View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (!hasFocus){
                Util.hideSoftKeyboard(view);
            } else {
                Util.showKeyboard(AddToCartActivity.this);
           }
         //   getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }

    };

    @Override
    public void onBackPressed() {
        if (action!=null && action.equalsIgnoreCase ( "EDIT")){
            //this case for update items
            popupMessageAddToCart (AddToCartActivity.this);
        }else {
            super.onBackPressed ();
            //this case for add normal items to cart
        }
    }

    @Override
    public void onClick(final View view) {
        if (view.getId() == R.id.backButton) {
            if (action!=null && action.equalsIgnoreCase ( "EDIT" )){
               popupMessageAddToCart ( AddToCartActivity.this );
            }else {
                finish();
            }
        } else if (view.getId() == R.id.sub) {
            if (count > 1) {
                count--;
                qtyTv.setText(String.format(Locale.US, "%d", count));
                itemPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(subTotal*count )));
                totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(count * (subTotal))));
            }
        } else if (view.getId() == R.id.add) {
            count++;
            qtyTv.setText(String.format(Locale.US, "%d", count));
            itemPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(subTotal *count)));
            totalPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(count * (subTotal))));
        } else if (view.getId() == R.id.addToCartButton) {
            Util.firebaseAnalytics(this,Constant.CLICK_ON,"add_to_cart");
            view.setEnabled(false);
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setEnabled(true);
                }
            },500);
            if (action!=null && action.equalsIgnoreCase ( "EDIT" )){
                addCart ();
            }else {
                if (MockupData.getCurrentOrderDetail() != null) {
                    shop = MockupData.getCurrentOrderDetail().getShop();
                }
                if (MockupData.getCurrentOrderDetail() == null || shopId == shop.getShopId()) { // if same stores can add new items
                    Log.d("dfkgf",pDetail.getOption().size()+ " : "+ mOptions.size());
                    addCart();
                } else {
                    Log.d("dfkgf",pDetail.getOption().size()+ " : "+ mOptions.size());
                    popupClearCart();
                }
            }
        }
    }

    private void popupClearCart(){
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(AddToCartActivity.this,R.style.DialogTheme);
        builder.setMessage(getString(R.string.cannot_order_multiple_store));
        builder.setNegativeButton(getString(R.string.cancel ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.setPositiveButton(getString(R.string.clear_and_add_new_items), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {// clear old cart
                emptyCart = true;
                MockupData.highLightItemIds = new HashMap<>();
                addCart();
            }
        });
        builder.show();
    }



    private boolean isExist(AddOn addOn, List<AddOn> addOnList) {
        for (AddOn aO : addOnList) {
            if (aO.getId() == addOn.getId()) {
                return true;
            }
        }
        return false;
    }

    private void loadItemDetailTask(int storeId, int productId) {
       new ProductWs().loadProductDetail(BaseActivity.countryCode, storeId, productId, new ProductWs.LoadProductCallback() {
            @Override
            public void onLoadSuccess(List<Product> productList) {
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                Util.popupMessage(null, errorMessage, AddToCartActivity.this);
            }

            @Override
            public void onLoadProductDetail(final ProductDetail productDetail) {
                // Hide Option & Add on if not exist
                progressBar.setVisibility(View.GONE);
                if (!productDetail.getAddOn().isEmpty()) {
                    findViewById(R.id.add_on_head).setVisibility(View.VISIBLE);
                }
                pDetail = productDetail;

                itemPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(productDetail.getPrice())));
                totalPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice((orderItem!=null)?productDetail.getPrice ()*count:productDetail.getPrice())));

                subTotal = ((orderItem!=null)?orderItem.getPrice ():pDetail.getPrice());


                if (pDetail.getOption().size()==0 && pDetail.getAddOn().size()==0){
                    LinearLayout linearLayout​ = findViewById(R.id.options_add_ons);
                    linearLayout​.setVisibility(View.GONE);
                }
                // Draw Option
                for (final Option option : pDetail.getOption()) {
                    RadioGroup optionGroupView = new RadioGroup(AddToCartActivity.this);
                    TextView groupNameTv = new TextView(AddToCartActivity.this);
                    groupNameTv.setTypeface ( ResourcesCompat.getFont (AddToCartActivity.this,R.font.montserrat_medium_body )  );
                    groupNameTv.setText(Html.fromHtml(option.getName().toUpperCase(Locale.US) + "<font color='red'>*</font>"));
                    listOption.addView(groupNameTv);
                    listOption.addView(optionGroupView);
                    List<AddOn> addOnList = option.getValue();
                    for (final AddOn addOn : addOnList) {
                        RadioButton radioButton = new RadioButton(AddToCartActivity.this);
                        radioButton.setTypeface ( ResourcesCompat.getFont (AddToCartActivity.this,R.font.montserrat_medium_body )  );
                        radioButton.setText(String.format("%s + %s %s", addOn.getLabel(),currencySign , Util.formatPrice ( addOn.getPrice() )));
                        CompoundButtonCompat.setButtonTintList(radioButton, ColorStateList
                                .valueOf(getResources().getColor(R.color.colorPrimary)));
                        optionGroupView.addView ( radioButton );
                        if (action!=null&&action.equalsIgnoreCase ( "EDIT" )){
                            //edit option
                            if (orderItem.getOption ().size ()!=0){
                                for (int i = 0; i < orderItem.getOption ().size (); i++) {
                                    if (orderItem.getOption ().get ( i ).getId ()== addOn.getId ()){
                                        subTotal += orderItem.getOption ().get ( i ).getPrice ();
                                        mOptions.add(addOn);
                                        radioButton.setChecked ( true );
                                    }
                                }
                            }
                            itemPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(subTotal*count)));
                            totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(subTotal*count )));
                        }
                        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b || !isExist(addOn, mOptions)) {
                                    subTotal += addOn.getPrice();
                                    mOptions.add(addOn);
                                } else {
                                    if (subTotal > 0) subTotal -= addOn.getPrice();
                                    mOptions.remove(addOn);
                                }
                                itemPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(subTotal*count )));
                                totalPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(count * subTotal)));
                            }
                        });
                    }
                }

                // Draw AddOn
                RadioGroup listAddOn = findViewById(R.id.list_addOn);
                listAddOn.setVisibility(View.VISIBLE);
                for (final AddOn addOn : productDetail.getAddOn()) {
                    CheckBox checkBox = new CheckBox(AddToCartActivity.this);
                    checkBox.setTypeface ( ResourcesCompat.getFont (AddToCartActivity.this,R.font.montserrat_medium_body ) );
                    checkBox.setText(String.format("%s +%s%s", addOn.getLabel(),currencySign ,Util.formatPrice((addOn.getPrice())) + ""));
                    CompoundButtonCompat.setButtonTintList(checkBox, ColorStateList
                            .valueOf(getResources().getColor(R.color.colorPrimary)));
                    listAddOn.addView ( checkBox );
                    if (action!=null&&action.equalsIgnoreCase ( "EDIT" )){
                        //edit add on
                        if (orderItem.getAddOns ().size ()!=0) {
                            for (int i = 0; i < orderItem.getAddOns ().size (); i++) {
                                if (orderItem.getAddOns ().get ( i ).getId () == addOn.getId ()) {
                                    subTotal += orderItem.getAddOns ().get ( i ).getPrice ();
                                    mAddonList.add(addOn);
                                    checkBox.setChecked ( true );
                                }
                            }
                        }
                        //calcute the check option
                        itemPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(subTotal*count)));
                        totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(subTotal*count )));
                    }
                    checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {
                                 subTotal += addOn.getPrice();
                                if (!isExist(addOn, mAddonList)) mAddonList.add(addOn);
                            } else {
                                if (subTotal > 0) subTotal -= addOn.getPrice();
                                if (isExist(addOn, mAddonList)) mAddonList.remove(addOn);
                            }
                            itemPriceTv.setText(String.format("%s %s",currencySign ,Util.formatPrice(subTotal*count )));
                            totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(count *subTotal )));
                        }
                    });
                }
            }

           @Override
           public void onLoadAllProduct(HashMap<String, List<Product>> allProduct) {

           }
       });
    }
    private void addCart(){
        Log.d("dfkgf",pDetail.getOption().size()+ " : "+ mOptions.size());
//        if ( pDetail != null  && mOptions != null && pDetail.getOption().size() == mOptions.size()) {
        if(isValid()){
            Intent intent = new Intent();
            if (emptyCart) {
                MockupData.setCurrentOrderDetail ( null );
                MockupData.DELIVERY_TIME= null;
                intent.putExtra("clearCart", true);
            }//check add on and option
            emptyCart = false; // reset to old state
            OrderItem orderItem = new OrderItem();
            orderItem.setProductId(pDetail.getId());
            orderItem.setProductImage(pDetail.getUrlImage());
            orderItem.setQty(count);
            orderItem.setName(pDetail.getName());
            orderItem.setPrice(pDetail.getPrice());
            orderItem.setOption(mOptions);
            orderItem.setAddOns(mAddonList);
            orderItem.setComment(specialInstructionTv.getText().toString());

            intent.putExtra("orderItem", orderItem);
            setResult( RESULT_OK,intent);
            finish();
        } else {
            if (Util.isConnectedToInternet ( AddToCartActivity.this )){
                Util.popupMessage(getString(R.string.error), getString(R.string.you_need_to_select_option), this);
            } else{
                popupMessage ( null, getResources ().getString ( R.string.failed_internet ),AddToCartActivity.this);
            }
        }
    }
    private void popupMessageAddToCart(Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(AddToCartActivity.this,R.style.DialogTheme);
        builder.setMessage(getResources ().getString ( R.string.cart_message ));
        builder.setTitle(getResources ().getString ( R.string.update_cart ));
        builder.setNegativeButton(String.format(Locale.US, "%s", getResources().getString(R.string.discard)), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.putExtra ("orderItem" ,orderItem );
                setResult ( RESULT_OK,intent );// send orderItem (old items back)
                finish();//clear the items from cart
            }
        });
        builder.setPositiveButton ( String.format ( Locale.US, "%s", activity.getString ( R.string.update ) ), new DialogInterface.OnClickListener () {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                addCart ();//update cart items
                finish ();
            }
        } );
        builder.show();
    }


}
