package com.station29.mealtemple.ui.payment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.PaymentHistory;
import com.station29.mealtemple.api.model.PaymentService;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.payment.adapter.PaymentHistoryAdapter;
import com.station29.mealtemple.ui.payment.adapter.PaymentListAdapter;
import com.station29.mealtemple.util.Redirect;
import com.station29.mealtemple.util.TopUpPayment;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PaymentDepositActivity extends BaseActivity implements View.OnClickListener {

    private ImageView backBtn;
    private TextView title, txtNoResult;
    private RecyclerView recyclerView , recyclerViewPaymentHistory;
    private List<Wallets> walletsList = new ArrayList<>();
    private View progressBar;
    private User user;
    private int size;
    private HashMap<String , Object> topUpParam;
    private final int REQUEST_GET_PIN = 2232;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_up);

        initView();
        initData();
        initAction();
    }

    private void initData(){
        if(getIntent().hasExtra("wallets") && getIntent().getSerializableExtra("wallets") != null){
            walletsList = (List<Wallets>) getIntent().getSerializableExtra("wallets");
        }
        if(getIntent().hasExtra("user") && getIntent().getSerializableExtra("user") != null){
            user = (User) getIntent().getSerializableExtra("user");
        }
        if(getIntent().hasExtra("size") && getIntent().getSerializableExtra("size") != null){
            size = getIntent().getIntExtra("size",0);
        }
    }

    private void initView(){
        backBtn = findViewById(R.id.backButton);
        title = findViewById(R.id.title);
        recyclerView = findViewById(R.id.list_service_payment);
        progressBar = findViewById(R.id.progressBar);
        recyclerViewPaymentHistory = findViewById(R.id.list_payment_history);
        txtNoResult = findViewById(R.id.txtNoResult);
    }

    private void initAction(){
        title.setText(getString(R.string.deposit_method));
        backBtn.setOnClickListener(this);

        PaymentListAdapter paymentListAdapter = new PaymentListAdapter(paymentClick, "profile", size, Constant.getConfig().getPaymentServiceList());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(paymentListAdapter);

        if(user.getPaymentHistories().isEmpty())
            txtNoResult.setVisibility(View.VISIBLE);
        recyclerViewPaymentHistory.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewPaymentHistory.setAdapter(new PaymentHistoryAdapter(user.getPaymentHistories(),"deposit",paymentHistory));
    }

    private final PaymentHistoryAdapter.OnClickOnPaymentHistory paymentHistory = new PaymentHistoryAdapter.OnClickOnPaymentHistory() {
        @Override
        public void onClickOnPayment(PaymentHistory paymentHistory) {
            PaymentService paymentService = new PaymentService();
            paymentService.setCountryCode(paymentHistory.getCountryCode());
            paymentService.setId(paymentHistory.getPivot().getPspId());
            paymentService.setPhone(paymentHistory.getPaymentToken());
            paymentService.setType(paymentHistory.getPspType() );
            Redirect.openPaymentServiceScreen(PaymentDepositActivity.this, paymentService, walletsList, new  Redirect.OnOpenPaymentCallback() {
                @Override
                public void onPaymentStart(HashMap<String, Object> param) {
                    progressBar.setVisibility(View.VISIBLE);
                    topUpParam = param;
                    Redirect.gotoPasswordConfirmActivity(PaymentDepositActivity.this,REQUEST_GET_PIN);
                }

                @Override
                public void onPaymentFinish() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }

        @Override
        public void onClickOnRemove(int paymentId, int position) {

        }
    };

    private final PaymentListAdapter.SetPaymentClick paymentClick = new PaymentListAdapter.SetPaymentClick() {
        @Override
        public void onClickPaymentClick(PaymentService paymentService) {
            Redirect.openPaymentServiceScreen(PaymentDepositActivity.this, paymentService, walletsList, new  Redirect.OnOpenPaymentCallback() {
                @Override
                public void onPaymentStart(HashMap<String, Object> param) {
                    progressBar.setVisibility(View.VISIBLE);
                    topUpParam = param;
                    Redirect.gotoPasswordConfirmActivity(PaymentDepositActivity.this,REQUEST_GET_PIN);
                }

                @Override
                public void onPaymentFinish() {
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GET_PIN && resultCode == RESULT_OK) {
            if (data != null) {
                progressBar.setVisibility(View.VISIBLE);
                topUpParam.put("pin_code", data.getStringExtra("pin"));
                TopUpPayment.onTopUpOrange(PaymentDepositActivity.this, topUpParam, new TopUpPayment.OnResponse() {
                    @Override
                    public void onSuccess(String message, int paymentId) {
                        progressBar.setVisibility(View.GONE);
                        Util.popupMessage(null,message,PaymentDepositActivity.this);
                    }

                    @Override
                    public void onResponseMessage(String message) {
                        progressBar.setVisibility(View.GONE);
                        Util.popupMessage(null,message,PaymentDepositActivity.this);
                    }
                });
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.backButton) {
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        finish();
    }

}