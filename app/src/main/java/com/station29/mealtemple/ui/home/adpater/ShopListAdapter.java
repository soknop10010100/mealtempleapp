package com.station29.mealtemple.ui.home.adpater;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.ui.home.ShopDetailActivity;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class ShopListAdapter extends RecyclerView.Adapter<ShopListAdapter.ShopViewHolder> {

    private List<Shop> shopList ;
    private OnShopClickListener mListener ;

    public ShopListAdapter(List<Shop> shopList, OnShopClickListener listener) {
        this.shopList = shopList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public ShopViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_shop_item, parent, false);
        return new ShopViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final ShopViewHolder holder, int position) {
        final Shop shop = shopList.get(position);

        if (shop != null) {
            Log.d("shopImage", shop.getImage());
            Glide.with(holder.imageView.getContext())
                    .load(shop.getImage()).apply ( new RequestOptions ().transforms (new CenterCrop (), new RoundedCorners ( 20 ) ) )
                    .into(holder.imageView);
            holder.shopNameTv.setText ( shop.getName () );
            Glide.with(holder.shopProfile.getContext())
                    .load(shop.getShopProfileImage())
                    .into( holder.shopProfile );
            holder.distanceTv.setText(String.format("%s km", Util.formatDistance(shop.getDistance())));
            holder.ratingBar.setRating ( shop.getShopRate () );
            String currencySign = "";
            if (Constant.getConfig()!=null) currencySign = Constant.getConfig().getCurrencySign ();
            if (shop.getMin() != 0) {
                    holder.minOrderTV.setText(String.format(" %s %s",currencySign ,Util.formatPrice (  shop.getMin() )));
                } else {
                    holder.minOrderTV.setText(String.format(" %s %s",currencySign , Util.formatPrice ( 0 )));
                }
                if (shop.getDelivery_fee() != 0) {
                    holder.deliveryFeeTV.setText(String.format(" %s %s", currencySign,Util.formatPrice ( shop.getDelivery_fee() )));
                } else {
                    holder.deliveryFeeTV.setText(String.format(" %s %s", currencySign, Util.formatPrice ( 0 )));
                }

            if (isMinDelivery(shop.getMinDelivery())>=60){
                int min ,hour;
                hour= isMinDelivery(shop.getMinDelivery())/60;
                min = isMinDelivery(shop.getMinDelivery())%60;

                if (min!=0) {
                    holder.shopDeliveryMinTv.setText(String.format(Locale.US, " %s hour(s) : %s mins",hour,min));
                }else {
                    holder.shopDeliveryMinTv.setText(String.format(Locale.US,  " %s hour(s)",hour));
                }

            }else {
                holder.shopDeliveryMinTv.setText(String.format(Locale.US,  " %s mins",isMinDelivery(shop.getMinDelivery())));
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onShopClick(holder.itemView, shop);
                }
            });
            //set shop close
            ShopDetailActivity.shopOpenHours= new ArrayList<>();
            ShopDetailActivity.checkShopClose ( shop,false );
            Calendar mCurrentTime = Calendar.getInstance();
            int mH = (mCurrentTime.get(Calendar.HOUR_OF_DAY) * 60) + mCurrentTime.get(Calendar.MINUTE);
            if (shop.getVacation().size()!=0){
                holder.shopCloseTv.setVisibility ( View.VISIBLE );
                holder.shopCloseTv.setText ( String.format ( Locale.US,"%s", holder.shopCloseTv.getContext().getString(R.string.close)));
            }else if(mH < ShopDetailActivity.shopOpenHours.get(0) || mH > ShopDetailActivity.shopOpenHours.get(1) && mH < ShopDetailActivity.shopOpenHours.get(2) || mH > ShopDetailActivity.shopOpenHours.get(3)){
                holder.shopCloseTv.setVisibility ( View.VISIBLE );
                holder.shopCloseTv.setText (String.format ( Locale.US,"%s",holder.shopCloseTv.getContext().getString(R.string.close)));
            }else {
                holder.shopCloseTv.setVisibility ( View.GONE );
            }

            if(Util.checkShopCloseServer()){
                holder.shopCloseTv.setVisibility ( View.VISIBLE );
                holder.shopCloseTv.setText (String.format ( Locale.US,"%s",holder.shopCloseTv.getContext().getString(R.string.close)));
            }
        }
    }


    @Override
    public int getItemCount() {
        return shopList.size();
    }

    class ShopViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView shopNameTv,distanceTv,minOrderTV,deliveryFeeTV,storeStatusTv,shopDeliveryMinTv,shopCloseTv;
        View view;
        RatingBar ratingBar;
        CircleImageView shopProfile;

        ShopViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;

            imageView = itemView.findViewById(R.id.shop_image);
            shopNameTv = itemView.findViewById(R.id.shop_name);
            distanceTv = itemView.findViewById(R.id.shop_distance);
//          shopStatus =itemView.findViewById(R.id.shop_status);
            ratingBar =itemView.findViewById ( R.id.shop_rated );
            shopProfile = itemView.findViewById ( R.id.shop_profileImage_homescreen );
            storeStatusTv = itemView.findViewById(R.id.store_status);
            minOrderTV = itemView.findViewById(R.id.shop_minorder);
            deliveryFeeTV =itemView.findViewById(R.id.shop_deliveryfee);
            shopDeliveryMinTv= itemView.findViewById(R.id.shop_deliverymin);
            shopCloseTv =itemView.findViewById ( R.id.shop_closeTv );
        }
    }

    public interface OnShopClickListener {
        void onShopClick(View itemView, Shop shop);
    }

    public void clear() {
        int size = shopList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                shopList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }

    private int isMinDelivery(Integer minShop){
        if (minShop != null){
            return minShop;
        }else {
            //if no minimum delivery we add 30 min
            return 30;
        }
    }

//    public float findLocation (double shopLat, double shopLng, double myLat, double myLng){
//        Location shopLocation = new Location("shopLocation");
//
//        shopLocation.setLatitude(shopLat);
//        shopLocation.setLongitude(shopLng);
//
//        Location myLocation = new Location("myLocation");
//
//        myLocation.setLatitude(myLat);
//        myLocation.setLongitude(myLng);
//
//        float distance = shopLocation.distanceTo(myLocation);
//
//        return distance/1000;
//    }

}