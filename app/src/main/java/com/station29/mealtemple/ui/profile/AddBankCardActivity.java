package com.station29.mealtemple.ui.profile;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.Bank;
import com.station29.mealtemple.ui.BaseActivity;

public class AddBankCardActivity extends BaseActivity {
    private Bank bank;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bank_card);

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ((TextView) findViewById(R.id.title)).setText(R.string.add_bank);
        TextView btnSave = findViewById(R.id.cancelBtn);
        btnSave.setVisibility(View.VISIBLE);
        btnSave.setText("Add");
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        if (getIntent() != null && getIntent().hasExtra("bank")){
            bank = (Bank) getIntent().getSerializableExtra("bank");
            if(bank == null)
                return;

            TextView bankName = findViewById(R.id.bank_name);
            bankName.setText(bank.getName());
            Glide.with(this)
                    .load(Constant.kessStoreUrl +bank.getLogo())
                    .into(new SimpleTarget<Drawable>() {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            bankName.setCompoundDrawablesWithIntrinsicBounds(resource,null,null,null);
                        }
                    });
        }
    }
}
