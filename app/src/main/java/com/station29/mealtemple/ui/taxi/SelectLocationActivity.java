package com.station29.mealtemple.ui.taxi;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Services;
import com.station29.mealtemple.api.request.TaxiWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.MapsActivity;
import com.station29.mealtemple.util.Util;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SelectLocationActivity extends BaseActivity implements View.OnClickListener {
    @Override
    public void onClick(View v) {

    }
//
//    private TextView txtFrom,txtGoto;
//    private ImageButton gotoMap;
//    private ImageView btnBack;
//    private int AUTOCOMPLETE = 2;
//    private String swipeSearch;
//    private final static int REQUEST_CODE_EDIT_ADDRESS = 236;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_select_location);
//
//        txtFrom = findViewById(R.id.txtFrom);
//        txtGoto = findViewById(R.id.txtTo);
//        gotoMap = findViewById(R.id.goToMap);
//        btnBack = findViewById(R.id.btnBack);
//
//        txtFrom.setOnClickListener(this);
//        txtGoto.setOnClickListener(this);
//        gotoMap.setOnClickListener(this);
//        btnBack.setOnClickListener(this);
//
//        Intent intent = getIntent();
//        if(intent.hasExtra("fromAddress")){
//            txtFrom.setText(getIntent().getStringExtra("fromAddress"));
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        if(v.getId() == R.id.txtFrom){
//            swipeSearch = "from";
//            searchMap();
//        }else if(v.getId() == R.id.txtTo){
//            swipeSearch = "goto";
//            searchMap();
//        }else if (v.getId() == R.id.goToMap){
//            Intent intent = new Intent(SelectLocationActivity.this,MapsActivity.class);
//            intent.putExtra("gotoMap","gotoMap");
//            startActivityForResult(intent, REQUEST_CODE_EDIT_ADDRESS);
//        }else if (v.getId() == R.id.btnBack){
//            finish();
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == AUTOCOMPLETE) {
//            if (resultCode == RESULT_OK) {
//                Place place = Autocomplete.getPlaceFromIntent(data);
//                if (swipeSearch.equals("goto")) {
//                    txtGoto.setText(place.getAddress());
//                    setresult();
//                } else {
//                    txtFrom.setText(place.getAddress());
//                    setresult();
//                }
//            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
//                Status status = Autocomplete.getStatusFromIntent(data);
//                Util.popupMessage("Error", status.getStatusMessage(), this);
//            } else if (resultCode == RESULT_CANCELED) {
//
//            }
//        }
//        if (requestCode == REQUEST_CODE_EDIT_ADDRESS && resultCode == RESULT_OK) {
//            if (data != null && data.hasExtra("delivery_address")) {
//                String deliveryAddress = data.getStringExtra("delivery_address");
//                if(data.getStringExtra("selectMapMethod").equals("goingTo")){
//                    txtGoto.setText(deliveryAddress);
//                }else{
//                    txtFrom.setText(deliveryAddress);
//                }
//                setresult();
//            }
//        }
//    }
//
//        private void searchMap () {
//            if (!Places.isInitialized()) {
//                Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
//            }
//            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS, Place.Field.LAT_LNG);
//
//            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                    .setCountry(BaseActivity.countryCode)
//                    .build(SelectLocationActivity.this);
//            startActivityForResult(intent, AUTOCOMPLETE);
//        }
//
//        private void setresult(){
//            if(!txtFrom.getText().toString().equals("") && !txtGoto.getText().toString().equals("")){
//                Intent myIntent = new Intent();
//                String gotomap = txtGoto.getText().toString();
//                String frommap = txtFrom.getText().toString();
//                myIntent.putExtra ( "delivery_address_from", frommap);
//                myIntent.putExtra ( "delivery_address_goto", gotomap );
//                setResult(RESULT_OK,myIntent);
//                finish();
//            }
//        }
//    private void getDriverSerive() {
//        new TaxiWs().getDriverServices (11.568932, 104.89096, 11.578819, 104.858880, new TaxiWs.getDriverCallback()  {
//            @Override
//            public void onLoadSuccess(List<Services> servicesList) {
//
//            }
//
//            @Override
//            public void onLoadFailed(String errorMessage) {
//
//            }
//        });
//    }
}
