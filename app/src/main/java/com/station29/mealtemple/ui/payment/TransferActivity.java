package com.station29.mealtemple.ui.payment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.MyQRCode;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.api.orangemoney.TransferWs;
import com.station29.mealtemple.api.request.ScanMeWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.RsaHelper;
import com.station29.mealtemple.util.Util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TransferActivity extends BaseActivity implements View.OnClickListener  {

    private MyQRCode myQRCode;
    private EditText amountEt,userIdEt;
    private String password;
    private final int REQUEST_PIN_CODE = 3463;
    private View progressBar;
    private TextView txtBalance;
    private User user;
    private LinearLayout userNameScreen, userIdScreen;
    private boolean isTransfer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        TextView currency = findViewById(R.id.currency);
        progressBar = findViewById(R.id.progress_loading);
        txtBalance = findViewById(R.id.acc_balance);
        userIdScreen = findViewById(R.id.userId);
        userNameScreen = findViewById(R.id.userName);
        userIdEt = findViewById(R.id.user_id_et);
        amountEt = findViewById(R.id.amount_et);

        if (getWallet() != null) {
            currency.setText(getWallet().getCurrency());
        }

        if (getSupportActionBar() != null) {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.transfer));
        }

        try{
            if (getIntent() != null && getIntent().hasExtra("qrCode")) {
                String qrCode = getIntent().getStringExtra("qrCode");
                myQRCode = new Gson().fromJson(qrCode, MyQRCode.class);

                TextView fullNameEt = findViewById(R.id.fullName);
                fullNameEt.setText(myQRCode.getFullName());
            }
        }catch (Exception exception){
            Util.popupMessageAndFinish(getString(R.string.error),getString(R.string.invalid_qr_code),TransferActivity.this);
        }

        if (getIntent() != null && getIntent().hasExtra("user")) {
            user = (User) getIntent().getSerializableExtra("user");
            txtBalance.setText(String.format("%s %s %s", getString(R.string.balance), user.getWalletsList().get(0).getCurrency(), user.getWalletsList().get(0).getBalance()));
        }
        if (getIntent() != null && getIntent().hasExtra("transfer")){
            isTransfer = true;
            userNameScreen.setVisibility(View.GONE);
            userIdScreen.setVisibility(View.VISIBLE);
        }

        View payButton = findViewById(R.id.pay);
        payButton.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String amount = amountEt.getText().toString();
        if (amount.isEmpty() || Double.parseDouble(amountEt.getText().toString()) <= 0){
            Util.popupMessage(getString(R.string.transfer), getString(R.string.please_enter_amount), TransferActivity.this);
        } else if( isTransfer && password == null || isTransfer && password.equalsIgnoreCase("")){
            if(!userIdEt.getText().toString().equals("")) {
                progressBar.setVisibility(View.VISIBLE);
                getMyQRCode(userIdEt.getText().toString());
            } else Toast.makeText(TransferActivity.this,getString( R.string.please_input_account_number),Toast.LENGTH_SHORT).show();
        } else if (password == null || password.equalsIgnoreCase("")) {
            Intent intent = new Intent(TransferActivity.this, PasswordConfirmActivity.class);
            intent.putExtra("userName",myQRCode.getFullName());
            startActivityForResult(intent,REQUEST_PIN_CODE );
        }
    }

    private void getMyQRCode(String id){
        new ScanMeWs().getUserById(TransferActivity.this, id, new ScanMeWs.ScanMeCallback() {
            @Override
            public void onSuccess(MyQRCode qrCode, JsonObject dataObj) {
                progressBar.setVisibility(View.GONE);
                myQRCode = qrCode;
                Intent intent = new Intent(TransferActivity.this, PasswordConfirmActivity.class);
                intent.putExtra("userName",qrCode.getFullName());
                startActivityForResult(intent,REQUEST_PIN_CODE );
            }

            @Override
            public void onError(String error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(TransferActivity.this,error,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Wallets getWallet() {
        List<Wallets> walletsList = MockupData.getUser().getWalletsList();
        if (walletsList != null && walletsList.size() > 0) {
            return walletsList.get(0);
        }
        return null;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_PIN_CODE){
            if(data != null){
                password = data.getStringExtra("pin");
                doTransfer();
            } else {
                password ="";
            }
        }
    }

    private void doTransfer() {
        progressBar.setVisibility(View.VISIBLE);
        if (myQRCode == null) return;
        HashMap<String, String> hashMap = new HashMap<>();
        Wallets wallet = getWallet();

        // data to sign ==============
        String amount = amountEt.getText().toString();
        if (amount.isEmpty() || Double.parseDouble(amountEt.getText().toString()) <= 0) {
            password= "";
            progressBar.setVisibility(View.GONE);
            Util.popupMessage(getString(R.string.transfer), getString(R.string.please_enter_amount), TransferActivity.this);
            return;
        }
        if (wallet != null) {
            hashMap.put("currency", Util.encodeByPublicKey(wallet.getCurrency()));
            hashMap.put("wallet_id", Util.encodeByPublicKey(String.valueOf(wallet.getWalletId())));
        }

        // ====================
        hashMap.put("amount", Util.encodeByPublicKey(amount));
        hashMap.put("receiver_id", Util.encodeByPublicKey(myQRCode.getAccountId()));
        hashMap.put("service", "confirm_pay");
        hashMap.put("sign_type", "RSA2");
        hashMap.put("pin_code", Util.encodeByPublicKey(password));
        // Sign all keys
        String dataToSign = "";
        Map<String, String> mapSort = new TreeMap<String, String>(hashMap);
        for (String key : mapSort.keySet()) {
            if ("sign_type".equals(key)) continue;
            String value = mapSort.get(key);
            if (!dataToSign.isEmpty()) dataToSign = dataToSign.concat("&");
            String pair = key.concat("=").concat(value);
            dataToSign = dataToSign.concat(pair);
            Log.d("keyOrdering", key +":" + mapSort.get(key));
        }
        String globalId = Util.getString("global_id", TransferActivity.this);
        String signRecord = RsaHelper.hashRecord(dataToSign, globalId);
        hashMap.put("sign", signRecord);
        new TransferWs().doTransfer(TransferActivity.this, hashMap, mTransferCallback);
        String json = new Gson().toJson(hashMap);
        Log.d("dataToHash", globalId+dataToSign);
        Log.d("dataToSignFinal", signRecord);
        Log.d("hashMapToTest", json);
    }

    private final TransferWs.TransferCallback mTransferCallback = new TransferWs.TransferCallback() {
        @Override
        public void onTransferSuccess(int status, String message) {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(TransferActivity.this,message,Toast.LENGTH_SHORT).show();
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onTransferFailed(String message) {
            progressBar.setVisibility(View.GONE);
            password= "";
            Util.popupMessage(getString(R.string.transfer), message, TransferActivity.this);
        }
    };
}