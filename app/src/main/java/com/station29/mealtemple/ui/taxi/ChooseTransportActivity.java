package com.station29.mealtemple.ui.taxi;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.PaymentHistory;
import com.station29.mealtemple.api.model.Services;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.api.model.Waypoint;
import com.station29.mealtemple.api.request.PaymentWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.api.request.TaxiWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.OrderConfirmationActivity;
import com.station29.mealtemple.ui.payment.PasswordConfirmActivity;
import com.station29.mealtemple.ui.payment.PaymentDepositActivity;
import com.station29.mealtemple.ui.payment.adapter.PaymentHistoryAdapter;
import com.station29.mealtemple.ui.payment.PaymentMethodActivity;
import com.station29.mealtemple.ui.payment.PaymentMethodDialogFragment;
import com.station29.mealtemple.ui.taxi.adapter.ChooseTransportAdapter;
import com.station29.mealtemple.util.TopUpPayment;
import com.station29.mealtemple.util.Util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChooseTransportActivity extends BaseActivity implements PaymentMethodDialogFragment.PaymentClickListener  {

    private ChooseTransportAdapter chooseTransportAdapter;
    private List<Services> servicesLists = new ArrayList<>() ;
    private Waypoint waypoint;
    private TextView titleProgress , setPaymentMethod;
    private HashMap<String,Object> orderTaxi;
    private boolean checkPayOnline = false, checkShipping = false , checkOrderWithoutDropLatLng = false , isDropOff = true;
    private View progressBar;
    private EditText applyCouponTv;
    private double pickLat, pickLng, dropLat, dropLng;
    private String userId= " ",pinCode;
    private RecyclerView recyclerView;
    private Services services;

    private int walletId , orderPaymentId;
    private List<Wallets> walletsList = new ArrayList<>();
    private final int REQUEST_PAYMENT_METHOD= 26232 ,REQUEST_DEPOSIT_METHOD=1237 ,REQUEST_PIN_CODE = 5334;
    private List<PaymentHistory> paymentHistories = new ArrayList<>();
    private User myUser;
    private final String PAYMENT_CASH = "Pay by cash", PAY_WALLET = "Pay by wallet";
    private PaymentHistory paymentHistory;
    private boolean isOnResume;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView ( R.layout.activity_choose_transport );

        TextView distanceValue = findViewById(R.id.distanceValue);
        TextView durationValue = findViewById(R.id.durationValue);
        TextView title =  findViewById(R.id.title);
        progressBar = findViewById(R.id.progressBar);
        titleProgress = findViewById(R.id.title_view);
        RelativeLayout btnPayment = findViewById(R.id.payment);
        LinearLayout screenPayment = findViewById(R.id.payment1);
        setPaymentMethod = findViewById(R.id.setPaymentMethod);

        title.setText(R.string.transport_option);
        progressBar.setVisibility(View.GONE);

        if(Constant.getBaseUrl()!=null)getWallet();
        if(getIntent()!=null && getIntent().hasExtra("servicesList")){
            servicesLists = (List<Services>) getIntent().getSerializableExtra("servicesList");
            if(getIntent() != null && getIntent().hasExtra("taxiWaypoint"))
                waypoint = (Waypoint) getIntent().getSerializableExtra("taxiWaypoint");
            orderTaxi = (HashMap<String, Object>) getIntent().getSerializableExtra("pickDropPlace");

            if(orderTaxi.get("dropoff_latitute").equals("0.0") && orderTaxi.get("dropoff_longitute").equals("0.0")){
                orderTaxi.remove("dropoff_latitute");
                orderTaxi.remove("dropoff_longitute");
                checkOrderWithoutDropLatLng = true;
            }
            if(getIntent().hasExtra("pickup_lat")){
                pickLat = getIntent().getDoubleExtra("pickup_lat",-1);
            }
            if(getIntent().hasExtra("pickup_long")){
                pickLng = getIntent().getDoubleExtra("pickup_long",-1);
            }
            if(getIntent().hasExtra("drop_lat")){
                dropLat = getIntent().getDoubleExtra("drop_lat",-1);
            }
            if(getIntent().hasExtra("drop_long")){
                dropLng = getIntent().getDoubleExtra("drop_long",-1);
            }

            if (waypoint != null) {
                isDropOff = false;
                distanceValue.setText(String.format("%s", Util.formatDistance(waypoint.getDistance() / 1000)) + " Km ");
                int caWaypoint = (int) Math.round(waypoint.getDuration() / 60);
                if(caWaypoint>=60){
                    String duration = "" + ((caWaypoint >= 60) ? caWaypoint / 60 + " h : " + caWaypoint % 60 + " mins" : caWaypoint + " mins");
                    durationValue.setText(duration);
                }else {
                    double time = Math.ceil(caWaypoint);
                    durationValue.setText(Util.formatTime((int) time));
                }
            }else{
                isDropOff = true;
                screenPayment.setVisibility(View.GONE);
                distanceValue.setText("--- Km");
                durationValue.setText("--- mins");
            }
        }

        recyclerView = findViewById(R.id.list_transport);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(servicesLists != null && !servicesLists.isEmpty()) {
            chooseTransportAdapter = new ChooseTransportAdapter(onTransportClickListener, servicesLists, checkOrderWithoutDropLatLng,0);
            recyclerView.setAdapter(chooseTransportAdapter);
        }

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadPaymentDialog(walletsList,paymentHistories);
            }
        });

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        userId = Util.getString("user_id",ChooseTransportActivity.this);
        applyCouponTv = findViewById(R.id.coupon_codes);
        MaterialButton btnApplyCoupon = findViewById(R.id.apply_coupons);
        btnApplyCoupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userId = Util.getString("user_id",ChooseTransportActivity.this);

                 if(applyCouponTv.getText().toString().isEmpty()) {
                   Util.popupMessage(null,getString(R.string.please_input_your_coupon),ChooseTransportActivity.this);
                 } else {
                    if(userId!=null&& !userId.isEmpty()){
                        servicesLists = new ArrayList<>();
                        applyCoupon(pickLat,pickLng,dropLat,dropLng,Integer.parseInt(userId),applyCouponTv.getText().toString());
                    }else return;

                }
            }
        });
    }

    public void loadPaymentDialog(List<Wallets> wallets, List<PaymentHistory> paymentHistories) {
        PaymentMethodDialogFragment dialogFragment = PaymentMethodDialogFragment.newInstance(wallets, paymentHistories,checkShipping);
        dialogFragment.show(getSupportFragmentManager(), "PaymentMethodDialogFragment");
    }

    private void applyCoupon(double pickLat, double pickLng,double dropLat,double dropLng,int userId,String txtCoupon){
        new TaxiWs().applyCoupon(pickLat, pickLng, dropLat, dropLng, userId, txtCoupon, new TaxiWs.getDriverCallback() {
            @Override
            public void onLoadSuccess(List<Services> servicesList, Waypoint waypoint) {
                chooseTransportAdapter.clear();
                servicesLists.addAll(servicesList);
                chooseTransportAdapter = new ChooseTransportAdapter(onTransportClickListener, servicesLists, checkOrderWithoutDropLatLng,1);
                recyclerView.setAdapter(chooseTransportAdapter);
                chooseTransportAdapter.notifyDataSetChanged();
            }

            @Override
            public void onLoadSuccessWithoutDrop(List<Services> servicesList) {

            }

            @Override
            public void onLoadFailed(String errorMessage, int code) {
                Util.popupMessage(null,errorMessage,ChooseTransportActivity.this);
            }

            @Override
            public void onOrder(String orderId) {

            }
        });
    }

    private ChooseTransportAdapter.OnTransportClickListener onTransportClickListener = new ChooseTransportAdapter.OnTransportClickListener() {
        @Override
        public void onTransportClick(View itemView, Services services) {
            Util.firebaseAnalytics(ChooseTransportActivity.this, Constant.SERVICE_NAME,services.getServiceType());
            if (!NotificationManagerCompat.from(getApplicationContext()).areNotificationsEnabled())
                Util.buildAlertMessageNoNotification(ChooseTransportActivity.this);
            else {
                chooseTaxi(services,applyCouponTv.getText().toString());
            }
        }
    };

    private void chooseTaxi(Services service,String coupon){
        services = service;
        orderTaxi.put("service_id", String.valueOf(services.getServiceId()));
        orderTaxi.put("country_code",BaseActivity.countryCode);
        orderTaxi.put("coupon_code",coupon);
        popupOrderTaxi(getString(R.string.booking), getString(R.string.do_you_want_to_book) + " " + services.getServiceType() + "?", ChooseTransportActivity.this,services);

    }

    private void popupOrderTaxi(final String title, String message, Activity activity ,Services services) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                progressBar.setVisibility(View.GONE);
                if(!checkPayOnline)
                    if (walletId != -1){
                        Intent intent = new Intent(ChooseTransportActivity.this , PasswordConfirmActivity.class);
                        intent.putExtra("orderType","payWallet");
                        startActivityForResult(intent,REQUEST_PIN_CODE);
                    } else{
                        if(orderTaxi.containsKey("wallet_id")) {
                            orderTaxi.remove("wallet_id");
                            orderTaxi.remove("pin_code");
                        }
                        orderTaxi();
                    }
                else {
                    Intent intent = new Intent(ChooseTransportActivity.this , PasswordConfirmActivity.class);
                    intent.putExtra("orderType","payAuto");
                    startActivityForResult(intent,REQUEST_PIN_CODE);
                }
            }
        });
        builder.setNegativeButton(activity.getString(R.string.cancel), null);
        builder.show();
    }

    private void orderTaxi(){
        if(walletId == -1 ){
            Util.firebaseAnalytics(ChooseTransportActivity.this,Constant.PAYMENT_TYPE, PAYMENT_CASH);
            orderTaxi.put("payment_type",PAYMENT_CASH);
        } else {
            Util.firebaseAnalytics(ChooseTransportActivity.this,Constant.PAYMENT_TYPE, PAY_WALLET);
            orderTaxi.put("payment_type",PAY_WALLET);
            orderTaxi.put("wallet_id",walletId);
            orderTaxi.put("pin_code",pinCode);
        }
        new TaxiWs().orderDriverService(ChooseTransportActivity.this,orderTaxi, new TaxiWs.getDriverCallback() {
            @Override
            public void onLoadSuccess(List<Services> servicesList, Waypoint waypoint) {

            }

            @Override
            public void onLoadSuccessWithoutDrop(List<Services> servicesList) {

            }

            @Override
            public void onLoadFailed(String errorMessage,int code) {
                pinCode= "";
                progressBar.setVisibility(View.GONE);
                if(code ==405){
                    popupPayment(getString(R.string.error),errorMessage,ChooseTransportActivity.this,code);
                }else {
                    Util.popupMessage(null, errorMessage, ChooseTransportActivity.this);
                }
            }

            @Override
            public void onOrder(String id) {
                progressBar.setVisibility(View.GONE);
                Util.clearStringq(ChooseTransportActivity.this,"waypointDrive");
                Intent intent  = new Intent(ChooseTransportActivity.this,FindDriverActivity.class);
                intent.putExtra("orderId",id);
                intent.putExtra("type_order","service");
                intent.putExtra("orderTaxi",orderTaxi);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_PIN_CODE && resultCode == RESULT_OK){
            isOnResume  =true;
            if(data != null && data.hasExtra("pin")){
                progressBar.setVisibility(View.GONE);
                pinCode = data.getStringExtra("pin");
                if(checkPayOnline){
                    topUpOrangePay(paymentHistory,Double.parseDouble(services.getTotalPrice()));
                } else {
                    orderTaxi();
                }
            } else {
                progressBar.setVisibility(View.GONE);
            }
        } else if (requestCode == REQUEST_DEPOSIT_METHOD && resultCode == RESULT_OK || requestCode == REQUEST_PAYMENT_METHOD && resultCode == RESULT_OK){
            progressBar.setVisibility(View.GONE);
            if(Constant.getBaseUrl()!=null)getWallet();
        }
    }

    private void getWallet(){
        new ProfileWs().viewProfiles(ChooseTransportActivity.this, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user) {
                progressBar.setVisibility(View.GONE);
                walletsList = user.getWalletsList();
                paymentHistories = user.getPaymentHistories();
                myUser = user;
                if(!Util.checkIsHaveCash())
                    walletsList.add(0,new Wallets(-1,PAYMENT_CASH,""));
                for(int i =0 ; walletsList.size() > i ; i++){
                    if (walletsList.get(i).getDefaultPayment() == 1){
                        setPaymentMethod.setText(String.format("%s : %s %s",getString(R.string.wallet),walletsList.get(i).getCurrency(),walletsList.get(i).getBalance()));
                        walletId = walletsList.get(i).getWalletId();
                        PaymentMethodDialogFragment.notePosition = i;
                        break;
                    } else if (!Util.checkIsHaveCash()) {
                        PaymentMethodDialogFragment.notePosition = 0;
                        walletId = -1;
                        setPaymentMethod.setText(PAYMENT_CASH);
                    }
                }
                if(isDropOff)
                    walletId = -1;
            }

            @Override
            public void onError(String errorMessage) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(ChooseTransportActivity.this,errorMessage,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUpdateProfilesSuccess(boolean isSendSms,User user) {

            }
        });
    }

    private void topUpOrangePay(PaymentHistory paymentHistory, Double amount){
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String , Object > hashMap = new HashMap<>();
        hashMap.put("psp_id",paymentHistory.getPivot().getPspId());
        hashMap.put("phone",paymentHistory.getPaymentToken());
        int changeAmount = (int) Math.round(amount);
        hashMap.put("amount",changeAmount);
        if(!Util.checkIsHaveCash()) walletId = walletsList.get(1).getWalletId();
        else walletId = walletsList.get(0).getWalletId();
        hashMap.put("wallet_id",walletId);
        if(BuildConfig.DEBUG)
        hashMap.put("code","cm");
        else hashMap.put("code",countryCode);
        hashMap.put("pin_code",pinCode);
        TopUpPayment.onTopUpOrange(ChooseTransportActivity.this, hashMap, new TopUpPayment.OnResponse() {
            @Override
            public void onSuccess(String message, int paymentId) {
                Log.d("checkPaymentId", "onSuccess: "+paymentId);
                orderPaymentId = paymentId;
                countTime();
                titleProgress.setText(R.string.place_verify_code);
                count = 0;
            }

            @Override
            public void onResponseMessage(String message) {
                pinCode = "";
                progressBar.setVisibility(View.GONE);
                Util.popupMessage(getString(R.string.error),message,ChooseTransportActivity.this);
            }
        });
    }

    private int count = 0;

    private void countTime(){
        new CountDownTimer(20000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("werwrewerwer"," second :"+  millisUntilFinished / 1000);
            }

            public void onFinish() {
                Log.d("werwrewerwer"," count :"+ count);
                if(count != 5)
                    getStatusTopUp();
                else {
                    count = 0;
                    titleProgress.setText(getString(R.string.loading));
                    progressBar.setVisibility(View.GONE);
                    if(!isFinishing())
                        Toast.makeText(ChooseTransportActivity.this,"Time out",Toast.LENGTH_LONG).show();
                }
            }
        }.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkPayOnline && !isOnResume)
            countTime();
        else isOnResume = false;
    }

    private void getStatusTopUp(){
        new PaymentWs().getPaymentStatus(ChooseTransportActivity.this, orderPaymentId, new PaymentWs.onTopUpWalletsCallBack() {
            @Override
            public void onSuccess(String status, int paymentId) {
                count++;
                if(status.equalsIgnoreCase("SUCCESSFULL")){
                    orderTaxi();
                } else {
                    countTime();
                }
            }

            @Override
            public void onError(String message) {
                Util.popupMessage(null,message,ChooseTransportActivity.this);
            }
        });
    }

    private void popupPayment(String title, String message, Activity activity, int code) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(activity.getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(code == 405 && myUser != null ){
                    Intent intent = new Intent(ChooseTransportActivity.this, PaymentDepositActivity.class);
                    intent.putExtra("wallets", (Serializable) walletsList);
                    intent.putExtra ( "user",myUser );
                    intent.putExtra("size",Util.checkVisiblePaymentService());
                    startActivityForResult(intent,REQUEST_DEPOSIT_METHOD);
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onPayByWallet(String typePay, int walletId1) {
        checkPayOnline = false;
        setPaymentMethod.setText(typePay);
        walletId = walletId1;
    }

    @Override
    public void onaPayByPaymentHistory(PaymentHistory onPaymentHistory) {
        paymentHistory = onPaymentHistory;
        checkPayOnline = true;
        setPaymentMethod.setText(String.format("%s : %s",onPaymentHistory.getPspName(),onPaymentHistory.getPaymentToken()));
    }

    @Override
    public void onOpenPaymentMethod() {
        PaymentHistoryAdapter.noteHistoryPostion = -1;
        Intent intent = new Intent(ChooseTransportActivity.this, PaymentMethodActivity.class);
        intent.putExtra("user",myUser);
        startActivityForResult(intent,REQUEST_PAYMENT_METHOD);
    }
}