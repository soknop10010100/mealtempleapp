package com.station29.mealtemple.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.Bank;
import com.station29.mealtemple.api.request.BankWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.profile.adapter.BankListAdapter;
import com.station29.mealtemple.util.Util;

import java.util.List;

public class BankListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_list);

        ((TextView) findViewById(R.id.title)).setText(R.string.bank_list);
        RecyclerView recyclerView = findViewById(R.id.bank_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        new BankWs().getListBank(BankListActivity.this,new BankWs.LoadListBank() {
            @Override
            public void onLoadSuccess(List<Bank> categoryList) {
                BankListAdapter adapter = new BankListAdapter(categoryList, bankListItemClicklistener);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                Util.popupMessage(null,errorMessage,BankListActivity.this);
            }
        });

    }
    private BankListAdapter.BankListItemClicklistener bankListItemClicklistener = new BankListAdapter.BankListItemClicklistener() {
        @Override
        public void onBanklistItemClick(View itemView, Bank bank) {
            Intent intent = new Intent(BankListActivity.this,AddBankCardActivity.class);
            intent.putExtra("bank",bank);
            startActivity(intent);
        }
    };
}
