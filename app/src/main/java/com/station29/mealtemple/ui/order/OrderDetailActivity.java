package com.station29.mealtemple.ui.order;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;
import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.Order;
import com.station29.mealtemple.api.model.OrderItem;
import com.station29.mealtemple.api.model.Services;
import com.station29.mealtemple.api.model.SurCharges;
import com.station29.mealtemple.api.model.Tax;
import com.station29.mealtemple.api.model.Waypoint;
import com.station29.mealtemple.api.model.response.ResponseOrderDetailTaxi;
import com.station29.mealtemple.api.model.response.OrderResponseDetail;
import com.station29.mealtemple.api.request.OrderWs;
import com.station29.mealtemple.api.request.ReviewWs;
import com.station29.mealtemple.api.request.TaxiWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.adpater.CartItemAdapter;
import com.station29.mealtemple.util.DateUtil;
import com.station29.mealtemple.util.Util;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class OrderDetailActivity extends BaseActivity {

    private Order orderDetail;
    private float couponDiscount;
    private Integer accountId=null,userId=null;
    private double lngClient;
    private double latClient;
    private String action =null,typeOrder=null,statusDelivery,orderDetailId ="-1",driverId;
    private HashMap<String, Object> reviewRatingData = new HashMap<>();
    private Integer cancelOrderId;
    private ResponseOrderDetailTaxi orderDetailTaxi;
    private OrderResponseDetail orderResponseDetail;
    private static final int REQUEST_TRACK=2012;
    private TextView cancelTv;
    private ProgressBar progressBarTaxi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null && getIntent().hasExtra("order")) {
            orderDetail = (Order) getIntent().getSerializableExtra("order");
            if (orderDetail == null) return;
//            typeOrder  ="service";
            typeOrder  =orderDetail.getType_order();
            orderDetailId = String.valueOf(orderDetail.getOrderId());
            statusDelivery = orderDetail.getStatus();
            //change layout for taxi or  shop order detail
        }else if(getIntent() != null && getIntent().hasExtra("orderId")){
            typeOrder  = (String) getIntent().getSerializableExtra("typeOrder");
            orderDetailId = getIntent().getStringExtra("orderId");
            action = (String) getIntent().getSerializableExtra("status");
            statusDelivery= action;
        }
        setContentView((typeOrder!=null&& typeOrder.equalsIgnoreCase("delivery"))?R.layout.activity_order_detail:R.layout.activity_driver_order_detail);

        TextView titleTv = findViewById(R.id.title);
        titleTv.setText(((typeOrder.equalsIgnoreCase("delivery"))?getResources().getString(R.string.order_detail):getResources().getString(R.string.trip_detail)));

        if (typeOrder.equalsIgnoreCase("delivery")){//food
            foodOrderService();
        } else {
            taxiOrderService();
        }

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!orderDetailId.equals("-1")){
                    Intent intent = new Intent(OrderDetailActivity.this, MainActivity.class);
                    intent.putExtra("tabId", R.id.navigation_dashboard);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    setResult(RESULT_OK);
                    startActivity(intent);
                }else{
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }
    //start service food
    private void foodOrderService(){
        final RecyclerView recyclerView = findViewById(R.id.list_items);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final TextView orderNumberTv,shopNameTv,deliveryFeeTv,totalPriceTv,discountTv,riderTipTv,orderTimeTv,taxTv,surchargeTv
                ,subTotalTv,deliveryTimeTv,qty,taxValuesTv,surchargeValuesTv,totalQtyTv,subtotalTv
            ,discountValuesTv,deliveyFeeTv,ridertipTv,totalPrice1,orderNumerTv,ordertimeTv,deliveryTime,storeContactTv,productDetail,paymentTypeTv,paymentText;

         orderNumberTv = findViewById(R.id.order_id);
         shopNameTv = findViewById(R.id.shopName);
         deliveryFeeTv = findViewById(R.id.deliveryFee);
         totalPriceTv = findViewById(R.id.total_price);
         discountTv = findViewById(R.id.discount);
         riderTipTv = findViewById(R.id.rider_tip);
         orderTimeTv = findViewById(R.id.orderTime);
        taxTv = findViewById(R.id.taxAfter);
        surchargeTv = findViewById ( R.id.surcharge );//
        subTotalTv = findViewById(R.id.subTotal);
        deliveryTimeTv = findViewById(R.id.deliveryTime);
        qty = findViewById(R.id.total_items);
        taxValuesTv = findViewById(R.id.txtAfter1);
        surchargeValuesTv = findViewById(R.id.surcharge1);
        totalQtyTv = findViewById(R.id.total_items1);
        subtotalTv= findViewById(R.id.subTotal1);
        discountValuesTv = findViewById(R.id.discount1);
        deliveyFeeTv = findViewById(R.id.deliveryfee1);
        ridertipTv = findViewById(R.id.rider_tip1);
        totalPrice1 = findViewById(R.id.total_price1);
        orderNumerTv = findViewById(R.id.order_idq);
        ordertimeTv = findViewById(R.id.order_time1);
        deliveryTime = findViewById(R.id.deliveryTime1);
        storeContactTv = findViewById(R.id.storeContact);
        productDetail = findViewById(R.id.product_detail_tv);
        paymentTypeTv = findViewById(R.id.payment_type);
        paymentText = findViewById(R.id.payment_ty);
        final NestedScrollView scrollView = findViewById(R.id.view_order_detail);
        progressBarTaxi = findViewById(R.id.progressBar);
        progressBarTaxi.setVisibility(View.VISIBLE);

        final MaterialButton trackRider = findViewById(R.id.btnReview);

        if (getIntent().hasExtra("status") && getIntent().getStringExtra("status").equalsIgnoreCase("NEW") || getIntent().getStringExtra("status").equalsIgnoreCase("PENDING") || getIntent().getStringExtra("status").equalsIgnoreCase("PREPARING") || getIntent().getStringExtra("status").equalsIgnoreCase("DELIVERING") || getIntent().hasExtra("status")|| getIntent().getStringExtra("status").equalsIgnoreCase("ACCEPTED")){
            trackRider.setText(((driverId==null)?getString(R.string.waiting_rider):getString(R.string.track_rider)));
        }
        if (getIntent().hasExtra("status")&& getIntent().getStringExtra("status").equalsIgnoreCase("COMPLETE")){
            action ="COMPLETE";
        }
        if (getIntent().hasExtra("status") && getIntent().getStringExtra("status").equalsIgnoreCase("REJECT") || getIntent().getStringExtra("status").equalsIgnoreCase("CANCELLED") || getIntent().getStringExtra("status").equalsIgnoreCase("FAILED")){
            trackRider.setVisibility(View.GONE);
        }

        findViewById(R.id.btnReview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().hasExtra("status") && getIntent().getStringExtra("status").equalsIgnoreCase("NEW") || getIntent().getStringExtra("status").equalsIgnoreCase("PENDING") || getIntent().getStringExtra("status").equalsIgnoreCase("PREPARING") || getIntent().getStringExtra("status").equalsIgnoreCase("DELIVERING")|| getIntent().getStringExtra("status").equalsIgnoreCase("ACCEPTED")){
                    if (driverId!=null){
                        Intent intent = new Intent(OrderDetailActivity.this, TrackRiderActivity.class);
                        intent.putExtra("driverId",orderResponseDetail.getDriverGlobalId());
                        intent.putExtra("typeOrder",typeOrder);
                        intent.putExtra("lngClient",lngClient);
                        intent.putExtra("latClient",latClient);
                        intent.putExtra("waypointCustomer",orderResponseDetail.getWaypoints());
                        intent.putExtra("waypointDriver",orderResponseDetail.getPickUpWaypoint());
                        intent.putExtra("status",orderResponseDetail.getOrderStatus());
                        setResult(RESULT_OK);
                        startActivityForResult(intent,REQUEST_TRACK);
                    }else Util.popupMessage(null,getString(R.string.your_order_no_rider_yet),OrderDetailActivity.this);

                }else popupInform("Store");

            }
        });

        if (Constant.getBaseUrl()!=null&& orderDetail.getOrderId()!=null){
            new OrderWs().readOrderDetail(orderDetail.getOrderId(), this,typeOrder,  new OrderWs.GetOrderListener() {
                @Override
                public void onLoadOrderSuccess(List<Order> orderList) {

                }

                @Override
                public void onError(String errorMessage) {

                }

                @SuppressLint("SetTextI18n")
                @Override
                public void onLoadDetailSuccess(OrderResponseDetail orderDetail) {
                    orderResponseDetail = orderDetail;
                    progressBarTaxi.setVisibility(View.GONE);
                    scrollView.setVisibility(View.VISIBLE);
                    String currencySign ="";
                    currencySign =((Constant.getConfig ()!=null)? Constant.getConfig ().getCurrencySign (): "") ;
                    if (orderDetail.getItems().size() == 0) return;
                    driverId =  (orderDetail.getDriverId());
                    latClient = orderDetail.getClientLat();
                    lngClient= orderDetail.getClientLng();
                    accountId = orderDetail.getAccountId();
                    userId = orderDetail.getUserId();
                    orderNumberTv.setText(String.format("%s :", getString(R.string.order_number)));
                    orderNumerTv.setText(orderDetail.getOrderNumber());
                    shopNameTv.setText(orderDetail.getAccountName());
                    productDetail.setText(String.format(Locale.US,"%s",getString(R.string.product_detail)));
                    subTotalTv.setText(String.format("%s :", getString(R.string.sub_total)));
                    subtotalTv.setText(String.format("%s %s",currencySign,Util.formatPrice(orderDetail.getSubTotal())));
                    totalPriceTv.setText(String.format("%s :", getString(R.string.total_price)));
                    totalPrice1.setText(String.format("%s %s",currencySign,Util.formatPrice(orderDetail.getTotalPrice())));
                    riderTipTv.setText(String.format("%s :", getString(R.string.rider_tip)));
                    ridertipTv.setText(String.format("%s %s",currencySign,Util.formatPrice(orderDetail.getRiderTip())));
                    orderTimeTv.setText(String.format("%s :", getString(R.string.order_time)));
                    ordertimeTv.setText(String.format("%s", DateUtil.formatDateUptoCurrentRegion(orderDetail.getOrderTime())));
                    deliveryTimeTv.setText(String.format("%s ", getString(R.string.delivery_time)));
//                  deliveryTime.setText(String.format("%s",DateUtil.formatTimeZoneLocal (orderDetail.getDeliveryTime ())));
                    deliveryTime.setText(String.format("%s",getString(R.string.delivery_asap)));
                    deliveryFeeTv.setText(String.format("%s :", getString(R.string.deliveryFee)));
                    deliveyFeeTv.setText(String.format("%s %s",currencySign,Util.formatPrice(orderDetail.getDeliveryFee())));
                    paymentText.setText(getString(R.string.payment_method) + " :");
                    paymentTypeTv.setText(String.format(Locale.US,"%s",orderDetail.getPaymentType()));

                    if (orderDetail.getTotalDiscount ()!=0){
                        discountTv.setText ( String.format("%s :", getString(R.string.discount)));
                        discountValuesTv.setText(String.format("%s %s",currencySign,Util.formatPrice(orderDetail.getTotalDiscount ())));
                    }else {
                        discountValuesTv.setVisibility(View.GONE);
                        discountTv.setVisibility ( View.GONE );
                    }
                    if (orderDetail.getTaxList ().size()==0){
                        taxTv.setVisibility ( View.GONE );
                        taxValuesTv.setVisibility(View.GONE);
                    }else {
                        String tax="";
                        String taxtotal = "";
                        Tax taxs = null;
                        for (int i = 0; i < orderDetail.getTaxList ().size (); i++) {
                            taxs = orderDetail.getTaxList ().get ( i );
                            tax= tax + String.format("%s( %s %s%s ) :%s", getString(R.string.tax) ,taxs.getNameTax(),taxs.getAmount(),"%","\n") ;
                            taxtotal = taxtotal + String.format("%s %s%s",currencySign,Util.formatPrice ( taxs.getTaxAmount()),"\n");
                        }
                        taxTv.setText (String.format ( Locale.US,"%s",tax.trim () )  );
                        taxValuesTv.setText(String.format(Locale.US,"%s",taxtotal.trim()));
                    }
                    // if (surchargeTv)
                    //no surcharge here
                    if (orderDetail.getSurChargesList().size ()==0){
                        surchargeTv.setVisibility ( View.GONE );
                        surchargeValuesTv.setVisibility(View.GONE);
                    }else {
                        String surCh ="";
                        String surChTotal="";
                        for (int i = 0; i < orderDetail.getSurChargesList ().size (); i++) {
                            SurCharges surCharges = orderDetail.getSurChargesList ().get ( i );
                            surCh= surCh + String.format(getString(R.string.surcharges) +"( %s %s%s ) :%s",surCharges.getNameSurCharges(),surCharges.getAmount(),"%","\n") ;
                            surChTotal = surChTotal + String.format("%s %s%s",currencySign,Util.formatPrice ( surCharges.getSurChargeAmount()),"\n");
                        }
                        surchargeTv.setText (String.format ( Locale.US,"%s",surCh.trim () )  );
                        surchargeValuesTv.setText(String.format(Locale.US,"%s",surChTotal.trim()));
                    }

                    storeContactTv.setText(String.format("%s ", orderDetail.getTel()));
                    int totalQty = 0;
                    for (OrderItem item : orderDetail.getItems()) {
                        totalQty += item.getQty();
                    }
                    qty.setText(String.format(Locale.US,"%s :", getString(R.string.qty)));
                    totalQtyTv.setText(String.format(Locale.US ,"%d",totalQty));
                    trackRider.setText(((driverId!=null)?getString(R.string.track_rider):getString(R.string.waiting_rider)));//request status
                    if (action!=null&&action.equalsIgnoreCase("COMPLETE")){
                        trackRider.setText(getResources().getString(R.string.write_a_review));
                    }
                    recyclerView.setAdapter(new CartItemAdapter(orderDetail.getItems(), onItemEditedListener, false,1));

                }

                @Override
                public void onLoadDetailTaxiSuccess(ResponseOrderDetailTaxi detailTaxi) {

                }
            });
        }

    }

    private CartItemAdapter.onItemEditedListener onItemEditedListener = new CartItemAdapter.onItemEditedListener() {
        @Override
        public void onOrderItemEditQty(OrderItem orderItem ,int count,int position,boolean isEditQty) {}

        @Override
        public void onLastItemRemove(boolean lastItemRemove) {

        }
    };

    private void popupInform(final String name) {

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(OrderDetailActivity.this,R.style.DialogTheme);
        builder.setCancelable(false);
        View view = LayoutInflater.from(this).inflate(R.layout.layout_dialog_review, null);
        builder.setView(view);
        final TextView rateForTv = view.findViewById(R.id.rateForTv);
        final EditText editText = view.findViewById(R.id.comment);
        final RatingBar ratingBar =view.findViewById(R.id.shop_detail_rated);
        final MaterialButton submitBtn,notNowBtn;
        submitBtn= view.findViewById(R.id.submitNowBtn);
        notNowBtn=view.findViewById(R.id.notNowBtn);
        if (editText != null)  editText.setOnFocusChangeListener(onFocusChangeListener);
        rateForTv.setText(((name.equalsIgnoreCase("Store"))?getString(R.string.review_store):getString(R.string.review_rider)));
        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (ratingBar.getRating()>0){
                    submitBtn.setEnabled(true);
                    reviewRatingData.put("rate",ratingBar.getRating());
                    reviewRatingData.put("review",editText.getText().toString());
                    reviewRatingData.put("customer_id",userId);
                    if (name.equals("Store")) {
                        reviewRatingData.put("account_id",accountId);
                    }else {
                        reviewRatingData.put("driver_id",driverId);
                    }
                }else {
                    submitBtn.setEnabled(false);
                }
            }
        });
        final Dialog dialog = builder.create();
        notNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRatingData= new HashMap<>();
                dialog.dismiss();//close
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reviewRating(reviewRatingData);
                dialog.dismiss();//close it before send

            }
        });

        if (dialog.getWindow() == null) return;
        dialog.show();
    }

    private View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean hasFocus) {
            if (!hasFocus) {
                Util.hideSoftKeyboard(view);
            }
        }
    };
    private void reviewRating(final HashMap<String, Object> reviewData){
        new ReviewWs().writeReview(reviewData,OrderDetailActivity.this,new ReviewWs.ReviewSuccess() {
            @Override
            public void onReviewSuccess(String message,boolean newData) {
                if (newData) reviewRatingData = new HashMap<>();
                if (!reviewData.containsKey("driver_id")) popupInform("Rider" );
                if (reviewData.containsKey("driver_id")&& newData){// finish review
                    finish();
                }
                Toast.makeText(OrderDetailActivity.this,message,Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onReviewFailed(String errorMessage) {
                Toast toast=  Toast.makeText(OrderDetailActivity.this,errorMessage,Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                toast.show();

            }
        });
    }
    //end service food here

    //start service taxi
    private void taxiOrderService(){
        final NestedScrollView view = findViewById(R.id.view_order_detail);
        final TextView driverNameTv,driverContactTv,orderIdTv,orderIdValueTv,pickUpTimeTv,pickUpTimeValueTv
                ,arrivedTimeTv,arrivedTimeValueTv,vehicleTypeTv,pickupAddressTv,dropOffAddressTV
                ,totalTv,totalValueTv,paymentTypesTv,paymentTypesValuesTv,couponCodeTv,couponCodeValueTv,viewTypeTv
                ,discountTv,discountValueTv,iconDropOff,iconPickUp,distanceTimeTv,driverInforTv,plateNumberTv,vehicleColorsTv,vehicleProvideIdTv;
        RelativeLayout relateItemWeight,relateItemCate;
        LinearLayout relateDes;
        TextView weightTv,cateTv,descriptionTv;
        relateItemCate = findViewById(R.id.cate_txt);
        relateItemWeight = findViewById(R.id.weight_txt);
        relateDes = findViewById(R.id.description_txt);
        weightTv = findViewById(R.id.weight_Va);
        cateTv = findViewById(R.id.cate_Va);

        plateNumberTv =findViewById(R.id.plateNumberTv);
        viewTypeTv= findViewById(R.id.view_vehicle_TypeTv);
        vehicleColorsTv = findViewById(R.id.vehicleColorsTv);
        vehicleProvideIdTv = findViewById(R.id.vehicleProvideIdTv);
        driverInforTv= findViewById(R.id.driver_inforTv);
        cancelTv =findViewById(R.id.cancelBtn);
        driverNameTv=findViewById(R.id.driverName);
        driverContactTv = findViewById(R.id.driverContact);
        orderIdTv =findViewById(R.id.order_idTv);
        orderIdValueTv =findViewById(R.id.orderId_ValueTv);
        pickUpTimeTv=findViewById(R.id.pickUpTimeTv);
        pickUpTimeValueTv=findViewById(R.id.pickUpTime_ValueTv);
        arrivedTimeTv= findViewById(R.id.arrived_TimeTv);
        arrivedTimeValueTv= findViewById(R.id.arrived_Time_ValueTv);
        vehicleTypeTv=  findViewById(R.id.vehicle_TypeTv);
        pickupAddressTv = findViewById(R.id.pickup_address);
        dropOffAddressTV= findViewById(R.id.drop_off_address);
        totalTv=findViewById(R.id.total_Tv);
        totalValueTv=findViewById(R.id.total_ValueTv);
        paymentTypesTv=findViewById(R.id.payment_TypesTv);
        paymentTypesValuesTv=findViewById(R.id.payment_Types_ValuesTv);
        couponCodeTv = findViewById(R.id.coupon_codeTv);
        couponCodeValueTv= findViewById(R.id.coupon_code_ValueTv);
        discountTv = findViewById(R.id.discountTv);
        discountValueTv =findViewById(R.id.discount_valueTv);
        discountValueTv.setVisibility(View.GONE);
        iconDropOff=findViewById(R.id.ic_drop_off);
        iconPickUp=findViewById(R.id.ic_pickup);
        distanceTimeTv = findViewById(R.id.duration_timeTv);
        TextView couponTv = findViewById(R.id.coupon_type);
        RelativeLayout relateCoupon = findViewById(R.id.relateC);
        final CircleImageView driverImage = findViewById(R.id.user_profile_image);
        final MaterialButton btnReview = findViewById(R.id.btnReview);
        final ProgressBar loadBar = findViewById(R.id.progressBar);
        final TextView commentTv = findViewById(R.id.comment_Va);
        final RelativeLayout commentRelate = findViewById(R.id.com_txt);

        if (getIntent().hasExtra("status") && getIntent().getStringExtra("status").equalsIgnoreCase("COMPLETE") || getIntent().getStringExtra("status").equalsIgnoreCase("REJECT") || getIntent().getStringExtra("status").equalsIgnoreCase("CANCELLED") || getIntent().getStringExtra("status").equalsIgnoreCase("FAILED")){
            btnReview.setVisibility(View.GONE);
        }

        btnReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driverId!=null){
                    if (getIntent().hasExtra("status") && getIntent().getStringExtra("status").equalsIgnoreCase("NEW") || getIntent().getStringExtra("status").equalsIgnoreCase("PENDING") || getIntent().getStringExtra("status").equalsIgnoreCase("PREPARING") || getIntent().getStringExtra("status").equalsIgnoreCase("DELIVERING") || getIntent().getStringExtra("status").equalsIgnoreCase("ACCEPTED")){
                        Intent intent = new Intent(OrderDetailActivity.this, TrackRiderActivity.class);
                        if(orderDetailTaxi.getWaypoints() != null && orderDetailTaxi.getPickUpWaypoint() != null) {
                            intent.putExtra("waypointCustomer", orderDetailTaxi.getWaypoints());
                            intent.putExtra("waypointDriver", orderDetailTaxi.getPickUpWaypoint());
                        }
                        intent.putExtra("typeOrder", typeOrder);
                        intent.putExtra("driverId", orderDetailTaxi.getDriverGlobalId());
                        intent.putExtra("lngClient",lngClient);
                        intent.putExtra("latClient",latClient);
                        intent.putExtra("status",orderDetailTaxi.getDeliveryStatus());
                        setResult(RESULT_OK);
                        startActivityForResult(intent,REQUEST_TRACK);
                    }
                }else Util.popupMessage(null,getString(R.string.your_order_no_rider_yet),OrderDetailActivity.this);
            }
        });

        new OrderWs().readOrderDetail((Integer.parseInt(orderDetailId) == -1 ? orderDetail.getOrderId() : Integer.parseInt(orderDetailId)),OrderDetailActivity.this,typeOrder, new OrderWs.GetOrderListener() {
            @Override
            public void onLoadOrderSuccess(List<Order> orderList) {

            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(OrderDetailActivity.this,errorMessage,Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onLoadDetailSuccess(OrderResponseDetail detail) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onLoadDetailTaxiSuccess(final ResponseOrderDetailTaxi detailTaxi) {
                // do everything here
                //check action here
                orderDetailTaxi = detailTaxi;
                loadBar.setVisibility(View.GONE);
                if (detailTaxi.getDropoffLat() != null){
                    latClient = Double.parseDouble(detailTaxi.getDropoffLat());
                }
                if (detailTaxi.getDropoffLong()!= null){
                    lngClient=  Double.parseDouble(detailTaxi.getDropoffLong());
                }
                view.setVisibility(View.VISIBLE);
                cancelTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        v.setEnabled(false);
                        v.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                v.setEnabled(true);
                            }
                        },100);
                       cancelOrderId = orderDetailTaxi.getOrderId();
                       popupMessageCancle("Order",OrderDetailActivity.this);
                    }
                });
                if (getIntent().hasExtra("status") && getIntent().getStringExtra("status").equalsIgnoreCase("NEW") || getIntent().getStringExtra("status").equalsIgnoreCase("PENDING") || getIntent().getStringExtra("status").equalsIgnoreCase("PREPARING") || getIntent().getStringExtra("status").equalsIgnoreCase("DELIVERING")||getIntent().getStringExtra("status").equalsIgnoreCase("ACCEPTED")) {
                    btnReview.setText(((detailTaxi.getDriverId()==null)?getResources().getString(R.string.wait_driver):getResources().getString(R.string.track_driver)));
                    if (statusDelivery.equalsIgnoreCase("NEW" )|| statusDelivery.equalsIgnoreCase("PREPARING") || statusDelivery.equalsIgnoreCase("ACCEPTED")){
                        cancelTv.setVisibility(View.VISIBLE); // cancel btn show only before driver slide pickup
                    }
                }
                if (getIntent().hasExtra("status")&& getIntent().getStringExtra("status").equalsIgnoreCase("COMPLETE")){
                    action ="COMPLETE";
                    cancelTv.setVisibility(View.GONE);
                    btnReview.setText(getResources().getString(R.string.write_a_review));
                }
                if (detailTaxi.getDriverId()!=null){
                    //have driver
                    findViewById(R.id.driver_infor_view).setVisibility(View.VISIBLE);
                    driverId =detailTaxi.getDriverId();
                    driverInforTv.setText(String.format(Locale.US,"%s",getResources().getString(R.string.driver_infor)));
                    viewTypeTv.setText(String.format(Locale.US,"%s :",getResources().getString(R.string.vehicle_type)));
                    vehicleTypeTv.setText(String.format(Locale.US,"%s", detailTaxi.getServiceDriver() ));

                    driverNameTv.setText(String.format(Locale.US,"%s : %s", getString(R.string.driver_name) ,  detailTaxi.getDriverName()));
                    driverContactTv.setText(String.format(Locale.US,"%s", detailTaxi.getDriverPhone()));
                    driverContactTv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                                callIntent.setData ( Uri.parse ("tel:" +detailTaxi.getDriverPhone()) );
                                startActivity ( callIntent );//user can call
                            }
                        });
                    Glide.with(getApplicationContext()).load(detailTaxi.getDriverImg()).into(driverImage);
                    plateNumberTv.setText(String.format(Locale.US,"%s : %s",getResources().getString(R.string.plate_number),detailTaxi.getPlateNumber()));
                    vehicleColorsTv.setText(String.format(Locale.US,"%s %s : %s", getString(R.string.color) ,detailTaxi.getServiceDriver().toUpperCase(), detailTaxi.getVehicleColor() ));
                    vehicleProvideIdTv.setText(String.format(Locale.US,"%s %s : %s", getString(R.string.ID) , detailTaxi.getServiceDriver().toUpperCase()  , detailTaxi.getVehicleId() ));
                   if(detailTaxi.getPriceAfterCouponDiscount()!=null){
                        couponCodeTv.setVisibility(View.VISIBLE);
                        couponCodeTv.setText(R.string.total_price_after_coupon);
                        couponCodeValueTv.setVisibility(View.VISIBLE);
                        relateCoupon.setVisibility(View.VISIBLE);
                        couponTv.setText(String.format(Locale.US,"%s",detailTaxi.getCoupon_discount_formatted()));
                        couponCodeValueTv.setText(String.format(Locale.US,"%s %s",detailTaxi.getCurrency(),detailTaxi.getPriceAfterCouponDiscount()));
                    }


                }else {
                    //doesn't have driver
                    findViewById(R.id.driver_infor_view).setVisibility(View.GONE);
                    viewTypeTv.setText(String.format(Locale.US,"%s",getResources().getString(R.string.wait_driver)));
                    btnReview.setText(String.format(Locale.US,"%s",getResources().getString(R.string.wait_driver)));
                }
                if(detailTaxi.getTypeOrder().equalsIgnoreCase("send")){
                    if(!detailTaxi.getSendPackage().getWeight().isEmpty()){
                        relateItemWeight.setVisibility(View.VISIBLE);
                        weightTv.setText(String.format(Locale.US,"%s",detailTaxi.getSendPackage().getWeight() + " Kg"));
                    }else {
                        relateItemWeight.setVisibility(View.GONE);
                    }
                    relateDes.setVisibility(View.VISIBLE);
                    if(detailTaxi.getSendPackage().getPackageDetail()!=null){
                        commentRelate.setVisibility(View.VISIBLE);
                        commentTv.setText(String.format(Locale.US,"%s",detailTaxi.getSendPackage().getPackageDetail()));
                       //
                       // descriptionTv.setText(String.format(Locale.US,"%s",detailTaxi.getSendPackage().getPackageDetail()));
                    }

                    if(!detailTaxi.getSendPackage().getTypePackage().isEmpty()){
                        relateItemCate.setVisibility(View.VISIBLE);
                        cateTv.setText(String.format(Locale.US,"%s",detailTaxi.getSendPackage().getTypePackage()));
                    }else {
                        relateItemCate.setVisibility(View.GONE);
                    }

                    if(detailTaxi.getSendPackage().getAddressDetailSender()!=null){
                        if(!detailTaxi.getStartAddress().equals(""))
                            pickupAddressTv.setText(detailTaxi.getSendPackage().getAddressDetailSender()+","+String.format(Locale.US,"%s", detailTaxi.getStartAddress()));
                        else if (detailTaxi.getPickUpLat() != null && detailTaxi.getPickUpLong() != null){
                            Geocoder geocoder = new Geocoder(OrderDetailActivity.this, Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(Double.parseDouble(detailTaxi.getPickUpLat()),Double.parseDouble(detailTaxi.getPickUpLong()), 1);
                                String fullAdd = addresses.get(0).getAddressLine(0);
                                pickupAddressTv.setText(detailTaxi.getSendPackage().getAddressDetailSender()+","+fullAdd);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else
                            pickupAddressTv.setText(R.string.no_destination_seleted);
                    } else{
                        if(!detailTaxi.getStartAddress().equals(""))
                            pickupAddressTv.setText(String.format(Locale.US,"%s", detailTaxi.getStartAddress()));
                        else if (detailTaxi.getPickUpLat() != null && detailTaxi.getPickUpLong() != null){
                            Geocoder geocoder = new Geocoder(OrderDetailActivity.this, Locale.getDefault());
                            List<Address> addresses = null;
                            try {
                                addresses = geocoder.getFromLocation(Double.parseDouble(detailTaxi.getPickUpLat()),Double.parseDouble(detailTaxi.getPickUpLong()), 1);
                                String fullAdd = addresses.get(0).getAddressLine(0);
                                pickupAddressTv.setText(fullAdd);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else
                            pickupAddressTv.setText(R.string.no_destination_seleted);
                    }
                    if (detailTaxi.getSendPackage().getAddressDetailReceiver()!=null) {
                        dropOffAddressTV.setText(detailTaxi.getSendPackage().getAddressDetailReceiver() + "," + String.format(Locale.US, "%s", detailTaxi.getStartAddress()));
                    }
                    else    dropOffAddressTV.setText(String.format(Locale.US,"%s", detailTaxi.getStartAddress()));
                } else {
                    if(!detailTaxi.getStartAddress().equals(""))
                        pickupAddressTv.setText(String.format(Locale.US,"%s", detailTaxi.getStartAddress()));
                    else if (detailTaxi.getPickUpLat() != null && detailTaxi.getPickUpLong() != null){
                        Geocoder geocoder = new Geocoder(OrderDetailActivity.this, Locale.getDefault());
                        List<Address> addresses = null;
                        try {
                            addresses = geocoder.getFromLocation(Double.parseDouble(detailTaxi.getPickUpLat()),Double.parseDouble(detailTaxi.getPickUpLong()), 1);
                            String fullAdd = addresses.get(0).getAddressLine(0);
                            pickupAddressTv.setText(fullAdd);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else
                        pickupAddressTv.setText(R.string.no_destination_seleted);


                    if(!detailTaxi.getEndAddress().equals(""))
                        dropOffAddressTV.setText(String.format(Locale.US,"%s",detailTaxi.getEndAddress()));
                    else
                        dropOffAddressTV.setText(R.string.no_destination_seleted);

                }
                iconPickUp.setVisibility(View.VISIBLE);
                iconDropOff.setVisibility(View.VISIBLE);

                if (detailTaxi.getDatePickup()!=null){
                    findViewById(R.id.rl_pick).setVisibility(View.VISIBLE);
                    pickUpTimeTv.setText(String.format(Locale.US,"%s",getResources().getString(R.string.pick_up_time)));
                    pickUpTimeValueTv.setText(String.format(Locale.US,"%s",DateUtil.formatTimeZoneSlash(detailTaxi.getDatePickup())));

                }else {
                    findViewById(R.id.rl_pick).setVisibility(View.GONE);
                }
                if (detailTaxi.getDateDropoff()!=null){
                    findViewById(R.id.rl_drop).setVisibility(View.VISIBLE);
                    arrivedTimeTv.setText(String.format(Locale.US,"%s",getResources().getString(R.string.drop_time)));
                    arrivedTimeValueTv.setText(String.format(Locale.US,"%s",DateUtil.formatTimeZoneSlash(detailTaxi.getDateDropoff())));
                }else {
                    findViewById(R.id.rl_drop).setVisibility(View.GONE);
                }
                paymentTypesTv.setText(String.format(Locale.US,"%s",getResources().getString(R.string.payment_method)));
                if(detailTaxi.getPaymentType()!=null){
                    paymentTypesValuesTv.setText(String.format(Locale.US,"%s",detailTaxi.getPaymentType()));
                }else {
                    paymentTypesValuesTv.setText(String.format(Locale.US,"%s",getResources().getString(R.string.pay_by_cash)));
                }

                orderIdTv.setText(String.format(Locale.US,"%s",getResources().getString(R.string.booking_id)));
                orderIdValueTv.setText(String.format(Locale.US,"%s", detailTaxi.getOrderNumber()));

                if(detailTaxi.getWaypoints() != null &&  String.valueOf(detailTaxi.getWaypoints().getDistance()).length()>3) {
                    int caWayPoint = (int) Math.round(detailTaxi.getWaypoints().getDuration()/ 60);
                    if(caWayPoint>=60){
                        String duration = "" + ((caWayPoint >= 60) ? caWayPoint / 60 + " h : " + caWayPoint % 60 + " mins" : caWayPoint + " mins");
                        distanceTimeTv.setText(String.format("%s %s", detailTaxi.getTotalDistance()+" km", duration));

                    }else {
                        double durations = Math.ceil(caWayPoint);
                        distanceTimeTv.setText(String.format("%s %s", detailTaxi.getTotalDistance()+" km",Util.formatTime((int) durations) ));
                    }


                } else {
                    distanceTimeTv.setText(String.format("%s %s","--- km" ,"--- mins"));
                }

                totalTv.setText(String.format(Locale.US,"%s ",getResources().getString(R.string.total_price)));
                if(!detailTaxi.getEndAddress().equals(""))
                    totalValueTv.setText(String.format(Locale.US,"%s %s",detailTaxi.getCurrency(),detailTaxi.getTotalPrice() ));
                else
                    totalValueTv.setText("---");
            }
        });

    }

    private void cancelDriver(int orderId){
        new TaxiWs().cancelTaxi(OrderDetailActivity.this, orderId, new TaxiWs.getDriverCallback() {
            @Override
            public void onLoadSuccess(List<Services> servicesList, Waypoint waypoint) {

            }

            @Override
            public void onLoadSuccessWithoutDrop(List<Services> servicesList) {

            }

            @Override
            public void onLoadFailed(String errorMessage,int code) {
//                progressBarTaxi.setVisibility(View.GONE);
                Util.popupMessage(getString(R.string.error
                ),errorMessage,OrderDetailActivity.this);
            }

            @Override
            public void onOrder(String orderId) {
//                progressBarTaxi.setVisibility(View.GONE);
                Toast.makeText(getBaseContext(),orderId,Toast.LENGTH_LONG).show();
                Intent intent = new Intent(OrderDetailActivity.this, MainActivity.class);
                intent.putExtra("tabId", R.id.navigation_dashboard);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });
    }

    private  void popupMessageCancle(String title, Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(OrderDetailActivity.this,R.style.DialogTheme);
        if (title != null) builder.setTitle(title);
        builder.setMessage(R.string.you_sure_tocancel);
        builder.setNegativeButton ( getResources ().getString ( R.string.cancel ) ,null);
        builder.setPositiveButton(getResources ().getString(R.string.ok ), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              //  progressBarTaxi.setVisibility(View.VISIBLE);
                if(Constant.getBaseUrl()!=null)
                cancelDriver(cancelOrderId);
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
        if(!orderDetailId.equals("-1")){
            Intent intent = new Intent(OrderDetailActivity.this, MainActivity.class);
            intent.putExtra("tabId", R.id.navigation_dashboard);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            setResult(RESULT_OK);
            startActivity(intent);
        }else{
            setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TRACK && resultCode == RESULT_OK) {
            if(MockupData.checkpickup && !typeOrder.equalsIgnoreCase("delivery"))
                cancelTv.setVisibility(View.GONE);
        }
    }
}