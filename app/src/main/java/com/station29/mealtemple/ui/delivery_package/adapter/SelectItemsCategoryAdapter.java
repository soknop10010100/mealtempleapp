package com.station29.mealtemple.ui.delivery_package.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;

import java.util.ArrayList;
import java.util.List;

public class SelectItemsCategoryAdapter extends RecyclerView.Adapter<SelectItemsCategoryAdapter.CatViewHolder> {
    private List<String> cate = new ArrayList<>();
    private CategoryItemClick itemClick ;
    private int mSelectedPos = -1;
    private int stat = -1;

    public SelectItemsCategoryAdapter(List<String> cate,CategoryItemClick itemClick,int stat) {
        this.cate = cate;
        this.itemClick = itemClick;
        this.stat = stat;
    }

    @NonNull
    @Override
    public CatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cateogry, parent, false);
        return  new CatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CatViewHolder holder, int position) {

        String item = cate.get(position);
        holder.textView.setText(item);
        if (position == mSelectedPos||position==stat){
            holder.linearLayout.setBackgroundResource(R.drawable.card_view_boder);
        } else {
            holder.linearLayout.setBackground(null);
        }

        holder.itemView.setOnClickListener(v -> {
            mSelectedPos = position;
            stat = mSelectedPos;
            itemClick.onCategoryClick(item,position,stat);
            notifyDataSetChanged();
        });

    }

    @Override
    public int getItemCount() {
        return cate.size();
    }
    public interface CategoryItemClick {
        void onCategoryClick(String category,int position,int stat);
    }
    class CatViewHolder extends RecyclerView.ViewHolder {

        TextView textView;
        LinearLayout linearLayout;

        CatViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.tex);
            linearLayout = itemView.findViewById(R.id.lin);
        }
    }
    public void clear() {
        int size = cate.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                cate.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }
}
