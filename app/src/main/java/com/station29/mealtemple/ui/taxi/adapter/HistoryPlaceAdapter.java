package com.station29.mealtemple.ui.taxi.adapter;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.WayPointLatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class HistoryPlaceAdapter extends RecyclerView.Adapter<HistoryPlaceAdapter.HistoryPlaceViewHolder> {

    private List<WayPointLatLng> wayPointLatLngs;
    private OnPlaceClickListener onPlaceClickListener;
    private Context context;

    public HistoryPlaceAdapter(List<WayPointLatLng> wayPointLatLngs, OnPlaceClickListener onPlaceClickListener) {
        this.wayPointLatLngs = wayPointLatLngs;
        this.onPlaceClickListener = onPlaceClickListener;
    }

    @NonNull
    @Override
    public HistoryPlaceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_place_history, parent, false);
        context = parent.getContext();
        return new HistoryPlaceAdapter.HistoryPlaceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HistoryPlaceViewHolder holder, int position) {
        final WayPointLatLng wayPointLatLng = wayPointLatLngs.get(position);
        if(wayPointLatLng!=null){
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(wayPointLatLng.getLatitute(),wayPointLatLng.getLongitute(), 1);
                String fullAdd = null;
                try {
                    if( addresses.get(0).getAddressLine(0) != null) {
                        fullAdd = addresses.get(0).getAddressLine(0);
                        holder.placeText.setText(fullAdd);
                        holder.view.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                try {
                                    onPlaceClickListener.onPlaceClick(holder.view, wayPointLatLng);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }catch (Exception e){
                    holder.placeText.setText(R.string.ocean);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public int getItemCount() {
        return wayPointLatLngs.size();
    }

    class HistoryPlaceViewHolder extends RecyclerView.ViewHolder {

        TextView placeText;
        View view;

        HistoryPlaceViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            placeText = itemView.findViewById(R.id.placeText);
        }
    }

    public interface OnPlaceClickListener {
        void onPlaceClick(View itemView, WayPointLatLng wayPointLatLng) throws IOException;
    }
}
