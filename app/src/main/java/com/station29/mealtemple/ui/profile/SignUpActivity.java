package com.station29.mealtemple.ui.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.ServerUrlWs;
import com.station29.mealtemple.api.request.SignUpWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import java.util.HashMap;

public class SignUpActivity extends BaseActivity  {

    public static boolean isDidLogin = false;
    private EditText phoneNumberEt, passwordEt, emailEt ;
    private final static int CONFIRM_SMS = 456;
    private String action = null;
    private TextView swTextTitle;
    private CountryCodePicker countryCodePicker;
    private final String EMAIL = "Use your Email Address", PHONE =  "Use your Phone Number";
    private String inputType = EMAIL, userType = "USER_ANDROID", countryCode = BaseActivity.countryCode;
    private HashMap<String, Object> userAccount = new HashMap<> (  ) ;
    private View progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_sign_up );

        ImageButton backbtn = findViewById ( R.id.btnSignupback );
        backbtn.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                onBackPressed ();
            }
        } );

        phoneNumberEt =findViewById ( R.id.txtPhoneNumber );
        emailEt = findViewById (R.id.txtEmail);
        emailEt.setVisibility ( View.GONE );
        passwordEt = findViewById ( R.id.txtpassword );
        passwordEt.getText ().toString ();
        progressBar = findViewById(R.id.progressBar);

        countryCodePicker = findViewById ( R.id.ccp );
        countryCodePicker.setDefaultCountryUsingNameCodeAndApply(BaseActivity.countryCode);
        swTextTitle =findViewById ( R.id.text3 );

        if (getIntent() != null && getIntent().hasExtra("action")) {
            action = getIntent().getStringExtra("action");
        }
        
        final EditText passwordEt = findViewById(R.id.txtpassword);

        findViewById(R.id.btnSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                progressBar.setVisibility(View.VISIBLE);
                view.setEnabled(false);
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        view.setEnabled(true);
                    }
                },500);
                if (Constant.getBaseUrl() != null) {
                    accountUser ();
                } else {
                    if (BaseActivity.countryCode == null){
                        Toast.makeText(SignUpActivity.this, R.string.unable_to_find_location, Toast.LENGTH_LONG).show();
                        return;
                    }
                    new ServerUrlWs().readServerUrl(BaseActivity.countryCode, new ServerUrlWs.loadServerUrl() {
                        @Override
                        public void onLoadSuccess(boolean success) {}

                        @Override
                        public void onLoadFailed(String errorMessage) {}
                    });
                }
            }
        });

        final TextView switchInputType = findViewById(R.id.btnswitch);

        switchInputType.setTag(inputType);
        switchInputType.setText(getString(R.string.use_your_email));
        switchInputType.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                MockupData.EMAIL = "";
                MockupData.LOGIN_PHONE = "";
                if (switchInputType.getTag().equals(PHONE)) {
                    inputType = EMAIL;
                    swTextTitle.setText(R.string.sign_up_with_phone);
                    findViewById(R.id.txtEmail).setVisibility(View.GONE);
                    findViewById(R.id.email_border).setVisibility(View.GONE);
                    findViewById(R.id.phone_border).setVisibility(View.VISIBLE);
                    countryCodePicker.setVisibility(View.VISIBLE);
                    passwordEt.clearFocus ();
                    emailEt.getText().clear();
                    switchInputType.setText(getString(R.string.use_your_email));
                } else {
                    inputType = PHONE;
                    swTextTitle.setText(R.string.sign_up_with_email);
                    findViewById(R.id.phone_border).setVisibility(View.GONE);
                    countryCodePicker.setVisibility(View.GONE);
                    findViewById(R.id.txtEmail).setVisibility(View.VISIBLE);
                    findViewById(R.id.email_border).setVisibility(View.VISIBLE);
                    passwordEt.clearFocus (  );
                    phoneNumberEt.getText().clear();
                    switchInputType.setText(getString(R.string.use_your_phone));
                }
                v.setTag(inputType);

            }
        });
    }

    public void accountUser(){
        MockupData.setAccountSignUp(null);
        if (!phoneNumberEt.getText().toString().isEmpty()) {
            userAccount.put("phone",phoneNumberEt.getText().toString().trim());
            userAccount.put("phone_code", countryCodePicker.getFullNumber());
        }else if (!emailEt.getText().toString().isEmpty()){
            userAccount.put("email",emailEt.getText().toString().trim());

        }
        userAccount.put("password",passwordEt.getText().toString().trim());
        userAccount.put ( "code",countryCode );
        userAccount.put ( "user_type",userType );
        userAccount.put("function","Customer");

        MockupData.LOGIN_PHONE = phoneNumberEt.getText ().toString ().trim ();
        MockupData.EMAIL = emailEt.getText ().toString ().trim ();
        MockupData.COUNTRY_CODE= countryCodePicker.getFullNumber ();
        //set current user to local
        MockupData.setAccountSignUp ( userAccount );
        new SignUpWs ().accountSignUp ( userAccount,requestServer );
    }

    private SignUpWs.RequestServer requestServer = new SignUpWs.RequestServer () {
        @Override
        public void onRequestSuccess() {
            progressBar.setVisibility(View.GONE);
            isDidLogin = true;
            Intent intent = new Intent(SignUpActivity.this, ConfirmSmsActivity.class);
            if (action != null) {
                intent.putExtra("action", action);
            }
            startActivityForResult(intent, CONFIRM_SMS);
        }

        @Override
        public void onRequestToken(String token, int id, User user) {

        }

        @Override
        public void onRequestFailed(String message) {
            progressBar.setVisibility(View.GONE);
            Util.popupMessage(getString(R.string.error), message,SignUpActivity.this);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == CONFIRM_SMS) {
            Intent intent = new Intent(SignUpActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

}

