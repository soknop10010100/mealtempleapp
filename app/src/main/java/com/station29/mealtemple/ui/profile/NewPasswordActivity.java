package com.station29.mealtemple.ui.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import java.util.HashMap;

public class NewPasswordActivity extends BaseActivity {

    private EditText txtpassword;
    private HashMap<String, Object> userData= new HashMap<>();
    private View progressBar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_password);

        txtpassword = findViewById(R.id.txtpassword);
        Button btnRest = findViewById(R.id.btnRest);
        progressBar = findViewById(R.id.progressBar);
        btnRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                if(txtpassword.getText().length()>=6) {
                    Util.hideSoftKeyboard(v);
                    updatePassword();
                }else{
                    Util.popupMessage(getString(R.string.error),getString(R.string.please_new_password),NewPasswordActivity.this);
                }
            }
        });

        findViewById(R.id.btnBackLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void updatePassword(){
        userData.put("new_password",txtpassword.getText().toString());
        MockupData.setAccountSignUp ( userData );
        new ProfileWs().updateProfiles(this, userData, new ProfileWs.ProfileViewAndUpdate() {
            @Override
            public void onLoadProfileSuccess(User user) {

            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(NewPasswordActivity.this,errorMessage,Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onUpdateProfilesSuccess(boolean isSendSms, User user) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(NewPasswordActivity.this,getString(R.string.success),Toast.LENGTH_SHORT).show();
                // Reset all activities in stack
                Intent intent = new Intent(NewPasswordActivity.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }
}
