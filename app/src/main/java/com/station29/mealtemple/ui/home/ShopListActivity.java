package com.station29.mealtemple.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.Category;
import com.station29.mealtemple.api.model.StoreGroup;
import com.station29.mealtemple.api.request.CategoryWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.home.adpater.MenuStoreFragmentsPagerAdapter;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

public class ShopListActivity extends BaseActivity {

    private MenuStoreFragmentsPagerAdapter menuStoreFragmentsPagerAdapters;
    private List<StoreGroup> storeGroupsByCategory;
    private int mCategoryId;
    ViewPager viewPagerLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        if (getIntent().hasExtra("category")) {
            TextView titleTv = findViewById(R.id.title);
            Category category = (Category) getIntent().getSerializableExtra("category");
            if (category == null) return;
            titleTv.setText(category.getName());

            listTags(BaseActivity.countryCode, category.getId());

            mCategoryId = category.getId();
        }

        TabLayout tabLayoutLocation = findViewById ( R.id.tablayout_location );
        viewPagerLocation = findViewById ( R.id.view_pager_shop_bycategory );
        menuStoreFragmentsPagerAdapters = new MenuStoreFragmentsPagerAdapter ( getSupportFragmentManager () );
        storeGroupsByCategory = new ArrayList<>();
        viewPagerLocation.setAdapter ( menuStoreFragmentsPagerAdapters );
        tabLayoutLocation.setupWithViewPager ( viewPagerLocation );

        findViewById(R.id.backButton).setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                onBackPressed ();
            }
        } );

        findViewById ( R.id.search_store_name ).setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ShopListActivity.this,ShopSearchActivity.class);
                intent.putExtra("categoryId1",mCategoryId);
                startActivityForResult(intent,RESULT_OK);
            }
        } );
    }

    private void listTags(String countryCode, int catId) {
        String lang = Util.getString("language", this);
        new CategoryWs().loadCategoryTag(countryCode, catId,lang, new CategoryWs.LoadTagCallback() {
            @Override
            public void onLoadSuccess(List<StoreGroup> categoryList) {
                storeGroupsByCategory.add(new StoreGroup(0,getString(R.string.all)));
                storeGroupsByCategory.addAll(categoryList);
                for (StoreGroup s : storeGroupsByCategory) {
                    menuStoreFragmentsPagerAdapters.addFragment(ShopListFragment.newInstance (mCategoryId, s.getId()),s.getLabel());
                }
                if (categoryList.isEmpty()) { // No tag then
                    findViewById(R.id.tablayout_location).setVisibility(View.GONE); // If only one => Hide Tab
                    menuStoreFragmentsPagerAdapters.addFragment(ShopListFragment.newInstance (mCategoryId), "All");
                }
                menuStoreFragmentsPagerAdapters.notifyDataSetChanged();
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                Util.popupMessage(null, errorMessage, ShopListActivity.this);
            }
        });
    }

}
