package com.station29.mealtemple.ui.order.adpater;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.Order;
import com.station29.mealtemple.util.DateUtil;
import com.station29.mealtemple.util.Util;

import java.util.List;
import java.util.Locale;

public class ListOrderAdapter extends RecyclerView.Adapter<ListOrderAdapter.MyViewHolder> {

    private List<Order> orderDetailArrayList;
    private OrderItemClickListener itemClickListener;

    public ListOrderAdapter(List<Order> orderDetailArrayList, OrderItemClickListener itemClickListener) {
        this.orderDetailArrayList = orderDetailArrayList;
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override

    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View row = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_order, parent, false);
        return new MyViewHolder(row);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Order order = orderDetailArrayList.get(position);
        String currencySign;
        currencySign = ((Constant.getConfig ()!=null)? Constant.getConfig ().getCurrencySign ():"N/A");
        holder.shopNameTv.setText(order.getAccountName());
        String fullStatus="";
        if(!order.getStatus().equalsIgnoreCase("COMPLETED")){
            holder.status.setTextColor(Color.parseColor("#9C50FD"));
            if (order.getType_order().equalsIgnoreCase("delivery")){
                //resturant
             //  fullStatus = order.getStatus()+" | " +"FOOD";
                holder.status.setText(String.format(Locale.US,"%s | %s ",order.getStatus(),holder.status.getContext().getString(R.string.food_st)));
            }else if(order.getType_order().equalsIgnoreCase("send")){
                //taxi
               // fullStatus = order.getStatus()+" | " +"TAXI";
                holder.status.setText(String.format(Locale.US,"%s | %s ",order.getStatus(),holder.status.getContext().getString(R.string.send_st)));
            } else {
                holder.status.setText(String.format(Locale.US,"%s | %s ",order.getStatus(),holder.status.getContext().getString(R.string.taxi_st)));
            }
        }else{
            holder.status.setTextColor(Color.parseColor("#9C50FD"));
            fullStatus = order.getStatus();
            holder.status.setText(fullStatus);

        }

        holder.orderNumberTv.setText(String.format(Locale.US , "%s: %d" ,  holder.orderNumberTv.getContext().getString(R.string.order_number), order.getOrder_number()));
        if(order.getWaypoint() == null || order.getWaypoint().getListLatLng() == null){
            holder.totalPriceTv.setText("---");
        }else{
            holder.totalPriceTv.setText(String.format("%s %s", currencySign,Util.formatPrice(order.getAmount())));
        }
        holder.dateTv.setText(DateUtil.formatDateUptoCurrentRegion(order.getDateOrder()));
        holder.row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onOrderItemClick(order);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orderDetailArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView orderNumberTv, totalPriceTv, dateTv, shopNameTv , status;
        View row;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            orderNumberTv = itemView.findViewById(R.id.order_number);
            totalPriceTv = itemView.findViewById(R.id.total_price);
            dateTv = itemView.findViewById(R.id.date);
            shopNameTv = itemView.findViewById(R.id.shop_name);
            row = itemView;
            status = itemView.findViewById(R.id.status);
        }
    }

    public interface OrderItemClickListener {
        void onOrderItemClick(Order orderDetail);

    }

    public void clear() {
        int size = orderDetailArrayList.size ();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                orderDetailArrayList.remove ( 0 );
            }
            notifyItemRangeRemoved ( 0, size );
        }
    }


}
