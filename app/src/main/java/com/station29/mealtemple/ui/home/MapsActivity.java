package com.station29.mealtemple.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.button.MaterialButton;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.request.ServerUrlWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.GPSTracker;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener {

    private GoogleMap mMap;
    private Geocoder geocoder;
    private ImageView centerImage;
    private LinearLayout search;
    private int AUTOCOMPLETE = 1;
    private TextView txtMap;
    private String saveCountryCode;
    private double saveLat, saveLng, userLat = 0, userLng = 0;
    public static String oldEditShippingDeliver = null;
    private String action, checkSelectMap = " ", fullAddress;
    private double intentLat, intentLng;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        ImageView btnBack = findViewById(R.id.backButton);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        search = findViewById(R.id.map_delivery_location);
        txtMap = findViewById(R.id.txtMap);
        centerImage = findViewById(R.id.center_image);

        if (getIntent() != null && getIntent().hasExtra("action")) {
            action = (String) getIntent().getSerializableExtra("action");
            if (action.isEmpty()) return;
            oldEditShippingDeliver = MockupData.DELIVERY_ADDRESS;
        }
        if (getIntent() != null && getIntent().hasExtra("gotoMap")) {
            checkSelectMap = getIntent().getStringExtra("gotoMap");
            intentLat = getIntent().getDoubleExtra("userLat", 0);
            intentLng = getIntent().getDoubleExtra("userLng", 0);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.google_map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);
        callLocation();


        MaterialButton setLocationButton = findViewById(R.id.setLocationButton);
        setLocationButton.setOnClickListener(this);
        geocoder = new Geocoder(this, Locale.getDefault());

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                v.setEnabled(false);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        v.setEnabled(true);
                    }
                }, 100);
                if (!Places.isInitialized()) {
                    Places.initialize(getApplicationContext(), getString(R.string.google_maps_key));
                }
                List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.ADDRESS, Place.Field.LAT_LNG);

                Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                        .build(MapsActivity.this);

                startActivityForResult(intent, AUTOCOMPLETE);
            }
        });


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (checkSelectMap.equals("gotoMap")) {
            zoomMap(userLat, userLng);
            checkSelectMap = "gotoMapTaxi";
        } else {
            zoomMap(BaseActivity.latitude, BaseActivity.longitude);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
            mMap.getUiSettings ().setZoomControlsEnabled ( false );
            mMap.getUiSettings ().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setCompassEnabled(false);//hide compass

            mMap.setOnCameraIdleListener ( new GoogleMap.OnCameraIdleListener () {
                @SuppressLint("LogConditional")
                @Override
                public void onCameraIdle() {
                    displayAddressByImageCenter();
                }
            } );
//            mMap.setMyLocationEnabled ( true );
//            mMap.getUiSettings ().setZoomControlsEnabled ( false );
        }

        private void zoomMap(double lat , double lng) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lng), 15));
        }

        private String displayAddressByImageCenter() {
        if (mMap == null) return "";
          try {
                int h = centerImage.getHeight () ;
                int w = centerImage.getWidth () / 2;
                int X = (int) centerImage.getX () + w;
                int Y = (int) centerImage.getY () + h / 2;
                LatLng ll = mMap.getProjection ().fromScreenLocation ( new Point ( X, Y + 45 ) );
                List<Address> addresses = geocoder.getFromLocation ( ll.latitude, ll.longitude, 1 );
                String fullAdd = getFullAddress ( addresses );
                try {
                      if(addresses.get(0).getCountryName() != null) saveCountryCode  = addresses.get(0).getCountryCode();
                      else saveCountryCode = "";
                }catch (Exception e){
                      e.printStackTrace();
                }
                txtMap.setText(String.format("%s",fullAdd));
                fullAddress= fullAdd;
                saveLat = ll.latitude;
                saveLng = ll.longitude;
                return fullAdd;
            } catch (IOException e) {
                Log.d ( e.getClass ().getName (), "onResult: " + e.getMessage () );
            }
            return "";
        }

        @Override
        public void onClick(View view) {
            if (view.getId () == R.id.setLocationButton) {
                String address = displayAddressByImageCenter();
                // Then
                if (checkSelectMap!= null && checkSelectMap.equals("gotoMapTaxi")) {
                    Intent data = new Intent ();
                    data.putExtra ( "delivery_address", fullAddress );
                    data.putExtra("lat",saveLat);
                    data.putExtra("lng",saveLng);
                    setResult ( RESULT_OK, data );
                    finish ();
                } else {
                    popupInformLocation ( address );
                }

            }else  if (view == findViewById(R.id.location_meBtn)) {
                if (mMap!=null)getMyLocation(mMap);
            }
        }

        private String getFullAddress(List<Address> addresses) {
            if (addresses.size () > 0) {
                return addresses.get ( 0 ).getAddressLine ( 0 );
            }
            return "";
        }

        private void popupInformLocation(final String fullAddress) {
            androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(MapsActivity.this,R.style.DialogTheme);
            builder.setMessage ( fullAddress +"\n"+getString(R.string.delivery_location) );
            builder.setNegativeButton ( getString(R.string.no), null);
            builder.setPositiveButton ( getString(R.string.yes), new DialogInterface.OnClickListener () {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    requestServerUrl(saveCountryCode, fullAddress);
                }
            } );
            builder.show ();
        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == AUTOCOMPLETE){
            if(resultCode == RESULT_OK) {
                 Place place = Autocomplete.getPlaceFromIntent(data);
                 mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
                 Log.d("placeAuto", "Place:" + place.getAddress()+" , "+place.getLatLng());
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR){
                 Status status = Autocomplete.getStatusFromIntent(data);
            }else if (resultCode == RESULT_CANCELED){

            }
        }
    }

    private void requestServerUrl (final String code, final String fullAddress){
            new ServerUrlWs ().readServerUrl( code, new ServerUrlWs.loadServerUrl () {
                @Override
                public void onLoadSuccess(boolean success) {
                    Intent data = new Intent ();
                    BaseActivity.countryCode = code;
                    latitude = saveLat;
                    longitude = saveLng;
                    data.putExtra("late",latitude);
                    data.putExtra("lng",longitude);
                    Log.d("chesterme","lat :"+latitude+" long :"+longitude);
                    data.putExtra ( "delivery_address", fullAddress );
                    MockupData.DELIVERY_ADDRESS = fullAddress;
                    setResult ( RESULT_OK, data );
                    finish ();
                }

                @Override
                public void onLoadFailed(String errorMessage) {
                    popupMessage ( MapsActivity.this );
                }
            } );
    }

    public static void popupMessage( Activity activity) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(activity,R.style.DialogTheme);
        builder.setTitle(null);
        builder.setMessage(R.string.it_seems_your_location);
        builder.setPositiveButton(String.format ( Locale.US,"%s", activity.getString(R.string.ok )), null);
        if (!activity.isFinishing()) builder.show();
    }

    private void getMyLocation(GoogleMap mMap) {
        //current location zoom
        if (userLat>0&&userLng>0){
            LatLng latLng = new LatLng(userLat,userLng);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.animateCamera(cameraUpdate);
        }
    }

    private void callLocation() {
        GPSTracker gps = new GPSTracker(MapsActivity.this);
        if (gps.canGetLocation()) {
            userLat = gps.getLatitude();
            userLng = gps.getLongitude();
        }
    }

}

