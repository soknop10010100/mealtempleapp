package com.station29.mealtemple.ui.home;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.zxing.Result;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.payment.TransferActivity;
import com.station29.mealtemple.util.Util;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class ScannerActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private final String TAG = ScannerActivity.this.getClass().getName();
    private ZXingScannerView mScannerView;
    private User user;
    private final int CAMERA = 123 ,REQUEST_TRANSFER = 2342;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.layout_scan_main);
        mScannerView = findViewById(R.id.scan_view);
        findViewById(R.id.backButton).setOnClickListener(v -> finish());

        if(getIntent() != null && getIntent().hasExtra("user")){
            user = (User) getIntent().getSerializableExtra("user");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CAMERA},CAMERA );
        } else {
            mScannerView.startCamera();          // Start camera on resume
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, R.string.camera_successful, Toast.LENGTH_LONG).show();
                mScannerView.startCamera();          // Start camera on resume
            } else {
                Toast.makeText(this, R.string.cannot_take_photo, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("asdfasdfasdfads", rawResult.getText()); // Prints scan results
        Log.v(TAG, rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)
//        Toast.makeText(ScannerActivity.this, rawResult.getText() , Toast.LENGTH_LONG).show();

        // If you would like to resume scanning, call this method below:
        doTransfer(rawResult.getText().toString());
        mScannerView.resumeCameraPreview(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == REQUEST_TRANSFER){
            setResult(RESULT_OK);
            finish();
        }
    }

    private void doTransfer(String text){
        Intent intent = new Intent(this, TransferActivity.class);
        intent.putExtra("qrCode", text);
        intent.putExtra("user",user);
        startActivityForResult(intent,REQUEST_TRANSFER);
        finish();
    }

}