package com.station29.mealtemple;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.OnBoardScreen;
import com.station29.mealtemple.ui.BaseFragment;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

import static com.station29.mealtemple.SplashScreenActivity.isFirst;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentBoarding#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentBoarding extends BaseFragment {

    private static final String MY_NUM_KEY = "num";
    private static final String MY_IMAGE_KEY = "IMAGE";
    private static final String MY_TITLE_KEY = "TITLE";
    private static final String MY_TEX_KEY = "TEX";
    private static final String MY_INDEX_KEY = "index";
    private static final String MY_LANG_KEY = "Lang";
    private int mNum =0,index;
    private int image;
    private String title;
    private String tex,lang;
    public FragmentBoarding() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FragmentBoarding newInstance(int image, String title, String text, int num, int index,String lang) {

        FragmentBoarding fragment = new FragmentBoarding();
        Bundle args = new Bundle();
        args.putInt(MY_NUM_KEY,num);
        args.putInt(MY_IMAGE_KEY,image);
        args.putString(MY_TITLE_KEY,title);
        args.putString(MY_TEX_KEY,text);
        args.putString(MY_LANG_KEY,lang);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNum = getArguments() != null ? getArguments().getInt(MY_INDEX_KEY) : -1;
        index = getArguments() != null ? getArguments().getInt(MY_NUM_KEY) : -1;
        image = getArguments() != null ? getArguments().getInt(MY_IMAGE_KEY) : R.drawable.ic_image_1;
        title = getArguments() !=null ? getArguments().getString(MY_TITLE_KEY) : "null";
        tex = getArguments() !=null ? getArguments().getString(MY_TEX_KEY):"null";
        lang = getArguments() !=null ? getArguments().getString(MY_LANG_KEY):"en";
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_boarding, container, false);
        ImageView imageView = view.findViewById(R.id.imageVie);
        TextView titleTv = view.findViewById(R.id.titles);
        TextView textTv = view.findViewById(R.id.des_Tv);
        TextView skipTv = view.findViewById(R.id.skip);
        MaterialButton btn = view.findViewById(R.id.btnContinues);
        if(index==4){
            skipTv.setVisibility(View.GONE);
        } else {
            skipTv.setVisibility(View.VISIBLE);
        }

        skipTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity,MainActivity.class);
                intent.putExtra("lang",lang);
                startActivity(intent);
                Constant.setStart("stop");
                Util.start(mActivity,"stop");
                Util.changeLanguage(mActivity,lang);
            }
        });

        imageView.setImageResource(image);
        titleTv.setText(title);
        textTv.setText(tex);
        return view;
    }


}