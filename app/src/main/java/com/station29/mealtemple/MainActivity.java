package com.station29.mealtemple;

import android.os.Bundle;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );

        BottomNavigationView navView = findViewById ( R.id.nav_view );
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
//                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
//                .build();
        NavController navController = Navigation.findNavController ( this, R.id.nav_host_fragment );
//        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController ( navView, navController );
//        navView.setSelectedItemId(R.id.navigation_dashboard);


        if (getIntent () != null && getIntent ().hasExtra ( "tabId" )) {
            int tabIndex = getIntent ().getIntExtra ( "tabId", -1 );
            navView.setSelectedItemId ( tabIndex );
        }
        String lang = "";
        if(getIntent().hasExtra("lang")){
            lang = getIntent().getStringExtra("lang");
        }
        Util.changeLanguage(this,lang);
        new GetLatestVersion(MainActivity.this).execute();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}


