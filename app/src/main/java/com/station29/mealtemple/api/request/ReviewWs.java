package com.station29.mealtemple.api.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewWs {

    public void writeReview(final HashMap<String, Object> reviewData, Context activity, final ReviewSuccess reviewSuccess){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        final Call<JsonElement> elementApi = serviceAPI.rateReview(reviewData);
        elementApi.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject object) {
                if (object.has("success") && object.get("success").getAsBoolean()) {
                    String message = object.get("message").getAsString();
                    if (Constant.serviceLanguage.containsKey(message))
                    reviewSuccess.onReviewSuccess(Constant.serviceLanguage.get(message), true);
                    else reviewSuccess.onReviewSuccess(message,true);
                }else {
                    if (Constant.serviceLanguage.containsKey(object.get("message").getAsString()))
                    reviewSuccess.onReviewFailed(Constant.serviceLanguage.get(object.get("message").getAsString()));
                    else reviewSuccess.onReviewFailed(object.get("message").getAsString());
                    Log.d("notTranslate", "onResponse: "+object.get("message").getAsString());
                }
            }

            @Override
            public void onError(String error,int code) {
                reviewSuccess.onReviewFailed(error);
            }
        }));
//        elementApi.enqueue(new Callback<JsonElement>(){
//
//            @Override
//            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
//                try {
//                    if (response.isSuccessful()) {
//                        assert response.body() != null;
//                        JsonObject object = response.body().getAsJsonObject();
//
//                        if (object.has("success") && object.get("success").getAsBoolean()) {
//                            String message = object.get("message").getAsString();
//                            reviewSuccess.onReviewSuccess(message, true);
//                        }else  reviewSuccess.onReviewFailed(object.get("message").getAsString());
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonElement> call, Throwable t) {
//
//            }
//        });
    }
    public interface ReviewSuccess {
        void onReviewSuccess(String message,boolean newData);

        void onReviewFailed(String errorMessage);
    }

}
