package com.station29.mealtemple.api.request;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.User;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpWs {

    public void accountSignUp(HashMap<String ,Object> signUp, final RequestServer requestServer) {
        ServiceAPI service = RetrofitGenerator.createService ();
        final Call<JsonElement> res = service.signUp ( signUp );
        res.enqueue ( new Callback<JsonElement> () {
            @Override
            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
                JSONObject jsonObject = new JSONObject ( );
                JsonElement jsonElement = response.body();
                if (response.isSuccessful()) {
                    try {
                        jsonObject.put ( "signup",response.toString () );
                        requestServer.onRequestSuccess ( );
                    } catch (JSONException e) {
                        e.getMessage();
                    }
                }else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                requestServer.onRequestFailed(Constant.serviceLanguage.get(message));
                                else requestServer.onRequestFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                            requestServer.onRequestFailed ( "Incorrect format Phone or Email" );//server error wrong phone num
                        } catch (IOException e2){
                            requestServer.onRequestFailed ( "Incorrect format Phone or Email" );//server error wrong phone num
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {

            }
        });

    }

    public void verifyAccountCode(HashMap<String , Object> account,final RequestServer requestServer){
        ServiceAPI serviceAPI = RetrofitGenerator.createService ();
        final Call<JsonElement> res = serviceAPI.verifyAccount ( account );
        res.enqueue ( new Callback<JsonElement> () {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonObject = response.body();
                    if(response.isSuccessful ()){
                        if (jsonObject.getAsJsonObject().has("data")) {
                            String token= "";
                            int id = 0;
                            JsonObject dataObj = jsonObject.getAsJsonObject().getAsJsonObject("data");
                            if (dataObj.has("access_token")) {
                                token = dataObj.get("access_token").getAsString();
                            }
                            if (dataObj.has("user")) {
                                JsonObject userObj = dataObj.getAsJsonObject("user");
                                if (userObj.has("id")) {
                                    id = userObj.get("id").getAsInt();
                                }
                                User user = new Gson().fromJson(userObj, User.class);
                                requestServer.onRequestToken(token,id,user);
                            }
                        }
                    }
                    else {
                        if (response.errorBody() != null) {
                            String message ="";
                            ResponseBody errorString = response.errorBody();
                            try {

                                String json = errorString.string();
                                Log.d("errror", json);
                                JSONObject jsonObject1 = new JSONObject(json);
                                if (jsonObject1.has("message")){
                                     message = jsonObject1.getString("message");
                                    if (Constant.serviceLanguage.containsKey(message))
                                    requestServer.onRequestFailed(Constant.serviceLanguage.get(message));
                                    else requestServer.onRequestFailed(message);
                                }  else if(jsonObject1.has("error")){
                                    message = jsonObject1.getString("error");
                                    if (Constant.serviceLanguage.containsKey(message))
                                    requestServer.onRequestFailed(Constant.serviceLanguage.get(message));
                                    else requestServer.onRequestFailed(message);
                                }
                                Log.d("notTranslate", "onResponse: "+message);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e(e.getClass().getName(), Objects.requireNonNull(e.getMessage()));
                                requestServer.onRequestFailed(String.valueOf(R.string.unexpected_problem));
                            } catch (IOException ignored){
                                requestServer.onRequestFailed(String.valueOf(R.string.unexpected_problem));
                            }
                        }
                    }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, Throwable t) {
                requestServer.onRequestFailed("Cannot reach Server.");
            }
        } );

    }

    public void forgetAccountPassword(HashMap<String,Object> account ,final RequestServer requestServer){
        ServiceAPI serviceAPI = RetrofitGenerator.createService ();
        final Call<JsonElement> res = serviceAPI.forgetPassword ( account );
        res.enqueue ( new Callback<JsonElement> () {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonObject = response.body();
                if(response.isSuccessful ()){
                    requestServer.onRequestSuccess ( );
                }
                else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                requestServer.onRequestFailed(Constant.serviceLanguage.get(message));
                                else requestServer.onRequestFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                            requestServer.onRequestFailed ( "Incorrect format Phone or Email" );//server error wrong phone num
                        } catch (IOException e2){
                            requestServer.onRequestFailed ( "Incorrect format Phone or Email" );//server error wrong phone num
                        }
                    }

                }
            }
            @Override
            public void onFailure(@NotNull Call<JsonElement> call, Throwable t) {

            }
        } );
    }


    public void SignUpFbGoogle(String social_name, HashMap<String,String> account, final RequestServer requestServer ) {
        ServiceAPI serviceAPI = RetrofitGenerator.createService();
        final Call<JsonElement> res = serviceAPI.loginGoogle(social_name,account);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonObject = response.body();
                if (response.isSuccessful()) {
                    assert jsonObject != null;
                    String token = null;
                    int id = 0;
                    if (jsonObject.getAsJsonObject().has("data")) {
                        JsonObject dataObj = jsonObject.getAsJsonObject().getAsJsonObject("data");
                        if (dataObj.has("access_token")) {
                            token  = dataObj.get("access_token").getAsString();
                        }
                        if (dataObj.has("user")) {
                            JsonObject userObj = dataObj.getAsJsonObject("user");
                            if (userObj.has("id"))
                              id = userObj.get("id").getAsInt();
                            User user = new Gson().fromJson(userObj, User.class);
                            requestServer.onRequestToken(token,id,user);
                        }
                    }
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")) {
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                requestServer.onRequestFailed(Constant.serviceLanguage.get(message));
                                else requestServer.onRequestFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2) {
                        }
                    }

                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, Throwable t) {

            }
        });
    }


    public interface RequestServer {
        void onRequestSuccess();
        void onRequestToken(String token, int userId,User user);
        void onRequestFailed(String message);
    }
}
