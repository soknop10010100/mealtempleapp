package com.station29.mealtemple.api;

import com.google.gson.JsonElement;

import com.station29.mealtemple.api.model.OrderRequestParams;


import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ServiceAPI {

    @POST("api/v1/login")
        Call<JsonElement> login(@Body HashMap requestModel);

    @GET("api/v2/category")
        Call<JsonElement> listCategory(@Query("country") String countryCode,@Query("lang_code") String lang);

    @GET("api/v2/category")
        Call<JsonElement> categoryTag(@Query("country") String countryCode, @Query("cate_id")int categoryId ,@Query("lang_code") String lang );

    @GET("api/v1/account")
        Call<JsonElement> listStore(@Query("country") String countryCode, @Query("cate_id") int categoryId, @Query("tag_id") int categoryTagId ,@Query("page") int page ,@Query("size") int size, @Query("lat") double lat,@Query("lng") double lng);

    @GET("api/v1/account")
        Call<JsonElement> storeDetail(@Query ( "country" ) String countryCode,@Query ( "account_id" ) int accID,@Query ( "cate_id" ) int cateId,@Query ( "tag_id" ) int tagId, @Query("lat") double lat,@Query("lng") double lng);

    @GET("api/v1/product")
        Call<JsonElement> listProduct(@Query("country") String countryCode ,@Query("product_group_id") int menuId,@Query("account_id") int accountId ,@Query("page") int page ,@Query("size") int size);

    @GET("api/v1/product")
        Call<JsonElement> productDetail(@Query("country") String countryCode, @Query("account_id") int accountId, @Query("product_id") int productId);

    @POST("api/v1/singup")
        Call<JsonElement> signUp(@Body HashMap signup );

    @POST("api/v1/login")
        Call<JsonElement> loginPhoneEmail(@Body HashMap account);

    @GET("api/v1/coupons")
        Call<JsonElement> verifyCoupon(@Query("code") String couponCode, @Query("account_id") int accountId);

    @POST("api/v1/coupons/apply")
        Call<JsonElement> couponVerify(@Body OrderRequestParams orderDetail);

    @POST("api/v1/preview/receipt")
        Call<JsonElement> previewReceipt(@Body HashMap orderDetail);

    @POST("api/v1/verify")
        Call<JsonElement> verifyAccount(@Body HashMap<String,Object> accountVerify);

    @POST("api/v2/order")
        Call<JsonElement> postOrder(@Body OrderRequestParams orderDetail);

    @GET("api/v1/order")
        Call<JsonElement> listOrder(@Query("statuses") String listStatuses,@Query("page") int page ,@Query("size") int size);

    @GET("api/v1/order/{orderId}")
        Call<JsonElement> readOrder(@Path("orderId") int orderId);

    @GET("api/v1/view-profile")
        Call<JsonElement> viewProfile();

    @PUT("api/v1/update-profile")
        Call<JsonElement> updateProfile(@Body HashMap updateProfile);

    @GET("api/v1/config")
        Call<JsonElement> readConfig(@Query("country") String countryCode);

    @GET("api/v1/config/lang")
        Call<JsonElement> readConfigLanguage(@Query("langcode") String countryCode);

    @POST("api/v1/forget-password")
        Call<JsonElement> forgetPassword(@Body HashMap forgetAccount);

    @PUT("api/v1/change-profile")
        Call<JsonElement>changeProfile(@Body RequestBody image);

    @POST("api/v1/login/{social_name}/social")
        Call<JsonElement> loginGoogle(@Path("social_name") String socialName , @Body HashMap loginGoogle);

    @POST("api/v2/device_token")
        Call<JsonElement> deviceToken(@Body HashMap params );

    @GET("api/v1/account")
        Call<JsonElement> searchShop(@Query("country") String country,@Query("s") String shopName,@Query("lat") double lat,@Query("lng") double lng,@Query("size")int page,@Query("cate_id") int cate_id);

    @GET("api/server")
        Call<JsonElement> serverUrl(@Query("code") String code);

    @POST("api/v2/logout")
        Call<JsonElement> logoutProfile (@Query("device_type") String deviceType , @Query("device_token") String deviceToken , @Query("topic") String topic);

    @POST("api/v1/rate-review")
        Call<JsonElement> rateReview (@Body HashMap reviewData);

    @GET("api/v1/getDriverServices")
        Call<JsonElement> getDriverServices(@Query("country_code") String countryCode ,@Query("pickup_latitute") double pickLat,@Query("pickup_longitute") double pickLng,@Query("dropoff_latitute") double dropLat,@Query("dropoff_longitute") double dropLng);

    @GET("api/v1/getDriverServices")
        Call<JsonElement> getDriverServicesWithoutDrop(@Query("country_code") String countryCode,@Query("pickup_latitute") double pickLat,@Query("pickup_longitute") double pickLng);
    @GET("api/v1/getDriverServices")
    Call<JsonElement> applyCouponDrop(@Query("pickup_latitute") double pickLat,@Query("pickup_longitute") double pickLng,@Query("dropoff_latitute") double dropLat,@Query("dropoff_longitute") double dropLng,@Query("customer_id") int id,@Query("coupon_code") String coupon );

    @GET("api/v1/getDriverServices")
    Call<JsonElement>applyCouponWithoutDrop(@Query("pickup_latitute") double pickLat,@Query("pickup_longitute") double pickLng,@Query("customer_id") int CustomerId,@Query("coupon_code") String coupon);

    @POST("api/v1/OrderDriverService")
        Call<JsonElement> orderDriverService (@Body HashMap orderDriverService);

    @GET("api/v2/order/{orderId}")
        Call<JsonElement> readOrderDetail(@Path("orderId") int orderId);

    @PUT("api/v2/service-driver/{orderId}/cancel")
        Call<JsonElement> cancelTaxi(@Path("orderId") int orderId);

    @GET("api/v1/payment/banks")
        Call<JsonElement> getBank();

    @POST("api/v1/payment/bank-account")
        Call<JsonElement> addBankAccount(@Body HashMap addBankAccount);

    @POST("api/v1/send-packages")
        Call<JsonElement> sendPackage(@Body HashMap<String,Object> hashMap);

    @GET("api/v1/get-send-packages")
        Call<JsonElement> getSendPackage(@Query("country_code") String countryCode , @Query("pickup_latitute") String pickLat,@Query("pickup_longitute") String pickLng,@Query("dropoff_latitute") String dropLat,@Query("dropoff_longitute") String dropLng);

    //Payment
    @POST("api/v1/wallets/top_up")
        Call<JsonElement> topUpWallets(@Body HashMap<String ,Object> topUpHasMap);

    @DELETE("api/v1/payment/user/history/{paymentHistoryId}")
        Call<JsonElement> deletePaymentHistory(@Path("paymentHistoryId") int paymentHistoryId);

    @GET("api/v1/payment/user/transaction")
        Call<JsonElement> getUserPaymentTransaction(@Query("page")int page);

    @GET("api/v1/payment/user/scan_me")
        Call<JsonElement> getMyQRCode();

    @GET("api/v1/payment/{id}")
        Call<JsonElement> getPaymentStatus(@Path("id") int paymentId);

    @POST("api/v1/payment/user/pay")
        Call<JsonElement> transfer(@Body HashMap<String, String> params);

    @PUT("api/v1/verify/pin_password")
        Call<JsonElement> setPin(@Body HashMap<String, Object> params);

    @GET("api/v1/payment/user/transfer/{id}")
        Call<JsonElement> getUserQRCode (@Path("id") String userId);

}
