package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PreView implements Serializable {


    @SerializedName("taxs")
    @Expose
    private List<Tax> taxs;

    @SerializedName("surcharge")
    @Expose
    private List<SurCharges> surCharges;
    @SerializedName("subtotal")
    @Expose
    private double subtotal;
    @SerializedName("amount")
    @Expose
    private double grandTotal;
    @SerializedName("delivery_fee")
    @Expose
    private float delivery_fee;
    @SerializedName("coupons_discount")
    @Expose
    List<CouponDis> copDIscount;

    public double getGrandTotal() {
        return grandTotal;
    }

    public List<Tax> getTaxs() {
        return taxs;
    }

    public List<SurCharges> getSurCharges() {
        return surCharges;
    }

    public double getSubtotal() {
        return subtotal;
    }

    public float getDelivery_fee() {
        return delivery_fee;
    }

    public List<CouponDis> getCopDIscount() {
        return copDIscount;
    }
}
