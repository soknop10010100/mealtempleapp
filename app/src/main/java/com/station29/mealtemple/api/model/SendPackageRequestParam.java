package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendPackageRequestParam implements Serializable {
    @SerializedName("pickup_latitute")
    @Expose
    private double pickupLatitute;
    @SerializedName("pickup_longitute")
    @Expose
    private double pickupLongitute;
    @SerializedName("dropoff_latitute")
    @Expose
    private double dropoffLatitute;
    @SerializedName("dropoff_longitute")
    @Expose
    private double dropoffLongitute;
    @SerializedName("pickup_address")
    @Expose
    private String pickupAddress;
    @SerializedName("dropoff_address")
    @Expose
    private String dropoffAddress;
    @SerializedName("phone_sender")
    @Expose
    private String phoneSender;
    @SerializedName("phone_receiver")
    @Expose
    private String phoneReceiver;
    @SerializedName("type_package")
    @Expose
    private String typePackage;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("delivery_fee")
    @Expose
    private String deliveryFee;
    @SerializedName("rider_tip")
    @Expose
    private String riderTip;
    @SerializedName("address_detail_sender")
    @Expose
    private String addressDetailSender;
    @SerializedName("address_detail_receiver")
    @Expose
    private String addressDetailReceiver;
    @SerializedName("name_sender")
    @Expose
    private String nameSender;
    @SerializedName("package_detail")
    @Expose
    private String packageDetail;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("name_receiver")
    @Expose
    private String nameReceiver;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("phone_code")
    @Expose
    private String phoneCode;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("wallet_id")
    @Expose
    private Integer walletId;

    public void setWalletId(Integer walletId) {
        this.walletId = walletId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public double getPickupLatitute() {
        return pickupLatitute;
    }

    public void setPickupLatitute(double pickupLatitute) {
        this.pickupLatitute = pickupLatitute;
    }

    public double getPickupLongitute() {
        return pickupLongitute;
    }

    public void setPickupLongitute(double pickupLongitute) {
        this.pickupLongitute = pickupLongitute;
    }

    public double getDropoffLatitute() {
        return dropoffLatitute;
    }

    public void setDropoffLatitute(double dropoffLatitute) {
        this.dropoffLatitute = dropoffLatitute;
    }

    public double getDropoffLongitute() {
        return dropoffLongitute;
    }

    public void setDropoffLongitute(double dropoffLongitute) {
        this.dropoffLongitute = dropoffLongitute;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getDropoffAddress() {
        return dropoffAddress;
    }

    public void setDropoffAddress(String dropoffAddress) {
        this.dropoffAddress = dropoffAddress;
    }

    public String getPhoneSender() {
        return phoneSender;
    }

    public void setPhoneSender(String phoneSender) {
        this.phoneSender = phoneSender;
    }

    public String getPhoneReceiver() {
        return phoneReceiver;
    }

    public void setPhoneReceiver(String phoneReceiver) {
        this.phoneReceiver = phoneReceiver;
    }

    public String getTypePackage() {
        return typePackage;
    }

    public void setTypePackage(String typePackage) {
        this.typePackage = typePackage;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(String riderTip) {
        this.riderTip = riderTip;
    }

    public String getAddressDetailSender() {
        return addressDetailSender;
    }

    public void setAddressDetailSender(String addressDetailSender) {
        this.addressDetailSender = addressDetailSender;
    }

    public String getAddressDetailReceiver() {
        return addressDetailReceiver;
    }

    public void setAddressDetailReceiver(String addressDetailReceiver) {
        this.addressDetailReceiver = addressDetailReceiver;
    }

    public String getNameSender() {
        return nameSender;
    }

    public void setNameSender(String nameSender) {
        this.nameSender = nameSender;
    }

    public String getPackageDetail() {
        return packageDetail;
    }

    public void setPackageDetail(String packageDetail) {
        this.packageDetail = packageDetail;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getNameReceiver() {
        return nameReceiver;
    }

    public void setNameReceiver(String nameReceiver) {
        this.nameReceiver = nameReceiver;
    }
}
