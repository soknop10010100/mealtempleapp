package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductDetail implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("sku")
    @Expose
    private String sku;

    @SerializedName("product_group_id")
    @Expose
    private int productGroupId;

    @SerializedName("account_id")
    @Expose
    private int accountId;

    @SerializedName("price")
    @Expose
    private double price;

    @SerializedName("url_image")
    @Expose
    private String urlImage;

    @SerializedName("pbe_id")
    @Expose
    private int pbeId;

    @SerializedName("options")
    @Expose
    private List<Option> option = null;

    @SerializedName("add_ons")
    @Expose
    private List<AddOn> addOn = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getProductGroupId() {
        return productGroupId;
    }

    public int getAccountId() {
        return accountId;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public int getPbeId() {
        return pbeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public double getPrice() {
        return price;
    }

    public List<Option> getOption() {
        if (option == null) {
            option = new ArrayList<>();
        }
        return option;
    }

    public void setOption(List<Option> option) {
        this.option = option;
    }

    public List<AddOn> getAddOn() {
        if (addOn == null){
            addOn = new ArrayList<>();
        }
        return addOn;
    }

    public void setAddOn(List<AddOn> addOn) {
        this.addOn = addOn;
    }

}
