package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CouponDis {

    @SerializedName("option")
    @Expose
    private String option;

    private String couponCode;

    @SerializedName("subtotal_after")
    @Expose
    private double afterDis;

    @SerializedName("subtotal_before")
    @Expose
    private double beforeDis;

    @SerializedName("total_discount")
    @Expose
    private double totalDiscount;

    @SerializedName("discount_items")
    @Expose
    private List<DisItems> disItemsList= new ArrayList<> (  );

    @SerializedName("taxs")
    @Expose
    private List<Tax> taxList;

    public List<Tax> getTaxList() {
        return taxList;
    }

    public String getOption() {
        return option;
    }

    public double getBeforeDis() {
        return beforeDis;
    }

    public double getAfterDis() {
        return afterDis;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public List<DisItems> getDisItemsList() { return disItemsList; }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
}
