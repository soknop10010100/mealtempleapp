package com.station29.mealtemple.api.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OpenHour implements Serializable {

    @SerializedName("day")
    @Expose
    private int day;

    @SerializedName("open_time")
    @Expose
    private int openTime;

    @SerializedName("close_time")
    @Expose
    private int closeTime;

    @SerializedName("close")
    @Expose
    private int isClose;

    public int getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public int getOpenTime() {
        return openTime;
    }

    public void setOpenTime(int openTime) {
        this.openTime = openTime;
    }

    public int getCloseTime() {
        return closeTime;
    }

    public boolean isOpen() {
        return isClose == 0; // 0 mean false
    }

    public int getIsClose() {
        return isClose;
    }

    public void setIsClose(int isClose) {
        this.isClose = isClose;
    }
}
