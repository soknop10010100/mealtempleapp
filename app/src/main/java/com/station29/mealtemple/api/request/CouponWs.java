package com.station29.mealtemple.api.request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.CouponDis;
import com.station29.mealtemple.api.model.OrderRequestParams;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponWs {

    public interface VerifyCouponListener {
        void onVerifyCouponSuccess(CouponDis disOrder, String couponToken,float values,String message);
        void onVerifyError(String message);
    }

    public void couponVerify(Activity activity, OrderRequestParams orderDetail, final VerifyCouponListener listener) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        final Call<JsonElement> res = service.couponVerify (orderDetail);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject object) {
                if (object.has("success") && object.get("success").getAsBoolean()) {
                    JsonObject data = (JsonObject) object.getAsJsonObject ().get ( "data" ).getAsJsonObject ().get ( "coupon_discount" );
                    float values= object.getAsJsonObject ( ).get ( "data" ).getAsJsonObject ().get ( "value" ).getAsFloat ();
                    String couponToken = object.getAsJsonObject (  ).get ( "data" ).getAsJsonObject ().get ( "token" ).getAsString ();
                    CouponDis disItems =  new Gson ().fromJson(data, CouponDis.class);
                    if(Constant.serviceLanguage.containsKey(object.get("message").getAsString()))
                        listener.onVerifyCouponSuccess ( disItems,couponToken,values,Constant.serviceLanguage.get(object.get ( "message" ).getAsString () ));
                    else listener.onVerifyCouponSuccess ( disItems,couponToken,values,object.get ( "message" ).getAsString ());
                }else {
                    String message = object.get ( "message" ).getAsString ();
                    if (Constant.serviceLanguage.containsKey(message))
                    listener.onVerifyError(Constant.serviceLanguage.get(message));
                    else listener.onVerifyError(message);
                    Log.d("notTranslate", "onResponse: "+message);
                }
            }

            @Override
            public void onError(String error,int code) {
                listener.onVerifyError(error);
            }
        }));
    }

}
