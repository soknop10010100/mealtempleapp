package com.station29.mealtemple.api.request;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.User;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginWs {

//    public void loginTask(HashMap<String, Object> params, Activity activity) {
//        ServiceAPI service = RetrofitGenerator.createService();
//        Call<JsonElement> res = service.login(params);
//        JsonObject jsonObject = new JsonObject();
//        res.enqueue(new Callback<JsonElement>() {
//            @Override
//            public void onResponse(@NonNull Call<JsonElement> call, @NonNull Response<JsonElement> response) {
//
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<JsonElement> call, @NonNull Throwable t) {
//
//            }
//        });
//    }

    public interface LoadLoginCallback {
        void onLoadSuccess(String token, int userId, User user);

        void onLoadFailed(String errorMessage, int code);
    }

    // ======================================================== //

    public void loginPhoneEmail(final HashMap<String, String> account, final Context context, final LoadLoginCallback loadLoginCallback) {
        ServiceAPI serviceAPI = RetrofitGenerator.createService();
        final Call<JsonElement> res = serviceAPI.loginPhoneEmail(account);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonObject = response.body();
                if (response.isSuccessful()) {
                    assert jsonObject != null;
                    if (jsonObject.getAsJsonObject().has("data")) {
                        String token= "";
                        int id = 0;
                        JsonObject dataObj = jsonObject.getAsJsonObject().getAsJsonObject("data");
                        if (dataObj.has("access_token")) {
                            token = dataObj.get("access_token").getAsString();
                        }
                        if (dataObj.has("user")) {
                            JsonObject userObj = dataObj.getAsJsonObject("user");
                            if (userObj.has("id")) {
                                id = userObj.get("id").getAsInt();
                            }
                            User user = new Gson().fromJson(userObj, User.class);
                            loadLoginCallback.onLoadSuccess(token,id,user);
                        }
                    }
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")) {
                                String message = jsonObject1.getString("message");
                                loadLoginCallback.onLoadFailed(message, response.code());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                            loadLoginCallback.onLoadFailed(context.getString(R.string.unexpected_problem), response.code());//server error ->>> <!DOCTYPE of type java.lang.String cannot be converted to JSONObject
                        } catch (IOException e2) {
                            loadLoginCallback.onLoadFailed(context.getString(R.string.unexpected_problem), response.code());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadLoginCallback.onLoadFailed(context.getString(R.string.unexpected_problem), 500);
            }
        });
    }

    public void logoutPhoneEmail(final Activity activity, String deviceType, final LoadLoginCallback loadLoginCallback) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth ( activity );
        final Call<JsonElement> res = serviceAPI.logoutProfile(deviceType);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    User user = new User();
                    loadLoginCallback.onLoadSuccess("",0,user);
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("error")) {
                                String message = jsonObject1.getString("error");
                                loadLoginCallback.onLoadFailed(message, response.code());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage()+"");
                            loadLoginCallback.onLoadFailed(activity.getString(R.string.unexpected_problem), response.code());//server error ->>> <!DOCTYPE of type java.lang.String cannot be converted to JSONObject
                        } catch (IOException e2) {
                            loadLoginCallback.onLoadFailed(activity.getString(R.string.unexpected_problem), response.code());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadLoginCallback.onLoadFailed(activity.getString(R.string.unexpected_problem), 500);
            }
        });
    }
}
