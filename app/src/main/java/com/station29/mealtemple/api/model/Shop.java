package com.station29.mealtemple.api.model;

import android.location.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.station29.mealtemple.api.model.response.OpenHour;

import com.station29.mealtemple.ui.BaseActivity;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Shop implements Serializable {

    @SerializedName("id")
    @Expose
    private int shopId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("cover")
    @Expose
    private String image;

    @SerializedName("logo")
    @Expose
    private String shopProfileImage;

    @SerializedName("latitute")
    @Expose
    private double latitute;

    @SerializedName("longitute")
    @Expose
    private double longitute;

    @SerializedName("category_id")
    @Expose
    private int categoryId;

    @SerializedName("tag_id")
    @Expose
    private int tagId;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("open_hours")
    @Expose
    private List<OpenHour> openHours;

    @SerializedName("min_delivery")
    @Expose
    private Integer minDelivery;

    @SerializedName ( "vacation_days" )
    @Expose
    private List<Integer> vacation;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("vat")
    @Expose
    private int vat;

    @SerializedName("taxs")
    @Expose
    private List<Tax> taxs;

    @SerializedName("sucharges")
    @Expose
    private List<SurCharges> surCharges;

    @SerializedName("min_order")
    @Expose
    private double min;

    @SerializedName("delivery_fee")
    @Expose
    private float delivery_fee;

    @SerializedName("distance")
    @Expose
    private float distance;

    private float shopRate;

    private boolean isOpen = true;

    public List<Menu> getProductMenu() {
        return productMenu;
    }

    public void setProductMenu(List<Menu> productMenu) {
        this.productMenu = productMenu;
    }

    private List<Menu> productMenu;

    public Shop(int shopId, String name, float distance, boolean isOpen, float shopRate,String shopProfileImage) {
        this.shopId = shopId;
        this.name = name;
        this.isOpen=isOpen;
        this.distance = distance;
        this.shopRate =shopRate;
        this.shopProfileImage =shopProfileImage;

    }
    public Integer getMinDelivery() {
        return minDelivery;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public float getDistance() {
        return distance;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public float getShopRate() {
        return shopRate;
    }

    public int getShopId() {
        return shopId;
    }

    public double getMin(){
        return min;
    }

    public void setMin(double min){
        this.min = min;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getShopProfileImage() {
        return shopProfileImage;

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setShopProfileImage(String shopProfileImage) {
        this.shopProfileImage = shopProfileImage;
    }

    public double getLatitute() {
        return latitute;
    }

    public void setLatitute(double latitute) {
        this.latitute = latitute;
    }

    public double getLongitute() {
        return longitute;
    }

    public void setLongitute(double longitute) {
        this.longitute = longitute;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getTagId() {
        return tagId;
    }

    public void setTagId(int tagId) {
        this.tagId = tagId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public void setShopRate(float shopRate) {
        this.shopRate = shopRate;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public List<Tax> getTaxs() {
        return taxs;
    }

    public List<SurCharges> getSurCharges() {
        return surCharges;
    }


    public List<OpenHour> getOpenHours() {
        if (openHours == null) openHours = new ArrayList<>();
        return openHours;
    }

    public List<Integer> getVacation() {
        if (vacation==null) vacation =new ArrayList<> (  );
        return vacation;
    }

    public String getCurrency() {
        return currency;
    }

    public int getVat() {
        return vat;
    }

    public float getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(float delivery_fee) {
        this.delivery_fee = delivery_fee;
    }
}
