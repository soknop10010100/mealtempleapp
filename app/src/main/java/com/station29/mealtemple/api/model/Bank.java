package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Bank implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("branch_code")
    @Expose
    private Object branchCode;
    @SerializedName("bic")
    @Expose
    private String bic;
    @SerializedName("owner_address_fk_country")
    @Expose
    private Integer ownerAddressFkCountry;
    @SerializedName("owner_address_city")
    @Expose
    private String ownerAddressCity;
    @SerializedName("owner_address_region")
    @Expose
    private String ownerAddressRegion;
    @SerializedName("owner_address_postalcode")
    @Expose
    private Object ownerAddressPostalcode;
    @SerializedName("owner_address_line1")
    @Expose
    private String ownerAddressLine1;
    @SerializedName("owner_address_line2")
    @Expose
    private Object ownerAddressLine2;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("fk_user_creat")
    @Expose
    private Integer fkUserCreat;
    @SerializedName("fk_user_modif")
    @Expose
    private Integer fkUserModif;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("payin_rate")
    @Expose
    private Integer payinRate;
    @SerializedName("payout_rate")
    @Expose
    private Integer payoutRate;


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Object getBranchCode() {
        return branchCode;
    }

    public String getBic() {
        return bic;
    }

    public Integer getOwnerAddressFkCountry() {
        return ownerAddressFkCountry;
    }

    public String getOwnerAddressCity() {
        return ownerAddressCity;
    }

    public String getOwnerAddressRegion() {
        return ownerAddressRegion;
    }

    public Object getOwnerAddressPostalcode() {
        return ownerAddressPostalcode;
    }

    public String getOwnerAddressLine1() {
        return ownerAddressLine1;
    }

    public Object getOwnerAddressLine2() {
        return ownerAddressLine2;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public Integer getStatus() {
        return status;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public Integer getFkUserCreat() {
        return fkUserCreat;
    }

    public Integer getFkUserModif() {
        return fkUserModif;
    }

    public String getLogo() {
        return logo;
    }

    public Integer getPosition() {
        return position;
    }

    public Integer getPayinRate() {
        return payinRate;
    }

    public Integer getPayoutRate() {
        return payoutRate;
    }
}
