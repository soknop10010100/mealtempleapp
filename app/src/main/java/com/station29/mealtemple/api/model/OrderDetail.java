package com.station29.mealtemple.api.model;

import java.util.HashMap;
import java.util.List;

public class OrderDetail extends OrderRequestParams {

    private List<OrderItem> orderItemList;

    // Use in Local only ------------------
    private String storeName;
    private int orderNumber;
    private float subTotal;
    private double totalPrice=0;
    private String orderTime;
    private String status;
    private float deliveryFee;
    private float discount;
    private float vat;
    private float tipTotal;
    private Shop shop;
    private int tipCount;
    private String discountType;
    private float value;
    private int itemCount=0;
    private HashMap<String,Object> requestParams = new HashMap<> (  );

    public HashMap<String, Object> getRequestParams() {
        return requestParams;
    }

    public void setRequestParams(HashMap<String, Object> requestParams) {
        this.requestParams = requestParams;
    }

    public int getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    private String operator;

    // ------------------------------------

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(List<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public float getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(float deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public float getVat() {
        return vat;
    }

    public void setVat(float vat) {
        this.vat = vat;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public float getTipTotal() {
        return tipTotal;
    }

    public void setTipTotal(float tipTotal) {
        this.tipTotal = tipTotal;
    }

    public int getTipCount() {
        return tipCount;
    }

    public void setTipCount(int tipCount) {
        this.tipCount = tipCount;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }
}
