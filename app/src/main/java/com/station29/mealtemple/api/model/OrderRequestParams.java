package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class OrderRequestParams implements Serializable {

    // Request Params ==============
    @SerializedName("account_id")
    @Expose
    private int storeId;

    @SerializedName("coupon_token")
    @Expose
    private String couponToken;

    @SerializedName("coupon_code")
    @Expose
    private String couponCode;

    @SerializedName("rider_tip")
    @Expose
    private String riderTip;

    @SerializedName("shipping_address")
    @Expose
    private String shippingAddress;

    @SerializedName("phone")
    @Expose
    private String receiverPhone;

    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;

    @SerializedName("payment_type")
    @Expose
    private String paymentType;

    @SerializedName("item_ids")
    @Expose
    private List<Integer> itemIds;

    @SerializedName("items")
    @Expose
    private List<ItemRequestParam> items;

    @SerializedName("delivery_fee")
    @Expose
    private float delivery_fee;

    @SerializedName("name")
    @Expose
    private String receiver_name;

    @SerializedName("latitute")
    @Expose
    private double lat;

    @SerializedName("longitute")
    @Expose
    private double lng;

    @SerializedName("country_code")
    @Expose
    private String country_code;

    @SerializedName("card_token")
    @Expose
    private String cardToken;

    @SerializedName("security_code")
    @Expose
    private String securityCode;

    @SerializedName("wallet_id")
    @Expose
    private Integer walletId;

    @SerializedName("shipping_method")
    @Expose
    private String shippingMethod;

    @SerializedName("pin_code")
    @Expose
    private String pinCode;

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getShippingMethod() {
        return shippingMethod;
    }

    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    public String getReceiver_name() {
        return receiver_name;
    }

    public void setReceiver_name(String receiver_name) {
        this.receiver_name = receiver_name;
    }

    public Integer getWalletId() {
        return walletId;
    }

    public void setWalletId(Integer walletId) {
        this.walletId = walletId;
    }

    public List<ItemRequestParam> getItems() {
        return items;
    }

    public void setItems(List<ItemRequestParam> orderItemRequestParamList) {
        this.items = orderItemRequestParamList;
    }
    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getCouponToken() {
        return couponToken;
    }

    public void setCouponToken(String couponToken) {
        this.couponToken = couponToken;
    }


    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public List<Integer> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<Integer> itemIds) {
        this.itemIds = itemIds;
    }

    public String getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(String riderTip) {
        this.riderTip = riderTip;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public float getDelivery_fee() {
        return delivery_fee;
    }

    public void setDelivery_fee(float delivery_fee) {
        this.delivery_fee = delivery_fee;
    }

    public String getFirst_name() {
        return receiver_name;
    }

    public void setFirst_name(String first_name) {
        this.receiver_name = first_name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getCardToken() {
        return cardToken;
    }

    public void setCardToken(String cardToken) {
        this.cardToken = cardToken;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }
}
