package com.station29.mealtemple.api.request;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.User;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileWs {

    public void viewProfiles(Activity activity,final ProfileViewAndUpdate getUserProfile){
            ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
            final retrofit2.Call<JsonElement> res = serviceAPI.viewProfile();
            res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
                @Override
                public void onSuccess(JsonObject jsonObject) {
                    if (jsonObject.has("success") && jsonObject.get("success").getAsBoolean()) {
                        JsonObject jsonElement = jsonObject.getAsJsonObject("data");
                        User user = new Gson().fromJson(jsonElement, User.class);
                        MockupData.setUser(user);
                        getUserProfile.onLoadProfileSuccess(user);

                    } else {
                        getUserProfile.onError("Invalid Data");
                    }
                }

                @Override
                public void onError(String error,int code) {
                    getUserProfile.onError(error);
                }
            }));

        }

    public void updateProfiles(Activity activity, HashMap<String,Object> mapUpdateProfile,final ProfileViewAndUpdate profileViewAndUpdate ){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth ( activity );
        final retrofit2.Call<JsonElement> res = serviceAPI.updateProfile ( mapUpdateProfile );
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (!resObj.isJsonNull() && resObj.has("status") && resObj.get("status").getAsInt() == 200) {
                    JsonObject dataObj = resObj.getAsJsonObject("data");
                    boolean isSendSms = false;
                    if (dataObj.has("is_send_sms")){
                        isSendSms = dataObj.get("is_send_sms").getAsInt() == 1;
                    }
                    User user = new Gson().fromJson(dataObj, User.class);
                    profileViewAndUpdate.onUpdateProfilesSuccess(isSendSms,user);
                }
            }

            @Override
            public void onError(String error,int code) {
                profileViewAndUpdate.onError(error);
            }
        }));

    }

    public  void changeProfile(Activity activity, HashMap<String,String> image, final ProfileViewAndUpdate getPictureView){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        FormBody.Builder bodyBuilder = new FormBody.Builder();
        Iterator it = image.entrySet().iterator();
        Map.Entry pair = (Map.Entry) it.next();
        bodyBuilder.add((String) pair.getKey(), (String) pair.getValue());
        it.remove();
        RequestBody requestBody = bodyBuilder.build();
        retrofit2.Call<JsonElement> res = serviceAPI.changeProfile(requestBody);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                getPictureView.onUpdateProfilesSuccess(false,new User());
            }

            @Override
            public void onError(String error,int code) {
                getPictureView.onError(error);
            }
        }));

    }

    public interface ProfileViewAndUpdate {
        void onLoadProfileSuccess(User user);
        void onError(String errorMessage);
        void onUpdateProfilesSuccess(boolean isSendSms,User user);
    }
}
