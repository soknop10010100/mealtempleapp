package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Services implements Serializable {

    @SerializedName("service_id")
    @Expose
    private int serviceId;

    @SerializedName("service_type")
    @Expose
    private String serviceType;

    @SerializedName("total_price")
    @Expose
    private String totalPrice;

    @SerializedName("currency_sign")
    @Expose
    private String currencySign;

    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("service_icon")
    @Expose
    private String serviceIcon;

    @SerializedName("flag_down_cost")
    @Expose
    private String flagDownCost;

    public Services(int serviceId, String serviceType, String totalPrice, String currencySign, String currencyCode) {
        this.serviceId = serviceId;
        this.serviceType = serviceType;
        this.totalPrice = totalPrice;
        this.currencySign = currencySign;
        this.currencyCode = currencyCode;
    }

    @SerializedName("driver_rank")
    @Expose
    private int driverRank;
    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("price_discount")
    @Expose
    private double priceDiscount;
    @SerializedName("total_discount")
    @Expose
    private double totalDiscount;
    @SerializedName("price_after_discount")
    @Expose
    private double priceAfterDiscount;
    @SerializedName("price_after_compare")
    @Expose
    private int priceAfterCompare;
    @SerializedName("total_price_after_discount")
    @Expose
    private double totalPriceAfterDiscount;
    @SerializedName("price_discount_formatted")
    @Expose
    private String priceDiscountFormatted;
    @SerializedName("total_price_after_discount_formatted")

    public String getFlagDownCost() {
        return flagDownCost;
    }

    public void setFlagDownCost(String flagDownCost) {
        this.flagDownCost = flagDownCost;
    }
    public int getServiceId() {
        return serviceId;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getServiceIcon() {
        return serviceIcon;
    }
    public int getDriverRank() {
        return driverRank;
    }

    public void setDriverRank(int driverRank) {
        this.driverRank = driverRank;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public double getPriceDiscount() {
        return priceDiscount;
    }

    public void setPriceDiscount(double priceDiscount) {
        this.priceDiscount = priceDiscount;
    }

    public double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public double getPriceAfterDiscount() {
        return priceAfterDiscount;
    }

    public void setPriceAfterDiscount(double priceAfterDiscount) {
        this.priceAfterDiscount = priceAfterDiscount;
    }

    public int getPriceAfterCompare() {
        return priceAfterCompare;
    }

    public void setPriceAfterCompare(int priceAfterCompare) {
        this.priceAfterCompare = priceAfterCompare;
    }

    public double getTotalPriceAfterDiscount() {
        return totalPriceAfterDiscount;
    }

    public void setTotalPriceAfterDiscount(double totalPriceAfterDiscount) {
        this.totalPriceAfterDiscount = totalPriceAfterDiscount;
    }

    public String getPriceDiscountFormatted() {
        return priceDiscountFormatted;
    }

    public void setPriceDiscountFormatted(String priceDiscountFormatted) {
        this.priceDiscountFormatted = priceDiscountFormatted;
    }
}
