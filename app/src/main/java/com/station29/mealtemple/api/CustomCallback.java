package com.station29.mealtemple.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.facebook.login.Login;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.request.LoginWs;
import com.station29.mealtemple.api.request.ProfileWs;
import com.station29.mealtemple.ui.profile.LoginActivity;
import com.station29.mealtemple.util.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomCallback implements Callback<JsonElement> {

    private CustomResponseListener customResponseListener;
    private Context mContext;

    public CustomCallback(Context context, CustomResponseListener listener) {
        customResponseListener = listener;
        // Obtain the FirebaseAnalytics instance.
        mContext = context;
    }

    @Override
    public void onResponse(@NotNull Call<JsonElement> call, Response<JsonElement> response) {
        if (response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            if (jsonElement != null && !jsonElement.isJsonNull() && jsonElement instanceof JsonObject) {
                Log.d("logcallback", "c: "+ jsonElement.getAsJsonObject());
                customResponseListener.onSuccess(jsonElement.getAsJsonObject());
            } else {
                customResponseListener.onError("Wrong Json Type!",response.code());
            }
        } else {
            if (response.errorBody() != null) {
                ResponseBody errorString = response.errorBody();
                String message = "";
                try {
                    String json = errorString.string();
                    Log.d("error", json);
                    JSONObject jsonOb = new JSONObject(json);
                    if (response.code() == 401) {
                        if(jsonOb.has("message")){
                            message = jsonOb.getString("message");
                            if (Constant.serviceLanguage.containsKey(message))
                            customResponseListener.onError(Constant.serviceLanguage.get(message),response.code());
                            else customResponseListener.onError(message,response.code());
                            Log.d("notTranslate", "onResponse: "+message);

                        } else if (jsonOb.has("error") && !jsonOb.getString("error").equals("Unauthenticated.")) {
                            message = jsonOb.getString("error");
                            if (Constant.serviceLanguage.containsKey(message))
                                customResponseListener.onError(Constant.serviceLanguage.get(message),response.code());
                            else customResponseListener.onError(message,response.code());
                            Log.d("notTranslate", "onResponse: "+message);

                        } else {
                            message = mContext.getString(R.string.session_expired);
                            popupSessionExpired(mContext, message);
                            Util.start((Activity) mContext,"stop");
                        }
                    } else if (jsonOb.has("message")) {
                        message = jsonOb.getString("message");
                        if (Constant.serviceLanguage.containsKey(message))
                            customResponseListener.onError(Constant.serviceLanguage.get(message),response.code());
                        else customResponseListener.onError(message,response.code());
                        Log.d("notTranslate", "onResponse: "+message);

                    } else if (jsonOb.has("error")) {
                        message = jsonOb.getString("error");
                        if (Constant.serviceLanguage.containsKey(message))
                            customResponseListener.onError(Constant.serviceLanguage.get(message),response.code());
                        else customResponseListener.onError(message,response.code());
                        Log.d("notTranslate", "onResponse: "+message);

                    }
                    Util.firebaseAnalytics(mContext,Constant.ERROR_MESSAGE,message);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e(e.getClass().getName(), e.getMessage() + "");
                    message = mContext.getString(R.string.unexpected_problem);
                    customResponseListener.onError(message,response.code());
                } catch (IOException e2) {
                    Log.e(e2.getClass().getName(), e2.getMessage() + "");
                    message = mContext.getString(R.string.unexpected_problem);
                    customResponseListener.onError(message,response.code());
                }
            }
        }
    }

    @Override
    public void onFailure(@NotNull Call<JsonElement> call, Throwable t) {
        customResponseListener.onError( t.getMessage(),500);
    }

    private void popupSessionExpired(final Context context, String message) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(message);
            builder.setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Util.clearString(context);
                    Intent intent = new Intent(context, LoginActivity.class);
                    intent.putExtra("isExpired",true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);

                }
            });

            AlertDialog dialog = builder.create();
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.show();

    }
}
