package com.station29.mealtemple.api.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.station29.mealtemple.api.model.OrderItem;
import com.station29.mealtemple.api.model.SurCharges;
import com.station29.mealtemple.api.model.Tax;
import com.station29.mealtemple.api.model.Waypoint;

import java.util.ArrayList;
import java.util.List;

public class OrderResponseDetail {

    @SerializedName("account_id")
    @Expose
    private int accountId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("account_name")
    @Expose
    private String accountName;
    @SerializedName("tel")
    @Expose
    private String tel;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("order_time")
    @Expose
    private String orderTime;
    @SerializedName("delivery_time")
    @Expose
    private String deliveryTime;
    @SerializedName("rider_tip")
    @Expose
    private float riderTip;
    @SerializedName("total_price")
    @Expose
    private double totalPrice;
    @SerializedName("delivery_fee")
    @Expose
    private float deliveryFee;
    @SerializedName("vat")
    @Expose
    private int vat;
    @SerializedName("coupon_id")
    @Expose
    private int couponId;
    @SerializedName("coupon_discount")
    @Expose
    private float couponDiscount;
    @SerializedName("coupon_type")
    @Expose
    private String couponType;
    @SerializedName("sub_total")
    @Expose
    private double subTotal;
    @SerializedName("items")
    @Expose
    private List<OrderItem> items = null;
    @SerializedName("vat_price")
    @Expose
    private float vatPrice;
    @SerializedName("type_order")
    @Expose
    private String typeOrder;
    @SerializedName("client_lat")
    @Expose
    private String clientLat;
    @SerializedName("client_long")
    @Expose
    private String clientLng;
    @SerializedName("total_discount")
    @Expose
    private float totalDiscount;
    @SerializedName("order_id")
    @Expose
    private int order_id;
    @SerializedName("taxs")
    @Expose
    private List<Tax> taxList = new ArrayList<>();
    @SerializedName("surcharge")
    @Expose
    private List<SurCharges> surChargesList= new ArrayList<>();
    @SerializedName("service_type")
    @Expose
    private String serviceType;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoints;
    @SerializedName("pick_up_waypoints")
    @Expose
    private Waypoint pickUpWaypoint;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("driver_global_id")
    @Expose
    private String driverGlobalId;

    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    public String getPaymentType() {
        return paymentType;
    }


    public String getDriverGlobalId() {
        return driverGlobalId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public Waypoint getWaypoints() {
        return waypoints;
    }

    public Waypoint getPickUpWaypoint() {
        return pickUpWaypoint;
    }

    public String getServiceType() {
        return serviceType;
    }

    public String getDriverId() {

        return ((driverId!=null)?driverId:null);
    }

    public String getDuration() {
        return duration;
    }

    public String getDistance() {
        return distance;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public float getTotalDiscount() {
        return totalDiscount;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public float getRiderTip() {
        return riderTip;
    }

    public void setRiderTip(float riderTip) {
        this.riderTip = riderTip;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public float getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(float deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public Object getCouponId() {
        return couponId;
    }

    public void setCouponId(int couponId) {
        this.couponId = couponId;
    }

    public float getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(float couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public Object getCouponType() {
        return couponType;
    }

    public void setCouponType(String couponType) {
        this.couponType = couponType;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public float getVatPrice() {
        return vatPrice;
    }

    public int getVat() {
        return vat;
    }
    public List<Tax> getTaxList() {
        return taxList;
    }
    public List<SurCharges> getSurChargesList() {
        return surChargesList;
    }

    public void setVat(int vat) {
        this.vat = vat;
    }

    public int getOrder_id() {
        return order_id;
    }

    public double getClientLat() {
        return Double.parseDouble(clientLat);
    }

    public double getClientLng() {
        return Double.parseDouble(clientLng);
    }

    public Integer getUserId() {
        return userId;
    }

    public String getTypeOrder() {
        return typeOrder;
    }

}
