package com.station29.mealtemple.api.request;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServerUrlWs {

    public void readServerUrl(String code, final loadServerUrl loadServerUrl){

        ServiceAPI service = RetrofitGenerator.createBaseUrl ();
        Call<JsonElement> res = service.serverUrl (code);
        res.enqueue(new Callback<JsonElement> () {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body ().getAsJsonObject ();
                    if (resObj.has ( "server_url" )) {
                        if (resObj.get("server_url").isJsonNull()) {
                            loadServerUrl.onLoadFailed("It seems your selected country is not supported");
                        } else {
                            String base = resObj.get("server_url").getAsString() + "/";
                            Constant.setBaseUrl(base);
                            loadServerUrl.onLoadSuccess(true);
                        }
                    }
                }else {
                    loadServerUrl.onLoadFailed ( "Country code is not found!" );
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                loadServerUrl.onLoadFailed ("Can't reach server" );
            }

        });

    }
    public interface loadServerUrl {
        void onLoadSuccess(boolean success);

        void onLoadFailed(String errorMessage);
    }

}
