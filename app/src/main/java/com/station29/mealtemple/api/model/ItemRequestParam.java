package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ItemRequestParam implements Serializable {

    @SerializedName("product_id")
    @Expose
    private int productId;

    @SerializedName("qty")
    @Expose
    private int qty;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("arr_option_ids")
    @Expose
    private List<Integer> optionIds = new ArrayList<> (  );

    @SerializedName("arr_add_on_ids")
    @Expose
    private List<Integer> addOnIds= new ArrayList<> (  );


    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<Integer> getOptionIds() {
        return optionIds;
    }

    public void setOptionIds(List<Integer> optionIds) {
        this.optionIds = optionIds;
    }

    public List<Integer> getAddOnIds() {
        return addOnIds;
    }

    public void setAddOnIds(List<Integer> addOnIds) {
        this.addOnIds = addOnIds;
    }
}
