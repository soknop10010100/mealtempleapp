package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentTransaction implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("payment_id")
    @Expose
    private Integer paymentId;
    @SerializedName("wallet_id")
    @Expose
    private Integer walletId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("txn_id")
    @Expose
    private String txnId;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("fee")
    @Expose
    private String fee;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("deleted_by")
    @Expose
    private String deletedBy;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("psp_id")
    @Expose
    private Integer pspId;
    @SerializedName("success_url")
    @Expose
    private String successUrl;
    @SerializedName("cancel_url")
    @Expose
    private String cancelUrl;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("ref_user_name")
    @Expose
    private String refUserName;

    public String getUserName() {
        return userName;
    }

    public String getRefUserName() {
        return refUserName;
    }

    public String getType() {
        return type;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public Integer getWalletId() {
        return walletId;
    }

    public String getDescription() {
        return description;
    }

    public String getTxnId() {
        return txnId;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public String getFee() {
        return fee;
    }

    public String getStatus() {
        return status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public Integer getPspId() {
        return pspId;
    }

    public String getSuccessUrl() {
        return successUrl;
    }

    public String getCancelUrl() {
        return cancelUrl;
    }
}
