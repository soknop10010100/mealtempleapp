package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SurCharges implements Serializable {

    @SerializedName("name")
    @Expose
    private String nameSurCharges;

    @SerializedName("amount")
    @Expose
    private float amount ;

    @SerializedName("money")
    @Expose
    private  float surChargeAmount;

    public String getNameSurCharges() {
        return nameSurCharges;
    }

    public float getAmount() {
        return amount;
    }

    public float getSurChargeAmount() {
        return surChargeAmount;
    }
}
