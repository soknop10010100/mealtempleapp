package com.station29.mealtemple.api.request;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.MyQRCode;

import retrofit2.Call;

public class ScanMeWs {

    public void getMyQRCode(Context context, ScanMeCallback callback) {
        ServiceAPI api = RetrofitGenerator.createServiceWithAuth(context);
        Call<JsonElement> res = api.getMyQRCode();
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (!resObj.isJsonNull() && resObj.has("data")&& resObj.has("status") && resObj.get("status").getAsInt() == 200) {
                    JsonElement data = resObj.get("data");
                    MyQRCode myQRCode = new Gson().fromJson(data, MyQRCode.class);
                    callback.onSuccess(myQRCode, (JsonObject) data);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callback.onError(error);
            }
        }));
    }

    public interface ScanMeCallback {
        void onSuccess(MyQRCode qrCode, JsonObject dataObj);

        void onError(String error);
    }

    public void getUserById(Context context, String id ,ScanMeCallback callback){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(context);
        Call<JsonElement> res = serviceAPI.getUserQRCode(id);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if (!resObj.isJsonNull() && resObj.has("data")&& resObj.has("status") && resObj.get("status").getAsInt() == 200) {
                    JsonElement data = resObj.get("data");
                    MyQRCode myQRCode = new Gson().fromJson(data, MyQRCode.class);
                    callback.onSuccess(myQRCode, (JsonObject) data);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                callback.onError(error);
            }
        }));
    }

}
