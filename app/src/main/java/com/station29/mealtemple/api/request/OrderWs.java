package com.station29.mealtemple.api.request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.Order;
import com.station29.mealtemple.api.model.OrderRequestParams;
import com.station29.mealtemple.api.model.PreView;
import com.station29.mealtemple.api.model.response.OrderResponseDetail;
import com.station29.mealtemple.api.model.response.ResponseOrderDetailTaxi;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderWs {

    public void createOrder(OrderRequestParams orderDetail, Activity activity, final CreateOrderListener listener) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        String map = new Gson().toJson(orderDetail);
        Log.d("maqwep", map);
        Call<JsonElement> res = service.postOrder(orderDetail);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject object) {
                if (object.has("success") && object.get("success").getAsBoolean()) {
                    listener.onCreateSuccess("");
                }
            }

            @Override
            public void onError(String error,int code) {
                listener.onCreateError(error,code);
            }
        }));
    }

    public void preViewOrder(HashMap<String, Object> orderDetail, Activity activity, final CreateOrderListener createOrderListener) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        String map = new Gson().toJson(orderDetail);
        Log.d("map", map);
        Call<JsonElement> res = service.previewReceipt (orderDetail);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject object) {
                if (object.has("success") && object.get("success").getAsBoolean()) {
                    JsonObject data = (JsonObject) object.getAsJsonObject().get("data");
                    PreView preView = new Gson().fromJson(data, PreView.class);
                    createOrderListener.onViewOrder(preView);
                } else {
                    if (Constant.serviceLanguage.containsKey(object.get("message").getAsString()))
                    createOrderListener.onCreateSuccess(Constant.serviceLanguage.get(object.get("message").getAsString()));
                    else createOrderListener.onCreateSuccess(object.get("message").getAsString());
                }
            }

            @Override
            public void onError(String error,int code) {
                createOrderListener.onCreateError(error,code);
            }
        }));
    }

    public interface CreateOrderListener {
        void onCreateSuccess(String success);
        void onCreateError(String error,int code);
        void onViewOrder(PreView preView);
    }

    public void listOrder(int tabIndex, final Activity activity, String listHashMapStatuses, int page, int size, final GetOrderListener orderListener) {
        if (Constant.getBaseUrl() != null) {
            ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
            Call<JsonElement> res = service.listOrder(listHashMapStatuses, page, size);
            if (tabIndex == 0) {
                res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
                    @Override
                    public void onSuccess(JsonObject object) {
                        if (object.has("success") && object.get("success").getAsBoolean() && object.has("data") && object.getAsJsonObject("data").has("data")) {
                            JsonArray dataArray = object.getAsJsonObject("data").getAsJsonArray("data");
                            List<Order> orderList = new ArrayList<>();
                            for (int i = 0; i < dataArray.size(); i++) {
                                JsonElement ele = dataArray.get(i);
                                Order order = new Gson().fromJson(ele, Order.class);
                                orderList.add(order);
                            }
                            orderListener.onLoadOrderSuccess(orderList);
                        }
                    }

                    @Override
                    public void onError(String error,int code) {
                        orderListener.onError(error);
                    }
                }));
            } else {
                res.enqueue(new Callback<JsonElement>() {
                    @Override
                    public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                        if (response.isSuccessful() && response.body()!= null){
                            JsonObject object = response.body().getAsJsonObject();
                            if (object.has("success") && object.get("success").getAsBoolean() && object.has("data") && object.getAsJsonObject("data").has("data")) {
                                JsonArray dataArray = object.getAsJsonObject("data").getAsJsonArray("data");
                                List<Order> orderList = new ArrayList<>();
                                for (int i = 0; i < dataArray.size(); i++) {
                                    JsonElement ele = dataArray.get(i);
                                    Order order = new Gson().fromJson(ele, Order.class);
                                    orderList.add(order);
                                }
                                orderListener.onLoadOrderSuccess(orderList);
                            }
                        } else {
                            if (response.errorBody() != null) {
                                String message = "";
                                ResponseBody errorString = response.errorBody();
                                try {
                                    String json = errorString.string();
                                    Log.d("error", json);
                                    JSONObject jsonOb = new JSONObject(json);

                                   if (jsonOb.has("message")) {
                                        message = jsonOb.getString("message");
                                    } else if (jsonOb.has("error")) {
                                        message = jsonOb.getString("error");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e(e.getClass().getName(), e.getMessage() + "");
                                    message = activity.getString(R.string.unexpected_problem);
                                } catch (IOException e2) {
                                    Log.e(e2.getClass().getName(), e2.getMessage() + "");
                                    message = activity.getString(R.string.unexpected_problem);
                                }
                                if (Constant.serviceLanguage.containsKey(message))
                                orderListener.onError(Constant.serviceLanguage.get(message));
                                else orderListener.onError(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<JsonElement> call, Throwable t) {
                        orderListener.onError(activity.getString(R.string.unexpected_problem));
                    }
                });
            }

        } else {
            orderListener.onError("Can't get Location");
        }
    }

    public interface GetOrderListener {
        void onLoadOrderSuccess(List<Order> orderList);

        void onError(String errorMessage);

        void onLoadDetailSuccess(OrderResponseDetail detail);

        void onLoadDetailTaxiSuccess(ResponseOrderDetailTaxi detailTaxi);
    }

    public void readOrder(final int orderId, Activity activity, final GetOrderListener orderListener) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = service.readOrder(orderId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject objectBody) {
                if (objectBody.has("success") && objectBody.get("success").getAsBoolean()) {
                    JsonElement data = objectBody.getAsJsonObject("data");
                    OrderResponseDetail orderDetail = new Gson().fromJson(data, OrderResponseDetail.class);
                    orderListener.onLoadDetailSuccess(orderDetail);
                }
            }

            @Override
            public void onError(String error,int code) {
                orderListener.onError(error);
            }
        }));
    }

    public void readOrderDetail(final int orderId, Activity activity, final String typeOrder, final GetOrderListener orderListener) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = service.readOrderDetail(orderId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject objectBody) {
                if (objectBody.has("success") && objectBody.get("success").getAsBoolean()) {
                    JsonElement data = objectBody.get("data");

                    if (typeOrder.equalsIgnoreCase("SERVICE") || typeOrder.equalsIgnoreCase("send") && data instanceof JsonObject) {//taxi
                        ResponseOrderDetailTaxi orderDetailTaxi = new Gson().fromJson(data, ResponseOrderDetailTaxi.class);
                        if (orderDetailTaxi != null) {
                            orderListener.onLoadDetailTaxiSuccess(orderDetailTaxi);
                        }
                    } else {//food
                        if (data instanceof JsonObject){
                            OrderResponseDetail orderDetail = new Gson().fromJson(data, OrderResponseDetail.class);
                            orderListener.onLoadDetailSuccess(orderDetail);
                        }
                    }
                }
            }

            @Override
            public void onError(String error,int code) {
                orderListener.onError(error);
            }
        }));
    }

}
