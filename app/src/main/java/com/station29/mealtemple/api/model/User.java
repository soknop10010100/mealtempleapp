package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    @SerializedName("global_id")
    @Expose
    private String globalId;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phoneNumber;
    @SerializedName("firstname")
    @Expose
    private String firstName;
    @SerializedName("lastname")
    @Expose
    private String lastName;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("url_image")
    @Expose
    private String url_Image;
    @SerializedName("shipping_address")
    @Expose
    private String shipping_address;
    @SerializedName("phone_code")
    @Expose
    private int phone_code;
    @SerializedName("wallets")
    @Expose
    private List<Wallets> walletsList;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("pay_tokens")
    @Expose
    private List<PaymentHistory> paymentHistories;

    public void setConfirmPin(boolean confirmPin) {
        isConfirmPin = confirmPin;
    }

    @SerializedName("confirm_pin")
    @Expose
    private boolean isConfirmPin;

    @SerializedName("account_number")
    @Expose
    private String accountId;
    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }


    public String getId() {
        return id;
    }

    public boolean isConfirmPin() {
        return isConfirmPin;
    }

    public List<PaymentHistory> getPaymentHistories() {
        return paymentHistories;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public List<Wallets> getWalletsList() {
        return walletsList;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getUrl_Image() {
        return url_Image;
    }

    public String getShipping_address() {
        return shipping_address;
    }

    public int getPhone_code() {
        return phone_code;
    }

    public void setPhone_code(int phone_code) {
        this.phone_code = phone_code;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    //notice that user is use email or phone to register
    public boolean isPhone(){
        if (phone_code!=0)return true;
        return false;
    }

    public String getGlobalId() {
        return globalId;
    }
}