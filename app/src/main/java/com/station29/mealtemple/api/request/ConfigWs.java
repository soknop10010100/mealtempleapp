package com.station29.mealtemple.api.request;

import android.app.Activity;
import android.os.Build;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.ProductDetail;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.model.response.Config;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ConfigWs {

    public void readConfig(String code , final ReadConfigCallback readConfigCallback) {
        ServiceAPI service = RetrofitGenerator.createService();
        Call<JsonElement> res;
        if(BuildConfig.DEBUG)
            res = service.readConfig("cm");
        else res = service.readConfig(code);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful()) {
                     if (response.body() instanceof JsonObject) {
                         JsonObject object = response.body().getAsJsonObject();
                         if (object.has("success") && object.get("success").getAsBoolean()){
                             if(object.has("data") && !object.get("data").isJsonNull()){
                                 JsonObject dataObj = object.getAsJsonObject("data");
                                 Config config = new Gson().fromJson(dataObj, Config.class);
                                 readConfigCallback.onSuccess(config);
                                 }
                             }
                         }
                     }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if(Constant.serviceLanguage.containsKey(message))
                                readConfigCallback.onError(Constant.serviceLanguage.get(message));
                                else readConfigCallback.onError(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                readConfigCallback.onError("Cannot reach Server.");
            }
        });
    }

    public void getConfigLanguage(String code , Activity activity, ReadConfigCallback readConfigCallback){
        ServiceAPI serviceAPI = RetrofitGenerator.createService();
        Call<JsonElement> res = serviceAPI.readConfigLanguage(code);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")){
                    JsonObject json = resObj.getAsJsonObject("data");
                    for (Map.Entry<String,JsonElement> entry : json.entrySet()) {
                        if (entry.getKey() == null || entry.getKey().equals("")) continue;
                        Constant.serviceLanguage.put(entry.getKey(),entry.getValue().getAsString());
                        Log.d("eraraeradsf",entry.getKey()+"  "+entry.getValue().getAsString());
                    }
                    readConfigCallback.onSuccess(new Config());
                }
            }

            @Override
            public void onError(String error, int resCode) {
                readConfigCallback.onError(error);
            }
        }));
    }

    public interface ReadConfigCallback {
        void onSuccess(Config config);
        void onError(String message);
    }
}
