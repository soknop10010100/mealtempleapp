package com.station29.mealtemple.api.request;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.Services;
import com.station29.mealtemple.api.model.User;
import com.station29.mealtemple.api.model.Waypoint;
import com.station29.mealtemple.ui.BaseActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaxiWs {

    public void getDriverServices(double pickLat,double pickLng,double dropLat,double dropLng , final TaxiWs.getDriverCallback getDriverCallback ){
        ServiceAPI api = RetrofitGenerator.createServiceWithHeader();
        final Call<JsonElement> res;
        if(dropLat == 0.0 && dropLng == 0.0){
            res = api.getDriverServicesWithoutDrop(BaseActivity.countryCode ,pickLat, pickLng);
        }else {
             res = api.getDriverServices(BaseActivity.countryCode ,pickLat, pickLng, dropLat, dropLng);
        }
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        JsonArray dataArray = resObj.getAsJsonObject("data").getAsJsonArray("services");
                        List<Services> servicesList = new ArrayList<>();
                        for (int i = 0; i < dataArray.size(); i++) {
                            JsonElement element = dataArray.get(i);
                            Services services = new Gson().fromJson(element, Services.class);
                            servicesList.add(services);
                        }
                        try {
                            if ( !resObj.getAsJsonObject("data").get("waypoint").isJsonNull() && resObj.getAsJsonObject("data").getAsJsonObject("waypoint") != null && resObj.getAsJsonObject("data").has("waypoint") ){
                                JsonObject jsonObject = resObj.getAsJsonObject("data").getAsJsonObject("waypoint");
                                Waypoint waypoint = new Gson().fromJson(jsonObject, Waypoint.class);
                                getDriverCallback.onLoadSuccess(servicesList, waypoint);
                            }else{
                                getDriverCallback.onLoadSuccessWithoutDrop(servicesList);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            int code = 0;
                            String message= "";
                            JSONObject jsonObject1 = new JSONObject(json);
                            if(jsonObject1.has("status")){
                                code = jsonObject1.getInt("status");
                            }
                            if (jsonObject1.has("message")){
                                message = jsonObject1.getString("message");
                            }
                            if (Constant.serviceLanguage.containsKey(message))
                            getDriverCallback.onLoadFailed(Constant.serviceLanguage.get(message),code);
                            else getDriverCallback.onLoadFailed(message,code);
                            Log.d("notTranslate", "onResponse: "+message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                getDriverCallback.onLoadFailed("Cannot reach Server",1);
            }
        });
    }

    public void applyCoupon(double pickLat,double pickLng,double dropLat,double dropLng,int userId,String coupon , final TaxiWs.getDriverCallback getDriverCallback ){
        ServiceAPI api = RetrofitGenerator.createServiceWithHeader();
        final Call<JsonElement> res;
        if(dropLat == 0.0 && dropLng == 0.0){
            res = api.applyCouponWithoutDrop(pickLat, pickLng,userId,coupon);
        }else {
            res = api.applyCouponDrop(pickLat, pickLng, dropLat, dropLng,userId,coupon);
        }
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        if(resObj.has("data")){
                            JsonObject jsonObject = resObj.getAsJsonObject("data");
                            if(jsonObject.has("services")){
                                JsonArray dataArray = jsonObject.getAsJsonArray("services");
                                List<Services> servicesList = new ArrayList<>();
                                for (int i = 0; i < dataArray.size(); i++) {
                                    JsonElement element = dataArray.get(i);
                                    Services services = new Gson().fromJson(element, Services.class);
                                    servicesList.add(services);
                                }
                                try {
                                    if ( !resObj.getAsJsonObject("data").get("waypoint").isJsonNull() && resObj.getAsJsonObject("data").getAsJsonObject("waypoint") != null && resObj.getAsJsonObject("data").has("waypoint") ){
                                        JsonObject jsonObjec = resObj.getAsJsonObject("data").getAsJsonObject("waypoint");
                                        Waypoint waypoint = new Gson().fromJson(jsonObjec, Waypoint.class);
                                        getDriverCallback.onLoadSuccess(servicesList, waypoint);
                                    }else if(jsonObject.has("coupon_message")){
                                        String message = jsonObject.get("coupon_message").getAsString();
                                        if (Constant.serviceLanguage.containsKey(message))
                                        getDriverCallback.onLoadFailed(Constant.serviceLanguage.get(message),0);
                                        else getDriverCallback.onLoadFailed(message,0);
                                    }else{
                                        getDriverCallback.onLoadSuccessWithoutDrop(servicesList);
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }

                            } else if(jsonObject.has("coupon_message")){
                                String   message = jsonObject.get("coupon_message").getAsString();
                                if (Constant.serviceLanguage.containsKey(message))
                                getDriverCallback.onLoadFailed(Constant.serviceLanguage.get(message),0);
                                else getDriverCallback.onLoadFailed(message,0);
                            }
//
                        }

                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            int code = 0;
                            String message= "";
                            JSONObject jsonObject1 = new JSONObject(json);
                            if(jsonObject1.has("status")){
                                code = jsonObject1.getInt("status");
                            }
                            if (jsonObject1.has("message")){
                                message = jsonObject1.getString("message");
                            }
                            if (Constant.serviceLanguage.containsKey(message))
                            getDriverCallback.onLoadFailed(Constant.serviceLanguage.get(message),code);
                            else getDriverCallback.onLoadFailed(message,code);
                            Log.d("notTranslate", "onResponse: "+message);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                getDriverCallback.onLoadFailed("Cannot reach Server",1);
            }
        });
    }

    public void orderDriverService(Activity activity , final HashMap<String, Object> order, final TaxiWs.getDriverCallback driverCallback) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        String map = new Gson().toJson(order);
        Log.d("map", map);
        final Call<JsonElement> res = serviceAPI.orderDriverService(order);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")) {
                    JsonObject jsonObject = resObj.getAsJsonObject("data");
                    int id = jsonObject.get("order_id").getAsInt();
                    String orderId = String.valueOf(id);
                    driverCallback.onOrder(orderId);
                }else{
                    driverCallback.onOrder("Rebooking success");
                }
            }

            @Override
            public void onError(String error,int code) {
                driverCallback.onLoadFailed(error,code);
            }
        }));
    }

    public void cancelTaxi(Activity activity ,int orderId, final TaxiWs.getDriverCallback driverCallback) {
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        final Call<JsonElement> res = serviceAPI.cancelTaxi(orderId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("success") &&resObj.get("success").getAsBoolean()){
                    if(resObj.has("data")) {
                        JsonObject jsonObject = resObj.getAsJsonObject("data");
                        String id = jsonObject.get("message").getAsString();
                        if (Constant.serviceLanguage.containsKey(id))
                        driverCallback.onOrder(Constant.serviceLanguage.get(id));
                        else driverCallback.onOrder(id);
                    }
                }
            }

            @Override
            public void onError(String error,int code) {
                driverCallback.onLoadFailed(error,code);
            }
        }));
    }

    public interface getDriverCallback {
        void onLoadSuccess (List<Services> servicesList, Waypoint waypoint);
        void onLoadSuccessWithoutDrop (List<Services> servicesList);
        void onLoadFailed(String errorMessage,int code);
        void onOrder(String orderId);
    }
}
