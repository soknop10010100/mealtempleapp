package com.station29.mealtemple.api.request;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.Menu;
import com.station29.mealtemple.api.model.Shop;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoreWs {

    public void loadStores(String countryCode, int categoryId, int tagId ,int page , int size,double lat, double lng, final LoadStoreCallback loadStoreCallback) {
        ServiceAPI service = RetrofitGenerator.createService();
        Call<JsonElement> res = service.listStore(countryCode, categoryId, tagId ,page , size,lat,lng);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    Log.d("shopList", resObj+"");
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        JsonArray dataArray = resObj.getAsJsonObject("data").getAsJsonArray("data");
                        List<Shop> shopList = new ArrayList<>();
                        for (int i = 0 ; i< dataArray.size() ; i++) {
                            JsonElement element = dataArray.get(i);
                            Shop shop = new Gson().fromJson(element, Shop.class);
                            shopList.add(shop);
                        }
                        loadStoreCallback.onLoadSuccess(shopList);
                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadStoreCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadStoreCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }
            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadStoreCallback.onLoadFailed("Can not reach Server!");
            }
        });
    }

    public interface LoadStoreCallback {
        void onLoadSuccess (List<Shop> shopList);
        void onLoadFailed(String errorMessage);
        void onLoadStoreDetail(Shop shop);
    }

    public void loadStoresDetail(String countryCode,int accId,int cateId,int tagId,double lat,double lng,final LoadStoreCallback loadStoreCallback){
        ServiceAPI serviceAPI = RetrofitGenerator.createService ();
        Call<JsonElement> res = serviceAPI.storeDetail ( countryCode,accId,cateId,tagId ,lat,lng );
        res.enqueue ( new Callback<JsonElement> () {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful () && response.body () instanceof JsonObject){
                   JsonObject jsonObject= response.body ().getAsJsonObject ();
                   if (jsonObject.has ( "success" )&& jsonObject.get ( "success" ).getAsBoolean ()){
                       try {
                           JsonObject ele = jsonObject.getAsJsonObject ( "data" );
                           Shop shop = new Gson().fromJson(ele, Shop.class);
                           JsonArray menArray = ele.getAsJsonArray ( "product_group" );
                           List<Menu> menuList = new ArrayList<> (  );
                           for (int i = 0; i <menArray.size (); i++) {
                               JsonObject jsonObject1 = menArray.get ( i ).getAsJsonObject ();
                               int id = jsonObject1.get ( "id" ).getAsInt ();
                               String name = jsonObject1.get ( "name" ).getAsString ();
                               menuList.add ( new Menu ( id,name ) );
                           }
                           shop.setProductMenu ( menuList );
                           loadStoreCallback.onLoadStoreDetail ( shop );
                       }catch (Exception e){
                           //case when user switch the url and shop is still resume \
                           // server respone with  "success": true,
                           //    "data": [],
                           //    "raspberry": false
                           //} data should be in object .
                           loadStoreCallback.onLoadFailed ( "Can not load base on your pin location." );
                       }

                   }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadStoreCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadStoreCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }

        } );
    }

    public void searchStore(String countryCode,String s ,double lat, double lng,int size,int cateId, final LoadStoreCallback loadStoreCallback) {
        ServiceAPI service = RetrofitGenerator.createService();
        Call<JsonElement> res = service.searchShop(countryCode,s,lat,lng,size,cateId);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() ) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        JsonArray dataArray = resObj.getAsJsonObject("data").getAsJsonArray("data");
                        List<Shop> shopList = new ArrayList<>();
                        for (int i = 0 ; i< dataArray.size() ; i++) {
                            JsonElement element = dataArray.get(i);
                            Shop shop = new Gson().fromJson(element, Shop.class);
                            shopList.add(shop);
                        }
                        loadStoreCallback.onLoadSuccess(shopList);
                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadStoreCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadStoreCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }
            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadStoreCallback.onLoadFailed("Can not reach Server!");
            }
        });
    }
}
