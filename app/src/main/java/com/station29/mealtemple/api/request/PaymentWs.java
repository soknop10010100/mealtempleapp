package com.station29.mealtemple.api.request;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.Order;
import com.station29.mealtemple.api.model.PaymentTransaction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;

public class PaymentWs {

    public interface onTopUpWalletsCallBack {
        void onSuccess(String message , int paymentId);
        void onError(String message);
    }

    public interface onGetPaymentTransaction {
        void onSuccess(List<PaymentTransaction> paymentTransaction);
        void onError(String message);
    }

    public void topUpWallets(HashMap<String, Object> body, Activity activity, final PaymentWs.onTopUpWalletsCallBack onTopUpWalletsCallBack) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        String map = new Gson().toJson(body);
        Log.d("map", map);
        Call<JsonElement> res = service.topUpWallets(body);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject object) {
                if(  object.has("data") && !object.getAsJsonObject("data").isJsonNull()){
                    JsonObject jsonObject = object.getAsJsonObject("data").getAsJsonObject();
                    if( jsonObject.has("txn_id") && jsonObject.get("txn_id").isJsonNull()){
                        onTopUpWalletsCallBack.onError("Something went wrong.");
                    } else {
                        if (object.has("message") && object.get("message").getAsBoolean()) {
                            JsonObject json = object.getAsJsonObject("data").getAsJsonObject();
                            int paymentId = json.get("payment_id").getAsInt();
                            if (Constant.serviceLanguage.containsKey(object.get("message").getAsString()))
                            onTopUpWalletsCallBack.onSuccess(Constant.serviceLanguage.get(object.get("message").getAsString()),paymentId);
                            else onTopUpWalletsCallBack.onSuccess(object.get("message").getAsString(),paymentId);
                        } else {
                            JsonObject json = object.getAsJsonObject("data").getAsJsonObject();
                            int paymentId = json.get("payment_id").getAsInt();
                            if (Constant.serviceLanguage.containsKey(object.get("message").getAsString()))
                            onTopUpWalletsCallBack.onSuccess(Constant.serviceLanguage.get(object.get("message").getAsString()),paymentId);
                            else onTopUpWalletsCallBack.onSuccess(object.get("message").getAsString(),paymentId);
                        }
                    }
                }

            }

            @Override
            public void onError(String error,int code) {
                onTopUpWalletsCallBack.onError(error);
            }
        }));
    }

    public void deletePaymentHistory(Activity activity, int paymentId,PaymentWs.onTopUpWalletsCallBack onTopUpWalletsCallBack){
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = service.deletePaymentHistory(paymentId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("message") && !resObj.get("message").isJsonNull()){
                    String message = resObj.get("message").getAsString();
                    if (Constant.serviceLanguage.containsKey(message))
                    onTopUpWalletsCallBack.onSuccess(Constant.serviceLanguage.get(message),0);
                    else onTopUpWalletsCallBack.onSuccess(message,0);
                }
            }

            @Override
            public void onError(String error,int code) {
                onTopUpWalletsCallBack.onError(error);
            }
        }));
    }

    public void getUserPaymentTransaction(Activity activity , int page, PaymentWs.onGetPaymentTransaction onTopUpWalletsCallBack){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.getUserPaymentTransaction(page);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data")){
                    JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                    JsonArray jsonArray = jsonObject.get("data").getAsJsonArray();
                    List<PaymentTransaction> paymentTransactionArrayList = new ArrayList<>();
                    for (int i = 0; i < jsonArray.size(); i++) {
                        JsonElement ele = jsonArray.get(i);
                        PaymentTransaction paymentTransaction = new Gson().fromJson(ele, PaymentTransaction.class);
                        paymentTransactionArrayList.add(paymentTransaction);
                    }
                    onTopUpWalletsCallBack.onSuccess(paymentTransactionArrayList);
                }
            }

            @Override
            public void onError(String error,int code) {
                onTopUpWalletsCallBack.onError(error);
            }
        }));
    }

    public void setUserPin(Activity activity,HashMap<String , Object> body , onTopUpWalletsCallBack onTopUpWalletsCallBack){
        String map = new Gson().toJson(body);
        Log.d("map", map);
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.setPin(body);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj != null && resObj.has("message")){
                    if (Constant.serviceLanguage.containsKey(resObj.get("message").getAsString()))
                    onTopUpWalletsCallBack.onSuccess(Constant.serviceLanguage.get(resObj.get("message").getAsString()),0);
                    else onTopUpWalletsCallBack.onSuccess(resObj.get("message").getAsString(),0);
                }
            }

            @Override
            public void onError(String error, int resCode) {
                onTopUpWalletsCallBack.onError(error);
            }
        }));
    }

    public void getPaymentStatus(Activity activity, int paymentId, onTopUpWalletsCallBack onTopUpWalletsCallBack){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(activity);
        Call<JsonElement> res = serviceAPI.getPaymentStatus(paymentId);
        res.enqueue(new CustomCallback(activity, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                if(resObj.has("data") && !resObj.get("data").isJsonNull() ){
                    JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                    if(jsonObject.has("status")){
                        String status = jsonObject.get("status").getAsString();
                        int id = jsonObject.get("id").getAsInt();
                        onTopUpWalletsCallBack.onSuccess(status,id);
                    }
                } else {
                    onTopUpWalletsCallBack.onError(activity.getString(R.string.something_went_wrong));
                }

            }

            @Override
            public void onError(String error, int resCode) {
                onTopUpWalletsCallBack.onError(error);
            }
        }));

    }
}
