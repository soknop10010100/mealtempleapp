package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Waypoint implements Serializable {

    @SerializedName("distance")
    @Expose
    private float distance;

    @SerializedName("arr_json_lat_lng")
    @Expose
    private List<WayPointLatLng> ListLatLng;

    @SerializedName("duration")
    @Expose
    private double duration;

    public float getDistance() {
        return distance;
    }

    public List<WayPointLatLng> getListLatLng() {
        return ListLatLng;
    }

    public double getDuration() {
        return duration;
    }
}
