package com.station29.mealtemple.api.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.station29.mealtemple.api.model.SendPackage;
import com.station29.mealtemple.api.model.Waypoint;

import java.io.Serializable;

public class ResponseOrderDetailTaxi implements Serializable {

    @SerializedName("order_id")
    @Expose
    private int orderId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("driver_name")
    @Expose
    private String driverName;
    @SerializedName("color")
    @Expose
    private String vehicleColor;
    @SerializedName("unique_driver")
    @Expose
    private String vehicleId;
    @SerializedName("plate_number")
    @Expose
    private String plateNumber ;
    @SerializedName("driver_phone")
    @Expose
    private String driverPhone;
    @SerializedName("driver_img")
    @Expose
    private String driverImg;
    @SerializedName("type_order")
    @Expose
    private String typeOrder;
    @SerializedName("delivery_status")
    @Expose
    private String deliveryStatus;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("dropoff_lat")
    @Expose
    private String dropoffLat;
    @SerializedName("date_pickup")
    @Expose
    private String datePickup;
    @SerializedName("date_dropoff")
    @Expose
    private String dateDropoff;
    @SerializedName("dropoff_long")
    @Expose
    private String dropoffLong;
    @SerializedName("customer_phone")
    @Expose
    private String customerPhone;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("pick_up_lat")
    @Expose
    private String pickUpLat;
    @SerializedName("pick_up_long")
    @Expose
    private String pickUpLong;
    @SerializedName("service_driver")
    @Expose
    private String serviceDriver;
    @SerializedName("cost_per_distance")
    @Expose
    private String costPerDistance;
    @SerializedName("total_distance")
    @Expose
    private String totalDistance;
    @SerializedName("total_price")
    @Expose
    private String totalPrice;
    @SerializedName("start_address")
    @Expose
    private String startAddress;
    @SerializedName("end_address")
    @Expose
    private String endAddress;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;
    @SerializedName("cost_per_distance_formatted")
    @Expose
    private String costPerDistanceFormatted;
    @SerializedName("total_price_formatted")
    @Expose
    private String totalPriceFormatted;
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoints;
    @SerializedName("pick_up_waypoints")
    @Expose
    private Waypoint pickUpWaypoint;
    @SerializedName("driver_global_id")
    @Expose
    private String driverGlobalId;
    @SerializedName("send_package")
    @Expose
    private SendPackage sendPackage;
    @SerializedName("coupon_discount")
    @Expose
    private String couponDiscount;
    @SerializedName("coupon_discount_price")
    @Expose
    private String couponDiscountPrice;
    @SerializedName("coupon_discount_type")
    @Expose
    private String couponDiscountType;
    @SerializedName("price_after_coupon_discount")
    @Expose
    private String  priceAfterCouponDiscount;

    public String getCoupon_discount_formatted() {
        return coupon_discount_formatted;
    }

    @SerializedName("coupon_discount_formatted")
    @Expose
    private String  coupon_discount_formatted;

    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    public String getPaymentType() {
        return paymentType;
    }

    public String getCouponDiscount() {
        return couponDiscount;
    }

    public void setCouponDiscount(String couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public String getCouponDiscountPrice() {
        return couponDiscountPrice;
    }

    public void setCouponDiscountPrice(String couponDiscountPrice) {
        this.couponDiscountPrice = couponDiscountPrice;
    }

    public String getCouponDiscountType() {
        return couponDiscountType;
    }

    public void setCouponDiscountType(String couponDiscountType) {
        this.couponDiscountType = couponDiscountType;
    }

    public String getPriceAfterCouponDiscount() {
        return priceAfterCouponDiscount;
    }

    public void setPriceAfterCouponDiscount(String priceAfterCouponDiscount) {
        this.priceAfterCouponDiscount = priceAfterCouponDiscount;
    }



    public SendPackage getSendPackage() {
        return sendPackage;
    }

    public void setSendPackage(SendPackage sendPackage) {
        this.sendPackage = sendPackage;
    }

    public String getDriverGlobalId() {
        return driverGlobalId;
    }

    public Waypoint getPickUpWaypoint() {
        return pickUpWaypoint;
    }

    public void setPickUpWaypoint(Waypoint pickUpWaypoint) {
        this.pickUpWaypoint = pickUpWaypoint;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getDriverId() {
        return ((driverId!=null)?driverId:null);
    }

    public void setDriverId(String  driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getDriverImg() {
        return driverImg;
    }

    public void setDriverImg(String driverImg) {
        this.driverImg = driverImg;
    }

    public String getTypeOrder() {
        return typeOrder;
    }

    public void setTypeOrder(String typeOrder) {
        this.typeOrder = typeOrder;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getDropoffLat() {
        return dropoffLat;
    }

    public void setDropoffLat(String dropoffLat) {
        this.dropoffLat = dropoffLat;
    }

    public String getDatePickup() {
        return ((datePickup!=null)?datePickup:null);
    }

    public void setDatePickup(String datePickup) {
        this.datePickup = datePickup;
    }

    public String getDateDropoff() {
        return ((dateDropoff!=null)?dateDropoff:null);
    }

    public void setDateDropoff(String dateDropoff) {
        this.dateDropoff = dateDropoff;
    }

    public String getDropoffLong() {
        return dropoffLong;
    }

    public void setDropoffLong(String dropoffLong) {
        this.dropoffLong = dropoffLong;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPickUpLat() {
        return pickUpLat;
    }

    public void setPickUpLat(String pickUpLat) {
        this.pickUpLat = pickUpLat;
    }

    public String getPickUpLong() {
        return pickUpLong;
    }

    public void setPickUpLong(String pickUpLong) {
        this.pickUpLong = pickUpLong;
    }


    public String getServiceDriver() {
        return serviceDriver;
    }

    public void setServiceDriver(String serviceDriver) {
        this.serviceDriver = serviceDriver;
    }

    public String getCostPerDistance() {
        return costPerDistance;
    }

    public void setCostPerDistance(String costPerDistance) {
        this.costPerDistance = costPerDistance;
    }

    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCostPerDistanceFormatted() {
        return costPerDistanceFormatted;
    }

    public void setCostPerDistanceFormatted(String costPerDistanceFormatted) {
        this.costPerDistanceFormatted = costPerDistanceFormatted;
    }

    public String getTotalPriceFormatted() {
        return totalPriceFormatted;
    }

    public void setTotalPriceFormatted(String totalPriceFormatted) {
        this.totalPriceFormatted = totalPriceFormatted;
    }

    public Waypoint getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(Waypoint waypoints) {
        this.waypoints = waypoints;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public String getPlateNumber() {
        return plateNumber;
    }


}
