package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.station29.mealtemple.api.model.Waypoint;

import java.io.Serializable;

public class SendPackageDetail implements Serializable {
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoints;
    @SerializedName("total_distance")
    @Expose
    private double totalDistance;
    @SerializedName("unit")
    @Expose
    private String unit;
    @SerializedName("cost_per_km")
    @Expose
    private String costPerKm;
    @SerializedName("total_price")
    @Expose
    private double totalPrice;
    @SerializedName("currency_sign")
    @Expose
    private String currencySign;
    @SerializedName("currency_code")
    @Expose
    private String currencyCode;

    @SerializedName("total_min")
    @Expose
    private double totalMin;

    public double getTotalMin() {
        return totalMin;
    }

    public void setTotalMin(double totalMin) {
        this.totalMin = totalMin;
    }

    public Waypoint getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(Waypoint waypoints) {
        this.waypoints = waypoints;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(String costPerKm) {
        this.costPerKm = costPerKm;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getCurrencySign() {
        return currencySign;
    }

    public void setCurrencySign(String currencySign) {
        this.currencySign = currencySign;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
