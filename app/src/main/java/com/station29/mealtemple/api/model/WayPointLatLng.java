package com.station29.mealtemple.api.model;

import java.io.Serializable;

public class WayPointLatLng implements Serializable {

    private double latitute;
    private double longitute;

    public WayPointLatLng(double latitute, double longitute) {
        this.latitute = latitute;
        this.longitute = longitute;
    }

    public double getLatitute() {
        return latitute;
    }

    public double getLongitute() {
        return longitute;
    }
}
