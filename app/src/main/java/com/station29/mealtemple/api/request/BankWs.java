package com.station29.mealtemple.api.request;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.Bank;
import com.station29.mealtemple.api.model.Category;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankWs {

    public void getListBank(Context context, final LoadListBank loadCategoryCallback) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(context);
        Call<JsonElement> res = service.getBank();
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject().getAsJsonObject("data");
                    if (resObj.has("data")) {
                        JsonArray dataArray = resObj.getAsJsonArray("data");
                        List<Bank> categoryList = new ArrayList<>();
                        for (int i = 0; i < dataArray.size(); i++) {
                            JsonElement element = dataArray.get(i);
                            Bank category = new Gson().fromJson(element, Bank.class);
                            categoryList.add(category);
                        }
                        loadCategoryCallback.onLoadSuccess(categoryList);
                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if(Constant.serviceLanguage.containsKey(message))
                                    loadCategoryCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadCategoryCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadCategoryCallback.onLoadFailed("Cannot reach Server");
            }
        });
    }

    public void addBankAccount(Context context, HashMap<String,String> param, final LoadListBank loadCategoryCallback) {
        ServiceAPI service = RetrofitGenerator.createServiceWithAuth(context);
        Call<JsonElement> res = service.addBankAccount(param);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {

                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                loadCategoryCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadCategoryCallback.onLoadFailed("Cannot reach Server");
            }
        });
    }

    public interface LoadListBank {
        void onLoadSuccess(List<Bank> categoryList);
        void onLoadFailed(String errorMessage);
    }
}
