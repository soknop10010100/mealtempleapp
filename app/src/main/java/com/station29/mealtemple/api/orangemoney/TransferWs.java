package com.station29.mealtemple.api.orangemoney;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;

import java.util.HashMap;

import retrofit2.Call;

public class TransferWs {

    public void doTransfer(Context context, HashMap<String, String> params, TransferCallback transferCallback){
        ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(context);
        Call<JsonElement> res = serviceAPI.transfer(params);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                Log.d("test", resObj.toString());
                if (resObj.has("status")){
                    int status = resObj.get("status").getAsInt();
                    String message = resObj.get("data").getAsJsonObject().get("message").getAsString();
                    transferCallback.onTransferSuccess(status, Constant.serviceLanguage.get(message));
                }
            }

            @Override
            public void onError(String error, int resCode) {
                Log.d("test", error.toString());
                transferCallback.onTransferFailed(error);
            }
        }));
    }

    public interface TransferCallback {
        void onTransferSuccess(int status, String message);
        void onTransferFailed(String message);
    }

}
