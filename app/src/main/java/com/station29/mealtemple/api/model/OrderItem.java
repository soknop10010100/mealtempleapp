package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class
OrderItem  extends ItemRequestParam implements Serializable {

    private long id;

    public OrderItem() {
        // Coz we want diff id to find item to edit/add qty
        this.id = new Date().getTime();
    }

    public long getId() {
        return id;
    }

    @SerializedName("image")
    @Expose
    private String productImage;

    @SerializedName("url_image")
    @Expose
    private String orderImageUrl;

    @SerializedName("product_name")
    @Expose
    private String productName;


    @SerializedName ( "default_price" )
    @Expose
    private float defaultPrice;

    @SerializedName ( "after_price" )
    @Expose
    private float afterPrice;
// Use in Local only ------------------

    private String name;

    private double price;

    @SerializedName("options")
    @Expose
    private List<AddOn> option;

    @SerializedName("add_ons")
    @Expose
    private List<AddOn> addOns;
    // ------------------------------------

    public List<AddOn> getOption() {
        if (option == null) option = new ArrayList<>();
        return option;
    }

    public void setOption(List<AddOn> option) {
        this.option = option;
    }

    public List<AddOn> getAddOns() {
        if (addOns == null) addOns = new ArrayList<>();
        return addOns;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public void setAddOns(List<AddOn> addOns) {
        this.addOns = addOns;
    }

    public String getName() {
        return name;
    }
    public String getProductImage() {
        return productImage;
    }

    public float getDefaultPrice() {
        return defaultPrice;
    }

    public float getAfterPrice() {
        return afterPrice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getOrderImageUrl() {
        return orderImageUrl;
    }

    public void setOrderImageUrl(String orderImageUrl) {
        this.orderImageUrl = orderImageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

}
