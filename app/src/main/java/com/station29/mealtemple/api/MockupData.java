package com.station29.mealtemple.api;

import android.content.Context;

import com.station29.mealtemple.R;
import com.station29.mealtemple.api.model.Bank;
import com.station29.mealtemple.api.model.OnBoardScreen;
import com.station29.mealtemple.api.model.OrderDetail;
import com.station29.mealtemple.api.model.Shop;
import com.station29.mealtemple.api.model.StoreGroup;
import com.station29.mealtemple.api.model.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MockupData {

    public static String LOGIN_PHONE ="";
    public static String USER_NAME = "";
    public static String HOUSE_NUMBER ="";
    public static String COUNTRY_CODE ="";
    public static String DELIVERY_ADDRESS = "";
    public static String EMAIL="";
    public static String DELIVERY_TIME = "";
    public static int DELIVERY_TIME_SHOPID = 1;
    public static String DELIVERY_DATE = "";
    private static ArrayList<OrderDetail> orderDetailArrayList;
    private static OrderDetail currentOrderDetail;
    private static List<Shop> favouriteshops = new ArrayList<>();
    private static HashMap<String ,List<Shop>> shopHistory = new HashMap<>();
    public static boolean FINDLOCATION = false;
    public static HashMap<String, Integer> highLightItemIds = new HashMap<>();
    public static boolean checkpickup = false;
    public static String RECEIVER_NAME = "";
    public static String RECEIVER_PHONE = "";
    public static String RECEIVER_ADDRESS = "";
    public static String RECEIVER_CATEGORY = "";
    public static String ITEM_WEIGHT = "";
    public static String RECEIVER_DESCRIPTION = "";
    public static String RECEIVER_ADDRESS_DROP = "";
    public static String TOTAL_PRICE = "";
    public static String PRICE_PER_UIT = "";
    public static String DISTANCE = "";
    public static double LATE_DROP ;
    public static double LNG_DROP ;
    public  static String SENDER_NAME="";
    public  static String SENDER_PHONE="";

    public  static String DETAIL_PICK ="";
    public  static String DETAIL_DROP ="";
    public  static Integer state = 0;
    public static Integer getState() {
        return state;
    }

    private static User user;

    public static void setState(Integer state) {
        MockupData.state = state;
    }

    public static void setLateDrop(double lateDrop) {
        LATE_DROP = lateDrop;
    }

    public static String getDetailPick() {
        return DETAIL_PICK;
    }

    public static void setDetailPick(String detailPick) {
        DETAIL_PICK = detailPick;
    }

    public static String getDetailDrop() {
        return DETAIL_DROP;
    }

    public static void setDetailDrop(String detailDrop) {
        DETAIL_DROP = detailDrop;
    }

    public static String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public static void setDESCRIPTION(String DESCRIPTION) {
        MockupData.DESCRIPTION = DESCRIPTION;
    }

    public  static String DESCRIPTION ="";
    //public  static String SENDER_PHONE="";
    public static String getSenderName() {
        return SENDER_NAME;
    }

    public static void setSenderName(String senderName) {
        SENDER_NAME = senderName;
    }

    public static String getSenderPhone() {
        return SENDER_PHONE;
    }

    public static void setSenderPhone(String senderPhone) {
        SENDER_PHONE = senderPhone;
    }

    public static String getSenderAddress() {
        return SENDER_ADDRESS;
    }

    public static void setSenderAddress(String senderAddress) {
        SENDER_ADDRESS = senderAddress;
    }

    public  static String SENDER_ADDRESS="";
    public static double getLateDrop() {
        return LATE_DROP;
    }

    public static double getLngDrop() {
        return LNG_DROP;
    }

    public static void setLngDrop(double lngDrop) {
        LNG_DROP = lngDrop;
    }


    public static String getTotalPrice() {
        return TOTAL_PRICE;
    }

    public static void setTotalPrice(String totalPrice) {
        TOTAL_PRICE = totalPrice;
    }

    public static String getPricePerUit() {
        return PRICE_PER_UIT;
    }

    public static void setPricePerUit(String pricePerUit) {
        PRICE_PER_UIT = pricePerUit;
    }

    public static String getDISTANCE() {
        return DISTANCE;
    }

    public static void setDISTANCE(String DISTANCE) {
        MockupData.DISTANCE = DISTANCE;
    }

    public static String getReceiverAddressDrop() {
        return RECEIVER_ADDRESS_DROP;
    }

    public static void setReceiverAddressDrop(String receiverAddressDrop) {
        RECEIVER_ADDRESS_DROP = receiverAddressDrop;
    }
    private static HashMap<String, Object> acc = new HashMap<>();
    public static ArrayList<Bank> getBankArrayList(){
        ArrayList<Bank> bankArrayList = new ArrayList<>();
        return bankArrayList;
    }
    public static ArrayList<StoreGroup> getShopGroupArrayList() {
        ArrayList<StoreGroup> shopGroupArrayList = new ArrayList<>();
        shopGroupArrayList.add(new StoreGroup(1, "Nearest Shop"));
       // shopGroupArrayList.add(new StoreGroup(2, "Top Rated"));
        return shopGroupArrayList;
    }

    public static void setAccountSignUp( HashMap<String,Object> accountState){
        MockupData.acc = accountState;
    }
    public static HashMap<String,Object> getAccountSignUp(){
        return MockupData.acc;
    }
    public static List<String> getOrderTab(Context context){
        return Arrays.asList(context.getString(R.string.requested), context.getString(R.string.completed),context.getString(R.string.tab_cancel));
    }

    public static ArrayList<OrderDetail> getOrderDetailArrayList() {
        if (orderDetailArrayList == null) {
            orderDetailArrayList = new ArrayList<>();
        }
        return orderDetailArrayList;
    }

    public static List<Shop> getFavouriteshops() {
        return favouriteshops;
    }

    public static OrderDetail getCurrentOrderDetail() {
        return currentOrderDetail;
    }

    public static void setCurrentOrderDetail(OrderDetail currentOrderDetail) {
        MockupData.currentOrderDetail = currentOrderDetail;
    }

    public static HashMap<String, List<Shop>> getShopHistory() {
        return shopHistory;
    }

    public static HashMap<String, Integer> getHighLightItemIds() {
        return highLightItemIds;
    }

    public static void setHighLightItemIds(HashMap<String, Integer> highLightItemIds) {
        MockupData.highLightItemIds = highLightItemIds;
    }
    public static String getHouseNumber () {
        return HOUSE_NUMBER;
    }

    public static void setHouseNumber (String houseNumber){
        HOUSE_NUMBER = houseNumber;

    }
    public static String getReceiverName() {
        return RECEIVER_NAME;
    }

    public static void setReceiverName(String receiverName) {
        RECEIVER_NAME = receiverName;
    }

    public static String getReceiverPhone() {
        return RECEIVER_PHONE;
    }

    public static void setReceiverPhone(String receiverPhone) {
        RECEIVER_PHONE = receiverPhone;
    }

    public static String getReceiverAddress() {
        return RECEIVER_ADDRESS;
    }

    public static void setReceiverAddress(String receiverAddress) {
        RECEIVER_ADDRESS = receiverAddress;
    }

    public static String getReceiverCategory() {
        return RECEIVER_CATEGORY;
    }

    public static void setReceiverCategory(String receiverCategory) {
        RECEIVER_CATEGORY = receiverCategory;
    }

    public static String getItemWeight() {
        return ITEM_WEIGHT;
    }

    public static void setItemWeight(String itemWeight) {
        ITEM_WEIGHT = itemWeight;
    }

    public static String getReceiverDescription() {
        return RECEIVER_DESCRIPTION;
    }

    public static void setReceiverDescription(String receiverDescription) {
        RECEIVER_DESCRIPTION = receiverDescription;
    }

    public static List<String> getCategory(){
        List<String> cateItem = new ArrayList<>();
        cateItem.add("Document");
        cateItem.add("Food");
        cateItem.add("Medical");
        cateItem.add("Clothing");
        cateItem.add("Fragile");
        cateItem.add("Electronics");
        cateItem.add("Others");
        return cateItem;
    }

    public static List<OnBoardScreen> getListBoardingScreen(Context mContext){
        List<OnBoardScreen> list = new ArrayList<>();
        list.add(new OnBoardScreen(R.drawable.ic_image_boarding_1,mContext.getString(R.string.easy_paypment),mContext.getString(R.string.easy_fast_and_secure_with_kiwigo ),0));
        list.add(new OnBoardScreen(R.drawable.ic_image_boarding_2,mContext.getString(R.string.easy_transaction),mContext.getString(R.string.night_and_day_moto_or_taxi)+"  "+mContext.getString(R.string.app_name)+".",1));
        list.add(new OnBoardScreen(R.drawable.ic_image_boarding_3,mContext.getString(R.string.easy_delivery),mContext.getString(R.string.shopping_groceries_drinks_food_baby),2));
        list.add(new OnBoardScreen(R.drawable.ic_image_boarding_4,mContext.getString(R.string.start_naow),mContext.getString(R.string.you_can_now_enjoy),3));
        return list;
    }
    public static User getUser() {
        if (user == null) user = new User();
        return user;
    }

    public static void setUser(User user) {
        MockupData.user = user;
    }

}
