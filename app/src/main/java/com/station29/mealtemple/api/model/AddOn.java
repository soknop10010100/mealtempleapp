package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddOn implements Serializable {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String label;

    @SerializedName("price")
    @Expose
    private float price;

    public AddOn(int id, String label, float price) {
        this.id = id;
        this.label = label;
        this.price = price;
    }

    public String getLabel() {
        return label;
    }

    public float getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }
}
