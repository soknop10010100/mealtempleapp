package com.station29.mealtemple.api.model;

public class OnBoardScreen {

    int image;
    String title;
    String text;


    int index;
    public OnBoardScreen(int image, String title, String text,int index) {
        this.image = image;
        this.title = title;
        this.text = text;
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }
}
