package com.station29.mealtemple.api.request;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.Menu;
import com.station29.mealtemple.api.model.Product;
import com.station29.mealtemple.api.model.ProductDetail;
import com.station29.mealtemple.api.model.Shop;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductWs {

    public void loadProducts(String countryCode, int menuId , int acctId ,final LoadProductCallback loadProductCallback , int page , int size){
        ServiceAPI api = RetrofitGenerator.createService();
        Call<JsonElement> res = api.listProduct(countryCode, menuId, acctId,page,size);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonElement =response.body();
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    Log.d("resItem", String.valueOf(resObj));
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        JsonArray dataArray = resObj.getAsJsonObject("data").getAsJsonArray("data");
                        List<Product> productList = new ArrayList<>();
                        for (int i = 0 ; i< dataArray.size() ; i++) {
                            JsonElement element = dataArray.get(i);
                            Product product = new Gson().fromJson(element, Product.class);
                            productList.add(product);
                        }
                        loadProductCallback.onLoadSuccess(productList);
                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadProductCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadProductCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadProductCallback.onLoadFailed("Cannot reach Server");
            }
        });
    }
    public interface LoadProductCallback {
        void onLoadSuccess (List<Product> productList);
        void onLoadFailed(String errorMessage);
        void onLoadProductDetail(ProductDetail productDetail);
        void onLoadAllProduct(HashMap<String,List<Product>> allProduct);
    }
    public void loadProductDetail(String countryCode, int accountId, int productId, final LoadProductCallback loadProductCallback){
        ServiceAPI api = RetrofitGenerator.createService();
        Call<JsonElement> res = api.productDetail(countryCode, accountId, productId);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject obj = response.body().getAsJsonObject();
                    Log.d("product_detail", obj +"");
                    if (obj.has("data") && !obj.get("data").isJsonNull() && obj.has("success")&& obj.get("success").getAsBoolean()) {
                        JsonObject dataObj = obj.getAsJsonObject("data");
                        ProductDetail productDetail = new Gson().fromJson(dataObj, ProductDetail.class);
                        loadProductCallback.onLoadProductDetail(productDetail);
                    } else {
                        loadProductCallback.onLoadFailed("Wrong record Type.");
                    }
                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadProductCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadProductCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadProductCallback.onLoadFailed("Cannot Reach Server");
            }
        });
    }
    public void loadAllProducts(String countryCode,int accId,int cateId,int tagId,double lat,double lng,final LoadProductCallback loadProductCallback){
        ServiceAPI serviceAPI = RetrofitGenerator.createService ();
        Call<JsonElement> res = serviceAPI.storeDetail ( countryCode,accId,cateId,tagId ,lat,lng );
        res.enqueue ( new Callback<JsonElement> () {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if (response.isSuccessful () && response.body () instanceof JsonObject){
                    JsonObject jsonObject= response.body ().getAsJsonObject ();
                    if (jsonObject.has ( "success" )&& jsonObject.get ( "success" ).getAsBoolean ()){
                        try {
                            JsonObject ele = jsonObject.getAsJsonObject ( "data" );
                            JsonArray menArray = ele.getAsJsonArray ( "product_group" );
                            HashMap<String,List<Product>> allProduct = new HashMap<>();
                            for (int i = 0; i < menArray.size (); i++) {
                                JsonObject jsonObject1 = menArray.get ( i ).getAsJsonObject ();
                                String id = jsonObject1.get("name").getAsString();
                                JsonArray productList = jsonObject1.getAsJsonArray("products");
                                List<Product> listProduct = new ArrayList<> (  );
                                for(int j = 0; j < productList.size(); j++ ){
                                    JsonElement element = productList.get(j);
                                    Product product = new Gson().fromJson(element, Product.class);
                                    listProduct.add(product);
                                    allProduct.put(id,listProduct);
                                }
                            }
                            loadProductCallback.onLoadAllProduct(allProduct);
                        }catch (Exception e){
                            loadProductCallback.onLoadFailed ( "Can not load base on your pin location." );
                        }

                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadProductCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadProductCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){
                        }
                    }
                }

            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {

            }

        } );
    }
}
