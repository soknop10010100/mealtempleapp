package com.station29.mealtemple.api;

import com.station29.mealtemple.api.model.response.Config;
import com.station29.mealtemple.ui.home.HomeFragment;

import java.util.HashMap;

public class Constant {

    private static String baseUrl;
    public static HashMap<String,String> serviceLanguage = new HashMap<>();
    private static String start = "start";

    private static final String dev = "http://161.117.230.89/";
    private static final String pro = "http://161.117.60.77/";
    public static final String ORGAN_PAY_URL = "https://api-list-psp.kiwigo.app/";
    public static String BASE_STORE_DEV = "https://clientdev.kesspay.io/storage/" ;
    public static String BASE_STORE_PRO = "https://mywallet.kesspay.io/";
    public static final String KESS_DEV_URL = "https://clientdev.kesspay.io/";
    public static final String KESS_PRO_URL = "https://mywallet.kesspay.io/";

    // ======================= Firebase Analytics Constant
    public final static String CLICK_ON = "click_on";
    public final static String SHOP_ID = "kiwiGo_shopId";
    public final static String USER_ID = "KiwiGo_userId";
    public final static String ERROR_MESSAGE = "error_message";
    public final static String PAYMENT_TYPE = "payment_type";
    public final static String SERVICE_NAME="service_name";
    // ========================================================

    public final static String SHARE_PREFERANCE_NAME="MealTempleCustomer";

    public static String getStart() {
        return start;
    }

    public static void setStart(String start) {
        Constant.start = start;
    }

    public final static String START_BOARDING= start;

    public static String resquestUrl = pro;

    public static String kessUrl  = KESS_DEV_URL;

    public static String kessStoreUrl = BASE_STORE_DEV;

    public final static String FIREBASE_TOKEN = "fToken";

    private static Config config;

    public static Config getConfig() {
        if (HomeFragment.splashConfig!=null) setConfig(HomeFragment.splashConfig);
        return config;
    }

    public static void setBaseUrl(String baseUrl) {
        Constant.baseUrl = baseUrl;
    }

    public static String getBaseUrl() {
        return baseUrl;
    }

    public static void setConfig(Config config) {
        Constant.config = config;
    }

    public static String getKessUrl() {
        return kessUrl;
    }

    public static void setKessUrl(String kessUrl) {
        Constant.kessUrl = kessUrl;
    }
}
