package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class DisItems  {

    @SerializedName("product_id")
    @Expose
    private int productId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("default_price")
    @Expose
    private double default_price;
    @SerializedName("after_price")
    @Expose
    private double afterDis;

    @SerializedName("option_id")
    @Expose
    private int optionId;
    @SerializedName("option_name")
    @Expose
    private String optionName;
    @SerializedName("discount")
    @Expose
    private float amount;
    @SerializedName("type")
    @Expose
    private String type;

    public int getOptionId() {
        return optionId;
    }

    public String getOptionName() {
        return optionName;
    }


    public int getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }

    public double getDefault_price() {
        return default_price;
    }

    public double getAfterDis() {
        return afterDis;
    }

    public float getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

}
