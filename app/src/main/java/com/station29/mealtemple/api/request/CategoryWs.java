package com.station29.mealtemple.api.request;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.Category;
import com.station29.mealtemple.api.model.StoreGroup;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryWs {

    public void listMainCategories(String countryCode ,String lang, final LoadCategoryCallback loadCategoryCallback) {
        ServiceAPI service = RetrofitGenerator.createService();
        Call<JsonElement> res = service.listCategory(countryCode,lang);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        JsonArray dataArray = resObj.getAsJsonArray("data");
                        List<Category> categoryList = new ArrayList<>();
                        for (int i = 0; i < dataArray.size(); i++) {
                            JsonElement element = dataArray.get(i);
                            Category category = new Gson().fromJson(element, Category.class);
                            categoryList.add(category);
                        }
                        loadCategoryCallback.onLoadSuccess(categoryList);
                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if(Constant.serviceLanguage.containsKey(message))
                                    loadCategoryCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadCategoryCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadCategoryCallback.onLoadFailed("Cannot reach Server");
            }
        });
    }

    public interface LoadCategoryCallback {
        void onLoadSuccess(List<Category> categoryList);
        void onLoadFailed(String errorMessage);
    }
    //*==================================================================================*/

    public void loadCategoryTag(String countryCode, int categoryId , String lang, final LoadTagCallback loadTagCallback){
        ServiceAPI service = RetrofitGenerator.createService();
        Call<JsonElement> res = service.categoryTag(countryCode, categoryId, lang);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                JsonElement jsonElement = response.body();
                if (response.isSuccessful() && response.body() instanceof JsonObject) {
                    JsonObject resObj = response.body().getAsJsonObject();
                    if (resObj.has("success") && resObj.get("success").getAsBoolean()) {
                        JsonArray dataArray = resObj.getAsJsonArray("data");
                        List<StoreGroup> tagList = new ArrayList<>();
                        for (int i = 0 ; i< dataArray.size() ; i++) {
                            JsonElement element = dataArray.get(i);
                            StoreGroup tag = new Gson().fromJson(element, StoreGroup.class);
                            tagList.add(tag);
                        }
                        loadTagCallback.onLoadSuccess(tagList);
                    }
                }else{
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        try {
                            String json = errorString.string();
                            Log.d("errror", json);
                            JSONObject jsonObject1 = new JSONObject(json);
                            if (jsonObject1.has("message")){
                                String message = jsonObject1.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadTagCallback.onLoadFailed(Constant.serviceLanguage.get(message));
                                else loadTagCallback.onLoadFailed(message);
                                Log.d("notTranslate", "onResponse: "+message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage());
                        } catch (IOException e2){

                        }
                    }
                }
            }
            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {
                loadTagCallback.onLoadFailed("Cannot reach Server");
            }
        });
    }

    public interface LoadTagCallback {
        void onLoadSuccess(List<StoreGroup> tagList);
        void onLoadFailed(String errorMessage);
    }
}
