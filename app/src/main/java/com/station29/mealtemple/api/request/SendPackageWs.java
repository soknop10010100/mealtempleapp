package com.station29.mealtemple.api.request;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.api.model.SendPackageDetail;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendPackageWs {
    public void sendPackage(Context context, HashMap<String,Object> hashMap,loadSendPackage loadSendPackage){
        ServiceAPI api = RetrofitGenerator.createServiceWithAuth(context);
        String map = new Gson().toJson(hashMap);
        Log.d("map", map);
        Call<JsonElement> res = api.sendPackage(hashMap);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                int orderId = jsonObject.get("order_id").getAsInt();
                loadSendPackage.onLoadSuccessfully(orderId);
            }

            @Override
            public void onError(String error,int code) {
                loadSendPackage.onLoadingFail(error,code);
            }
        }));
    }
    public void resendPackage(Context context, HashMap<String,Object> order,loadSendPackage loadSendPackage){
        ServiceAPI api = RetrofitGenerator.createServiceWithAuth(context);
        String map = new Gson().toJson(order);
        Log.d("map_df", map);
        Call<JsonElement> res = api.sendPackage(order);
        res.enqueue(new CustomCallback(context, new CustomResponseListener() {
            @Override
            public void onSuccess(JsonObject resObj) {
                JsonObject jsonObject = resObj.get("data").getAsJsonObject();
                int orderId = jsonObject.get("order_id").getAsInt();
                loadSendPackage.onLoadSuccessfully(orderId);
            }

            @Override
            public void onError(String error,int code) {
                loadSendPackage.onLoadingFail(error,code);
            }
        }));
    }
    public void getSendPackage(Context context,String pickLat ,String pickLng, String dropLat , String dropLng,String countryCode ,loadSendPackage loadSendPackage){
        ServiceAPI api = RetrofitGenerator.createServiceWithAuth(context);
        Call<JsonElement> res = api.getSendPackage(BaseActivity.countryCode,pickLat,pickLng,dropLat,dropLng);
        res.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(@NotNull Call<JsonElement> call, @NotNull Response<JsonElement> response) {
                if(response.isSuccessful()){
                    JsonElement jsonObject = response.body();
                    JsonObject object = jsonObject.getAsJsonObject();
                    if(object.has("data")){
                        JsonObject resObj1 = object.get("data").getAsJsonObject();
                        SendPackageDetail detail = new Gson().fromJson(resObj1, SendPackageDetail.class);
                        loadSendPackage.onLoadGetDetailSuccess(detail);
                    }

                } else {
                    if (response.errorBody() != null) {
                        ResponseBody errorString = response.errorBody();
                        String message = "";
                        try {
                            String json = errorString.string();
                            Log.d("error", json);
                            JSONObject jsonOb = new JSONObject(json);
                            if(jsonOb.has("message")){
                                message = jsonOb.getString("message");
                                if (Constant.serviceLanguage.containsKey(message))
                                loadSendPackage.onLoadingFail(Constant.serviceLanguage.get(message),401);
                                else loadSendPackage.onLoadingFail(message,401);
                                Log.d("notTranslate", "onResponse: "+message);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(e.getClass().getName(), e.getMessage() + "");
                            message = context.getString(R.string.unexpected_problem);
                            loadSendPackage.onLoadingFail(message,500);
                        } catch (IOException e2) {
                            Log.e(e2.getClass().getName(), e2.getMessage() + "");
                            message = context.getString(R.string.unexpected_problem);
                            loadSendPackage.onLoadingFail(message,500);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<JsonElement> call, @NotNull Throwable t) {

            }
        });
    }

     public interface loadSendPackage{
        void onLoadSuccessfully(int orderId);
        void onLoadGetDetailSuccess(SendPackageDetail detail);
        void onLoadingFail(String message,int code);
    }
}
