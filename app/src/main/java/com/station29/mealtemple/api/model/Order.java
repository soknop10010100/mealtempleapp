package com.station29.mealtemple.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Order implements Serializable {

    @SerializedName("account_id")
    @Expose
    private int accountId;
    @SerializedName("order_id")
    @Expose
    private int orderId;
    @SerializedName("account_name")
    @Expose
    private String accountName;
    @SerializedName("date_order")
    @Expose
    private String dateOrder;
    @SerializedName("amount")
    @Expose
    private double amount;
    @SerializedName("order_number")
    @Expose
    private long order_number;
    @SerializedName("type_order")
    @Expose
    private String type_order;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("waypoints")
    @Expose
    private Waypoint waypoint;

    public Waypoint getWaypoint() {
        return waypoint;
    }

    public String getType_order() {
        return type_order;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getDateOrder() {
        return dateOrder;
    }

    public double getAmount() {
        return amount;
    }

    public long getOrder_number() {
        return order_number;
    }

    public void setOrder_number(int order_number) {
        this.order_number = order_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
