package com.station29.mealtemple.api.model;

public class Menu {

    private int menuId;
    private String label;

    public Menu(int menuId, String label) {
        this.menuId = menuId;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public int getMenuId() {
        return menuId;
    }
}
