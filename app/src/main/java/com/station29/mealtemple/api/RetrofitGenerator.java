package com.station29.mealtemple.api;


import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.util.Util;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitGenerator {

    public static ServiceAPI createService() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.getBaseUrl ());
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

    //create service with local token
    public static ServiceAPI createServiceWithAuth(Context activity) {
        final String authToken = "Bearer " + Util.getString("token", activity);
        Log.d("authToken", authToken);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            @NonNull
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder builder = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Authorization", authToken)
                        .header("channel",BuildConfig.TAXI_CHANNEL)
                        .header("key",BuildConfig.TAXI_KEY)
                        .method(original.method(), original.body());

                Request request = builder.build();
                return chain.proceed(request);
            }
        });

        httpClient.readTimeout(60, TimeUnit.SECONDS).connectTimeout(60, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.getBaseUrl ());
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

    public static ServiceAPI createBaseUrl() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.resquestUrl );
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }

    public static ServiceAPI createServiceWithHeader() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder builder = original.newBuilder()
                        .header("channel", BuildConfig.TAXI_CHANNEL)
                        .header("key",BuildConfig.TAXI_KEY)
                        .method(original.method(),original.body());
                Request request = builder.build();
                return chain.proceed(request);
            }
        });
        httpClient.readTimeout(30, TimeUnit.SECONDS).connectTimeout(30, TimeUnit.SECONDS);
        Retrofit.Builder builder = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(Constant.getBaseUrl ());
        builder.client(httpClient.build());
        Retrofit retrofit = builder.build();
        return retrofit.create(ServiceAPI.class);
    }
}
