package com.station29.mealtemple;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.OnBoardScreen;
import com.station29.mealtemple.dummy.DummyContent.DummyItem;
import com.station29.mealtemple.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem}.
 * TODO: Replace the implementation with code for your data type.
 */
public class BoardingViewAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private List<OnBoardScreen> lists = new ArrayList<>();
    private ViewPager viewPager;
    private int n = 0;
    private String lang;
    public BoardingViewAdapter(FragmentManager fm,List<OnBoardScreen> lists,String lang) {
        super(fm,lists.size());
        this.lists = lists;
        this.lang = lang;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
       // n = position;
        n +=1;
        OnBoardScreen item = lists.get(position);
        if(item!=null){
            return FragmentBoarding.newInstance(item.getImage(),item.getTitle(),item.getText(),n,item.getIndex(),lang);
        }else {
            return null;
        }

    }

    @Override
    public int getCount() {

        return lists.size();
    }


}