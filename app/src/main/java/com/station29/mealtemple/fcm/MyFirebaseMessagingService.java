package com.station29.mealtemple.fcm;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.station29.mealtemple.MainActivity;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.CustomCallback;
import com.station29.mealtemple.api.CustomResponseListener;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.RetrofitGenerator;
import com.station29.mealtemple.api.ServiceAPI;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.order.OrderDetailActivity;
import com.station29.mealtemple.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.station29.mealtemple.util.Util.getBitmapFromURL;

public class MyFirebaseMessagingService  extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    private String orderId="-1",typeOrder , status , image;
    private Bitmap urlBitmap;
   // private final String channelId = "com.kiwigo.app.customer";
    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);x
//        sendNotification(remoteMessage.getMessageType());
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if(!Util.getString("token",getBaseContext()).isEmpty()) {
                if (remoteMessage.getData().size() > 0) {
                    Log.d(TAG, "Message data payload: " + remoteMessage.getData());
                    if (remoteMessage.getNotification() != null && remoteMessage.getNotification().getBody() != null) {
                        String title = remoteMessage.getNotification().getTitle();
                        String message = remoteMessage.getNotification().getBody();
                        if(remoteMessage.getData().containsKey("status"))
                            status = remoteMessage.getData().get("status");
                        if(remoteMessage.getData().containsKey("image"))
                            image = remoteMessage.getData().get("image");
                        if(remoteMessage.getData().containsKey("order_id"))
                            orderId = remoteMessage.getData().get("order_id");
                        if(remoteMessage.getData().containsKey("order_type"))
                            typeOrder = remoteMessage.getData().get("order_type");

                        sendNotification(message,title,image);

                        if(status !=null && !status.equalsIgnoreCase("ACCEPTED"))
                            MockupData.checkpickup = true;
                        else
                            MockupData.checkpickup = false;

                        if(typeOrder != null && status != null && status.equalsIgnoreCase("ACCEPTED") && typeOrder.equalsIgnoreCase("service")|| typeOrder != null && typeOrder.equalsIgnoreCase("send") && status.equalsIgnoreCase("ACCEPTED")){
                            Intent intent1 = new Intent(getBaseContext(), OrderDetailActivity.class);
                            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent1.putExtra("status",status);
                            intent1.putExtra("typeOrder",typeOrder);
                            intent1.putExtra("orderId", orderId);
                            startActivity(intent1);
                        }else if(status !=null && status.equalsIgnoreCase("FAILED")){
                            Intent intent = new Intent(getBaseContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }

                    } else {
                        String message = remoteMessage.getData().get("message");
                        sendNotification(message, getString(R.string.app_name),"");
                    }
//            if (/* Check if data needs to be processed by long running job */ true) {
//                // For long-running tasks (10 seconds or more) use WorkManager.
//                scheduleJob();
//            } else {
//                // Handle message within 10 seconds
//                handleNow();
//            }
            }
            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }

            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.
        }
    }

    // [END receive_message]


    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed_token: " + token);
        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        Util.saveString(Constant.FIREBASE_TOKEN,token,getApplicationContext());
        String tToken = Util.getString(Constant.FIREBASE_TOKEN, getApplicationContext());
        Log.d("testtstest", tToken);
        sendRegistrationToServer(token,this);
    }

    // [END on_new_token]

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob() {
        // [START dispatch_job]
//        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
//                .build();
//        WorkManager.getInstance().beginWith(work).enqueue();
        // [END dispatch_job]
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    private int randomNotificationId() {
        Random rand = new Random();
        return rand.nextInt(100);
    }


    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    public static void sendRegistrationToServer(final String token, Context ctx) {
        // TODO: Implement this method to send token to your app server.
        if (Constant.getBaseUrl() != null) {
            ServiceAPI serviceAPI = RetrofitGenerator.createServiceWithAuth(ctx);
            String userId = Util.getString("user_id", ctx);
            if (userId.isEmpty()) {
                return;
            }
            HashMap<String, Object> param = new HashMap<>();
            param.put("device_token", token);
            param.put("user_id", userId);
            param.put("device_type", "ANDROID");
            param.put("longitute", BaseActivity.longitude);
            param.put("latitute",BaseActivity.latitude);
            param.put("country_code",BaseActivity.countryCode);
            param.put("user_type","CUSTOMER");
            final retrofit2.Call<JsonElement> res = serviceAPI.deviceToken(param);
            res.enqueue(new CustomCallback(ctx, new CustomResponseListener() {
                @Override
                public void onSuccess(JsonObject resObj) {
                    Log.d("uploadToken", "Success" + resObj);
                    if(resObj.has("data")){
                        JsonObject jsonObject = resObj.getAsJsonObject("data");
                        String topic = jsonObject.get("topic").getAsString();
                        Util.saveString("topic",topic,ctx);
                    }
                }

                @Override
                public void onError(String error,int code) {
                    Log.d("uploadToken", "error" + error);
                }
            }));
        }
    }
    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String messageBody, String title,String image) {
        Bitmap bMap = BitmapFactory.decodeFile(image);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(MyFirebaseMessagingService.this,channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentIntent(pendingIntent);

        if(image != null && !image.equals("")){
           notificationBuilder.setLargeIcon(getBitmapFromURL(image));
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(this.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(channel);
        }

        int id = new Random().nextInt(1000);
        if (notificationManager != null)
            notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
    }

}
