package com.station29.mealtemple;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.ui.BaseFragment;
import com.station29.mealtemple.util.Util;

import org.w3c.dom.Text;

public class SelectLanguageActivity extends BaseActivity {
    String lang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        TextView englishTV = findViewById(R.id.english);
        TextView frenchTv = findViewById(R.id.french);
        Constant.setStart("start");

        englishTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String lange = "en";
//                Util.saveString("language", lange, SelectLanguageActivity.this);
                Intent intent = new Intent(SelectLanguageActivity.this,BoardingActivity.class);
                intent.putExtra("en","en");
                startActivity(intent);
//
            }
        });
        frenchTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String lange = "fr";
//                Util.saveString("language", lange, SelectLanguageActivity.this);
                Intent intent = new Intent(SelectLanguageActivity.this,BoardingActivity.class);
                intent.putExtra("fr","fr");
                startActivity(intent);
            }
        });
    }
}