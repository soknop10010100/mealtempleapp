package com.station29.mealtemple;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import org.jsoup.Jsoup;

import java.util.concurrent.ExecutionException;

public class GetLatestVersion extends AsyncTask<Void, String, String> {
    private Context mActivity;
    public GetLatestVersion(Context mActivity) {
        this.mActivity = mActivity;
    }

    @Override
    protected String doInBackground(Void... voids) {
        String newVersion = "";
        try {
            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + mActivity.getPackageName() + "&hl=it")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select(".hAyfc .htlgb")
                    .get(7)
                    .ownText();
            return newVersion;
        } catch (Exception e) {
            return newVersion;
        }
    }

    @Override
    protected void onPostExecute(String onlineVersion) {
        super.onPostExecute(onlineVersion);
        float currentVersion = getAppVersionName(mActivity);
        Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        if (onlineVersion != null && !onlineVersion.isEmpty()) {
            if ((float) currentVersion < Float.parseFloat(onlineVersion)) {
                //show dialog
                Log.d("currentVersionStore", currentVersion + "");
                checkUpdatePopUp();
            }
        }
    }
    private float getAppVersionName(Context context){
        String versionCode = "0";
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            versionCode = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return Float.parseFloat(versionCode);
    }

    @SuppressLint("SetTextI18n")
    private void checkUpdatePopUp() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mActivity,R.style.DialogTheme);
        View view = LayoutInflater.from(mActivity).inflate(R.layout.layout_updat_version, null);
        builder.setView(view);
        builder.setCancelable(false);
        TextView title  = view.findViewById(R.id.titles);
        TextView text = view.findViewById(R.id.text);
        Button btnUpdateTo = view.findViewById(R.id.btn_update);
        Button btnNoThank = view.findViewById(R.id.no_thank);
        title.setText(mActivity.getString(R.string.app_name)+ " " + mActivity.getString(R.string.update) +"?");
        text.setText(mActivity.getString(R.string.app_name) + " "+ mActivity.getString(R.string.recommends_that_you_update));
        final Dialog dialog = builder.create();
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        btnNoThank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog.cancel();
            }
        });
        btnUpdateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = mActivity.getPackageName();
                try {
                    mActivity. startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }

        });

        dialog.show();


    }
}