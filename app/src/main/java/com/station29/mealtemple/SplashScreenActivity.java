package com.station29.mealtemple;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.MockupData;
import com.station29.mealtemple.api.model.response.Config;
import com.station29.mealtemple.api.request.ConfigWs;
import com.station29.mealtemple.api.request.ServerUrlWs;
import com.station29.mealtemple.ui.BaseActivity;
import com.station29.mealtemple.util.GPSTracker;
import com.station29.mealtemple.util.Util;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.station29.mealtemple.util.Util.popupMessage;
import static com.station29.mealtemple.util.Util.popupMessageAndFinish;

public class SplashScreenActivity extends BaseActivity {

    private final static int REQUEST_LOCATION_PERMISSION = 546;
    private final static int REQUEST_CODE_ENABLE_LOCATION = 246;

    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    String lang = "",start;
    private boolean isGetConfig = false;
    String currentVersion , latestVersion;
    private static final int REQ_CODE_VERSION_UPDATE = 530;
    Dialog dialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        lang = Util.getString("language", this);
        start = Util.getString("start",this);
        switch (lang) {
            case "":
                lang = "en";
                break;
            case "fr":
                lang = "fr";
                break;
            case "my":
                lang = "my";
                break;
        }
        Util.changeLanguage(this, lang);

        checkBackgroundLocationPermission();
        initLocationRequest();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ENABLE_LOCATION && resultCode == RESULT_OK) {
            requestLocationUpdate();
        }
    }

    private void checkBackgroundLocationPermission() {
        boolean permissionAccessCoarseLocationApproved =
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED;

        if (permissionAccessCoarseLocationApproved) {
            checkGps();
        } else {
            // App doesn't have access to the device's location at all. Make full request
            // for permission.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_BACKGROUND_LOCATION
                }, REQUEST_LOCATION_PERMISSION);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION
                }, REQUEST_LOCATION_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                checkGps();
            } else {
                Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void checkGps() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert manager != null;
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        } else {
            getLastKnowLocation();
        }
    }

    private void stopLocationUpdates() {
        if (mLocationCallback == null) return;
        FusedLocationProviderClient fusedLocationProviderClient = new FusedLocationProviderClient(this);
        fusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
    }

    private void startLocationUpdates() {
        if (mLocationCallback == null) return;
        mFusedLocationClient = new FusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest,
                mLocationCallback,
                Looper.myLooper());
    }

    private void initLocationRequest() {

        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30 * 1000);
        locationRequest.setFastestInterval(5 * 1000);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    Log.d("locationLoginUpdate", location.getLatitude() + " : " + location.getLongitude());
                    findZipCode(location.getLatitude(), location.getLongitude());
                    break;
                }
            }
        };
    }

    private void buildAlertMessageNoGps() {
        if (locationRequest == null) initLocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(locationSettingsResponse -> Log.d("locationLoginGPSRequest", "locationSettingsResponse"));

        task.addOnFailureListener(e -> {
            if (e instanceof ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(SplashScreenActivity.this,
                            REQUEST_CODE_ENABLE_LOCATION);
                } catch (IntentSender.SendIntentException sendEx) {
                    // Ignore the error.
                }
            }
        });
    }

    private void requestLocationUpdate() {
        if (mFusedLocationClient == null || mLocationCallback == null) {
            // If code enter this block, there is no way to solve.
            Log.d("requestLocationUpdate", "This block happens only when user clear Location while app already granted those permissions.");
            Log.d("locationLoginRequest", "unexpected problem" + mFusedLocationClient + "," + locationRequest + ", " + mLocationCallback);
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest,
                mLocationCallback, Looper.getMainLooper());
    }

    private void getLastKnowLocation() {
        GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation() && gpsTracker.getLatitude() != 0 && gpsTracker.getLongitude() != 0) {
            double lat = gpsTracker.getLatitude();
            double lng = gpsTracker.getLongitude();
            Log.d("locationLoginLastKnown", lat + " : " + lng);
            findZipCode(lat, lng);
        } else {
            requestLocationUpdate();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLocationUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeLocationUpdate();
    }

    private void removeLocationUpdate() {
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }

    private void findZipCode(final double lat, final double lng) {

        BaseActivity.latitude = lat;
        BaseActivity.longitude = lng;
        // -------------
        Geocoder geocoder = new Geocoder(SplashScreenActivity.this, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            if (addresses.size() > 0) {
                Log.d("latlngFromGoogle", lat + "," + lng);
                countryCode = addresses.get(0).getCountryCode();
                MockupData.DELIVERY_ADDRESS = addresses.get(0).getAddressLine(0);
            }
        } catch (IOException e) {
            Log.e(e.getClass().getName(), e.getMessage() + "");
            BaseActivity.countryCode = Util.getCountryCodeFromSimCard(SplashScreenActivity.this);
            MockupData.DELIVERY_ADDRESS = Util.getCountryNameFromCode(countryCode, SplashScreenActivity.this);
        }

//        String code = Util.getString("countryCode", SplashScreenActivity.this);
        String baseUrl = Util.getString("baseUrl", SplashScreenActivity.this);
        if(!isGetConfig)
            if (countryCode != null && !countryCode.isEmpty() && baseUrl != null && !baseUrl.isEmpty()) {
                Constant.setBaseUrl(baseUrl);
                isGetConfig = true;
    //            BaseActivity.countryCode = code;
                removeLocationUpdate();//To stop Get Location Update when we got Base Url (Khmer or Loas)
               if(Constant.getStart().equals("start") && start.equals("")||start.equals("start")){
                   getLanguage(lang,"selectLang");
               } else {
                   getLanguage(lang,"goToMain");
               }
            } else {
                isGetConfig = true;
                requestBaseUrlCodeTask(countryCode);
            }
//    }
        // ============

//        new AsyncTask<Void, Void, String>() {
//            @Override
//            protected String doInBackground(Void... voids) {
//                Geocoder geocoder = new Geocoder(SplashScreenActivity.this, Locale.getDefault());
//                List<Address> addresses;
//                try {
//                    addresses = geocoder.getFromLocation(lat, lng, 1);
//                    if (addresses.size() > 0) {
//                        Log.d("latlngFromGoogle", lat + "," + lng);
//                        countryCode = addresses.get(0).getCountryCode();
//                        MockupData.DELIVERY_ADDRESS = addresses.get(0).getAddressLine(0);
//                        return countryCode;
//                    }
//                } catch (IOException e) {
//                    Log.e(e.getClass().getName(), e.getMessage() + "");
//                    BaseActivity.countryCode = Util.getCountryCodeFromSimCard(SplashScreenActivity.this);
//                    MockupData.DELIVERY_ADDRESS = Util.getCountryNameFromCode(countryCode, SplashScreenActivity.this);
//                    return BaseActivity.countryCode;
//                }
//                return null;
//            }
//
//            @Override
//            protected void onPostExecute(String s) {
//                super.onPostExecute(s);
//                String code = Util.getString("countryCode", SplashScreenActivity.this);
//                String baseUrl = Util.getString("baseUrl", SplashScreenActivity.this);
//                if (s != null && !s.isEmpty())
//                    if (code != null && code.equalsIgnoreCase(s) && baseUrl != null) {
//                        Constant.setBaseUrl(baseUrl);
//                        removeLocationUpdate(); //To stop Get Location Update when we got Base Url (Khmer or Loas)
//                        startActivity(new Intent(SplashScreenActivity.this, MainActivity.class));
//                        finish();
//                    } else {
//                        requestBaseUrlCodeTask(s);
//                    }
//                }
//        }.execute();
    }

    private void requestBaseUrlCodeTask(final String code) {

        new ServerUrlWs().readServerUrl(code, new ServerUrlWs.loadServerUrl() {
            @Override
            public void onLoadSuccess(boolean success) {
                if (success) {
                    Util.saveString("countryCode", code, SplashScreenActivity.this);
                    Util.saveString("baseUrl", Constant.getBaseUrl(), SplashScreenActivity.this);
                    removeLocationUpdate();
                    //To stop Get Location Update when we got Base Url (Khmer or Loas)
                    if(Constant.getStart().equals("start") && start.equals("")||start.equals("start")){
                        getLanguage(lang,"selectLang");
                    } else {
                        getLanguage(lang,"goToMain");
                    }
                }
            }

            @Override
            public void onLoadFailed(String errorMessage) {
                popupMessageAndFinish(null, errorMessage, SplashScreenActivity.this);
            }
        });
    }

    private void goToSelectCountry(){
        Intent intent = new Intent(SplashScreenActivity.this, SelectLanguageActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToMain(){
        Intent intent = new Intent(SplashScreenActivity.this,MainActivity.class);
        intent.putExtra("lang",lang);
        startActivity(intent);
    }

    private void getLanguage(String lang ,String type){
        new ConfigWs().getConfigLanguage("en", SplashScreenActivity.this, new ConfigWs.ReadConfigCallback() {
            @Override
            public void onSuccess(Config config) {
                if("selectLang".equals(type)){
                    goToSelectCountry();
                } else {
                    goToMain();
                }
            }

            @Override
            public void onError(String message) {
                popupMessageAndFinish(getString(R.string.error),message,SplashScreenActivity.this);
            }
        });
    }


}