package com.station29.mealtemple.util;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static com.station29.mealtemple.util.Util.formatString;

public class DateUtil {

    public static String getCurrentDate(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
    }

    public static String getCurrentDateNewFormat(){
        return new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
    }

    public static String formatDateUptoCurrentRegion(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        Date date = formatDateTime(dateTime);

        // Get Raw Offset
        Calendar mCalendar = new GregorianCalendar();
        TimeZone mTimeZone = mCalendar.getTimeZone();
        int mGMTOffset = mTimeZone.getRawOffset();

        return format.format(date.getTime() + mGMTOffset);
    }

    public static Date formatDateTime(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date newDate = null;
        try {
            newDate = format.parse(dateTime);
            Log.d("newDate", String.valueOf(format));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assert newDate != null;
        return newDate;
    }

    public static long getCurrentTimeInMinute() {
        long hours = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        long mn = Calendar.getInstance().get(Calendar.MINUTE);
        Log.d("hour_mn", hours + " : " + mn);
        return hours * 60 + mn;
    }

    public static String formatDateFromServer(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        Date date = formatDateTime(dateTime);
        return format.format(date.getTime() );
    }


    //get tomorrow date
    public static String getTomorrowDate() {
        Calendar cal = new GregorianCalendar();
        cal.add(Calendar.DATE, 1);
        return new SimpleDateFormat("yyyy-MM-dd",Locale.US).format(cal.getTime());
    }
    //send time to server  with - location
    public static String formatTimeZoneServer(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Date date = formatDateTime(dateTime);
        return format.format(date.getTime() - new GregorianCalendar().getTimeZone().getRawOffset());
    }
    //set time to local after get time from server
    public static String formatTimeZoneLocal(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a", Locale.US);
        Date date = formatDateTime(dateTime);
        return format.format(date.getTime() + new GregorianCalendar().getTimeZone().getRawOffset());
    }
    public static String formatTimeZoneSlash(String dateTime){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm a", Locale.US);
        Date date = formatDateTime(dateTime);
        return format.format(date.getTime() +new GregorianCalendar().getTimeZone().getRawOffset());
    }

    public static long parseDated(String text)
    {
        String[] tokens = text.split(":");
        int secondsToMs = Integer.parseInt(tokens[2]) * 1000;
        int minutesToMs = Integer.parseInt(tokens[1]) * 60000;
        int hoursToMs = Integer.parseInt(tokens[0]) * 3600000;
        long total = secondsToMs + minutesToMs + hoursToMs;
        return total;

    }

    public static String formatHourByMillis(long millis){
       // SimpleDateFormat format= new SimpleDateFormat ( "HH:mm:ss" );

        return String.format ( "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
    }

    public static int formatMinutes(int minute){
        int min =0;
        if (minute!=0&&minute<=15){
            min= 15;
        }else if (minute!=0&&minute<=30){
            min= 30;
        }else if (minute!=0&&minute<=45){
            min = 45;
        }else if (minute!=0&&minute>45 && minute <=60){
            return 60;
        }else return minute;
        return min;
    }

    public static int isMinDelivery(Integer minShop){
        if (minShop != null){
            return minShop;
        }else {
            //if no minimum delivery we add 30 min
            return 30;
        }
    }

    public static ArrayList<String> getTime(int hour, int minute){
        //this func is formart minute to 0 15 30 45
        ArrayList<String> timesDelivery = new ArrayList<> (  );
        if (hour==24)hour=0;
        if (minute<10 && minuteDelivery ( minute ).get ( 0 )<10){
            timesDelivery.add (formatString(hour,minuteDelivery (minute ).get ( 0 )) );
        }else if (minute<=15){
            if ( minuteDelivery ( minute ).get ( 0 ) >10){
                timesDelivery.add ( formatString(hour,0)  );
                timesDelivery.add(formatString ( hour,minuteDelivery (minute ).get ( 0 ) ) );
            }
        }else if (minute<=30 && minuteDelivery (minute ).size ()>=1){
            timesDelivery.add ( formatString(hour,0) );
            for (int j = 0; j <  minuteDelivery (minute ).size (); j++) {
                timesDelivery.add(formatString ( hour,minuteDelivery ( minute ).get ( j ) ) );
            }

        }else if (minute<=45 && minuteDelivery ( minute).size ()==3){
            timesDelivery.add ( formatString(hour,0) );
            for (int j = 0; j < 3; j++) {
                timesDelivery.add(formatString ( hour,minuteDelivery ( minute ).get ( j ) ) );
            }

        }else if (minute<60) {
            if (minute>45&& minuteDelivery ( minute ).size ()==4){
                timesDelivery.add (formatString(hour,0) );
                for (int j = 0; j <4 ; j++) {
                    timesDelivery.add(formatString ( hour,minuteDelivery ( minute).get ( j ) ) );
                }
            }
        }

        return timesDelivery;
    }

    public static List<Integer> minuteDelivery(int deliveryMin){
        //generate the format for minute provide by store
        List<Integer> minuteList = new ArrayList<> (  );
        for (int i = 0; i < 60; i++) {
            if (deliveryMin<=15){
                minuteList.add (deliveryMin );
                break;
            }else if (deliveryMin<=30){
                if ( deliveryMin>=20 && deliveryMin<=30){
                    minuteList.add (15 );
                    minuteList.add (deliveryMin );
                    break;
                }else  {
                    minuteList.add (deliveryMin );
                    break;
                }

            }else if (deliveryMin<=45){
                if ( deliveryMin>20 && deliveryMin<=45){
                    minuteList.add (15 );
                    minuteList.add (30 );
                    minuteList.add (deliveryMin );
                    break;
                }
            }else if (deliveryMin<60) {
                if (deliveryMin>45){
                    minuteList.add (15 );
                    minuteList.add (30 );
                    minuteList.add (45 );
                    minuteList.add (deliveryMin );
                    break;
                }
            }
        }
        return minuteList;
    }

    public static int workingHour(int open , int close){
        //this func is calculate the working hours
        int timeLoop =0;
        if (open>=close ){
            timeLoop = 24+ close - open;
        }else {
            timeLoop =  close - open;
        }
        return timeLoop;
    }

    public static ArrayList<String> getDeliveryTomorrow(List<Integer> deliveryTimes,List<Integer> minutesList , int min){
        ArrayList<String> timesDelivery = new ArrayList<> (  );
        int minute = 0,openHour =deliveryTimes.get ( 0 ) ,sizeCount,loopTime, minDelivery = isMinDelivery ( min );
        sizeCount = deliveryTimes.size ();

        loopTime = ((sizeCount==2)?workingHour ( deliveryTimes.get ( 0 ) ,deliveryTimes.get ( 1 )):workingHour ( deliveryTimes.get ( 0 ) ,deliveryTimes.get ( 1 ))+workingHour ( deliveryTimes.get ( 2 ) ,deliveryTimes.get ( 3 )));
        minute= minutesList.get ( 0 )+minDelivery;
        if (minute >=60 ){
            openHour =openHour + minute/60;
            minute = minute %60;
        }
        //format minute to 0 15 30 45
        minute=formatMinutes ( minute );
        if (minute >=60 ){
            openHour =openHour + minute/60;
            minute = minute %60;
        }
        //loopTime * 4 because the format is 0 15 30 45 to show
        for (int i = 0; i <= loopTime *4; i++) {
            //show only open hour
            if (openHour== deliveryTimes.get ( 1 ) && minute>=0){
                if (openHour==24) openHour =0;
                if (openHour== deliveryTimes.get ( 1 )&& minutesList.get ( 1 )>=0){
                    timesDelivery.addAll ( getTime(openHour,minutesList.get(1)));
                }
                if (sizeCount!=2){
                    openHour = deliveryTimes.get ( 2 );
                    minute=minutesList.get ( 2 )+ minDelivery;
                    // add all hour +1 in the open second hour shift
                    int ihour = openHour;
                    for (int j = 0; j <= ihour+1; j++) {

                        minute = formatMinutes ( minute );
                        if (minute >= 60) {
                            openHour= openHour+ minute/60;
                            minute = minute %60;
                        }
                        if (openHour==deliveryTimes.get ( 3 ))break;
                        timesDelivery.add ( formatString ( openHour,minute ) );
                        minute+=15;
                        if (openHour== ihour+1) break;
                    }
                }else return timesDelivery;
            }
            if (sizeCount!=2){
                if (openHour== deliveryTimes.get ( 3 )) {
                    if (openHour==24) openHour =0;
                    if(minutesList.get ( 3 )>=0) {
                        timesDelivery.addAll(getTime(openHour,minutesList.get(3)));
                        return timesDelivery;
                    }
                }
            }
            if (minute == 0 )timesDelivery.add ( formatString ( openHour,minute ));
            else {
                timesDelivery.add (   formatString ( openHour,minute ) );
            }
            minute = minute + 15;
            //get min
            if (minute >= 60) {
                openHour += 1;
                minute = minute -60;
            }
            if (sizeCount!=2 ){
                if (openHour>deliveryTimes.get(3))
                    return timesDelivery;
            }else {
                if (openHour>deliveryTimes.get ( 1 )) return timesDelivery;
            }
        }
        return timesDelivery ;
    }

    public static ArrayList<String> getDeliveryToday(List<Integer> deliveryTimes,List<Integer> minutesList, int min){
        //closed and open on min delivery both today and tmr
        ArrayList<String> timesDelivery = new ArrayList<> (  );
        Calendar mCurrentTime = Calendar.getInstance();
        int mHour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        int mMinute = mCurrentTime.get ( Calendar.MINUTE );
        int minute = isMinDelivery (min )+ mMinute,hour ,hourSize,loopTime;
        int minDelivery= isMinDelivery ( min );
        hourSize = deliveryTimes.size ();
        //find current hour is equals to open hour
        hour = deliveryTimes.get ( 0 );
        //find current time checked
        for (int i = mHour; i <=24; i++) {
            if (hour==24)hour=0;
            if (hourSize==2){
                //if current > min
                if (i>=deliveryTimes.get ( 1 ) && mMinute<minutesList.get ( 1 )+minDelivery) {return timesDelivery;}else if (i>deliveryTimes.get ( 1 )) {return timesDelivery;}
                if (i+1== deliveryTimes.get ( 1 )){
                    //num =20 , ismin =45;
                    if (mMinute+isMinDelivery ( min )>=60 && minutesList.get ( 1 )==0){
                        return timesDelivery;
                    }
                }
                if (i>=deliveryTimes.get ( 0 )&& i<=deliveryTimes.get ( 1 ) ){
                    hour = i;
                    break;
                }
                minute = minutesList.get ( 0 );
                minute= formatMinutes ( minute );
            }else if (hourSize==4){
                //current hour > shopclose on second
                if (mHour<deliveryTimes.get ( 0 )){
                    hour=deliveryTimes.get ( 0 );
                    break;
                }else {
                    if (i >= deliveryTimes.get(0) && i <= deliveryTimes.get(1)) {
                        hour = i;//2
                        break;
                    } else if (i >= deliveryTimes.get(2) && i <= deliveryTimes.get(3)) {
                        hour = i;
                        if (mHour+1==deliveryTimes.get ( 2 )){//calcute the minute
                            if(60-mMinute<=minDelivery|| 60-mMinute>=minDelivery){//the minute that aviable to show
                                minute= minutesList.get ( 2 )+minDelivery;
                            }
                        }else if (mHour<deliveryTimes.get ( 2 )){
                            minute= minutesList.get ( 2 );
                        }

                        break;
                    }
                }

                if (i==deliveryTimes.get ( 3 )||i>deliveryTimes.get ( 3 )&& mMinute<minutesList.get(3)+minDelivery) {return timesDelivery;}

                if (i+1== deliveryTimes.get ( 3 )){
                    //num =20 , ismin =45;
                    int num = minDelivery+ mMinute;
                    minute=0;
                    if (minute<num){
                        for (int j = 0; minute<num; j++) {
                            minute=15;
                        }
                    }
                    if (minute>=60){
                        if(hour==24) hour=0;
                        hour= hour+minute/60;
                        minute=minute%60;
                    }
                    minute= formatMinutes ( minute );
                }
            }
        }


        if (minutesList.get(0)>=0 && hour<= deliveryTimes.get(0)){
            //show all list of delivery time
            minute=   minutesList.get ( 0 ) + minDelivery;
        }
        //if current min < min delivery show
        minute-=15;
        if (minute >=60 ){
            hour =hour + minute/60;
            minute = minute %60;
            minute = formatMinutes ( minute );
        }
        if (minute >=60 ){
            hour =hour + minute/60;
            minute = minute %60;
        }
        loopTime = ((hourSize==2)?workingHour ( deliveryTimes.get ( 0 ),deliveryTimes.get ( 1 ) ): workingHour ( deliveryTimes.get ( 0 ),deliveryTimes.get ( 1 ) )+ workingHour ( deliveryTimes.get ( 2 ),deliveryTimes.get ( 3 ) ));

        for (int i = 0; i <= loopTime *4; i++) {
            //show only open hou
            //in case hour + > close hour
            boolean isNow = false;
            // this case if for current hour and current minute to show compare to min delivery
            if ( hour==0 && minute == 0 ){
                timesDelivery.add ( String.format ( Locale.US, "%d:%s", hour, "00" ) );
            } else if (mHour<= hour && mMinute <60 && minute <=120) {
                isNow = true;
                if (hour<= mHour){ // check current time only
                    int h = hour+1;
                    int num = minDelivery+mMinute;
                    //this case show the min > current minute
                    if (minute<num){
                        minute=0;
                        for (int j = 0; minute<num; j++) {
                            minute+=15;
                        }
                    }
                    if (minute>=60){
                        if(hour==24) hour=0;
                        hour += minute/60;
                        minute=minute%60;
                    }
                    //loop for next hour and break this condition
                    for (int k = 0; hour<=h ; k++) {
                        if (minute >= 60) {
                            hour += 1;
                            minute = minute - 60;
                        }
                        //in case of hour == clsoe time and open time and minute is still aviable
                        if (hour== deliveryTimes.get ( 1 )) {
                            if(hour==24) hour=0;
                            if (minute<=minutesList.get ( 1 )) {
                                if (minDelivery>minutesList.get(1)&& minute==0&& 60-mMinute<minDelivery&&mHour+1==hour)return timesDelivery;
                                for (int j = 0; minute<=minutesList.get ( 1 ); j++) {
                                    timesDelivery.add ( formatString ( hour,minute ) );
                                    minute+=15;
                                }
                            }
                            if (hourSize==4){
                                hour = deliveryTimes.get ( 2 );
                                minute= minutesList.get ( 2 );
                                if (minute >= 60) {
                                    hour += 1;
                                    minute = minute - 60;
                                }
                                minute= formatMinutes ( minute );
                            }else{
                                return timesDelivery;
                            }
                        }else if (hourSize==4){
                            if (hour== deliveryTimes.get ( 3 )){
                                if(hour==24) hour=0;
                                if (minute<=minutesList.get ( 3 )) {
                                    if (minDelivery>minutesList.get(3)&& minute==0&& 60-mMinute<minDelivery&&mHour+1==hour)return timesDelivery;
                                    for (int j = 0; minute<=minutesList.get ( 3 ); j++) {
                                        timesDelivery.add ( formatString ( hour,minute ) );
                                        minute+=15;
                                    }
                                }
                                return timesDelivery;
                            }
                            if (hour>=deliveryTimes.get ( 1 )&&hour<deliveryTimes.get ( 2 ) ){
                                hour=deliveryTimes.get ( 2 );
                                minute = minutesList.get ( 2 );
                                minute= formatMinutes ( minute );
                                break;
                            }
                        }
                        if (minute >= 60) {
                            hour += 1;
                            minute = minute - 60;
                        }
                        minute= formatMinutes ( minute );
                        if (minute >= 60) {
                            hour += 1;
                            minute = minute - 60;
                        }
                        if (hourSize==2 && hour> deliveryTimes.get ( 1 )) return timesDelivery;
                        if (hourSize==4 && hour> deliveryTimes.get ( 3 )) return timesDelivery;
                        timesDelivery.add ( formatString(hour,minute));
                        minute+=15;
                        if (k>=4) break;
                    }
                    minute-=15;//this case when out of the case it will add more 15 minute //hour > h bacase it equeal next open hour
                }
                //sum the next minute + 15
                minute+=15;
                if (minute>=60){
                    hour+=1;
                    minute=minute-60;
                }


                if (hour== deliveryTimes.get ( 1 )) {
                    if(hour==24) hour=0;
                    if (minute<=minutesList.get ( 1 )) {
                        if (minDelivery>minutesList.get(1)&& minute==0&& 60-mMinute<minDelivery&&mHour+1==hour)return timesDelivery;
                        for (int j = 0; minute<=minutesList.get ( 1 ); j++) {
                            timesDelivery.add ( formatString ( hour,minute ) );
                            minute+=15;
                        }
                    }
                    if (hourSize==4){
                        hour = deliveryTimes.get ( 2 );
                        minute= minutesList.get ( 2 );
                        minute = formatMinutes ( minute );
                    }else {
                        return timesDelivery;
                    }

                }else if (hourSize==4){
                    if (hour== deliveryTimes.get ( 3 )){
                        if(hour==24) hour=0;
                        if (minute<=minutesList.get ( 3 )) {
                            if (minDelivery>minutesList.get(3)&& minute==0 && 60-mMinute<minDelivery &&mHour+1==hour)return timesDelivery;
                            for (int j = 0; minute<=minutesList.get ( 3 ); j++) {
                                timesDelivery.add ( formatString ( hour,minute ) );
                                minute+=15;
                            }
                        }
                        return timesDelivery;
                    }

                    if (hour>=deliveryTimes.get ( 1 )&&hour<deliveryTimes.get ( 2 ) ){
                        hour=deliveryTimes.get ( 2 );
                        minute = minutesList.get ( 2 );
                    }

                }
                minute= formatMinutes ( minute );
                if (minute >= 60) {
                    hour += 1;
                    minute = minute - 60;
                }
                if (hourSize==2 && hour> deliveryTimes.get ( 1 )) return timesDelivery;
                if (hourSize==4 && hour> deliveryTimes.get ( 3 )) return timesDelivery;
                timesDelivery.add (formatString(hour,minute));
            }
            if (!isNow){
                minute = minute + 15;
                //get min
                if (minute >= 60) {
                    hour += 1;
                    minute = minute - 60;
                }
            }
        }

        return timesDelivery ;
    }
}
