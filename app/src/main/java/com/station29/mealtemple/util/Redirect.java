package com.station29.mealtemple.util;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Intent;
import android.view.WindowManager;
import android.widget.Toast;

import com.station29.mealtemple.api.model.PaymentService;
import com.station29.mealtemple.api.model.Wallets;
import com.station29.mealtemple.ui.payment.OrangePayAlertDialog;
import com.station29.mealtemple.ui.payment.PasswordConfirmActivity;

import java.util.HashMap;
import java.util.List;

public class Redirect {

    public static void openPaymentServiceScreen(Activity activity, PaymentService paymentService , List<Wallets> walletsList, OnOpenPaymentCallback onOpenPaymentCallback){

        if(paymentService.getType().equalsIgnoreCase("MOBILE_WALLET")){
            OrangePayAlertDialog orangePayAlertDialog = new OrangePayAlertDialog(activity, activity, paymentService, walletsList, new OrangePayAlertDialog.OnPaymentCallback() {
                @Override
                public void onPaymentStart(HashMap<String, Object> param) {
                    onOpenPaymentCallback.onPaymentStart(param);
                }

                @Override
                public void onPaymentFinish() {
                    onOpenPaymentCallback.onPaymentFinish();
                }
            });
            orangePayAlertDialog.show();

        } else {
            Toast.makeText(activity,paymentService.getPspName() + " is coming soon!",Toast.LENGTH_SHORT).show();
        }
    }

    public static void gotoPasswordConfirmActivity(Activity activity, int REQUEST_CODE) {
        Intent intent = new Intent(activity, PasswordConfirmActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        if (REQUEST_CODE != -1) {
            activity.startActivityForResult(intent, REQUEST_CODE);
        } else {
            activity.startActivity(intent);
        }
    }

    public interface OnOpenPaymentCallback {
        void onPaymentStart(HashMap<String , Object> param);
        void onPaymentFinish();
    }
}
