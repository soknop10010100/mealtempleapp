package com.station29.mealtemple.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.maps.android.PolyUtil;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.station29.mealtemple.BuildConfig;
import com.station29.mealtemple.R;
import com.station29.mealtemple.api.Constant;
import com.station29.mealtemple.api.model.PaymentService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Util {

    public static String formatDistance(float number) {
        return String.format(Locale.US, "%.2f", number);
    }

    public static String formatPrice(double number) {
        return String.format(Locale.US, "%.2f", number);
    }

    public static String formatInteger(int number) {
        return String.format(Locale.US, "%d", number);
    }

    public static String formatTime(int millisecond) {
        return millisecond + " min";
    }

    public static void popupMessage(String title, String message, Activity activity) {
        if (!activity.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.DialogTheme));
            if (title != null) builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.ok)), null);
            builder.show();
        }
    }

    public static void popupMessageAndFinish(String title, String message, Activity activity) {
        if (!activity.isFinishing()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.DialogTheme));
            if (title != null) builder.setTitle(title);
            builder.setMessage(message);
            builder.setPositiveButton(String.format(Locale.US, "%s", activity.getString(R.string.close)), (dialog, which) -> activity.finish());
            builder.setCancelable(false);
            builder.show();
        }
    }

    public static void hideSoftKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showKeyboard(Activity activity) {
        if (activity != null) {
            activity.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static void saveString(String key, String value, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(
                Constant.SHARE_PREFERANCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor aSharedPreferencesEdit = aSharedPreferences
                .edit();

        aSharedPreferencesEdit.putString(key, value);
        aSharedPreferencesEdit.apply();
    }

    public static String getString(String key, Context activity) {
        SharedPreferences aSharedPreferences = activity.getSharedPreferences(
                Constant.SHARE_PREFERANCE_NAME, Context.MODE_PRIVATE);
        return aSharedPreferences.getString(key, "");
    }

    public static void clearString(Context activity) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(Constant.SHARE_PREFERANCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    public static void clearStringq(Activity activity, String keys) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences(keys, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }


    public static String getBase64FromPath(String path) {

        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap bm = BitmapFactory.decodeStream(fis);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String base64 = Base64.encodeToString(b, Base64.DEFAULT);
        return base64;
    }

    public static String convert(Bitmap bitmap) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT);

    }

    //check connection either wifi or mobile network
    public static boolean isConnectedToInternet(Activity activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static void buildAlertMessageNoNotification(final Activity activity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.DialogTheme);
        builder.setMessage(R.string.your_notification)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        Intent intent = new Intent();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                            intent.putExtra(Settings.EXTRA_APP_PACKAGE, activity.getPackageName());
                        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
                            intent.putExtra("app_package", activity.getPackageName());
                            intent.putExtra("app_uid", activity.getApplicationInfo().uid);
                        } else {
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            intent.setData(Uri.parse("package:" + activity.getPackageName()));
                        }
                        activity.startActivity(intent);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        if (!activity.isFinishing()) alert.show();
    }

    public static boolean checkShopCloseServer() {
        List<Integer> shopOpenHours = new ArrayList<>();
        boolean checkSeverTime = false;
        Calendar mCurrentTime = Calendar.getInstance();
        int mH = (mCurrentTime.get(Calendar.HOUR_OF_DAY) * 60) + mCurrentTime.get(Calendar.MINUTE);

        int open1 = 0, open2 = 0, close1 = 0, close2 = 0, tmrtime1 = 0, tmrTime2 = 0;
        if (Constant.getConfig() == null) return false;
        if (Constant.getConfig().getHourList().get(0).getIsClose() != 1) {
            shopOpenHours.add(Constant.getConfig().getHourList().get(0).getOpenTime());
            shopOpenHours.add(Constant.getConfig().getHourList().get(0).getCloseTime());
        } else {
            shopOpenHours.add(open1);
            shopOpenHours.add(close1);
        }
        if (Constant.getConfig().getHourList().get(1).getIsClose() != 1) {
            shopOpenHours.add(Constant.getConfig().getHourList().get(1).getCloseTime());
            shopOpenHours.add(Constant.getConfig().getHourList().get(1).getOpenTime());
        } else {
            shopOpenHours.add(open2);
            shopOpenHours.add(close2);
        }
        Collections.sort(shopOpenHours);

        if (mH < shopOpenHours.get(0) || mH > shopOpenHours.get(1) && mH < shopOpenHours.get(2) || mH > shopOpenHours.get(3)) {
            checkSeverTime = true;
        }
        return checkSeverTime;
    }

    public static void changeLanguage(Activity context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Util.saveString("language", language, context);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
    public static void start(Activity context, String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Util.saveString("start", language, context);

        Resources res = context.getResources();
        Configuration config = new Configuration(res.getConfiguration());
        config.locale = locale;
        res.updateConfiguration(config, res.getDisplayMetrics());
    }
    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics());
        return (int) px;
    }

    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static void firebaseAnalytics(Context context, String event, String clickEvent) {
        FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle params = new Bundle();
        params.putString(Constant.USER_ID, Util.getString("user_id", context));
        params.putString(event, clickEvent);
        params.putString(FirebaseAnalytics.Param.ITEM_NAME, context.getClass().getSimpleName());
        firebaseAnalytics.logEvent("log_kiwiGo_android", params);
        Log.d("asdfadsfasdf", "     " + params);
    }

    public static String formatString(int hour, int minute) {
        String stringFormat = "";
        if (minute < 10) {
            stringFormat = String.format(Locale.US, "%d:%s", hour, "0" + minute);
        } else {
            stringFormat = String.format(Locale.US, "%d:%s", hour, minute);
        }
        return stringFormat;

    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    public static String getCountryNameFromCode(String zipCode, Context context) {
        String[] isoCountryCodes = Locale.getISOCountries();
        // Iterate through all country codes:
        for (String code : isoCountryCodes) {
            // Create a locale using each country code
            if (code.equalsIgnoreCase(zipCode)) {
                Locale locale = new Locale("", code);
                // Get country name for each code.
                String name = locale.getDisplayCountry();
                Log.d("countryCode", code + " : " + name);
                return name;
            }
        }
        return context.getString(R.string.unable_to_find_location);
    }

    public static String getCountryCodeFromSimCard(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
        if (tm != null && tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT) {
            return tm.getNetworkCountryIso();
        }
        return null;
    }

    public static boolean checkIsHaveCash() {
        boolean isCash = false;
        for (PaymentService paymentService : Constant.getConfig().getPaymentServiceList())
            isCash = paymentService.getIsCash() != 1;
        return isCash;
    }

    public static String formatDuration(int millisecond) {
        return millisecond + " h ";
    }

    public static int checkVisiblePaymentService() {
        int size = 0;
        if (!Constant.getConfig().getPaymentServiceList().isEmpty())
            for (int i = 0; i < Constant.getConfig().getPaymentServiceList().size(); i++) {
                if (Constant.getConfig().getPaymentServiceList().get(i).getStatus() != 0 && Constant.getConfig().getPaymentServiceList().get(i).getIsCash() != 1 && Constant.getConfig().getPaymentServiceList().get(i).getIsCash() != 1)
                    size++;
            }
        return size;
    }

    /*
This method takes the text to be encoded, the width and height of the QR Code,
and returns the QR Code in the form of a byte array.
*/
    public static Bitmap getQRCodeImage(String text) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, 512, 512);
            int w = bitMatrix.getWidth();
            int h = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(w, h, Bitmap.Config.RGB_565);
            for (int x = 0; x < w; x++) {
                for (int y = 0; y < h; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;
        } catch (WriterException e) {
            e.printStackTrace();
            Log.e(e.getClass().getName(), e.getMessage() + "");
        }
        return null;
    }

//    public static void myCodePopup(Context context, String container, int width) {
//        AlertDialog.Builder alert = new AlertDialog.Builder(context);
//        ImageView imageView = new ImageView(context);
//        int imgWidth = Util.dpToPx(context, width);
//        imageView.setLayoutParams(new ViewGroup.LayoutParams(imgWidth, imgWidth));
//        Bitmap code = getQRCodeImage(container);
//        Glide.with(context).load(code).into(imageView);
//        alert.setView(imageView);
//        alert.show();
//    }

    public static List<LatLng> decode(String encodedPath) {
        int len = encodedPath.length();
        ArrayList path = new ArrayList();
        int index = 0;
        int lat = 0;
        int lng = 0;

        while(index < len) {
            int result = 1;
            int shift = 0;

            int b;
            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while(b >= 31);

            lat += (result & 1) != 0?~(result >> 1):result >> 1;
            result = 1;
            shift = 0;

            do {
                b = encodedPath.charAt(index++) - 63 - 1;
                result += b << shift;
                shift += 5;
            } while(b >= 31);

            lng += (result & 1) != 0?~(result >> 1):result >> 1;
            path.add(new LatLng((double)lat * 1.0E-5D, (double)lng * 1.0E-5D));
        }

        return path;
    }

    public static void testPolylinesOnMap(GoogleMap mMap, Context context){
        String key = "{jgeAutc_S????????DA?E@DACC?B@??AAC@@?????????F???A?B?C?C@FAKBBADA?@?AA?A?@A@A??@??????????@@???B@FEJG?????A??@@??A????????@IFEJ@A???B?ELO?ABVHLQDzMjDpD`@?AKOCD@TCFABA?@?BCFA?AA?A???A???@AEDH`ABtBHjA?n@@f@?^EdABr@?h@A^BpBFdAB~@L|FDbC_BJ_B?wAD}ADmA?mA@gALcABaCCaADw@BeCBcA@a@Ek@Bg@@g@?i@?k@Ci@?i@@c@@a@?Y?]A]@a@AW@M@S?[?Y?]?O?S?[?WA_@?[?OAIAK?G?M?O@_@@_@ASMIa@Ci@Em@Gq@Im@O{AImAGmAGoAAi@Gs@Em@I_@EQCQIEOEC?_@RuB~@e@ZQNMHMH[Tw@x@g@x@e@n@g@VSBYDe@K_@UsFeAuBKQBeAu@gBm@YIQHQnAG~@EpAE`AId@EFo@XcDOy@GuBA}AAqAEs@?CCmCFkAMO?s@?i@NcKYeAAcCH_BAYv@^~BP`BJvARfAKDGK?L@H}El@cADs@Dw@HqAF{AHwAH_BJcBJeBL_BJgBHgBDiFH{EXgFZy@FAHB?e@AkADmCJ{AFuAFgADy@AiEh@SFsDRuAF{AE_AJcALeADo@DWIGCiARs@HWDSDOCE@UBIDD?D@@??CBGCFAC?A?????A@??A????C@BEwCNmAL_BJ_CBiBLeBLwA?{AIiAE_B?gCb@iALkALoAKgGAqAG_A?{@S[EQEWCiAGW@a@Jo@TgAKi@KBABAA@????????A?OCYAs@Bs@DyAQkJMoFBgKTi@Bg@Fc@CIAWDMDMBe@Ey@?k@Ba@?MEM?QBOA_@@c@D[@c@@U@K?E@O@C?A???C?I@E?G?C?C@CBEACAGCIAK?]BuAaBIwAKuAMsAQcBQ_BKwAIsAQeCKq@G}@Eu@KcAIaACm@?M?]COkDXIf@NtALvAs@`BEG@@?B?AA?????@A??????????????FJ^IB@BB@?";
        List<LatLng> routelist = PolyUtil.decode(key.replace("\\\\","\\"));
        PolylineOptions rectLine = new PolylineOptions().width(5).color(context.getResources().getColor(R.color.colorPrimary));//set line color to app color
        for (int i = 0; i < routelist.size(); i++) {
            rectLine.add(routelist.get(i));
        }
        mMap.addMarker(new MarkerOptions().position(routelist.get(routelist.size() - 1)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_drop_off)));
        mMap.addMarker(new MarkerOptions().position(routelist.get(0)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_icon_pickup)));
        // Adding route on the map
        mMap.addPolyline(rectLine);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(routelist.get(routelist.size() - 1));
        builder.include(routelist.get(0));
        LatLngBounds bounds = builder.build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, context.getResources().getDisplayMetrics().widthPixels, context.getResources().getDisplayMetrics().heightPixels / 2, (int) (context.getResources().getDisplayMetrics().widthPixels * 0.18));
        mMap.animateCamera(cameraUpdate);
    }

    public static String encodeByPublicKey(String fields) {
        Log.d("encodeString","  :"+fields);
        String pubKeyBase64 = null;
        try {
            String publicKey = BuildConfig.ORANGE_PUBLIC_KEY;
            byte[] rsaData = fields.getBytes(StandardCharsets.UTF_8);
            byte[] encodeData = RsaHelper.encryptByPublicKey(rsaData, publicKey);
            pubKeyBase64 = RsaHelper.encryptBASE64(encodeData);
            Log.d("TAGasa", "encodePubKey: " + pubKeyBase64 );
        } catch (Exception e) {
            e.printStackTrace();
        }
        // remove \n from pubKeyBase64
        if (pubKeyBase64 != null) pubKeyBase64 = pubKeyBase64.replace(System.getProperty("line.separator"), "");
        return pubKeyBase64;
    }

    public static String decryptByPrivateKey(String fields) {
        String pubKeyBase64 = null;
        try {
//            Map<String, Object> keyMap = RsaHelper.initKey();
//            String publicKey = RsaHelper.getPrivateKey(keyMap);
            String privateKey = BuildConfig.KESS_PRIVATE_KEY;
            byte[] rsaData = fields.getBytes(StandardCharsets.UTF_8);
            byte[] encodeData = RsaHelper.decryptByPrivateKey(rsaData, privateKey);
            pubKeyBase64 = RsaHelper.encryptBASE64(encodeData);
            Log.d("TAGasa", "encodePubKey: " + pubKeyBase64 );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pubKeyBase64;
    }
}
