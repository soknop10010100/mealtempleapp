package com.station29.mealtemple.util;

import android.app.Activity;

import com.station29.mealtemple.api.request.PaymentWs;

import java.util.HashMap;

public class TopUpPayment {

    public static void onTopUpOrange(Activity mActivity , HashMap<String , Object > topUpParam , OnResponse onResponse){
        new PaymentWs().topUpWallets(topUpParam, mActivity, new PaymentWs.onTopUpWalletsCallBack() {
            @Override
            public void onSuccess(String message , int paymentId) {
                onResponse.onSuccess(message,paymentId);
            }

            @Override
            public void onError(String message) {
               onResponse.onResponseMessage(message);
            }
        });
    }

    public interface OnResponse{
        void onSuccess(String message ,int paymentId);
        void onResponseMessage(String message);
    }
}
